#!/bin/sh
# Author           : ManojKumar Jeganathan
# Date             : 4th Feb 2020
# Modified Date    : 20th Feb 2020
# Environment      : QA
# Explanation      :

# Updating Process Starts

echo "Updating process start"
sudo -H -u hybris bash -c 'cd /opt/hybris/bin/platform && . ./setantenv.sh && ant -version && ant updatesystem -Dtenant=master -DconfigFile=$CI_PROJECT_DIR/scripts/update-script/prod/update.json'
