#!/bin/sh
# Author           : ManojKumar Jeganathan
# Date             : 4th Feb 2020
# Modified Date    : 20th Feb 2020
# Environment      : QA
# Explanation      :

# Updating Process Starts

echo "Updating process start"
sudo -H -u hybris bash -c 'cd /opt/hybris/bin/platform && . ./setantenv.sh && ant -version && ant updatesystem -Dtenant=master -DconfigFile=$CI_PROJECT_DIR/scripts/update-script/non-prod/update.json'

echo "Check the tomcat logs"
tail -f /opt/hybris/log/tomcat/console.log | while read UpdateLogs
do
   #[[ "${LOGLINE}" == *""* ]] && pkill -P $$ tail
    [[ "${UpdateLogs}" == *"Update finished"* ]] && pkill -P $$ tail && echo "Update process completed Successfully" && exit 0
    [[ "${UpdateLogs}" == *"Update failed"* ]] && pkill -P $$ tail && echo "Update process failed . Please check the tomcat logs" && exit 1
done
