#!/bin/sh
# Author           : ManojKumar Jeganathan
# Date             : 4th Feb 2020
# Modified Date    : 20th Feb 2020
# Environment      : QA
# Explanation      :
    # 1. Declaring Global Variables
    # 2. Check NFS Disk space because script get failed due to the enough disk space is not available
    # 3. If NFS_DATA disk space is greater than or equal to 90% then the script won't execute
    # 4. Checking whether git is installed. If not install then the script won't execute
    # 5. Getting User Input
    # 6. Clone the repository
    # 7. Zip the custom folder from the cloned repository
    # 8. Checking whether artifact is available in NFS SHARE and if it's not there only then it will copy the artifact to NFS SHARE location
    # 9. Stopping the hybris service
    # 10. Removing the custom folder from Hybris Home location
    # 11. Copying the artifact from NFS to Hybris Home location
    # 12. Unzip the artifact and execute the build steps
    # 13. Starting Hybris service
    # 14. Removing local copy from the machine

# Script follows here:


# echo "Starting Hybris Service"
# sudo service hybris start

# echo "Check the tomcat logs"
# tail -f /opt/hybris_6.4.0/log/tomcat/console.log | while read LOGLINE
# do
#    #[[ "${LOGLINE}" == *""* ]] && pkill -P $$ tail
#     [[ "${LOGLINE}" == *"Server startup"* ]] && pkill -P $$ tail && echo "Tomcat Started Successfully" && exit 0
#     [[ "${LOGLINE}" == *"Wrapper Stopped"* ]] && pkill -P $$ tail && echo "Tomcat not started. Please check the tomcat logs" && exit 1
# done

##########################
# Json File Modification #
##########################

echo "Copy: Deployment Template folder from repository into home location - $HOME"
echo "Debug: NFS_SHARE_Q---$NFS_SHARE_Q---environment_type---$environment_type---package_name---$package_name---host_name---$host_name---NFS_SHARE_RELEASE----$NFS_SHARE_RELEASE"
#mkdir $HOME/$package_name
sudo cp -rf $NFS_SHARE_Q/config/$environment_type $HOME/
sudo cp -rf $NFS_SHARE_Q/config/template.json $HOME/

echo "Backing up template Json file"
sudo cp -rf $HOME/template.json $HOME/template.json.backup && sudo chmod 777 $HOME/template.json

echo "Replace the ARTIFACT and ENV name with respective version in JSON file template under HOME location"
sudo sed -i "s/packageValue/$package_name.zip/g; s/hostnameValue/$host_name/g; s/envValue/$environment_type/g; s~nfsValue~$NFS_SHARE_RELEASE~g;" $HOME/template.json

#### Renaming template json file name into package name #############
sudo mv -f $HOME/template.json "$HOME/${environment_type}_${package_name}.json"

echo "Copy the Hybris AllExtension zip file into sqr folder"
sudo cp -rf /opt/hybris_6.4.0/temp/hybris/hybrisServer/hybrisServer-AllExtensions.zip $HOME/$environment_type/$environment_type-sqr/hybris/bin/

echo "Copy the Hybris Platform zip file into sqr folder"
sudo cp -rf /opt/hybris_6.4.0/temp/hybris/hybrisServer/hybrisServer-Platform.zip $HOME/$environment_type/$environment_type-sqr/hybris/bin/

echo "Renaming the folder"
sudo mv -f $HOME/$environment_type/$environment_type-sqr $HOME/$environment_type/$package_name

echo "Zip the sqr folder"
cd $HOME/$environment_type && sudo zip -r "$package_name.zip" $package_name
echo "Zip is completed"

###################
# Create MD5 file #
###################

cd $HOME/$environment_type/ && sudo md5sum $package_name.zip > /tmp/$package_name.md5

##########################
# Copy ZIP and JSON file #
##########################

echo "Copying ZIP, MD5 and JSON file from HOME into NFS location"
sudo mv -f $HOME/$environment_type/$package_name.zip $NFS_SHARE_Q/
sudo mv -f "$HOME/${environment_type}_${package_name}.json" $NFS_SHARE_Q/
sudo mv -f /tmp/$package_name.md5 $NFS_SHARE_Q/

############ Changing my user ID #################
sudo chown -Rf m_jega:linux_client $NFS_SHARE_Q/$package_name.zip
sudo chown -Rf m_jega:linux_client $NFS_SHARE_Q/${environment_type}_${package_name}.json
sudo chown -Rf m_jega:linux_client $NFS_SHARE_Q/$package_name.md5

#########################
## Clean-Up Activities ##
#########################

echo "Removing local working copy"
sudo rm -rf $HOME
#sudo rm -rf /tmp/$package_name.md5
