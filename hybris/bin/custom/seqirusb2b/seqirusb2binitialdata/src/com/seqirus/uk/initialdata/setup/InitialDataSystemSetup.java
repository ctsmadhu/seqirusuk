/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.uk.initialdata.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.CoreDataImportedEvent;
import de.hybris.platform.commerceservices.setup.events.SampleDataImportedEvent;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.seqirus.uk.initialdata.constants.Seqirusb2bInitialDataConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 */
@SystemSetup(extension = Seqirusb2bInitialDataConstants.EXTENSIONNAME)
public class InitialDataSystemSetup extends AbstractSystemSetup
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(InitialDataSystemSetup.class);

	private static final String IMPORT_CORE_DATA = "importCoreData";
	private static final String IMPORT_SAMPLE_DATA = "importSampleData";
	private static final String ACTIVATE_SOLR_CRON_JOBS = "activateSolrCronJobs";

	private static final String IMPORT_STAGING_DATA = "importStagingData";
	private static final String IMPORT_PRODUCTION_DATA = "importProdData";
	private static final String IMPORT_QA_DATA = "importQAData";

	private CoreDataImportService coreDataImportService;
	private SampleDataImportService sampleDataImportService;

/*	@Resource(name = "configurationService")
	private ConfigurationService configurationService;*/

	private static final String SEQIRUSB2B_UK = "seqirusb2b-uk";

	/**
	 * Generates the Dropdown and Multi-select boxes for the project data import
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<SystemSetupParameter>();

		params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", true));
		params.add(createBooleanSystemSetupParameter(ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs", true));

		/*final String environmentName = configurationService.getConfiguration().getString("environment.name");
		params.add(createBooleanSystemSetupParameter(IMPORT_STAGING_DATA, "Import Staging Data",
				null != environmentName && environmentName.equals("STAG")));
		params.add(createBooleanSystemSetupParameter(IMPORT_PRODUCTION_DATA, "Import Production Data",
				null != environmentName && environmentName.equals("PROD")));
		params.add(createBooleanSystemSetupParameter(IMPORT_QA_DATA, "Import QA Data",
				null != environmentName && environmentName.equals("QA")));*/
		// Add more Parameters here as you require

		return params;
	}

	/**
	 * Implement this method to create initial objects. This method will be called by system creator during
	 * initialization and system update. Be sure that this method can be called repeatedly.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
	public void createEssentialData(final SystemSetupContext context)
	{
		// Add Essential Data here as you require

		importImpexFile(context, "/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cdc-registration.impex");
		importImpexFile(context, "/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cdc-login.impex");
		importImpexFile(context,
				"/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cdc-consenttemplate.impex");
		importImpexFile(context, "/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cdc-resetpassword.impex");
		importImpexFile(context,
				"/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cdc-changepassword.impex");
		importImpexFile(context,
				"/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cdc-changepasswordsuccess.impex");
		importImpexFile(context,
				"/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cms-content.impex");
		importImpexFile(context,
				"/seqirusb2binitialdata/import/sampledata/contentCatalogs/seqirusb2b-ukContentCatalog/cms-content.impex");
		importImpexFile(context,
				"/seqirusb2binitialdata/import/sampledata/contentCatalogs/seqirusb2b-ukContentCatalog/returnsandcredit_en.impex");
		importImpexFile(context,
				"/seqirusb2binitialdata/import/coredata/contentCatalogs/seqirusb2b-ukContentCatalog/cdc-updateprofile.impex");
	}

	/**
	 * Implement this method to create data that is used in your project. This method will be called during the system
	 * initialization. <br>
	 * Add import data for each site you have configured
	 *
	 * <pre>
	 * final List<ImportData> importData = new ArrayList<ImportData>();
	 *
	 * final ImportData sampleImportData = new ImportData();
	 * sampleImportData.setProductCatalogName(SAMPLE_PRODUCT_CATALOG_NAME);
	 * sampleImportData.setContentCatalogNames(Arrays.asList(SAMPLE_CONTENT_CATALOG_NAME));
	 * sampleImportData.setStoreNames(Arrays.asList(SAMPLE_STORE_NAME));
	 * importData.add(sampleImportData);
	 *
	 * getCoreDataImportService().execute(this, context, importData);
	 * getEventService().publishEvent(new CoreDataImportedEvent(context, importData));
	 *
	 * getSampleDataImportService().execute(this, context, importData);
	 * getEventService().publishEvent(new SampleDataImportedEvent(context, importData));
	 * </pre>
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		final List<ImportData> importData = new ArrayList<ImportData>();

		final ImportData seqirusImportData = new ImportData();
		seqirusImportData.setProductCatalogName(SEQIRUSB2B_UK);
		seqirusImportData.setContentCatalogNames(Arrays.asList(SEQIRUSB2B_UK));
		seqirusImportData.setStoreNames(Arrays.asList(SEQIRUSB2B_UK));
		importData.add(seqirusImportData);

		getCoreDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new CoreDataImportedEvent(context, importData));

		getSampleDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new SampleDataImportedEvent(context, importData));
	}

	public CoreDataImportService getCoreDataImportService()
	{
		return coreDataImportService;
	}

	@Required
	public void setCoreDataImportService(final CoreDataImportService coreDataImportService)
	{
		this.coreDataImportService = coreDataImportService;
	}

	public SampleDataImportService getSampleDataImportService()
	{
		return sampleDataImportService;
	}

	@Required
	public void setSampleDataImportService(final SampleDataImportService sampleDataImportService)
	{
		this.sampleDataImportService = sampleDataImportService;
	}
}
