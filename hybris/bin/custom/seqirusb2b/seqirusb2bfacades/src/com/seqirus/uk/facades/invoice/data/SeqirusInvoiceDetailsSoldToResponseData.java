/**
 *
 */
package com.seqirus.uk.facades.invoice.data;

/**
 * @author 700196
 *
 */
public class SeqirusInvoiceDetailsSoldToResponseData
{
	private String soldToId;
	private String soldToName;
	private String soldToStreet;
	private String soldToAdditionalStreet;
	private String soldToCity;
	private String soldToCountry;

	/**
	 * @return the soldToId
	 */
	public String getSoldToId()
	{
		return soldToId;
	}

	/**
	 * @param soldToId
	 *           the soldToId to set
	 */
	public void setSoldToId(final String soldToId)
	{
		this.soldToId = soldToId;
	}

	/**
	 * @return the soldToName
	 */
	public String getSoldToName()
	{
		return soldToName;
	}

	/**
	 * @param soldToName
	 *           the soldToName to set
	 */
	public void setSoldToName(final String soldToName)
	{
		this.soldToName = soldToName;
	}

	/**
	 * @return the soldToStreet
	 */
	public String getSoldToStreet()
	{
		return soldToStreet;
	}

	/**
	 * @param soldToStreet
	 *           the soldToStreet to set
	 */
	public void setSoldToStreet(final String soldToStreet)
	{
		this.soldToStreet = soldToStreet;
	}

	/**
	 * @return the soldToAdditionalStreet
	 */
	public String getSoldToAdditionalStreet()
	{
		return soldToAdditionalStreet;
	}

	/**
	 * @param soldToAdditionalStreet
	 *           the soldToAdditionalStreet to set
	 */
	public void setSoldToAdditionalStreet(final String soldToAdditionalStreet)
	{
		this.soldToAdditionalStreet = soldToAdditionalStreet;
	}

	/**
	 * @return the soldToCity
	 */
	public String getSoldToCity()
	{
		return soldToCity;
	}

	/**
	 * @param soldToCity
	 *           the soldToCity to set
	 */
	public void setSoldToCity(final String soldToCity)
	{
		this.soldToCity = soldToCity;
	}

	/**
	 * @return the soldToCountry
	 */
	public String getSoldToCountry()
	{
		return soldToCountry;
	}

	/**
	 * @param soldToCountry
	 *           the soldToCountry to set
	 */
	public void setSoldToCountry(final String soldToCountry)
	{
		this.soldToCountry = soldToCountry;
	}


}
