/**
 *
 */
package com.seqirus.uk.facades.invoice.data;

/**
 * @author 700196
 *
 */
public class SeqirusInvoiceDetailsShipToResponseData
{
	private String shipToId;
	private String shipToName;
	private String shipToStreet;
	private String shipToAdditionalStreet;
	private String shipToCity;
	private String shipToCountry;

	/**
	 * @return the shipToId
	 */
	public String getShipToId()
	{
		return shipToId;
	}

	/**
	 * @param shipToId
	 *           the shipToId to set
	 */
	public void setShipToId(final String shipToId)
	{
		this.shipToId = shipToId;
	}

	/**
	 * @return the shipToName
	 */
	public String getShipToName()
	{
		return shipToName;
	}

	/**
	 * @param shipToName
	 *           the shipToName to set
	 */
	public void setShipToName(final String shipToName)
	{
		this.shipToName = shipToName;
	}

	/**
	 * @return the shipToStreet
	 */
	public String getShipToStreet()
	{
		return shipToStreet;
	}

	/**
	 * @param shipToStreet
	 *           the shipToStreet to set
	 */
	public void setShipToStreet(final String shipToStreet)
	{
		this.shipToStreet = shipToStreet;
	}

	/**
	 * @return the shipToAdditionalStreet
	 */
	public String getShipToAdditionalStreet()
	{
		return shipToAdditionalStreet;
	}

	/**
	 * @param shipToAdditionalStreet
	 *           the shipToAdditionalStreet to set
	 */
	public void setShipToAdditionalStreet(final String shipToAdditionalStreet)
	{
		this.shipToAdditionalStreet = shipToAdditionalStreet;
	}

	/**
	 * @return the shipToCity
	 */
	public String getShipToCity()
	{
		return shipToCity;
	}

	/**
	 * @param shipToCity
	 *           the shipToCity to set
	 */
	public void setShipToCity(final String shipToCity)
	{
		this.shipToCity = shipToCity;
	}

	/**
	 * @return the shipToCountry
	 */
	public String getShipToCountry()
	{
		return shipToCountry;
	}

	/**
	 * @param shipToCountry
	 *           the shipToCountry to set
	 */
	public void setShipToCountry(final String shipToCountry)
	{
		this.shipToCountry = shipToCountry;
	}


}
