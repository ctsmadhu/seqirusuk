/**
 *
 */
package com.seqirus.uk.facades.customer.data;

import java.util.List;


/**
 * @author 700196
 *
 */
public class SeqirusCustomerResponseData
{
	private SeqirusCustomerLocationResponseData companyDetails;
	private SeqirusCustomerLocationResponseData payingContactInfo;
	private SeqirusCustomerLocationResponseData invoicingContractInfo;
	private List<SeqirusCustomerLocationResponseData> shippingLocations;
	private String accountnumber;
	private String sapAccessCode;

	/**
	 * @return the companyDetails
	 */
	public SeqirusCustomerLocationResponseData getCompanyDetails()
	{
		return companyDetails;
	}

	/**
	 * @param companyDetails
	 *           the companyDetails to set
	 */
	public void setCompanyDetails(final SeqirusCustomerLocationResponseData companyDetails)
	{
		this.companyDetails = companyDetails;
	}

	/**
	 * @return the payingContactInfo
	 */
	public SeqirusCustomerLocationResponseData getPayingContactInfo()
	{
		return payingContactInfo;
	}

	/**
	 * @param payingContactInfo
	 *           the payingContactInfo to set
	 */
	public void setPayingContactInfo(final SeqirusCustomerLocationResponseData payingContactInfo)
	{
		this.payingContactInfo = payingContactInfo;
	}

	/**
	 * @return the invoicingContractInfo
	 */
	public SeqirusCustomerLocationResponseData getInvoicingContractInfo()
	{
		return invoicingContractInfo;
	}

	/**
	 * @param invoicingContractInfo
	 *           the invoicingContractInfo to set
	 */
	public void setInvoicingContractInfo(final SeqirusCustomerLocationResponseData invoicingContractInfo)
	{
		this.invoicingContractInfo = invoicingContractInfo;
	}

	/**
	 * @return the shippingLocations
	 */
	public List<SeqirusCustomerLocationResponseData> getShippingLocations()
	{
		return shippingLocations;
	}

	/**
	 * @param shippingLocations
	 *           the shippingLocations to set
	 */
	public void setShippingLocations(final List<SeqirusCustomerLocationResponseData> shippingLocations)
	{
		this.shippingLocations = shippingLocations;
	}

	/**
	 * @return the accountnumber
	 */
	public String getAccountnumber()
	{
		return accountnumber;
	}

	/**
	 * @param accountnumber
	 *           the accountnumber to set
	 */
	public void setAccountnumber(final String accountnumber)
	{
		this.accountnumber = accountnumber;
	}

	/**
	 * @return the sapAccessCode
	 */
	public String getSapAccessCode()
	{
		return sapAccessCode;
	}

	/**
	 * @param sapAccessCode
	 *           the sapAccessCode to set
	 */
	public void setSapAccessCode(final String sapAccessCode)
	{
		this.sapAccessCode = sapAccessCode;
	}


}
