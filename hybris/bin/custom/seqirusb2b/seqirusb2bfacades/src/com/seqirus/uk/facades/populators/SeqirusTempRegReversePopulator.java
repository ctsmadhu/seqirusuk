/**
 *
 */
package com.seqirus.uk.facades.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.seqirus.core.model.CustomerAddressModel;
import com.seqirus.core.model.SeqirusB2BTempCustomerModel;
import com.seqirus.core.model.SeqirusCompanyInfoModel;
import com.seqirus.uk.core.constants.GeneratedSeqirusb2bCoreConstants.Enumerations.AddressType;
import com.seqirus.uk.core.enums.TempObjectStatusEnum;


/**
 * @author 172553
 *
 */
public class SeqirusTempRegReversePopulator implements Populator<B2BUnitModel, SeqirusB2BTempCustomerModel>
{

	@Autowired
	private ModelService modelService;

	@Override
	public void populate(final B2BUnitModel source, final SeqirusB2BTempCustomerModel target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		//set business details
		final SeqirusCompanyInfoModel companyInfo = modelService.create(SeqirusCompanyInfoModel.class);
		companyInfo.setOwner(target);
		companyInfo.setFirstName(source.getFirstName());
		companyInfo.setLastName(source.getLastName());
		companyInfo.setJobTitle(source.getJobTitle());
		companyInfo.setEmail(source.getEmail());
		companyInfo.setPhoneNumber(source.getPhoneNumber());
		companyInfo.setPhoneExt(source.getPhoneExt());
		companyInfo.setCompanyName(source.getName());
		companyInfo.setCompanyType(source.getCompanyType());
		companyInfo.setBusinessType(source.getBusinessType());
		companyInfo.setRegistrationNumber(source.getRegistrationNumber());
		companyInfo.setVatNumber(source.getVatNumber());
		companyInfo.setNhcNumber(source.getNhcNumber());
		companyInfo.setStatus(TempObjectStatusEnum.SYNC);
		//set company address
		List<CustomerAddressModel> customerAddressModels = getAddressByType(source, target, AddressType.SP);
		if (CollectionUtils.isNotEmpty(customerAddressModels))
		{
			final CustomerAddressModel companyAddress = customerAddressModels.get(0);
			if (null != companyAddress)
			{
				companyInfo.setBuildingStreet(companyAddress.getBuildingStreet());
				companyInfo.setAdditionalStreet(companyAddress.getAdditionalStreet());
				companyInfo.setCity(companyAddress.getCity());
				companyInfo.setPostCode(companyAddress.getZipCode());
				companyInfo.setCountry(companyAddress.getCountry());
			}
		}
		target.setCompanyInfo(companyInfo);
		// Set Invoice Information
		customerAddressModels = getAddressByType(source, target, AddressType.BP);
		if (CollectionUtils.isNotEmpty(customerAddressModels))
		{
			target.setInvoiceAddress(customerAddressModels.get(0));
		}
		// Set Payer Information
		customerAddressModels = getAddressByType(source, target, AddressType.PY);
		if (CollectionUtils.isNotEmpty(customerAddressModels))
		{
			target.setPayingAddress(customerAddressModels.get(0));
		}
		//Set Shipping and License Information
		target.setShippingAddress(getAddressByType(source, target, AddressType.SH));
	}


	private List<CustomerAddressModel> getAddressByType(final B2BUnitModel b2bUnit, final SeqirusB2BTempCustomerModel target,
			final String addressType)
	{
		final Collection<AddressModel> addresses = b2bUnit.getAddresses();
		final List<CustomerAddressModel> custAddresses = new ArrayList<>();
		for (final AddressModel address : addresses)
		{
			final CustomerAddressModel custAddress = modelService.create(CustomerAddressModel.class);
			custAddress.setOwner(target);
			if (address.getSapCustomerType().contains(addressType))
			{
				custAddress.setJobTitle(address.getJobTitle());
				custAddress.setOrganizationName(address.getCompany());
				custAddress.setAddressId(address.getAddressId());
				custAddress.setFirstname(address.getFirstname());
				custAddress.setLastname(address.getLastname());
				custAddress.setEmail(address.getEmail());
				custAddress.setPhone1(address.getPhone1());
				custAddress.setPhoneExt(address.getPhone2());
				custAddress.setBuildingStreet(address.getStreetname());
				custAddress.setAdditionalStreet(address.getStreetnumber());
				custAddress.setCity(address.getTown());
				custAddress.setZipCode(address.getPostalcode());
				custAddress.setCountry(address.getCountry());
				//only for shipping
				custAddress.setNhsCode(address.getNhsCode());
				custAddress.setLicenseNumber(address.getLicenseNumber());
				custAddress.setLicenseName(address.getLicenseName());
				custAddress.setStatus(TempObjectStatusEnum.SYNC);
				custAddresses.add(custAddress);
			}
		}
		return custAddresses;
	}
}
