/**
 *
 */
package com.seqirus.uk.facades.customer.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.seqirus.uk.facades.customer.SeqirusB2BCustomerFacade;
import com.seqirus.uk.facades.cutomer.data.SeqirusCDCCustomerData;


// TODO: Auto-generated Javadoc
/**
 * The Class SeqirusB2BCustomerFacadeImpl.
 *
 * @author Avaneesh Chauhan
 */
public class SeqirusB2BCustomerFacadeImpl implements SeqirusB2BCustomerFacade
{

	/** The seqirus CDC customer converter. */
	@Resource
	private Converter<B2BCustomerModel, SeqirusCDCCustomerData> seqirusCDCCustomerConverter;

	/**
	 * Gets the customer profile data.
	 *
	 * @param b2bCustomerModel
	 *           the b 2 b customer model
	 * @return the customer profile data
	 */
	@Override
	public SeqirusCDCCustomerData getCustomerProfileData(final B2BCustomerModel b2bCustomerModel)
	{
		return seqirusCDCCustomerConverter.convert(b2bCustomerModel);
	}

	/**
	 * Sets the seqirus CDC customer converter.
	 *
	 * @param seqirusCDCCustomerConverter
	 *           the seqirus CDC customer converter
	 */
	public void setSeqirusCDCCustomerConverter(
			final Converter<B2BCustomerModel, SeqirusCDCCustomerData> seqirusCDCCustomerConverter)
	{
		this.seqirusCDCCustomerConverter = seqirusCDCCustomerConverter;
	}

}
