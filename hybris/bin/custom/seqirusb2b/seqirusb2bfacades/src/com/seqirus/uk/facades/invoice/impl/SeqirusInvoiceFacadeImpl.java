/**
 *
 */
package com.seqirus.uk.facades.invoice.impl;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;

import java.io.InputStream;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;

import com.seqirus.uk.core.constants.Seqirusb2bCoreConstants;
import com.seqirus.uk.core.dataObjects.InvoiceDetailsResponse;
import com.seqirus.uk.core.dataObjects.InvoiceLandingRequest;
import com.seqirus.uk.core.dataObjects.InvoiceLandingResponse;
import com.seqirus.uk.core.dataObjects.ReturnsAndCreditsRequest;
import com.seqirus.uk.core.dataObjects.ReturnsAndCreditsResponse;
import com.seqirus.uk.core.exceptions.SeqirusCustomException;
import com.seqirus.uk.core.invoice.service.SeqirusInvoiceService;
import com.seqirus.uk.facades.invoice.SeqirusInvoiceFacade;
import com.seqirus.uk.facades.invoice.data.SeqirusInvoiceDetailsResponseData;
import com.seqirus.uk.facades.invoice.data.SeqirusInvoiceLandingRequestData;
import com.seqirus.uk.facades.invoice.data.SeqirusInvoiceLandingResponseData;
import com.seqirus.uk.facades.invoice.data.SeqirusReturnsAndCreditsResponseData;


/**
 * The Class SeqirusInvoiceFacadeImpl.
 *
 * @author 700196
 */
public class SeqirusInvoiceFacadeImpl implements SeqirusInvoiceFacade
{

	/** The Constant DEFAULT_RETURNS_CREDITS_TO_DATE. */
	private static final String DEFAULT_RETURNS_CREDITS_TO_DATE = "9999-12-31";

	/** The Constant DEFAULT_RETURNS_CREDITS_FROM_DATE. */
	private static final String DEFAULT_RETURNS_CREDITS_FROM_DATE = "2000-01-01";

	/** The Constant RETURNS_CREDITS_PARAM. */
	private static final String RETURNS_CREDITS_PARAM = "returnsandcredits.param.returns";

	/** The Constant DEFAULT_RETURNS_CREDITS_PARAM. */
	private static final String DEFAULT_RETURNS_CREDITS_PARAM = "X";

	/** The Constant RETURNS_CREDITS_FROM_DATE. */
	private static final String RETURNS_CREDITS_FROM_DATE = "returnsandcredits.param.fromdate";

	/** The Constant RETURNS_CREDITS_TO_DATE. */
	private static final String RETURNS_CREDITS_TO_DATE = "returnsandcredits.param.todate";

	/** The Constant DEFAULT_CUSTOMER_NUMBER. */
	private static final String DEFAULT_CUSTOMER_NUMBER = "12345";

	/** The seqirus invoice listing request populator. */
	@Resource
	private Populator<SeqirusInvoiceLandingRequestData, InvoiceLandingRequest> seqirusInvoiceListingRequestPopulator;

	/** The seqirus invoice listing response populator. */
	@Resource
	private Populator<InvoiceLandingResponse, SeqirusInvoiceLandingResponseData> seqirusInvoiceListingResponsePopulator;

	/** The seqirus invoice details response populator. */
	@Resource
	private Populator<InvoiceDetailsResponse, SeqirusInvoiceDetailsResponseData> seqirusInvoiceDetailsResponsePopulator;


	/** The seqirus returns and credits response populator. */
	@Resource
	private Populator<ReturnsAndCreditsResponse, SeqirusReturnsAndCreditsResponseData> seqirusReturnsAndCreditsResponsePopulator;


	/**
	 * Sets the seqirus returns and credits response populator.
	 *
	 * @param seqirusReturnsAndCreditsResponsePopulator
	 *           the seqirus returns and credits response populator
	 */
	public void setSeqirusReturnsAndCreditsResponsePopulator(
			final Populator<ReturnsAndCreditsResponse, SeqirusReturnsAndCreditsResponseData> seqirusReturnsAndCreditsResponsePopulator)
	{
		this.seqirusReturnsAndCreditsResponsePopulator = seqirusReturnsAndCreditsResponsePopulator;
	}

	/** The seqirus invoice service. */
	@Resource(name = "seqirusInvoiceService")
	private SeqirusInvoiceService seqirusInvoiceService;

	/** The configuration service. */
	@Autowired
	protected ConfigurationService configurationService;

	/** The user service. */
	@Resource(name = "userService")
	private UserService userService;


	/**
	 * Sets the user service.
	 *
	 * @param userService
	 *           the new user service
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * Sets the configuration service.
	 *
	 * @param configurationService
	 *           the new configuration service
	 */
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	/**
	 * Retrieve invoice list.
	 *
	 * @param season
	 *           the season
	 * @return the seqirus invoice landing response data
	 * @throws SeqirusCustomException
	 *            the seqirus custom exception
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.seqirus.uk.facades.invoice.SeqirusInvoiceFacade#retrieveInvoiceList()
	 */
	@Override
	public SeqirusInvoiceLandingResponseData retrieveInvoiceList(final String season) throws SeqirusCustomException
	{
		final SeqirusInvoiceLandingRequestData source = new SeqirusInvoiceLandingRequestData();
		final SeqirusInvoiceLandingResponseData response = new SeqirusInvoiceLandingResponseData();
		final InvoiceLandingRequest target = new InvoiceLandingRequest();
		mapSeasonToRequest(season, source);
		seqirusInvoiceListingRequestPopulator.populate(source, target);
		final InvoiceLandingResponse invoiceLandingResponse = seqirusInvoiceService.retrieveInvoicesList(target);
		seqirusInvoiceListingResponsePopulator.populate(invoiceLandingResponse, response);
		return response;
	}

	/**
	 * Gets the returns and credits response.
	 *
	 * @param defaultB2bUnit
	 *           the default B 2 b unit
	 * @return the returns and credits response
	 * @throws SeqirusCustomException
	 *            the seqirus custom exception
	 */
	@Override
	public SeqirusReturnsAndCreditsResponseData getReturnsAndCreditsResponse(final String defaultB2bUnit)
			throws SeqirusCustomException
	{
		final ReturnsAndCreditsRequest request = new ReturnsAndCreditsRequest();
		setCreditsRequestPayload(request, defaultB2bUnit);
		final ReturnsAndCreditsResponse returnsAndCreditsResponse = seqirusInvoiceService.getReturnsAndCreditsList(request);
		final SeqirusReturnsAndCreditsResponseData response = new SeqirusReturnsAndCreditsResponseData();
		seqirusReturnsAndCreditsResponsePopulator.populate(returnsAndCreditsResponse, response);
		return response;
	}


	/**
	 * Sets the credits request payload.
	 *
	 * @param request
	 *           the request
	 * @param b2bUnit
	 *           the b 2 b unit
	 */
	private void setCreditsRequestPayload(final ReturnsAndCreditsRequest request, final String b2bUnit)
	{
		request.setCustomerNumber(b2bUnit);
		request.setFromDate(
				configurationService.getConfiguration().getString(RETURNS_CREDITS_FROM_DATE, DEFAULT_RETURNS_CREDITS_FROM_DATE));
		request.setToDate(
				configurationService.getConfiguration().getString(RETURNS_CREDITS_TO_DATE, DEFAULT_RETURNS_CREDITS_TO_DATE));
		request.setOrganizationId(configurationService.getConfiguration().getString(
				Seqirusb2bCoreConstants.GET_SEQIRUS_ORGANIZATION_NUMBER, Seqirusb2bCoreConstants.DEFAULT_ORGANIZATION_NUMBER));
		request.setReturns(configurationService.getConfiguration().getString(RETURNS_CREDITS_PARAM, DEFAULT_RETURNS_CREDITS_PARAM));
	}

	/**
	 * Retrieve invoice details.
	 *
	 * @param invoiceNumber
	 *           the invoice number
	 * @return the seqirus invoice details response data
	 * @throws SeqirusCustomException
	 *            the seqirus custom exception
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.seqirus.uk.facades.invoice.SeqirusInvoiceFacade#retrieveInvoiceDetails()
	 */
	@Override
	public SeqirusInvoiceDetailsResponseData retrieveInvoiceDetails(final String invoiceNumber) throws SeqirusCustomException
	{
		final SeqirusInvoiceDetailsResponseData response = new SeqirusInvoiceDetailsResponseData();
		final InvoiceDetailsResponse invoiceDetailsResponse = seqirusInvoiceService.retrieveInvoiceDetails(invoiceNumber);
		seqirusInvoiceDetailsResponsePopulator.populate(invoiceDetailsResponse, response);
		return response;
	}

	/**
	 * Implemented INvoice download API call.
	 *
	 * @param invoiceNumber
	 *           the invoice number
	 * @return InputStream
	 * @throws SeqirusCustomException
	 *            the seqirus custom exception
	 */
	public InputStream downloadInvoice(final String invoiceNumber) throws SeqirusCustomException
	{
		return seqirusInvoiceService.downloadInvoice(invoiceNumber);
	}

	/**
	 * Map season to request.
	 *
	 * @param season
	 *           the season
	 * @param source
	 *           the source
	 */
	private void mapSeasonToRequest(final String season, final SeqirusInvoiceLandingRequestData source)
	{
		if (null != season)
		{
			source.setSeason(season);
		}
		else
		{
			source.setSeason(configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.DEFAULT_SEASONENTRY));
		}
	}

}
