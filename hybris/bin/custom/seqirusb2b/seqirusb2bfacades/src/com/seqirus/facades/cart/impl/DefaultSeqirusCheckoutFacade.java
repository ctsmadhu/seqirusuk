package com.seqirus.facades.cart.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.order.impl.DefaultCheckoutFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.seqirus.facades.cart.SeqirusCheckoutFacade;
import com.seqirus.uk.core.constants.Seqirusb2bCoreConstants;


/**
 * @author nesingh DefaultSeqirusCheckoutFacade deals with checkout activies
 *
 */
public class DefaultSeqirusCheckoutFacade extends DefaultCheckoutFacade implements SeqirusCheckoutFacade
{
	protected static final Logger LOG = Logger.getLogger(DefaultSeqirusCheckoutFacade.class);
	private static final boolean IS_DEBUG_ENABLED = LOG.isDebugEnabled();

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "defaultAddressReverseConverter")
	private Converter<AddressData, AddressModel> defaultAddressReverseConverter;
	private Converter<AddressModel, AddressData> addressConverter;


	/**
	 * @return the addressConverter
	 */
	@Override
	public Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	/**
	 * @param addressConverter
	 *           the addressConverter to set
	 */
	@Override
	public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}


	/**
	 * @return the defaultAddressReverseConverter
	 */
	public Converter<AddressData, AddressModel> getDefaultAddressReverseConverter()
	{
		return defaultAddressReverseConverter;
	}

	/**
	 * @param defaultAddressReverseConverter
	 *           the defaultAddressReverseConverter to set
	 */
	public void setDefaultAddressReverseConverter(
			final Converter<AddressData, AddressModel> defaultAddressReverseConverter)
	{
		this.defaultAddressReverseConverter = defaultAddressReverseConverter;
	}


	/**
	 * Method to populate all addresses for Sold-To account linked to user.
	 *
	 * @return
	 */
	@Override
	public List<AddressData> populateSoldToLinkedAddresses()
	{
		if (IS_DEBUG_ENABLED)
		{
			LOG.debug("Logged in:::: populateSoldToDeliveryAddress()");
		}
		final List<AddressData> addressData = new ArrayList<AddressData>();

		final UserModel checkoutCustomer = userService.getCurrentUser();
			setupAddressData(addressData, checkoutCustomer);

		if (CollectionUtils.isEmpty(addressData))
		{
			LOG.error("Error Case: No addresses attached to the User.");
		}

		return addressData;
	}

	/**
	 * @param addressData
	 * @param checkoutCustomer
	 */
	private void setupAddressData(final List<AddressData> addressData, final UserModel checkoutCustomer)
	{

		if (checkoutCustomer instanceof B2BCustomerModel)
		{
			final B2BCustomerModel b2bcustomer = (B2BCustomerModel) checkoutCustomer;
			if (b2bcustomer.getDefaultB2BUnit() != null
					&& CollectionUtils.isNotEmpty(b2bcustomer.getDefaultB2BUnit().getAddresses()))
			{
				for (final AddressModel address : b2bcustomer.getDefaultB2BUnit().getAddresses())
				{
					final AddressData addr = new AddressData();
					addressConverter.convert(address, addr);
					addressData.add(addr);

				}

			}

		}
	}


	@Override
	public List<AddressData> populateLinkedAddressesForGivenType(final String addressType, final List<AddressData> allAddressList)
	{
		if (IS_DEBUG_ENABLED)
		{
			LOG.debug("Logged in:::: populateLinkedAddressesForGivenType(): addressType= " + addressType);
		}

		final List<AddressData> custAddressDataList = new ArrayList<AddressData>();
		AddressData contactAddress = null;

		if (CollectionUtils.isNotEmpty(allAddressList))
		{
			for (final AddressData address : allAddressList)
			{
				if (StringUtils.equalsIgnoreCase(addressType, Seqirusb2bCoreConstants.SHIPPING_ADDRESS)
						&& address.isShippingAddress())
				{
					custAddressDataList.add(address);
				}

				if (null == contactAddress && address.isDefaultAddress())
				{
					contactAddress = address;
				}
			}

			//adding contact address as default billing/shipping address in-case No such address available.
			if (CollectionUtils.isEmpty(custAddressDataList) && null != contactAddress)
			{
				LOG.info("Making Contact address as default billing/shipping address as No such address available");
				custAddressDataList.add(contactAddress);
			}
			else if (contactAddress == null)
			{
				LOG.error("Error Case: There is No Contact address attached to the User's B2BUnit.");
			}

		}
		else
		{
			LOG.error("Error Case: No addresses attached to the User's B2BUnit.");
		}

		return custAddressDataList;
	}



}

