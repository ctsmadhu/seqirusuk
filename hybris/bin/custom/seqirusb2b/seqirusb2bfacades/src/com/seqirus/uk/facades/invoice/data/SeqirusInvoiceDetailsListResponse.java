/**
 *
 */
package com.seqirus.uk.facades.invoice.data;

/**
 * @author 700196
 *
 */
public class SeqirusInvoiceDetailsListResponse
{
	private String batchNumber;
	private String productName;
	private String materialNumber;
	private String unitOfMeasure;
	private float unitPrice;
	private String doses;
	private float totalCost;
	private float netAmount;
	private float lineItemTax;

	/**
	 * @return the batchNumber
	 */
	public String getBatchNumber()
	{
		return batchNumber;
	}

	/**
	 * @param batchNumber
	 *           the batchNumber to set
	 */
	public void setBatchNumber(final String batchNumber)
	{
		this.batchNumber = batchNumber;
	}

	/**
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * @param productName
	 *           the productName to set
	 */
	public void setProductName(final String productName)
	{
		this.productName = productName;
	}

	/**
	 * @return the unitOfMeasure
	 */
	public String getUnitOfMeasure()
	{
		return unitOfMeasure;
	}

	/**
	 * @param unitOfMeasure
	 *           the unitOfMeasure to set
	 */
	public void setUnitOfMeasure(final String unitOfMeasure)
	{
		this.unitOfMeasure = unitOfMeasure;
	}



	/**
	 * @return the unitPrice
	 */
	public float getUnitPrice()
	{
		return unitPrice;
	}

	/**
	 * @param unitPrice
	 *           the unitPrice to set
	 */
	public void setUnitPrice(final float unitPrice)
	{
		this.unitPrice = unitPrice;
	}

	/**
	 * @return the totalCost
	 */
	public float getTotalCost()
	{
		return totalCost;
	}

	/**
	 * @param totalCost
	 *           the totalCost to set
	 */
	public void setTotalCost(final float totalCost)
	{
		this.totalCost = totalCost;
	}

	/**
	 * @return the doses
	 */
	public String getDoses()
	{
		return doses;
	}

	/**
	 * @param doses
	 *           the doses to set
	 */
	public void setDoses(final String doses)
	{
		this.doses = doses;
	}

	/**
	 * @return the netAmount
	 */
	public float getNetAmount()
	{
		return netAmount;
	}

	/**
	 * @param netAmount
	 *           the netAmount to set
	 */
	public void setNetAmount(final float netAmount)
	{
		this.netAmount = netAmount;
	}

	/**
	 * @return the lineItemTax
	 */
	public float getLineItemTax()
	{
		return lineItemTax;
	}

	/**
	 * @param lineItemTax
	 *           the lineItemTax to set
	 */
	public void setLineItemTax(final float lineItemTax)
	{
		this.lineItemTax = lineItemTax;
	}

	/**
	 * @return the materialNumber
	 */
	public String getMaterialNumber()
	{
		return materialNumber;
	}

	/**
	 * @param materialNumber
	 *           the materialNumber to set
	 */
	public void setMaterialNumber(final String materialNumber)
	{
		this.materialNumber = materialNumber;
	}



}
