/**
 *
 */
package com.seqirus.uk.facades.invoice.data;


public class SeqirusReturnsAndCreditsResponse
{
	protected String invoiceNumber;
	protected String invoiceDate;
	protected String status;
	protected double amoutWithTax;
	protected String currency;

	public String getCurrency()
	{
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency
	 *           the new currency
	 */
	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public double getAmoutWithTax()
	{
		return amoutWithTax;
	}

	public void setAmoutWithTax(final double amoutWithTax)
	{
		this.amoutWithTax = amoutWithTax;
	}

	/**
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}

	/**
	 * @param invoiceNumber
	 *           the invoiceNumber to set
	 */
	public void setInvoiceNumber(final String invoiceNumber)
	{
		this.invoiceNumber = invoiceNumber;
	}


	/**
	 * @return the invoiceDate
	 */
	public String getInvoiceDate()
	{
		return invoiceDate;
	}

	/**
	 * @param invoiceDate
	 *           the invoiceDate to set
	 */
	public void setInvoiceDate(final String invoiceDate)
	{
		this.invoiceDate = invoiceDate;
	}


	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *           the status to set
	 */
	public void setStatus(final String status)
	{
		this.status = status;
	}

}
