/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.uk.facades.constants;

/**
 * Global class for all Seqirusb2bFacades constants.
 */
public class Seqirusb2bFacadesConstants extends GeneratedSeqirusb2bFacadesConstants
{
	public static final String EXTENSIONNAME = "seqirusb2bfacades";

	public static final String COMMA_SIGN = ",";
	public static final String DAY_FORMAT = "d'";
	public static final String MNTHYR_FORMAT = "MMMM, yyyy";
	public static final String DAY_STRING = "Day";
	public static final String YYYYMMDD_FORMAT = "yyyy/MM/dd";
	public static final String DDMMYYYY_FORMAT = "dd/MM/yyyy";
	public static final String DAYNUMBERSUFFIX_ST = "st";
	public static final String DAYNUMBERSUFFIX_RD = "rd";
	public static final String DAYNUMBERSUFFIX_TH = "th";
	public static final String DAYNUMBERSUFFIX_ND = "nd";
	public static final String APOSTROPHE_STRING = "'";
	public static final String EMPTY_SPACE = " ";
	public static final String INVOICE_STATUS_OPEN = "Open";
	public static final String PAYMENT_STATUS_PAID = "Paid";
	public static final String BLANK_DATE = "0000-00-00";
	public static final String NA_STATUS = "N/A";






	private Seqirusb2bFacadesConstants()
	{
		//empty
	}
}
