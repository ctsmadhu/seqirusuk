/**
 *
 */
package com.seqirus.uk.facades.orders;

import java.util.List;

import com.seqirus.uk.core.dataObjects.OrderSummary;


/**
 * @author 614269
 *
 */
public interface SeqirusOrdersFacade
{
	/**
	 * @param customerId
	 * @param season
	 * @return
	 */
	List<OrderSummary> getOrders(String customerId, String season);
}
