/**
 *
 */
package com.seqirus.uk.facades.populators.invoice;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.fest.util.Collections;

import com.seqirus.uk.core.dataObjects.InvoiceLandingResponse;
import com.seqirus.uk.core.dataObjects.InvoiceListLandingResponse;
import com.seqirus.uk.facades.constants.Seqirusb2bFacadesConstants;
import com.seqirus.uk.facades.invoice.data.SeqirusInvoiceLandingListResponse;
import com.seqirus.uk.facades.invoice.data.SeqirusInvoiceLandingResponseData;

/**
 * @author 700196
 *
 */
public class SeqirusInvoiceListingResponsePopulator implements Populator<InvoiceLandingResponse, SeqirusInvoiceLandingResponseData>
{
	private final SimpleDateFormat parseDate = new SimpleDateFormat("yyyy/MM/dd");
	private final SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");

	/* (non-Javadoc)
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final InvoiceLandingResponse source, final SeqirusInvoiceLandingResponseData target)
			throws ConversionException
	{
		final List<SeqirusInvoiceLandingListResponse> invoiceList = new ArrayList<>();
		final Date currentDate = getCurrentDate();
		double openStatusAmount = 0;
		double paidStatusAmount = 0;
		if(!Collections.isEmpty(source.getInvoices())){
			for(final InvoiceListLandingResponse response :source.getInvoices()){
				final SeqirusInvoiceLandingListResponse invoiceResponse = new SeqirusInvoiceLandingListResponse();
				mapLandingReponse(response, invoiceResponse);
					if ((StringUtils.isBlank(response.getClearingDate())
							|| response.getClearingDate().trim().equals(Seqirusb2bFacadesConstants.BLANK_DATE)))
					{
							openStatusAmount = openStatusAmount + invoiceResponse.getAmount();
							invoiceResponse.setStatus(Seqirusb2bFacadesConstants.INVOICE_STATUS_OPEN);
					}
					else
					{
						paidStatusAmount = paidStatusAmount + invoiceResponse.getAmount();
						invoiceResponse.setStatus(Seqirusb2bFacadesConstants.PAYMENT_STATUS_PAID);
					}
				invoiceList.add(invoiceResponse);
			}
			target.setOpenStatusInvoiceAmount(openStatusAmount);
			target.setPaidStatusInvoiceAmount(paidStatusAmount);
			target.setInvoices(invoiceList);
		}
		else
		{
			final SeqirusInvoiceLandingListResponse invoiceResponse = new SeqirusInvoiceLandingListResponse();
			mapNoInvoicesReponse(invoiceResponse);
			invoiceList.add(invoiceResponse);
			target.setInvoices(invoiceList);
		}
	}

	/**
	 * @param response
	 * @param invoiceResponse
	 */
	private void mapLandingReponse(final InvoiceListLandingResponse response,
			final SeqirusInvoiceLandingListResponse invoiceResponse)
	{
		if(StringUtils.isNotBlank(response.getInvoiceNumber())){
			invoiceResponse.setInvoiceNumber(response.getInvoiceNumber());
		}
		if(StringUtils.isNotBlank(response.getInvoiceDate())){
			invoiceResponse.setInvoiceDate(convertDate(response.getInvoiceDate()));
		}
		if(StringUtils.isNotBlank(response.getSalesOrderNumber())){
			invoiceResponse.setSalesOrderNumber(response.getSalesOrderNumber());
		}
		if(StringUtils.isNotBlank(response.getDueDate())){
			invoiceResponse.setDueDate(convertDate(response.getDueDate()));
		}
		if(response.getDiscount() > 0){
			invoiceResponse.setAmount(response.getAmountwithDiscount());
		}else{
			invoiceResponse.setAmount(response.getAmoutWithTax());
		}
	}

	/**
	 * @param response
	 * @param invoiceResponse
	 */
	private void mapNoInvoicesReponse(final SeqirusInvoiceLandingListResponse invoiceResponse)
	{
		invoiceResponse.setInvoiceNumber(Seqirusb2bFacadesConstants.NA_STATUS);
		invoiceResponse.setInvoiceDate(Seqirusb2bFacadesConstants.NA_STATUS);
		invoiceResponse.setSalesOrderNumber(Seqirusb2bFacadesConstants.NA_STATUS);
		invoiceResponse.setDueDate(Seqirusb2bFacadesConstants.NA_STATUS);
		//invoiceResponse.setAmount(Seqirusb2bFacadesConstants.NA_STATUS.);
		invoiceResponse.setStatus(Seqirusb2bFacadesConstants.NA_STATUS);
	}

	private String convertDate(final String date)
	{
		try
		{
			return date == null ? null : formatDate.format(parseDate.parse(date));
		}
		catch (final ParseException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private Date parseDate(final String date) throws ParseException
	{
		return parseDate.parse(date);
	}
	private Date getCurrentDate()
	{
   	final Calendar c = Calendar.getInstance();
   	Date currentDate = null;
   	try
   	{
   		currentDate = parseDate.parse(parseDate.format(c.getTime()));
   	}
   	catch (final ParseException e)
   	{
   		e.printStackTrace();
   		currentDate = c.getTime();
   	}
   	return currentDate;
	}
}
