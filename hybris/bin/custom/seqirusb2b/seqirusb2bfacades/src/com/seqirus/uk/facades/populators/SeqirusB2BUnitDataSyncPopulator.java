/**
 *
 */
package com.seqirus.uk.facades.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.seqirus.core.model.CustomerAddressModel;
import com.seqirus.core.model.SeqirusB2BTempCustomerModel;
import com.seqirus.core.model.SeqirusCompanyInfoModel;
import com.seqirus.uk.core.enums.TempObjectStatusEnum;
import com.seqirus.uk.core.exceptions.SeqirusCustomException;
import com.seqirus.uk.core.registration.service.SeqirusCustomerRegistrationService;
import com.seqirus.uk.facades.customer.impl.SeqirusCustomerRegistrationFacadeImpl;


/**
 * @author 614269
 *
 */
public class SeqirusB2BUnitDataSyncPopulator implements Populator<B2BUnitModel, SeqirusB2BTempCustomerModel>
{

	static Logger logger = Logger.getLogger(SeqirusCustomerRegistrationFacadeImpl.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	private SeqirusCustomerRegistrationService seqirusCustomerRegistrationService;

	@Override
	public void populate(final B2BUnitModel source, final SeqirusB2BTempCustomerModel target) throws ConversionException
	{
		if (null != target)
		{
			populateCustomerModel(source, target);
			if (null != source.getAddresses())
			{
				for (final AddressModel address : source.getAddresses())
				{
					populatePayingAddressModel(address, target);
					populateinvoicingAddressModel(address, target);
					populateShippingAddressModel(source, target);
				}
			}
			modelService.save(target);
		}
	}


	private void populateCustomerModel(final B2BUnitModel source, final SeqirusB2BTempCustomerModel target)
	{
		final SeqirusCompanyInfoModel customerTempObject = target.getCompanyInfo();
		try
		{
			if (StringUtils.isNotBlank(source.getCompanyName()))
			{
				customerTempObject.setCompanyName(source.getCompanyName());
			}
			else
			{
				customerTempObject.setCompanyName(StringUtils.EMPTY);
			}
			if (StringUtils.isNotBlank(source.getCompanyType()))
			{
				customerTempObject.setCompanyType(source.getCompanyType());
			}
			else
			{
				customerTempObject.setCompanyType(StringUtils.EMPTY);
			}
			if (StringUtils.isNotBlank(source.getRegistrationNumber()))
			{
				customerTempObject.setRegistrationNumber(source.getRegistrationNumber());
			}
			else
			{
				customerTempObject.setRegistrationNumber(StringUtils.EMPTY);
			}
			if (StringUtils.isNotBlank(source.getBusinessType()))
			{
				customerTempObject.setBusinessType(source.getBusinessType());
			}
			else
			{
				customerTempObject.setBusinessType(StringUtils.EMPTY);
			}
			if (StringUtils.isNotBlank(source.getVatNumber()))
			{
				customerTempObject.setVatNumber(source.getVatNumber());
			}
			else
			{
				customerTempObject.setVatNumber(StringUtils.EMPTY);
			}

			if (null != source.getTradingSince())
			{
				customerTempObject.setTradingSince(source.getTradingSince());
			}
			if (StringUtils.isNotBlank(source.getFirstName()))
			{
				customerTempObject.setFirstName(source.getFirstName());
			}
			else
			{
				customerTempObject.setFirstName(StringUtils.EMPTY);
			}
			if (StringUtils.isNotBlank(source.getLastName()))
			{
				customerTempObject.setLastName(source.getLastName());
			}
			else
			{
				customerTempObject.setLastName(StringUtils.EMPTY);
			}
			if (StringUtils.isNotBlank(source.getEmail()))
			{
				customerTempObject.setEmail(source.getEmail());
			}
			else
			{
				customerTempObject.setEmail(StringUtils.EMPTY);
			}
			if (StringUtils.isNotBlank(source.getPhoneNumber()))
			{
				customerTempObject.setPhoneNumber(source.getPhoneNumber());
			}
			else
			{
				customerTempObject.setPhoneNumber(StringUtils.EMPTY);
			}
			if (StringUtils.isNotBlank(source.getPhoneExt()))
			{
				customerTempObject.setPhoneExt(source.getPhoneExt());
			}
			else
			{
				customerTempObject.setPhoneExt(StringUtils.EMPTY);
			}
			if (StringUtils.isNotBlank(source.getJobTitle()))
			{
				customerTempObject.setJobTitle(source.getJobTitle());
			}
			else
			{
				customerTempObject.setJobTitle(StringUtils.EMPTY);
			}
			if (StringUtils.isNotBlank(source.getNhcNumber()))
			{
				customerTempObject.setNhcNumber(source.getNhcNumber());
			}
			else
			{
				customerTempObject.setNhcNumber(StringUtils.EMPTY);
			}
			for (final AddressModel addresses : source.getAddresses())
			{
				if (addresses.getContactAddress())
				{
					if (StringUtils.isNotBlank(addresses.getLine1()))
					{
						customerTempObject.setBuildingStreet(addresses.getLine1());
					}
					else
					{
						customerTempObject.setBuildingStreet(StringUtils.EMPTY);
					}
					if (StringUtils.isNotBlank(addresses.getLine2()))
					{
						customerTempObject.setAdditionalStreet(addresses.getLine2());
					}
					else
					{
						customerTempObject.setAdditionalStreet(StringUtils.EMPTY);
					}
					if (StringUtils.isNotBlank(addresses.getTown()))
					{
						customerTempObject.setCity(addresses.getTown());
					}
					else
					{
						customerTempObject.setCity(StringUtils.EMPTY);
					}
					if (StringUtils.isNotBlank(addresses.getPostalcode()))
					{
						customerTempObject.setPostCode(addresses.getPostalcode());
					}
					else
					{
						customerTempObject.setPostCode(StringUtils.EMPTY);
					}

					final CountryModel payerCountry = fetchCountry(addresses.getCountry());
					if (null != payerCountry)
					{
						customerTempObject.setCountry(payerCountry);
					}
				}
			}
			customerTempObject.setStatus(TempObjectStatusEnum.SYNC);
			modelService.save(customerTempObject);
		}
		catch (final Exception e)
		{
			logger.error(":::ExceptionIn-populatecustomermodel::: " + e.getMessage());
			throw new SeqirusCustomException(e);
		}
	}

	/**
	 * @param customer
	 * @param custRegistrationForm
	 */


	private void populatePayingAddressModel(final AddressModel source, final SeqirusB2BTempCustomerModel target)
	{
		if (source.getSapCustomerType().contains("PY"))
		{
			try
			{
				final CustomerAddressModel payingDetailsModel = target.getPayingAddress();
				if (null != source.getAddressId())
				{
					payingDetailsModel.setAddressId(source.getAddressId());
					payingDetailsModel.setStatus(TempObjectStatusEnum.SYNC);

				}

				if (StringUtils.isNotBlank(source.getCompany()))
				{
					payingDetailsModel.setCompany(source.getCompany());
				}
				else
				{
					payingDetailsModel.setCompany(StringUtils.EMPTY);
				}

				if (StringUtils.isNotBlank(source.getEmail()))
				{
					payingDetailsModel.setEmail(source.getEmail());
				}
				else
				{
					payingDetailsModel.setEmail(StringUtils.EMPTY);
				}

				if (StringUtils.isNotBlank(source.getLine1()))
				{
					payingDetailsModel.setBuildingStreet(source.getLine1());
				}
				else
				{
					payingDetailsModel.setBuildingStreet(StringUtils.EMPTY);
				}

				if (StringUtils.isNotBlank(source.getPhone1()))
				{
					payingDetailsModel.setPhone1(source.getPhone1());
				}
				else
				{
					payingDetailsModel.setPhone1(StringUtils.EMPTY);
				}

				if (StringUtils.isNotBlank(source.getLine2()))
				{
					payingDetailsModel.setAdditionalStreet(source.getLine2());
				}
				else
				{
					payingDetailsModel.setAdditionalStreet(StringUtils.EMPTY);
				}

				if (StringUtils.isNotBlank(source.getPhone2()))
				{
					payingDetailsModel.setPhoneExt(source.getPhone2());
				}
				else
				{
					payingDetailsModel.setPhoneExt(StringUtils.EMPTY);
				}

				if (StringUtils.isNotBlank(source.getTown()))
				{
					payingDetailsModel.setCity(source.getTown());
				}
				else
				{
					payingDetailsModel.setCity(StringUtils.EMPTY);
				}

				if (StringUtils.isNotBlank(source.getFirstname()))
				{
					payingDetailsModel.setFirstname(source.getFirstname());
				}
				else
				{
					payingDetailsModel.setFirstname(StringUtils.EMPTY);
				}
				if (StringUtils.isNotBlank(source.getLastname()))
				{
					payingDetailsModel.setLastname(source.getLastname());
				}
				else
				{
					payingDetailsModel.setLastname(StringUtils.EMPTY);
				}
				if (StringUtils.isNotBlank(source.getCompany()))
				{
					payingDetailsModel.setOrganizationName(source.getCompany());
				}
				else
				{
					payingDetailsModel.setOrganizationName(StringUtils.EMPTY);
				}
				if (StringUtils.isNotBlank(source.getPostalcode()))
				{
					payingDetailsModel.setZipCode(source.getPostalcode());
				}
				else
				{
					payingDetailsModel.setZipCode(StringUtils.EMPTY);
				}
				if (StringUtils.isNotBlank(source.getJobTitle()))
				{
					payingDetailsModel.setJobTitle(source.getJobTitle());
				}
				else
				{
					payingDetailsModel.setJobTitle(StringUtils.EMPTY);
				}
				if (StringUtils.isNotBlank(source.getOptionalEmail()))
				{
					payingDetailsModel.setOptionalEmail(source.getOptionalEmail());
				}
				else
				{
					payingDetailsModel.setOptionalEmail(StringUtils.EMPTY);
				}
				final CountryModel payerCountry = fetchCountry(source.getCountry());
				if (null != payerCountry)
				{
					payingDetailsModel.setCountry(payerCountry);
				}

				modelService.save(payingDetailsModel);
				target.setPayingAddress(payingDetailsModel);
			}
			catch (final Exception e)
			{
				logger.error(":::ExceptionIn-Payer::: " + e.getMessage());
				throw new SeqirusCustomException(e);
			}
		}
	}

	/**
	 * @param custRegistrationForm
	 * @param customerRegistrationData
	 * @param customer
	 */


	private void populateinvoicingAddressModel(final AddressModel source, final SeqirusB2BTempCustomerModel target)
	{
		if (source.getSapCustomerType().contains("BP"))
		{
			try
			{
				final CustomerAddressModel invoicingContractModel = target.getInvoiceAddress();

				if (null != source.getAddressId())
				{

					invoicingContractModel.setAddressId(source.getAddressId());
					invoicingContractModel.setStatus(TempObjectStatusEnum.SYNC);

				}


				if (StringUtils.isNotBlank(source.getCompany()))
				{
					invoicingContractModel.setOrganizationName(source.getCompany());
				}
				else
				{
					invoicingContractModel.setOrganizationName(StringUtils.EMPTY);
				}

				if (StringUtils.isNotBlank(source.getEmail()))
				{
					invoicingContractModel.setEmail(source.getEmail());
				}
				else
				{
					invoicingContractModel.setEmail(StringUtils.EMPTY);
				}

				if (StringUtils.isNotBlank(source.getLine1()))
				{
					invoicingContractModel.setBuildingStreet(source.getLine1());
				}
				else
				{
					invoicingContractModel.setBuilding(StringUtils.EMPTY);
				}

				if (StringUtils.isNotBlank(source.getPhone1()))
				{
					invoicingContractModel.setPhone1(source.getPhone1());
				}
				else
				{
					invoicingContractModel.setPhone1(StringUtils.EMPTY);
				}

				if (StringUtils.isNotBlank(source.getLine2()))
				{
					invoicingContractModel.setAdditionalStreet(source.getLine2());
				}
				else
				{
					invoicingContractModel.setAdditionalStreet(StringUtils.EMPTY);
				}

				if (StringUtils.isNotBlank(source.getPhone2()))
				{
					invoicingContractModel.setPhoneExt(source.getPhone2());
				}
				else
				{
					invoicingContractModel.setPhoneExt(StringUtils.EMPTY);
				}

				if (StringUtils.isNotBlank(source.getTown()))
				{
					invoicingContractModel.setCity(source.getTown());
				}
				else
				{
					invoicingContractModel.setCity(StringUtils.EMPTY);
				}

				if (StringUtils.isNotBlank(source.getPostalcode()))
				{
					invoicingContractModel.setZipCode(source.getPostalcode());
				}
				else
				{
					invoicingContractModel.setZipCode(StringUtils.EMPTY);
				}

				if (StringUtils.isNotBlank(source.getFirstname()))
				{
					invoicingContractModel.setFirstname(source.getFirstname());
				}
				else
				{
					invoicingContractModel.setFirstname(StringUtils.EMPTY);
				}
				if (StringUtils.isNotBlank(source.getLastname()))
				{
					invoicingContractModel.setLastname(source.getLastname());
				}
				else
				{
					invoicingContractModel.setLastname(StringUtils.EMPTY);
				}
				if (StringUtils.isNotBlank(source.getJobTitle()))
				{
					invoicingContractModel.setJobTitle(source.getJobTitle());
				}
				else
				{
					invoicingContractModel.setJobTitle(StringUtils.EMPTY);
				}
				if (StringUtils.isNotBlank(source.getOptionalEmail()))
				{
					invoicingContractModel.setOptionalEmail(source.getOptionalEmail());
				}
				else
				{
					invoicingContractModel.setOptionalEmail(StringUtils.EMPTY);
				}
				final CountryModel invoiceCountry = fetchCountry(source.getCountry());
				if (null != invoiceCountry)
				{
					invoicingContractModel.setCountry(invoiceCountry);
				}

				modelService.save(invoicingContractModel);
				target.setInvoiceAddress(invoicingContractModel);
			}
			catch (final Exception e)
			{
				logger.error(":::ExceptionIn-Invoice::: " + e.getMessage());
				throw new SeqirusCustomException(e);
			}
		}
	}


	/**
	 * @param custRegistrationForm
	 * @param customerRegistrationData
	 * @param customer
	 */
	private void populateShippingAddressModel(final B2BUnitModel source, final SeqirusB2BTempCustomerModel target)
	{
		final List<CustomerAddressModel> shippingLocModelList = new ArrayList<CustomerAddressModel>();
		try
		{

			for (final CustomerAddressModel shippingModel : target.getShippingAddress())
			{
				shippingLocModelList.add(shippingModel);
			}

			for (final AddressModel address : source.getAddresses())
			{
				if (address.getSapCustomerType().contains("SH"))
				{

					boolean isAddressAvailable = false;
					for (final CustomerAddressModel shippingModel : shippingLocModelList)
					{


						if (!shippingModel.getStatus().equals(TempObjectStatusEnum.CLOSED) && null != shippingModel.getAddressId()
								&& shippingModel.getAddressId().equalsIgnoreCase(address.getAddressId())
								|| (!shippingModel.getStatus().equals(TempObjectStatusEnum.CLOSED)
										&& null != shippingModel.getLicenseNumber()
										&& shippingModel.getLicenseNumber().equalsIgnoreCase(address.getLicenseNumber())))
						{
							isAddressAvailable = true;
							setShippingData(address, shippingModel);
							shippingModel.setStatus(TempObjectStatusEnum.SYNC);
							modelService.save(shippingModel);
							//shippingLocModelList.add(shippingModel);

						}
					}

					if (!isAddressAvailable)
					{

						final CustomerAddressModel shippingLocationsModel = modelService.create(CustomerAddressModel.class);
						shippingLocationsModel.setStatus(TempObjectStatusEnum.SYNC);
						setShippingData(address, shippingLocationsModel);
						shippingLocationsModel.setOwner(target);
						modelService.save(shippingLocationsModel);
						shippingLocModelList.add(shippingLocationsModel);
						isAddressAvailable = false;
					}
				}
				}


			target.setShippingAddress(shippingLocModelList);

		}
		catch (final Exception e)
		{
			logger.error(":::ExceptionIn-Shipping::: " + e.getMessage());
			throw new SeqirusCustomException(e);
		}
	}

	/**
	 * @param shippingAddress
	 * @param shippingLocationsModel
	 */
	private void setShippingData(final AddressModel shippingAddress, final CustomerAddressModel shippingLocationsModel)
	{
		if (StringUtils.isNotBlank(shippingAddress.getLine1()))
		{
			shippingLocationsModel.setBuildingStreet(shippingAddress.getLine1());
		}
		else
		{
			shippingLocationsModel.setBuildingStreet(StringUtils.EMPTY);
		}
		if (StringUtils.isNotBlank(shippingAddress.getAddressId()))
		{
			shippingLocationsModel.setAddressId(shippingAddress.getAddressId());
		}
		else
		{
			shippingLocationsModel.setAddressId(StringUtils.EMPTY);
		}

		if (StringUtils.isNotBlank(shippingAddress.getLine2()))
		{
			shippingLocationsModel.setAdditionalStreet(shippingAddress.getLine2());
		}
		else
		{
			shippingLocationsModel.setAdditionalStreet(StringUtils.EMPTY);
		}

		if (StringUtils.isNotBlank(shippingAddress.getTown()))
		{
			shippingLocationsModel.setCity(shippingAddress.getTown());
		}
		else
		{
			shippingLocationsModel.setCity(StringUtils.EMPTY);
		}

		if (StringUtils.isNotBlank(shippingAddress.getPostalcode()))
		{
			shippingLocationsModel.setZipCode(shippingAddress.getPostalcode());
		}
		else
		{
			shippingLocationsModel.setZipCode(StringUtils.EMPTY);
		}
		if (StringUtils.isNotBlank(shippingAddress.getCompany()))
		{
			shippingLocationsModel.setOrganizationName(shippingAddress.getCompany());
		}
		else
		{
			shippingLocationsModel.setOrganizationName(StringUtils.EMPTY);
		}
		if (StringUtils.isNotBlank(shippingAddress.getFirstname()))
		{
			shippingLocationsModel.setFirstname(shippingAddress.getFirstname());
		}
		else
		{
			shippingLocationsModel.setFirstname(StringUtils.EMPTY);
		}
		if (StringUtils.isNotBlank(shippingAddress.getLastname()))
		{
			shippingLocationsModel.setLastname(shippingAddress.getLastname());
		}
		else
		{
			shippingLocationsModel.setLastname(StringUtils.EMPTY);
		}
		if (StringUtils.isNotBlank(shippingAddress.getEmail()))
		{
			shippingLocationsModel.setEmail(shippingAddress.getEmail());
		}
		else
		{
			shippingLocationsModel.setEmail(StringUtils.EMPTY);
		}
		if (StringUtils.isNotBlank(shippingAddress.getPhone1()))
		{
			shippingLocationsModel.setPhone1(shippingAddress.getPhone1());
		}
		else
		{
			shippingLocationsModel.setPhone1(StringUtils.EMPTY);
		}
		if (StringUtils.isNotBlank(shippingAddress.getCellphone()))
		{
			shippingLocationsModel.setPhone2(shippingAddress.getCellphone());
		}
		else
		{
			shippingLocationsModel.setPhone2(StringUtils.EMPTY);
		}
		if (StringUtils.isNotBlank(shippingAddress.getPhone2()))
		{
			shippingLocationsModel.setPhoneExt(shippingAddress.getPhone2());
		}
		else
		{
			shippingLocationsModel.setPhoneExt(StringUtils.EMPTY);
		}
		if (StringUtils.isNotBlank(shippingAddress.getNhsCode()))
		{
			shippingLocationsModel.setNhsCode(shippingAddress.getNhsCode());
		}
		else
		{
			shippingLocationsModel.setNhsCode(StringUtils.EMPTY);
		}
		if (StringUtils.isNotBlank(shippingAddress.getJobTitle()))
		{
			shippingLocationsModel.setJobTitle(shippingAddress.getJobTitle());
		}
		else
		{
			shippingLocationsModel.setJobTitle(StringUtils.EMPTY);
		}
		if (StringUtils.isNotBlank(shippingAddress.getOptionalEmail()))
		{
			shippingLocationsModel.setOptionalEmail(shippingAddress.getOptionalEmail());
		}
		else
		{
			shippingLocationsModel.setOptionalEmail(StringUtils.EMPTY);
		}
		if (StringUtils.isNotBlank(shippingAddress.getLicenseNumber()))
		{
			shippingLocationsModel.setLicenseNumber(shippingAddress.getLicenseNumber());
		}
		else
		{
			shippingLocationsModel.setLicenseNumber(StringUtils.EMPTY);
		}
		if (StringUtils.isNotBlank(shippingAddress.getLicenseName()))
		{
			shippingLocationsModel.setLicenseName(shippingAddress.getLicenseName());
		}
		else
		{
			shippingLocationsModel.setLicenseName(StringUtils.EMPTY);
		}
		if (StringUtils.isNotBlank(shippingAddress.getNhsCode()))
		{
			shippingLocationsModel.setNhsCode(shippingAddress.getNhsCode());
		}
		else
		{
			shippingLocationsModel.setNhsCode(StringUtils.EMPTY);
		}

		final CountryModel shipCountry = fetchCountry(shippingAddress.getCountry());
		if (null != shipCountry)
		{
			shippingLocationsModel.setCountry(shipCountry);
		}
	}

	public final CountryModel fetchCountry(final CountryModel countryData)
	{
		if (null != countryData && StringUtils.isNotBlank(countryData.getName()))
		{
			final CountryModel countryModelByName = seqirusCustomerRegistrationService.getCountryByName(countryData.getName());
			if (null != countryModelByName)
			{
				return commonI18NService.getCountry(countryModelByName.getIsocode());
			}
		}
		return null;
	}


	/**
	 * @return the seqirusCustomerRegistrationService
	 */
	public SeqirusCustomerRegistrationService getSeqirusCustomerRegistrationService()
	{
		return seqirusCustomerRegistrationService;
	}


	/**
	 * @param seqirusCustomerRegistrationService
	 *           the seqirusCustomerRegistrationService to set
	 */
	public void setSeqirusCustomerRegistrationService(final SeqirusCustomerRegistrationService seqirusCustomerRegistrationService)
	{
		this.seqirusCustomerRegistrationService = seqirusCustomerRegistrationService;
	}


}
