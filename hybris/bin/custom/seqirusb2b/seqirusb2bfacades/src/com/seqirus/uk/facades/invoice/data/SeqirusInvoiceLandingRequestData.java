/**
 *
 */
package com.seqirus.uk.facades.invoice.data;

/**
 * @author 700196
 *
 */
public class SeqirusInvoiceLandingRequestData
{
	private String organizationId;
	private String fromDate;
	private String toDate;
	private String customerNumber;
	private String season;


	/**
	 * @return the season
	 */
	public String getSeason()
	{
		return season;
	}

	/**
	 * @param season
	 *           the season to set
	 */
	public void setSeason(final String season)
	{
		this.season = season;
	}

	/**
	 * @return the organizationId
	 */
	public String getOrganizationId()
	{
		return organizationId;
	}

	/**
	 * @param organizationId
	 *           the organizationId to set
	 */
	public void setOrganizationId(final String organizationId)
	{
		this.organizationId = organizationId;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate()
	{
		return fromDate;
	}

	/**
	 * @param fromDate
	 *           the fromDate to set
	 */
	public void setFromDate(final String fromDate)
	{
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate()
	{
		return toDate;
	}

	/**
	 * @param toDate
	 *           the toDate to set
	 */
	public void setToDate(final String toDate)
	{
		this.toDate = toDate;
	}

	/**
	 * @return the customerNumber
	 */
	public String getCustomerNumber()
	{
		return customerNumber;
	}

	/**
	 * @param customerNumber
	 *           the customerNumber to set
	 */
	public void setCustomerNumber(final String customerNumber)
	{
		this.customerNumber = customerNumber;
	}



}
