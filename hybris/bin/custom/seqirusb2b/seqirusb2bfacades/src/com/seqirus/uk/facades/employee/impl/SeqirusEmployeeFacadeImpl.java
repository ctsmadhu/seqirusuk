/**
 *
 */
package com.seqirus.uk.facades.employee.impl;

import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.user.UserMatchingService;
import de.hybris.platform.core.model.user.EmployeeModel;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.seqirus.uk.core.services.SeqirusEmployeeService;
import com.seqirus.uk.facades.employee.SeqirusEmployeeFacade;


/**
 * @author 172553
 *
 */
public class SeqirusEmployeeFacadeImpl implements SeqirusEmployeeFacade
{
	@Autowired
	private SeqirusEmployeeService seqirusEmployeeService;

	@Autowired
	private UserMatchingService userMatchingService;

	@Override
	public void forgottenPassword(final String id)
	{
		Assert.hasText(id, "The field [id] cannot be empty");
		final EmployeeModel employee = userMatchingService.getUserByProperty(id.toLowerCase(Locale.ENGLISH), EmployeeModel.class);
		seqirusEmployeeService.forgottenPassword(employee);
	}

	@Override
	public void updatePassword(final String token, final String newPassword) throws TokenInvalidatedException
	{
		seqirusEmployeeService.updatePassword(token, newPassword);

	}

}
