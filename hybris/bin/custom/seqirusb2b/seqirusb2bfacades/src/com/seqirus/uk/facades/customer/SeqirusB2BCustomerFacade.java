/**
 *
 */
package com.seqirus.uk.facades.customer;

import de.hybris.platform.b2b.model.B2BCustomerModel;

import com.seqirus.uk.facades.cutomer.data.SeqirusCDCCustomerData;


/**
 * The Interface SeqirusB2BCustomerFacade.
 *
 * @author Avaneesh Chauhan
 */
public interface SeqirusB2BCustomerFacade
{

	/**
	 * Gets the customer profile data.
	 *
	 * @param customer
	 *           the customer
	 * @return the customer profile data
	 */
	public SeqirusCDCCustomerData getCustomerProfileData(B2BCustomerModel customer);
}
