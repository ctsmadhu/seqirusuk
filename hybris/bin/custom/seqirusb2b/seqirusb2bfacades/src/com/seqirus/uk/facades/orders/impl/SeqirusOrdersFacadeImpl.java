/**
 *
 */
package com.seqirus.uk.facades.orders.impl;

import java.util.List;

import javax.annotation.Resource;

import com.seqirus.uk.core.dataObjects.OrderSummary;
import com.seqirus.uk.core.orders.service.SeqirusOrdersService;
import com.seqirus.uk.facades.orders.SeqirusOrdersFacade;


/**
 * @author 614269
 *
 */
public class SeqirusOrdersFacadeImpl implements SeqirusOrdersFacade
{

	@Resource(name = "seqirusOrdersService")
	private SeqirusOrdersService seqirusOrdersService;

	@Override
	public List<OrderSummary> getOrders(final String customerId, final String season)
	{

		return seqirusOrdersService.getOrders(customerId, season);
	}

}
