/**
 *
 */
package com.seqirus.uk.facades.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.seqirus.uk.facades.customer.data.SeqirusCustomerData;


/**
 * @author 700196
 *
 */
public class SeqirusCustomerPopulator implements Populator<B2BCustomerModel, SeqirusCustomerData>
{

	private Converter<CurrencyModel, CurrencyData> currencyConverter;
	private Converter<LanguageModel, LanguageData> languageConverter;
	private CustomerNameStrategy customerNameStrategy;

	@Override
	public void populate(final B2BCustomerModel source, final SeqirusCustomerData target) throws ConversionException
	{

		if (source.getSessionCurrency() != null)
		{
			target.setCurrency(getCurrencyConverter().convert(source.getSessionCurrency()));
		}
		if (source.getSessionLanguage() != null)
		{
			target.setLanguage(getLanguageConverter().convert(source.getSessionLanguage()));
		}

		if (StringUtils.isNotBlank(source.getWelcomeCustomerSoldToId()))
		{
			target.setSapConsumerID(source.getWelcomeCustomerSoldToId());
		}

		if (null != source.getDefaultB2BUnit() && StringUtils.isNotBlank(source.getDefaultB2BUnit().getCompanyName()))
		{
			target.setOrgName(source.getDefaultB2BUnit().getCompanyName());
		}

		final String[] names = getCustomerNameStrategy().splitName(source.getName());
		if (names != null)
		{
			target.setFirstName(names[0]);
			target.setLastName(names[1]);
		}

		final TitleModel title = source.getTitle();
		if (title != null)
		{
			target.setTitleCode(title.getCode());
		}

		target.setName(source.getName());
		setUid(source, target);
		target.setCustomerId(source.getCustomerID());
		target.setDeactivationDate(source.getDeactivationDate());
	}

	protected void setUid(final UserModel source, final CustomerData target)
	{
		target.setUid(source.getUid());
		if (source instanceof CustomerModel)
		{
			final CustomerModel customer = (CustomerModel) source;
			if (isOriginalUidAvailable(customer))
			{
				target.setDisplayUid(customer.getOriginalUid());
			}
		}
	}

	protected boolean isOriginalUidAvailable(final CustomerModel source)
	{
		return source.getOriginalUid() != null;
	}

	protected Converter<CurrencyModel, CurrencyData> getCurrencyConverter()
	{
		return currencyConverter;
	}

	@Required
	public void setCurrencyConverter(final Converter<CurrencyModel, CurrencyData> currencyConverter)
	{
		this.currencyConverter = currencyConverter;
	}

	protected Converter<LanguageModel, LanguageData> getLanguageConverter()
	{
		return languageConverter;
	}

	@Required
	public void setLanguageConverter(final Converter<LanguageModel, LanguageData> languageConverter)
	{
		this.languageConverter = languageConverter;
	}

	protected CustomerNameStrategy getCustomerNameStrategy()
	{
		return customerNameStrategy;
	}

	@Required
	public void setCustomerNameStrategy(final CustomerNameStrategy customerNameStrategy)
	{
		this.customerNameStrategy = customerNameStrategy;
	}
}
