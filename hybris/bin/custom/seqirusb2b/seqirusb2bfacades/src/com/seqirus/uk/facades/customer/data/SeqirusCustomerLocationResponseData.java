/**
 *
 */
package com.seqirus.uk.facades.customer.data;

/**
 * @author 700196
 *
 */
public class SeqirusCustomerLocationResponseData
{
	private String mobilePhone;
	private String organizationName;
	private String additionalEmail;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String postalCode;
	private String country;
	private String licenseNumber;
	private String licenseName;
	private String nhsCode;
	private String addressID;
	private String partnerId;
	private String sapCustomerType;
	private String registrationNumber;
	private String vatNumber;
	private String businessType;
	private String contactFirstName;
	private String contactLastName;
	private String contactJobTitle;
	private String contactEmail;
	private String contactTelephone;
	private String contactExtension;
	private String contactNumber;


	/**
	 * @return the contactFirstName
	 */
	public String getContactFirstName()
	{
		return contactFirstName;
	}

	/**
	 * @param contactFirstName
	 *           the contactFirstName to set
	 */
	public void setContactFirstName(final String contactFirstName)
	{
		this.contactFirstName = contactFirstName;
	}

	/**
	 * @return the contactLastName
	 */
	public String getContactLastName()
	{
		return contactLastName;
	}

	/**
	 * @param contactLastName
	 *           the contactLastName to set
	 */
	public void setContactLastName(final String contactLastName)
	{
		this.contactLastName = contactLastName;
	}

	

	/**
	 * @return the contactJobTitle
	 */
	public String getContactJobTitle()
	{
		return contactJobTitle;
	}

	/**
	 * @param contactJobTitle the contactJobTitle to set
	 */
	public void setContactJobTitle(final String contactJobTitle)
	{
		this.contactJobTitle = contactJobTitle;
	}

	/**
	 * @return the contactNumber
	 */
	public String getContactNumber()
	{
		return contactNumber;
	}

	/**
	 * @param contactNumber the contactNumber to set
	 */
	public void setContactNumber(final String contactNumber)
	{
		this.contactNumber = contactNumber;
	}

	/**
	 * @return the contactEmail
	 */
	public String getContactEmail()
	{
		return contactEmail;
	}

	/**
	 * @param contactEmail
	 *           the contactEmail to set
	 */
	public void setContactEmail(final String contactEmail)
	{
		this.contactEmail = contactEmail;
	}

	/**
	 * @return the contactTelephone
	 */
	public String getContactTelephone()
	{
		return contactTelephone;
	}

	/**
	 * @param contactTelephone
	 *           the contactTelephone to set
	 */
	public void setContactTelephone(final String contactTelephone)
	{
		this.contactTelephone = contactTelephone;
	}

	/**
	 * @return the contactExtension
	 */
	public String getContactExtension()
	{
		return contactExtension;
	}

	/**
	 * @param contactExtension
	 *           the contactExtension to set
	 */
	public void setContactExtension(final String contactExtension)
	{
		this.contactExtension = contactExtension;
	}

	/**
	 * @return the partnerId
	 */
	public String getPartnerId()
	{
		return partnerId;
	}

	/**
	 * @param partnerId
	 *           the partnerId to set
	 */
	public void setPartnerId(final String partnerId)
	{
		this.partnerId = partnerId;
	}


	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1()
	{
		return addressLine1;
	}

	/**
	 * @param addressLine1
	 *           the addressLine1 to set
	 */
	public void setAddressLine1(final String addressLine1)
	{
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2()
	{
		return addressLine2;
	}

	/**
	 * @param addressLine2
	 *           the addressLine2 to set
	 */
	public void setAddressLine2(final String addressLine2)
	{
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *           the city to set
	 */
	public void setCity(final String city)
	{
		this.city = city;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * @param postalCode
	 *           the postalCode to set
	 */
	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}

	

	/**
	 * @return the additionalEmail
	 */
	public String getAdditionalEmail()
	{
		return additionalEmail;
	}

	/**
	 * @param additionalEmail
	 *           the additionalEmail to set
	 */
	public void setAdditionalEmail(final String additionalEmail)
	{
		this.additionalEmail = additionalEmail;
	}


	/**
	 * @return the country
	 */
	public String getCountry()
	{
		return country;
	}

	/**
	 * @param country
	 *           the country to set
	 */
	public void setCountry(final String country)
	{
		this.country = country;
	}


	/**
	 * @return the organizationName
	 */
	public String getOrganizationName()
	{
		return organizationName;
	}

	/**
	 * @param organizationName
	 *           the organizationName to set
	 */
	public void setOrganizationName(final String organizationName)
	{
		this.organizationName = organizationName;
	}

	/**
	 * @return the licenseNumber
	 */
	public String getLicenseNumber()
	{
		return licenseNumber;
	}

	/**
	 * @param licenseNumber
	 *           the licenseNumber to set
	 */
	public void setLicenseNumber(final String licenseNumber)
	{
		this.licenseNumber = licenseNumber;
	}

	/**
	 * @return the licenseName
	 */
	public String getLicenseName()
	{
		return licenseName;
	}

	/**
	 * @param licenseName
	 *           the licenseName to set
	 */
	public void setLicenseName(final String licenseName)
	{
		this.licenseName = licenseName;
	}

	/**
	 * @return the nhsCode
	 */
	public String getNhsCode()
	{
		return nhsCode;
	}

	/**
	 * @param nhsCode
	 *           the nhsCode to set
	 */
	public void setNhsCode(final String nhsCode)
	{
		this.nhsCode = nhsCode;
	}

	/**
	 * @return the addressID
	 */
	public String getAddressID()
	{
		return addressID;
	}

	/**
	 * @param addressID
	 *           the addressID to set
	 */
	public void setAddressID(final String addressID)
	{
		this.addressID = addressID;
	}

	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone()
	{
		return mobilePhone;
	}

	/**
	 * @param mobilePhone
	 *           the mobilePhone to set
	 */
	public void setMobilePhone(final String mobilePhone)
	{
		this.mobilePhone = mobilePhone;
	}

	/**
	 * @return the sapCustomerType
	 */
	public String getSapCustomerType()
	{
		return sapCustomerType;
	}

	/**
	 * @param sapCustomerType
	 *           the sapCustomerType to set
	 */
	public void setSapCustomerType(final String sapCustomerType)
	{
		this.sapCustomerType = sapCustomerType;
	}

	/**
	 * @return the registrationNumber
	 */
	public String getRegistrationNumber()
	{
		return registrationNumber;
	}

	/**
	 * @param registrationNumber
	 *           the registrationNumber to set
	 */
	public void setRegistrationNumber(final String registrationNumber)
	{
		this.registrationNumber = registrationNumber;
	}

	/**
	 * @return the vatNumber
	 */
	public String getVatNumber()
	{
		return vatNumber;
	}

	/**
	 * @param vatNumber
	 *           the vatNumber to set
	 */
	public void setVatNumber(final String vatNumber)
	{
		this.vatNumber = vatNumber;
	}

	/**
	 * @return the businessType
	 */
	public String getBusinessType()
	{
		return businessType;
	}

	/**
	 * @param businessType
	 *           the businessType to set
	 */
	public void setBusinessType(final String businessType)
	{
		this.businessType = businessType;
	}


}
