/**
 *
 */
package com.seqirus.uk.facades.invoice.data;

import java.util.List;


public class SeqirusReturnsAndCreditsResponseData
{
	protected List<SeqirusReturnsAndCreditsResponse> creditList;
	protected String totalAvailableCredit;
	protected String accountName;

	public String getAccountName()
	{
		return accountName;
	}

	public void setAccountName(final String accountName)
	{
		this.accountName = accountName;
	}

	public String getTotalAvailableCredit()
	{
		return totalAvailableCredit;
	}

	public void setTotalAvailableCredit(final String totalAvailableCredit)
	{
		this.totalAvailableCredit = totalAvailableCredit;
	}

	public List<SeqirusReturnsAndCreditsResponse> getCreditList()
	{
		return creditList;
	}

	public void setCreditList(final List<SeqirusReturnsAndCreditsResponse> creditList)
	{
		this.creditList = creditList;
	}

}
