/**
 *
 */
package com.seqirus.uk.facades.invoice;

import java.io.InputStream;

import com.seqirus.uk.core.exceptions.SeqirusCustomException;
import com.seqirus.uk.facades.invoice.data.SeqirusInvoiceDetailsResponseData;
import com.seqirus.uk.facades.invoice.data.SeqirusInvoiceLandingResponseData;
import com.seqirus.uk.facades.invoice.data.SeqirusReturnsAndCreditsResponseData;


/**
 * @author 700196
 *
 */
public interface SeqirusInvoiceFacade
{

	/**
	 * @return SeqirusInvoiceLandingResponseData
	 */
	SeqirusInvoiceLandingResponseData retrieveInvoiceList(String season);

	/**
	 * @param invoiceNumber
	 * @return SeqirusInvoiceDetailsResponseData
	 */
	SeqirusInvoiceDetailsResponseData retrieveInvoiceDetails(String invoiceNumber);

	/**
	 * @param invoiceNumber
	 * @return InputStream
	 */
	InputStream downloadInvoice(String invoiceNumber);

	/**
	 * @param defaultB2BUnit
	 * @return
	 * @throws SeqirusCustomException
	 */
	SeqirusReturnsAndCreditsResponseData getReturnsAndCreditsResponse(String defaultB2BUnit) throws SeqirusCustomException;

}
