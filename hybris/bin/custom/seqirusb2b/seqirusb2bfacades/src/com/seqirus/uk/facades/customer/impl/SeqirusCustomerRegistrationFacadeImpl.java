/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.uk.facades.customer.impl;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.consent.converters.populator.ConsentPopulator;
import de.hybris.platform.commercefacades.consent.data.ConsentData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.event.RegisterEvent;
import de.hybris.platform.commerceservices.model.consent.ConsentModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.seqirus.core.model.CustomerAddressModel;
import com.seqirus.core.model.SeqirusB2BTempCustomerModel;
import com.seqirus.core.model.SeqirusCompanyInfoModel;
import com.seqirus.uk.constants.Seqirusb2bworkflowConstants;
import com.seqirus.uk.core.dataObjects.JoinAccountAPIResponse;
import com.seqirus.uk.core.enums.AccountStatusEnum;
import com.seqirus.uk.core.enums.AddressType;
import com.seqirus.uk.core.enums.TempObjectStatusEnum;
import com.seqirus.uk.core.event.SeqirusCustomerRegistrationEvent;
import com.seqirus.uk.core.event.SeqirusCustomerUpdateRegistrationEvent;
import com.seqirus.uk.core.event.SeqirusRegisterEvent;
import com.seqirus.uk.core.exceptions.SeqirusCustomException;
import com.seqirus.uk.core.registration.service.SeqirusCustomerRegistrationService;
import com.seqirus.uk.data.B2BRegistrationData;
import com.seqirus.uk.exceptions.CustomerAlreadyExistsException;
import com.seqirus.uk.facades.B2BRegistrationFacade;
import com.seqirus.uk.facades.customer.SeqirusCustomerRegistrationFacade;
import com.seqirus.uk.facades.customer.data.SeqirusCustomerLocationResponseData;
import com.seqirus.uk.facades.customer.data.SeqirusCustomerResponseData;
import com.seqirus.uk.facades.cutomer.data.SeqirusCustomerRegistrationAddressData;
import com.seqirus.uk.facades.cutomer.data.SeqirusCustomerRegistrationData;
import com.seqirus.uk.facades.populators.SeqirusB2BRegistrationWFDataPopulator;
import com.seqirus.uk.facades.populators.SeqirusConsentPopulator;
import com.seqirus.uk.facades.populators.SeqirusCustomerB2BTempDataPopulator;
import com.seqirus.uk.facades.populators.SeqirusTempRegReversePopulator;
import com.seqirus.uk.model.B2BRegistrationModel;


/**
 * @author 700196
 *
 */
public class SeqirusCustomerRegistrationFacadeImpl implements SeqirusCustomerRegistrationFacade
{
	static Logger logger = Logger.getLogger(SeqirusCustomerRegistrationFacadeImpl.class);
	private static final String B2B_UNIT = "b2bUnit";

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "userService")
	UserService userService;

	private SeqirusCustomerRegistrationService seqirusCustomerRegistrationService;

	@Autowired
	B2BRegistrationFacade b2bRegistrationFacade;

	private SeqirusCustomerB2BTempDataPopulator seqirusCustomerB2BTempDataPopulator;

	@Autowired
	private SessionService sessionService;

	@Autowired
	EventService eventService;

	@Autowired
	BaseStoreService baseStoreService;

	@Autowired
	BaseSiteService baseSiteService;

	@Autowired
	AssistedServiceFacade assistedServiceFacade;

	@Autowired
	ConsentPopulator consentPopultor;

	@Autowired
	SeqirusConsentPopulator seqirusConsentPopulator;

	private SeqirusB2BRegistrationWFDataPopulator seqirusB2BRegistrationWFDataPopulator;

	@Autowired
	private SeqirusTempRegReversePopulator seqirusTempRegReversePopulator;

	@Resource
	private Populator<JoinAccountAPIResponse, SeqirusCustomerResponseData> seqirusJoinAccountResponsePopulator;


	/**
	 * @return the seqirusConsentPopulator
	 */
	public SeqirusConsentPopulator getSeqirusConsentPopulator()
	{
		return seqirusConsentPopulator;
	}

	/**
	 * @param seqirusConsentPopulator
	 *           the seqirusConsentPopulator to set
	 */
	public void setSeqirusConsentPopulator(final SeqirusConsentPopulator seqirusConsentPopulator)
	{
		this.seqirusConsentPopulator = seqirusConsentPopulator;
	}

	@Override
	public void register(final SeqirusCustomerRegistrationData customerRegistrationData)
	{

		final UserModel currentUser = userService.getCurrentUser();
		final B2BCustomerModel customer = (B2BCustomerModel) currentUser;
		final SeqirusB2BTempCustomerModel customerTempObject = modelService.create(SeqirusB2BTempCustomerModel.class);
		final SeqirusCompanyInfoModel businessDetailsModel = modelService.create(SeqirusCompanyInfoModel.class);
		try
		{
			logger.info("::::UserID::::" + customerRegistrationData.getEmail());
   		populateCustomerModel(customerRegistrationData, businessDetailsModel, false);
   		customerTempObject.setCompanyInfo(businessDetailsModel);
   		final B2BRegistrationData data = new B2BRegistrationData();
   		data.setWorkflowName(Seqirusb2bworkflowConstants.Workflows.REGISTRATION_WORKFLOW);
   		if (null != customerRegistrationData.getUserEmail())
   		{
   			customerTempObject.setUserId(customerRegistrationData.getUserEmail());
   			data.setEmail(customerRegistrationData.getUserEmail());
   		}
   		if (null != customerRegistrationData.getUserName())
   		{
   			customerTempObject.setUserName(customerRegistrationData.getUserName());
   		}
			logger.info("::::ProfileEmail::::" + currentUser.getUid());
   		populatePayingAddressModel(customerTempObject, customerRegistrationData, customer);
			//populateBillingAddressModel(customerTempObject, customerRegistrationData, customer);
   		populateinvoicingAddressModel(customerTempObject, customerRegistrationData, customer);
   		populateShippingAddressModel(customerTempObject, customerRegistrationData, customer, false);

   		modelService.save(customerTempObject);
   		modelService.refresh(customerTempObject);
			logger.info("::::UserSavedSuccessfully::::");
			logger.debug("::::UserSavedSuccessfully::::");
			b2bRegistrationFacade.register(data);
			sendRegistrationEmail();
			logger.info("::::WorkflowUserSavedSuccessfully::::");
			logger.debug("::::WorkflowUserSavedSuccessfully::::");
		}
		catch (final CustomerAlreadyExistsException ce)
		{
			logger.error("CustomerAlreadyExistsException::: " + ce.getMessage());
			throw new SeqirusCustomException(ce);
		}
		catch (final Exception e)
		{
			logger.error(":::Error during registration::: " + e.getMessage());
			throw new SeqirusCustomException(e);
		}
	}

	@Override
	public void joinAccount()
	{
		final UserModel currentUser = userService.getCurrentUser();
		B2BCustomerModel customer = null;
		//create registration model
		final SeqirusB2BTempCustomerModel regModel = modelService.create(SeqirusB2BTempCustomerModel.class);
		if (currentUser != null && !userService.isAnonymousUser(currentUser))
		{
			customer = (B2BCustomerModel) currentUser;
		}
		final B2BUnitModel b2bUnit = (B2BUnitModel) sessionService.getAttribute(B2B_UNIT);
		if (null != customer)
		{
			customer.setDefaultB2BUnit(b2bUnit);
			customer.setStatus(AccountStatusEnum.JA_COMPLETE);
			customer.setApproved(Boolean.TRUE);
			regModel.setUserId(customer.getUid());
			regModel.setUserName(customer.getName());
		}
		if (null != b2bUnit)
		{
			seqirusTempRegReversePopulator.populate(b2bUnit, regModel);
		}
		//update customer model
		modelService.save(customer);
		modelService.refresh(customer);
		//save registration model
		modelService.save(regModel);
		modelService.refresh(regModel);
		sendEmail(customer);
	}


	@Override
	public void joinExistingAccount(final String accessCode, final String accountNumber)
	{
		final UserModel currentUser = userService.getCurrentUser();
		B2BCustomerModel customer = null;
		//create registration model
		try
		{
		final SeqirusB2BTempCustomerModel regModel = modelService.create(SeqirusB2BTempCustomerModel.class);
		if (currentUser != null && !userService.isAnonymousUser(currentUser))
		{
			customer = (B2BCustomerModel) currentUser;
		}
		userService.setCurrentUser(userService.getAdminUser());
		B2BUnitModel b2bUnitModel = null;
		if (null != customer)
		{
			if(StringUtils.isNotBlank(accessCode)) {
				b2bUnitModel = getOrgNameByAccount(accessCode);
			}
			else if (StringUtils.isNotBlank(accountNumber))
			{
				b2bUnitModel = getOrgNameByAccount(accountNumber);
			}
			if (null == b2bUnitModel)
			{
				b2bUnitModel = modelService.create(B2BUnitModel.class);
			final SeqirusCustomerResponseData responseData = (SeqirusCustomerResponseData) sessionService
					.getAttribute("apiResponse");
			if (null != responseData && null != responseData.getCompanyDetails())
				{
					mapCompayInformation(b2bUnitModel, responseData);
					final Collection<AddressModel> addresses = new ArrayList<AddressModel>();
					mapCompanyAddress(AddressType.SP, responseData, b2bUnitModel, addresses);
					mapBillingAddress(AddressType.BP, responseData, b2bUnitModel, addresses);
					mapPayerAddress(AddressType.PY, responseData, b2bUnitModel, addresses);
					mapShipAddress(AddressType.SH, responseData, b2bUnitModel, addresses);
					b2bUnitModel.setAddresses(addresses);
					modelService.save(b2bUnitModel);
					modelService.refresh(b2bUnitModel);
				}
			}
			customer.setDefaultB2BUnit(b2bUnitModel);
			customer.setStatus(AccountStatusEnum.JA_COMPLETE);
			customer.setApproved(Boolean.TRUE);
			regModel.setUserId(customer.getUid());
			regModel.setUserName(customer.getName());
		}
		if (null != b2bUnitModel)
		{
			seqirusTempRegReversePopulator.populate(b2bUnitModel, regModel);
		}
		//update customer model
		modelService.save(customer);
		modelService.refresh(customer);
		//save registration model
		modelService.save(regModel);
		modelService.refresh(regModel);
		sendEmail(currentUser);
	}
	finally
	{
		userService.setCurrentUser(currentUser);
	}
	}

	/**
	 * @param b2bUnitModel
	 * @param responseData
	 */
	private void mapCompayInformation(final B2BUnitModel b2bUnitModel, final SeqirusCustomerResponseData responseData)
	{
		if (StringUtils.isNotBlank(responseData.getCompanyDetails().getPartnerId()))
		{
			b2bUnitModel.setUid(responseData.getCompanyDetails().getPartnerId());
		}
		b2bUnitModel.setActive(Boolean.TRUE);
		if (StringUtils.isNotBlank(responseData.getCompanyDetails().getOrganizationName()))
		{
			b2bUnitModel.setCompanyName(responseData.getCompanyDetails().getOrganizationName());
		}
		b2bUnitModel.setName(new StringBuilder().append(responseData.getCompanyDetails().getContactFirstName())
				.append(responseData.getCompanyDetails().getContactLastName()).toString());
		b2bUnitModel.setLocname(new StringBuilder().append(responseData.getCompanyDetails().getContactFirstName())
				.append(responseData.getCompanyDetails().getContactLastName()).toString(), Locale.ENGLISH);
		if (StringUtils.isNotBlank(responseData.getCompanyDetails().getContactFirstName()))
		{
			b2bUnitModel.setFirstName(responseData.getCompanyDetails().getContactFirstName());
		}
		if (StringUtils.isNotBlank(responseData.getCompanyDetails().getContactLastName()))
		{
			b2bUnitModel.setLastName(responseData.getCompanyDetails().getContactLastName());
		}
		if (StringUtils.isNotBlank(responseData.getCompanyDetails().getContactJobTitle()))
		{
			b2bUnitModel.setJobTitle(responseData.getCompanyDetails().getContactJobTitle());
		}
		if (StringUtils.isNotBlank(responseData.getCompanyDetails().getContactEmail()))
		{
			b2bUnitModel.setEmail(responseData.getCompanyDetails().getContactEmail());
		}
		if (StringUtils.isNotBlank(responseData.getCompanyDetails().getContactTelephone()))
		{
			b2bUnitModel.setPhoneNumber(responseData.getCompanyDetails().getContactTelephone());
		}
		if (StringUtils.isNotBlank(responseData.getCompanyDetails().getContactExtension()))
		{
			b2bUnitModel.setPhoneExt(responseData.getCompanyDetails().getContactExtension());
		}
		if (StringUtils.isNotBlank(responseData.getCompanyDetails().getNhsCode()))
		{
			b2bUnitModel.setNhcNumber(responseData.getCompanyDetails().getNhsCode());
		}
		if (StringUtils.isNotBlank(responseData.getCompanyDetails().getRegistrationNumber()))
		{
			b2bUnitModel.setRegistrationNumber(responseData.getCompanyDetails().getRegistrationNumber());
		}
		if (StringUtils.isNotBlank(responseData.getCompanyDetails().getBusinessType()))
		{
			b2bUnitModel.setBusinessType(responseData.getCompanyDetails().getBusinessType());
		}
		if (StringUtils.isNotBlank(responseData.getCompanyDetails().getVatNumber()))
		{
			b2bUnitModel.setVatNumber(responseData.getCompanyDetails().getVatNumber());
		}
		b2bUnitModel.setGroups(Collections.emptySet());
		b2bUnitModel.setReportingOrganization(b2bUnitModel);
	}



	/**
	 * @param sh
	 * @param responseData
	 * @param b2bUnitModel
	 * @param addresses
	 */
	private void mapShipAddress(final AddressType sh, final SeqirusCustomerResponseData responseData,
			final B2BUnitModel b2bUnitModel,
			final Collection<AddressModel> addresses)
	{
		if (CollectionUtils.isNotEmpty(responseData.getShippingLocations()))
		{
			for (final SeqirusCustomerLocationResponseData shipLocation : responseData.getShippingLocations())
			{
				final AddressModel shipToAddress = new AddressModel();
				final Set<String> locationType = new HashSet<String>();
				locationType.add(sh.getCode());
				shipToAddress.setShippingAddress(Boolean.TRUE);
				shipToAddress.setOwner(b2bUnitModel);
				shipToAddress.setSapCustomerType(locationType);
				shipToAddress.setStreetname(shipLocation.getAddressLine1());
				shipToAddress.setStreetnumber(shipLocation.getAddressLine2());
				shipToAddress.setTown(shipLocation.getCity());
				shipToAddress.setPostalcode(shipLocation.getPostalCode());
				shipToAddress.setCompany(shipLocation.getOrganizationName());
				shipToAddress.setSapCustomerID(shipLocation.getPartnerId());
				shipToAddress.setLicenseName(shipLocation.getLicenseName());
				shipToAddress.setLicenseNumber(shipLocation.getLicenseNumber());
				shipToAddress.setNhsCode(shipLocation.getNhsCode());
				if (StringUtils.isNotBlank(shipLocation.getCountry()))
				{
					shipToAddress.setCountry(commonI18NService.getCountry(shipLocation.getCountry()));
				}
				mapLocationContact(shipLocation, shipToAddress);
				addresses.add(shipToAddress);
			}
		}

	}

	/**
	 * @param bp
	 * @param responseData
	 * @param b2bUnitModel
	 * @param addresses
	 */
	private void mapBillingAddress(final AddressType bp, final SeqirusCustomerResponseData responseData,
			final B2BUnitModel b2bUnitModel, final Collection<AddressModel> addresses)
	{
		final AddressModel billToAddress = new AddressModel();
		final Set<String> locationType = new HashSet<String>();
		locationType.add(bp.getCode());
		billToAddress.setBillingAddress(Boolean.TRUE);
		billToAddress.setOwner(b2bUnitModel);
		billToAddress.setSapCustomerType(locationType);
		billToAddress.setStreetname(responseData.getInvoicingContractInfo().getAddressLine1());
		billToAddress.setStreetnumber(responseData.getInvoicingContractInfo().getAddressLine2());
		billToAddress.setTown(responseData.getInvoicingContractInfo().getCity());
		billToAddress.setPostalcode(responseData.getInvoicingContractInfo().getPostalCode());
		billToAddress.setCompany(responseData.getInvoicingContractInfo().getOrganizationName());
		billToAddress.setSapCustomerID(responseData.getInvoicingContractInfo().getPartnerId());
		if (StringUtils.isNotBlank(responseData.getInvoicingContractInfo().getCountry()))
		{
			billToAddress.setCountry(commonI18NService.getCountry(responseData.getInvoicingContractInfo().getCountry()));
		}
		mapLocationContact(responseData.getInvoicingContractInfo(), billToAddress);
		addresses.add(billToAddress);
	}

	private void mapPayerAddress(final AddressType py, final SeqirusCustomerResponseData responseData,
			final B2BUnitModel b2bUnitModel, final Collection<AddressModel> addresses)
	{
		final AddressModel payerAddress = new AddressModel();
		final Set<String> locationType = new HashSet<String>();
		locationType.add(py.getCode());
		payerAddress.setOwner(b2bUnitModel);
		payerAddress.setSapCustomerType(locationType);
		payerAddress.setStreetname(responseData.getPayingContactInfo().getAddressLine1());
		payerAddress.setStreetnumber(responseData.getPayingContactInfo().getAddressLine2());
		payerAddress.setTown(responseData.getPayingContactInfo().getCity());
		payerAddress.setPostalcode(responseData.getPayingContactInfo().getPostalCode());
		payerAddress.setCompany(responseData.getPayingContactInfo().getOrganizationName());
		payerAddress.setSapCustomerID(responseData.getPayingContactInfo().getPartnerId());
		if (StringUtils.isNotBlank(responseData.getPayingContactInfo().getCountry()))
		{
			payerAddress.setCountry(commonI18NService.getCountry(responseData.getPayingContactInfo().getCountry()));
		}
		mapLocationContact(responseData.getPayingContactInfo(), payerAddress);
		addresses.add(payerAddress);
	}

	/**
	 * @param sp
	 * @param responseData
	 * @param b2bUnitModel
	 * @param addresses
	 */
	private void mapCompanyAddress(final AddressType sp, final SeqirusCustomerResponseData responseData,
			final B2BUnitModel b2bUnitModel, final Collection<AddressModel> addresses)
	{
		final AddressModel companyAddress = new AddressModel();
		final Set<String> locationType = new HashSet<String>();
		locationType.add(sp.getCode());
		companyAddress.setContactAddress(Boolean.TRUE);
		companyAddress.setOwner(b2bUnitModel);
		companyAddress.setSapCustomerType(locationType);
		companyAddress.setStreetname(responseData.getCompanyDetails().getAddressLine1());
		companyAddress.setStreetnumber(responseData.getCompanyDetails().getAddressLine2());
		companyAddress.setTown(responseData.getCompanyDetails().getCity());
		companyAddress.setPostalcode(responseData.getCompanyDetails().getPostalCode());
		companyAddress.setSapCustomerID(responseData.getCompanyDetails().getPartnerId());
		companyAddress.setAddressId(responseData.getCompanyDetails().getContactNumber());
		if (StringUtils.isNotBlank(responseData.getCompanyDetails().getCountry()))
		{
			companyAddress.setCountry(commonI18NService.getCountry(responseData.getCompanyDetails().getCountry()));
		}
		mapLocationContact(responseData.getCompanyDetails(), companyAddress);
		addresses.add(companyAddress);
	}

	/**
	 * @param location
	 * @param billToAddress
	 */
	private void mapLocationContact(final SeqirusCustomerLocationResponseData location, final AddressModel address)
	{
		address.setFirstname(location.getContactFirstName());
		address.setLastname(location.getContactLastName());
		address.setEmail(location.getContactEmail());
		address.setJobTitle(location.getContactJobTitle());
		address.setPhone1(location.getContactTelephone());
		address.setPhone2(location.getContactExtension());
		address.setPhone2(location.getContactExtension());
		address.setAddressId(location.getContactNumber());
	}

	/**
	 * @param customerRegistrationData
	 * @param customerTempObject
	 */


	private void populateCustomerModel(final SeqirusCustomerRegistrationData customerRegistrationData,
			final SeqirusCompanyInfoModel customerTempObject, final boolean update)
	{
		try
		{
		if (StringUtils.isNotBlank(customerRegistrationData.getCompanyName()))
		{
			customerTempObject.setCompanyName(customerRegistrationData.getCompanyName());
		}
		if (StringUtils.isNotBlank(customerRegistrationData.getCompanyType()))
		{
			customerTempObject.setCompanyType(customerRegistrationData.getCompanyType());
		}
		if (null != customerRegistrationData.getCompanyRegNumber())
		{
			customerTempObject.setRegistrationNumber(customerRegistrationData.getCompanyRegNumber());
		}
		if (StringUtils.isNotBlank(customerRegistrationData.getBusinessType()))
		{
			customerTempObject.setBusinessType(customerRegistrationData.getBusinessType());
		}
		if (StringUtils.isNotBlank(customerRegistrationData.getVatNumber()))
		{
			customerTempObject.setVatNumber(customerRegistrationData.getVatNumber());
		}

		if (null != customerRegistrationData.getTradingSince())
		{
			customerTempObject.setTradingSince(customerRegistrationData.getTradingSince());
		}
		if (null != customerRegistrationData.getFirstName())
		{
			customerTempObject.setFirstName(customerRegistrationData.getFirstName());
		}
		if (null != customerRegistrationData.getLastName())
		{
			customerTempObject.setLastName(customerRegistrationData.getLastName());
		}
		if (null != customerRegistrationData.getEmail())
		{
			customerTempObject.setEmail(customerRegistrationData.getEmail());
		}
		if (null != customerRegistrationData.getPhoneNumber())
		{
			customerTempObject.setPhoneNumber(customerRegistrationData.getPhoneNumber());
		}
		if (null != customerRegistrationData.getPhoneExt())
		{
			customerTempObject.setPhoneExt(customerRegistrationData.getPhoneExt());
		}
		if (null != customerRegistrationData.getJobTitle())
		{
			customerTempObject.setJobTitle(customerRegistrationData.getJobTitle());
		}
		if (null != customerRegistrationData.getNhcNumber())
		{
			customerTempObject.setNhcNumber(customerRegistrationData.getNhcNumber());
		}
		if (null != customerRegistrationData.getBuildingStreet())
		{
			customerTempObject.setBuildingStreet(customerRegistrationData.getBuildingStreet());
		}
		if (null != customerRegistrationData.getAdditionalStreet())
		{
			customerTempObject.setAdditionalStreet(customerRegistrationData.getAdditionalStreet());
		}
		if (null != customerRegistrationData.getCity())
		{
			customerTempObject.setCity(customerRegistrationData.getCity());
		}
		if (null != customerRegistrationData.getPostCode())
		{
			customerTempObject.setPostCode(customerRegistrationData.getPostCode());
		}
		final CountryModel businessCountry = fetchCountry(customerRegistrationData.getCountry());
		if (null != businessCountry)
		{
			customerTempObject.setCountry(businessCountry);
		}
		if (update)
		{

			customerTempObject.setStatus(TempObjectStatusEnum.CHANGE);
		}
		else
		{
			customerTempObject.setStatus(TempObjectStatusEnum.NEW);
		}
		modelService.save(customerTempObject);
	}
	catch (final Exception e)
	{
		logger.error(":::ExceptionIn-populatecustomermodel::: " + e.getMessage());
		throw new SeqirusCustomException(e);
	}
	}

	/**
	 * @param customer
	 * @param custRegistrationForm
	 */


	private void populatePayingAddressModel(final SeqirusB2BTempCustomerModel customerTempObject,
			final SeqirusCustomerRegistrationData customerRegistrationData, final B2BCustomerModel customer)
	{
		if (null != customerRegistrationData.getPayingContactData())
		{
			try
			{
			final SeqirusCustomerRegistrationAddressData payerAddress = customerRegistrationData.getPayingContactData();
			final CustomerAddressModel payingDetailsModel = modelService.create(CustomerAddressModel.class);
			payingDetailsModel.setOwner(customerTempObject);
			if (null == payerAddress.getAddressID())
			{
				payingDetailsModel.setAddressId(customer.getCustomerID() + "-Paying");
				payingDetailsModel.setStatus(TempObjectStatusEnum.NEW);

			}
			else
			{
				payingDetailsModel.setAddressId(payerAddress.getAddressID());
				payingDetailsModel.setStatus(TempObjectStatusEnum.CHANGE);
			}


			if (null != payerAddress.getCompanyName())
			{
				payingDetailsModel.setCompany(payerAddress.getCompanyName());
			}

			if (null != payerAddress.getEmail())
			{
				payingDetailsModel.setEmail(payerAddress.getEmail());
			}

			if (null != payerAddress.getLine1())
			{
				payingDetailsModel.setBuildingStreet(payerAddress.getLine1());
			}

			if (null != payerAddress.getPhone())
			{
				payingDetailsModel.setPhone1(payerAddress.getPhone());
			}

			if (null != payerAddress.getLine2())
			{
				payingDetailsModel.setAdditionalStreet(payerAddress.getLine2());
			}

			if (null != payerAddress.getPhoneExt())
			{
				payingDetailsModel.setPhoneExt(payerAddress.getPhoneExt().toString());
			}

			if (null != payerAddress.getCity())
			{
				payingDetailsModel.setCity(payerAddress.getCity());
			}

			if (null != payerAddress.getJobTitle())
			{
				payingDetailsModel.setJobTitle(payerAddress.getJobTitle());
			}

			if (null != payerAddress.getFirstName())
			{
				payingDetailsModel.setFirstname(payerAddress.getFirstName());
			}
			if (null != payerAddress.getLastName())
			{
				payingDetailsModel.setLastname(payerAddress.getLastName());
			}
			if (null != payerAddress.getJobTitle())
			{
				payingDetailsModel.setJobTitle(payerAddress.getJobTitle());
			}
			if (null != payerAddress.getCompanyName())
			{
				payingDetailsModel.setOrganizationName(payerAddress.getCompanyName());
			}
			if (null != payerAddress.getPostalCode())
			{
				payingDetailsModel.setZipCode(payerAddress.getPostalCode());
			}
			final CountryModel payerCountry = fetchCountry(payerAddress.getCountry());
			if (null != payerCountry)
			{
				payingDetailsModel.setCountry(payerCountry);
			}

			modelService.save(payingDetailsModel);
			customerTempObject.setPayingAddress(payingDetailsModel);
		}
		catch (final Exception e)
		{
			logger.error(":::ExceptionIn-Payer::: " + e.getMessage());
			throw new SeqirusCustomException(e);
		}
		}
	}

	/**
	 * @param custRegistrationForm
	 * @param customerRegistrationData
	 * @param customer
	 */


	private void populateBillingAddressModel(final SeqirusB2BTempCustomerModel customerTempObject,
			final SeqirusCustomerRegistrationData customerRegistrationData, final B2BCustomerModel customer)
	{
		if (null != customerRegistrationData.getBillingContactData())
		{
			final SeqirusCustomerRegistrationAddressData billingAddress = customerRegistrationData.getBillingContactData();

			try
			{
			final CustomerAddressModel billingDetailsModel;
			if (null == billingAddress.getAddressID())
			{
				billingDetailsModel = modelService.create(CustomerAddressModel.class);
				billingDetailsModel.setAddressId(customer.getCustomerID() + "-Billing");
				billingDetailsModel.setStatus(TempObjectStatusEnum.PENDING);
				billingDetailsModel.setOwner(customerTempObject);

			}
			else
			{
				billingDetailsModel = customerTempObject.getBillingAddress();
			}



			if (null != billingAddress.getCompanyName())
			{
				billingDetailsModel.setCompany(billingAddress.getCompanyName());
			}

			if (null != billingAddress.getEmail())
			{
				billingDetailsModel.setEmail(billingAddress.getEmail());
			}

			if (null != billingAddress.getLine1())
			{
				billingDetailsModel.setBuildingStreet(billingAddress.getLine1());
			}

			if (null != billingAddress.getPhone())
			{
				billingDetailsModel.setPhone1(billingAddress.getPhone());
			}

			if (null != billingAddress.getLine2())
			{
				billingDetailsModel.setAdditionalStreet(billingAddress.getLine2());
			}

			if (null != billingAddress.getPhoneExt())
			{
				billingDetailsModel.setPhoneExt(billingAddress.getPhoneExt().toString());
			}

			if (null != billingAddress.getCity())
			{
				billingDetailsModel.setCity(billingAddress.getCity());
			}

			if (null != billingAddress.getJobTitle())
			{
				billingDetailsModel.setJobTitle(billingAddress.getJobTitle());
			}

			if (null != billingAddress.getPostalCode())
			{
				billingDetailsModel.setZipCode(billingAddress.getPostalCode());
			}

			if (null != billingAddress.getFirstName())
			{
				billingDetailsModel.setFirstname(billingAddress.getFirstName());
			}
			if (null != billingAddress.getLastName())
			{
				billingDetailsModel.setLastname(billingAddress.getLastName());
			}
			if (null != billingAddress.getJobTitle())
			{
				billingDetailsModel.setJobTitle(billingAddress.getJobTitle());
			}
			if (null != billingAddress.getCompanyName())
			{
				billingDetailsModel.setOrganizationName(billingAddress.getCompanyName());
			}
			final CountryModel billingCountry = fetchCountry(billingAddress.getCountry());
			if (null != billingCountry)
			{
				billingDetailsModel.setCountry(billingCountry);
			}


			modelService.save(billingDetailsModel);
			customerTempObject.setBillingAddress(billingDetailsModel);
		}
		catch (final Exception e)
		{
			logger.error(":::ExceptionIn-Billing::: " + e.getMessage());
			throw new SeqirusCustomException(e);
		}
	}
	}

	/**
	 * @param custRegistrationForm
	 * @param customerRegistrationData
	 * @param customer
	 */


	private void populateinvoicingAddressModel(final SeqirusB2BTempCustomerModel customerTempObject,
			final SeqirusCustomerRegistrationData customerRegistrationData, final B2BCustomerModel customer)
	{
		if (null != customerRegistrationData.getInvoiceContractData())
		{
			final SeqirusCustomerRegistrationAddressData invoiceContract = customerRegistrationData.getInvoiceContractData();
			try {
			final CustomerAddressModel invoicingContractModel = modelService.create(CustomerAddressModel.class);
			invoicingContractModel.setOwner(customerTempObject);

			if (null == invoiceContract.getAddressID())
			{

				invoicingContractModel.setAddressId(customer.getCustomerID() + "-Invoicing");
				invoicingContractModel.setStatus(TempObjectStatusEnum.NEW);

			}
			else
			{
				invoicingContractModel.setAddressId(invoiceContract.getAddressID());
				invoicingContractModel.setStatus(TempObjectStatusEnum.CHANGE);
			}


			if (null != invoiceContract.getCompanyName())
			{
				invoicingContractModel.setCompany(invoiceContract.getCompanyName());
			}

			if (null != invoiceContract.getEmail())
			{
				invoicingContractModel.setEmail(invoiceContract.getEmail());
			}

			if (null != invoiceContract.getLine1())
			{
				invoicingContractModel.setBuildingStreet(invoiceContract.getLine1());
			}

			if (null != invoiceContract.getPhone())
			{
				invoicingContractModel.setPhone1(invoiceContract.getPhone().toString());
			}

			if (null != invoiceContract.getLine2())
			{
				invoicingContractModel.setAdditionalStreet(invoiceContract.getLine2());
			}

			if (null != invoiceContract.getPhoneExt())
			{
				invoicingContractModel.setPhoneExt(invoiceContract.getPhoneExt().toString());
			}

			if (null != invoiceContract.getCity())
			{
				invoicingContractModel.setCity(invoiceContract.getCity());
			}

			if (null != invoiceContract.getJobTitle())
			{
				invoicingContractModel.setJobTitle(invoiceContract.getJobTitle());
			}

			if (null != invoiceContract.getPostalCode())
			{
				invoicingContractModel.setZipCode(invoiceContract.getPostalCode());
			}

			if (null != invoiceContract.getAdditionalEmail())
			{
				invoicingContractModel.setOptionalEmail(invoiceContract.getAdditionalEmail());
			}
			if (null != invoiceContract.getFirstName())
			{
				invoicingContractModel.setFirstname(invoiceContract.getFirstName());
			}
			if (null != invoiceContract.getLastName())
			{
				invoicingContractModel.setLastname(invoiceContract.getLastName());
			}
			if (null != invoiceContract.getJobTitle())
			{
				invoicingContractModel.setJobTitle(invoiceContract.getJobTitle());
			}
			if (null != invoiceContract.getCompanyName())
			{
				invoicingContractModel.setOrganizationName(invoiceContract.getCompanyName());
			}
			final CountryModel invoiceCountry = fetchCountry(invoiceContract.getCountry());
			if (null != invoiceCountry)
			{
				invoicingContractModel.setCountry(invoiceCountry);
			}

			modelService.save(invoicingContractModel);
			customerTempObject.setInvoiceAddress(invoicingContractModel);
		}
		catch (final Exception e)
		{
			logger.error(":::ExceptionIn-Invoice::: " + e.getMessage());
			throw new SeqirusCustomException(e);
		}
		}
	}


	/**
	 * @param custRegistrationForm
	 * @param customerRegistrationData
	 * @param customer
	 */
	private void populateShippingAddressModel(final SeqirusB2BTempCustomerModel customerTempObject,
			final SeqirusCustomerRegistrationData customerRegistrationData, final B2BCustomerModel customer,
			final boolean updateShipping)
	{
		final List<CustomerAddressModel> shippingLocModelList = new ArrayList<CustomerAddressModel>();
		try {
		if (updateShipping)
		{
			for (final CustomerAddressModel shippingModel : customerTempObject.getShippingAddress())
			{
				for (final SeqirusCustomerRegistrationAddressData shippingAddress : customerRegistrationData
						.getShippingLocationsData())
				{
				shippingLocModelList.add(shippingModel);
				if (shippingModel.getAddressId().equals(shippingAddress.getAddressID()))
				{
					shippingModel.setStatus(TempObjectStatusEnum.CLOSED);
				}
				}
			}

		}
		int count = 0;
		if (null != customerRegistrationData.getShippingLocationsData())
		{

			final List<SeqirusCustomerRegistrationAddressData> shippingAddressList = customerRegistrationData
					.getShippingLocationsData();

			for (final SeqirusCustomerRegistrationAddressData shippingAddress : shippingAddressList)
			{
				count++;

				final CustomerAddressModel shippingLocationsModel = modelService.create(CustomerAddressModel.class);
				if (null != shippingAddress.getAddressID())
				{
					shippingLocationsModel.setAddressId(shippingAddress.getAddressID());
					shippingLocationsModel.setStatus(TempObjectStatusEnum.CHANGE);

				}
				else
				{
					shippingLocationsModel.setAddressId(customer.getCustomerID() + "-Shipping" + count);
					shippingLocationsModel.setStatus(TempObjectStatusEnum.NEW);
				}
				setShippingData(shippingAddress, shippingLocationsModel);
				shippingLocationsModel.setOwner(customerTempObject);
				modelService.save(shippingLocationsModel);
				shippingLocModelList.add(shippingLocationsModel);


			}
			customerTempObject.setShippingAddress(shippingLocModelList);
		}
	}
	catch (final Exception e)
	{
		logger.error(":::ExceptionIn-Shipping::: " + e.getMessage());
		throw new SeqirusCustomException(e);
	}
}

	/**
	 * @param shippingAddress
	 * @param shippingLocationsModel
	 */
	private void setShippingData(final SeqirusCustomerRegistrationAddressData shippingAddress,
			final CustomerAddressModel shippingLocationsModel)
	{
		if (null != shippingAddress.getLine1())
		{
			shippingLocationsModel.setBuildingStreet(shippingAddress.getLine1());
		}

		if (null != shippingAddress.getLine2())
		{
			shippingLocationsModel.setAdditionalStreet(shippingAddress.getLine2());
		}

		if (null != shippingAddress.getCity())
		{
			shippingLocationsModel.setCity(shippingAddress.getCity());
		}

		if (null != shippingAddress.getPostalCode())
		{
			shippingLocationsModel.setZipCode(shippingAddress.getPostalCode());
		}
		if (null != shippingAddress.getCompanyName())
		{
			shippingLocationsModel.setOrganizationName(shippingAddress.getCompanyName());
		}
		if (null != shippingAddress.getFirstName())
		{
			shippingLocationsModel.setFirstname(shippingAddress.getFirstName());
		}
		if (null != shippingAddress.getLastName())
		{
			shippingLocationsModel.setLastname(shippingAddress.getLastName());
		}
		if (null != shippingAddress.getEmail())
		{
			shippingLocationsModel.setEmail(shippingAddress.getEmail());
		}
		if (null != shippingAddress.getPhone())
		{
			shippingLocationsModel.setPhone1(shippingAddress.getPhone());
		}
		if (null != shippingAddress.getCellphone())
		{
			shippingLocationsModel.setPhone2(shippingAddress.getCellphone());
		}
		if (null != shippingAddress.getPhoneExt())
		{
			shippingLocationsModel.setPhoneExt(shippingAddress.getPhoneExt().toString());
		}
		if (null != shippingAddress.getLicenseNumber())
		{
			shippingLocationsModel.setLicenseNumber(shippingAddress.getLicenseNumber());
		}
		if (null != shippingAddress.getLicenseName())
		{
			shippingLocationsModel.setLicenseName(shippingAddress.getLicenseName());
		}
		if (null != shippingAddress.getNhsNumber())
		{
			shippingLocationsModel.setNhsCode(shippingAddress.getNhsNumber());
		}
		final CountryModel shipCountry = fetchCountry(shippingAddress.getCountry());
		if (null != shipCountry)
		{
			shippingLocationsModel.setCountry(shipCountry);
		}
	}

	@Override
	public B2BUnitModel getOrgNameByAccount(final String account)

	{
		if (null != account)

		{

			return seqirusCustomerRegistrationService.getOrgNameByB2BUnit(account);

		}
		return null;


	}

	/**
	 * @return the seqirusCustomerRegistrationService
	 */
	protected SeqirusCustomerRegistrationService getSeqirusCustomerRegistrationService()
	{
		return seqirusCustomerRegistrationService;
	}

	/**
	 * @param seqirusCustomerRegistrationService
	 *           the seqirusCustomerRegistrationService to set
	 */
	@Required
	public void setSeqirusCustomerRegistrationService(final SeqirusCustomerRegistrationService seqirusCustomerRegistrationService)
	{
		this.seqirusCustomerRegistrationService = seqirusCustomerRegistrationService;
	}


	/*
	 * protected <T extends SeqirusCustomerRegistrationService> T getSeqirusCustomerRegistrationService() { return (T)
	 * seqirusCustomerRegistrationService; }
	 *
	 * @Required public void setSeqirusCustomerRegistrationService(final SeqirusCustomerRegistrationService
	 * seqirusCustomerRegistrationService) { this.seqirusCustomerRegistrationService =
	 * seqirusCustomerRegistrationService; }
	 */
	public CountryModel fetchCountry(final CountryData countryData)
	{
		if (null != countryData && StringUtils.isNotBlank(countryData.getName()))
		{
			final CountryModel countryModelByName = seqirusCustomerRegistrationService.getCountryByName(countryData.getName());
			if (null != countryModelByName)
			{
				return commonI18NService.getCountry(countryModelByName.getIsocode());
			}
		}
		return null;
	}

	@Override
	public SeqirusCustomerRegistrationData fetchCustData(final B2BCustomerModel customer, final boolean isMyLoc)
	{
		final SeqirusB2BTempCustomerModel customerDataMoel = seqirusCustomerRegistrationService.fetchCustModel(customer.getUid());

		final SeqirusCustomerRegistrationData customerData = new SeqirusCustomerRegistrationData();
		if (null != customerDataMoel.getCompanyInfo().getStatus()
				&& !(customerDataMoel.getCompanyInfo().getStatus().equals(TempObjectStatusEnum.NEW)
						|| customerDataMoel.getCompanyInfo().getStatus().equals(TempObjectStatusEnum.CHANGE))
				&& null != customer.getDefaultB2BUnit() && null != customer.getDefaultB2BUnit().getCompanyName() && !isMyLoc)
		{
			getSeqirusCustomerB2BTempDataPopulator().populate(customer.getDefaultB2BUnit(), customerData);
			return customerData;
		}
		else
		{

			if (null != customerDataMoel)
			{

				getSeqirusCustomerB2BTempDataPopulator().populate(customerDataMoel, customerData);
				return customerData;
			}
		}
		return null;

	}

	/**
	 * @return the seqirusCustomerB2BTempDataPopulator
	 */
	public SeqirusCustomerB2BTempDataPopulator getSeqirusCustomerB2BTempDataPopulator()
	{
		return seqirusCustomerB2BTempDataPopulator;
	}

	/**
	 * @param seqirusCustomerB2BTempDataPopulator
	 *           the seqirusCustomerB2BTempDataPopulator to set
	 */
	public void setSeqirusCustomerB2BTempDataPopulator(
			final SeqirusCustomerB2BTempDataPopulator seqirusCustomerB2BTempDataPopulator)
	{
		this.seqirusCustomerB2BTempDataPopulator = seqirusCustomerB2BTempDataPopulator;
	}

	@Override
	public SeqirusCustomerRegistrationData updateProfile(final SeqirusCustomerRegistrationData customerRegistrationData)
	{

		final UserModel currentUser = userService.getCurrentUser();
		final B2BCustomerModel customer = (B2BCustomerModel) currentUser;

		final SeqirusB2BTempCustomerModel customerDataMoel = seqirusCustomerRegistrationService.fetchCustModel(customer.getUid());

		if (null != customerRegistrationData.getUpdateCustInfoFlag()
				&& customerRegistrationData.getUpdateCustInfoFlag().equals("true"))
		{
			final SeqirusCompanyInfoModel businessDetailsModel = modelService.create(SeqirusCompanyInfoModel.class);
			populateCustomerModel(customerRegistrationData, businessDetailsModel, true);
			customerDataMoel.setCompanyInfo(businessDetailsModel);
			modelService.save(customerDataMoel);

		}

		if (null != customerRegistrationData.getPayingContactData())
		{
			populatePayingAddressModel(customerDataMoel, customerRegistrationData, customer);
			modelService.save(customerDataMoel);
		}

		if (null != customerRegistrationData.getInvoiceContractData())
		{
			populateinvoicingAddressModel(customerDataMoel, customerRegistrationData, customer);
			modelService.save(customerDataMoel);
		}

		if (null != customerRegistrationData.getShippingLocationsData()
				&& !customerRegistrationData.getShippingLocationsData().isEmpty())
		{
			populateShippingAddressModel(customerDataMoel, customerRegistrationData, customer, true);
			modelService.save(customerDataMoel);
		}
		final B2BRegistrationModel regModel = new B2BRegistrationModel();
		getSeqirusB2BRegistrationWFDataPopulator().populate(customerRegistrationData, regModel);
		sendUpdateRegistrationEmail();
		return null;

	}

	@Override
	public void sendEmail(final UserModel currentUser)
	{
		try
		{
			eventService.publishEvent(initializeEvent(new SeqirusRegisterEvent(), currentUser));
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}
	}

	public void sendRegistrationEmail()
	{
		try
		{
			eventService
					.publishEvent(initializeRegistrationEvent(new SeqirusCustomerRegistrationEvent(), userService.getCurrentUser()));
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void sendUpdateRegistrationEmail()
	{
		try
		{
			eventService.publishEvent(
					initializeUpdateRegistrationEvent(new SeqirusCustomerUpdateRegistrationEvent(), userService.getCurrentUser()));
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}
	}

	private RegisterEvent initializeRegistrationEvent(final SeqirusCustomerRegistrationEvent event, final UserModel user)
	{
		event.setBaseStore(baseStoreService.getCurrentBaseStore());
		event.setSite(baseSiteService.getCurrentBaseSite());
		event.setCustomer((B2BCustomerModel) user);
		if (null != getAgentDetails())
		{
			event.setEmployee((EmployeeModel) getAgentDetails());
		}
		event.setLanguage(commonI18NService.getCurrentLanguage());
		event.setCurrency(commonI18NService.getCurrentCurrency());
		return event;
	}

	private RegisterEvent initializeUpdateRegistrationEvent(final SeqirusCustomerUpdateRegistrationEvent event,
			final UserModel user)
	{
		event.setBaseStore(baseStoreService.getCurrentBaseStore());
		event.setSite(baseSiteService.getCurrentBaseSite());
		event.setCustomer((B2BCustomerModel) user);
		if (null != getAgentDetails())
		{
			event.setEmployee((EmployeeModel) getAgentDetails());
		}
		event.setLanguage(commonI18NService.getCurrentLanguage());
		event.setCurrency(commonI18NService.getCurrentCurrency());
		return event;
	}

	private RegisterEvent initializeEvent(final SeqirusRegisterEvent event, final UserModel user)
	{
		event.setBaseStore(baseStoreService.getCurrentBaseStore());
		event.setSite(baseSiteService.getCurrentBaseSite());
		event.setCustomer((B2BCustomerModel) user);
		if (null != getAgentDetails())
		{
			event.setEmployee((EmployeeModel) getAgentDetails());
		}
		event.setLanguage(commonI18NService.getCurrentLanguage());
		event.setCurrency(commonI18NService.getCurrentCurrency());
		return event;
	}

	private UserModel getAgentDetails()
	{
		final AssistedServiceSession asmSession = assistedServiceFacade.getAsmSession();
		if (null != asmSession)
		{
			return asmSession.getAgent();
		}
		return null;
	}

	@Override
	public List<ConsentData> getConsentsForCustomer(final CustomerModel customer)
	{
		final List<ConsentModel> allConstModel = seqirusCustomerRegistrationService.fetchConstModel(customer);
		final List<ConsentData> allConstData = new ArrayList<ConsentData>();
		for (final ConsentModel constModel : allConstModel)
		{
			final ConsentData consentData = new ConsentData();
			seqirusConsentPopulator.populate(constModel, consentData);
			allConstData.add(consentData);
		}
		return allConstData;
	}

	/**
	 * @return the seqirusB2BRegistrationWFDataPopulator
	 */
	public SeqirusB2BRegistrationWFDataPopulator getSeqirusB2BRegistrationWFDataPopulator()
	{
		return seqirusB2BRegistrationWFDataPopulator;
	}

	/**
	 * @param seqirusB2BRegistrationWFDataPopulator
	 *           the seqirusB2BRegistrationWFDataPopulator to set
	 */
	public void setSeqirusB2BRegistrationWFDataPopulator(
			final SeqirusB2BRegistrationWFDataPopulator seqirusB2BRegistrationWFDataPopulator)
	{
		this.seqirusB2BRegistrationWFDataPopulator = seqirusB2BRegistrationWFDataPopulator;
	}

	@Override
	public SeqirusCustomerResponseData getCustomerData(final String accountNumber, final String zipCode)
	{
		final SeqirusCustomerResponseData customerResponseData = new SeqirusCustomerResponseData();
		final JoinAccountAPIResponse apiResponse = seqirusCustomerRegistrationService.getCustomerData(accountNumber, zipCode);
		seqirusJoinAccountResponsePopulator.populate(apiResponse, customerResponseData);
		return customerResponseData;
	}

}
