/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.uk.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.seqirus.uk.fulfilmentprocess.constants.Seqirusb2bFulfilmentProcessConstants;

public class Seqirusb2bFulfilmentProcessManager extends GeneratedSeqirusb2bFulfilmentProcessManager
{
	public static final Seqirusb2bFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (Seqirusb2bFulfilmentProcessManager) em.getExtension(Seqirusb2bFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
