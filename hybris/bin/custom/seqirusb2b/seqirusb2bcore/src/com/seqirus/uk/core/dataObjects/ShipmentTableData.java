package com.seqirus.uk.core.dataObjects;




public class ShipmentTableData
{

	public String orders;
	public String location;
	public String status;
	public String delivery;
	public String viewdetails;
	/**
	 * @return the orders
	 */
	public String getOrders()
	{
		return orders;
	}
	/**
	 * @param orders the orders to set
	 */
	public void setOrders(String orders)
	{
		this.orders = orders;
	}
	/**
	 * @return the location
	 */
	public String getLocation()
	{
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location)
	{
		this.location = location;
	}
	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status)
	{
		this.status = status;
	}
	/**
	 * @return the delivery
	 */
	public String getDelivery()
	{
		return delivery;
	}
	/**
	 * @param delivery the delivery to set
	 */
	public void setDelivery(String delivery)
	{
		this.delivery = delivery;
	}
	/**
	 * @return the viewdetails
	 */
	public String getViewdetails()
	{
		return viewdetails;
	}
	/**
	 * @param viewdetails the viewdetails to set
	 */
	public void setViewdetails(String viewdetails)
	{
		this.viewdetails = viewdetails;
	}
	
}

