package com.seqirus.core.dataimport.decorator;

import de.hybris.platform.impex.jalo.header.AbstractImpExCSVCellDecorator;

import java.util.Map;

import org.apache.commons.lang.StringUtils;


/**
 * class to decorate Email value i.e ignoring the case of email value.
 *
 */
public class SeqirusEmailValueDecorator extends AbstractImpExCSVCellDecorator
{
	/**
	 * method to decorate Email value i.e converting to lower case of email value.
	 */
	@Override
	public String decorate(final int pos, final Map<Integer, String> srcLine)
	{
		final String emailString = srcLine.get(Integer.valueOf(pos));
		String modifiedValue = emailString;

		if (!StringUtils.isAllLowerCase(emailString))
		{
			modifiedValue = StringUtils.lowerCase(emailString);
		}
		return modifiedValue;
	}

}
