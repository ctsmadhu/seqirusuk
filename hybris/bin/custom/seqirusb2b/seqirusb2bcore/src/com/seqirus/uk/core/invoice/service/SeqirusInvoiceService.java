/**
 *
 */
package com.seqirus.uk.core.invoice.service;

import java.io.InputStream;

import com.seqirus.uk.core.dataObjects.InvoiceDetailsResponse;
import com.seqirus.uk.core.dataObjects.InvoiceLandingRequest;
import com.seqirus.uk.core.dataObjects.InvoiceLandingResponse;
import com.seqirus.uk.core.dataObjects.ReturnsAndCreditsRequest;
import com.seqirus.uk.core.dataObjects.ReturnsAndCreditsResponse;
import com.seqirus.uk.core.exceptions.SeqirusCustomException;


/**
 * The Interface SeqirusInvoiceService.
 *
 * @author 700196
 */
public interface SeqirusInvoiceService
{

	/**
	 * Retrieve invoices list.
	 *
	 * @param target
	 *           the target
	 * @return InvoiceLandingResponse
	 */
	InvoiceLandingResponse retrieveInvoicesList(InvoiceLandingRequest target);

	/**
	 * Retrieve invoice details.
	 *
	 * @param invoiceNumber
	 *           the invoice number
	 * @return InvoiceDetailsResponse
	 */
	InvoiceDetailsResponse retrieveInvoiceDetails(String invoiceNumber);

	/**
	 * Download invoice.
	 *
	 * @param invoiceNumber
	 *           the invoice number
	 * @return the input stream
	 */
	InputStream downloadInvoice(String invoiceNumber);

	/**
	 * Gets the returns and credits list.
	 *
	 * @param request
	 *           the request
	 * @return the returns and credits list
	 * @throws SeqirusCustomException
	 *            the seqirus custom exception
	 */
	ReturnsAndCreditsResponse getReturnsAndCreditsList(ReturnsAndCreditsRequest request) throws SeqirusCustomException;

}
