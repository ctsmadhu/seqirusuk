package com.seqirus.core.dataimport.decorator;

import de.hybris.platform.impex.jalo.header.AbstractImpExCSVCellDecorator;

import java.util.Map;

import org.apache.commons.lang.StringUtils;


/**
 * class to decorate region value i.e cleaning the region value from other data.
 *
 */
public class SeqirusRegionValueDecorator extends AbstractImpExCSVCellDecorator
{

	/**
	 * method to decorate region value i.e cleaning the region value from other data.
	 */
	@Override
	public String decorate(final int pos, final Map<Integer, String> srcLine)
	{
		String modifiedValue = StringUtils.EMPTY;
		final String regionString = srcLine.get(Integer.valueOf(pos));

		if (StringUtils.contains(regionString, "/") && !StringUtils.endsWith(regionString.trim(), "/"))
		{
			modifiedValue = StringUtils.replace(regionString, "/", "-");
		}
		return modifiedValue;
	}

}
