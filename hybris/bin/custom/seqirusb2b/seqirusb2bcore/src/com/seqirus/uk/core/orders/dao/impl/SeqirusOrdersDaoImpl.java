/**
 *
 */
package com.seqirus.uk.core.orders.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import javax.annotation.Resource;

import com.seqirus.core.model.SeasonEntryModel;

/**
 * @author 614269
 *
 */
public class SeqirusOrdersDaoImpl implements SeqirusOrdersDao
{
	@Resource
	FlexibleSearchService flexibleSearchService;


	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}


	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}


	@Override
	public SeasonEntryModel seasonEntry()
	{

		final StringBuilder buildQuery = new StringBuilder();
		buildQuery.append("SELECT { ").append(SeasonEntryModel.PK).append(" } FROM { ").append(SeasonEntryModel._TYPECODE)
				.append("}");
		final SearchResult<SeasonEntryModel> result = getFlexibleSearchService()
				.search(new FlexibleSearchQuery(buildQuery.toString()));

		return result.getResult().get(0);
	}

}
