/**
 *
 */
package com.seqirus.uk.core.exceptions;

/**
 * @author 700196
 *
 */
public class SeqirusCustomException extends RuntimeException
{

	/**
	 * The ws message.
	 *
	 */

	String wsMessage;//NOSONAR

	/**
	 * Instantiates a new SeqirusCustomExceptionextends.
	 *
	 * @param message
	 *           the message
	 */
	public SeqirusCustomException(final String message)
	{
		super(message);
	}

	/**
	 * Instantiates a new SeqirusCustomExceptionextends.
	 *
	 * @param e
	 *           the e
	 */
	public SeqirusCustomException(final Exception e)
	{
		super(e);

	}

	/**
	 * Gets the ws message.
	 *
	 * @return the wsMessage
	 */
	public String getWsMessage()
	{
		return wsMessage;
	}

	/**
	 * Sets the ws message.
	 *
	 * @param wsMessage
	 *           the wsMessage to set
	 */
	public void setWsMessage(final String wsMessage)
	{
		this.wsMessage = wsMessage;
	}
}
