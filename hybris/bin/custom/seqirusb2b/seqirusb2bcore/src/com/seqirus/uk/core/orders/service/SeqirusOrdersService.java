/**
 *
 */
package com.seqirus.uk.core.orders.service;

import java.util.List;

import com.seqirus.core.model.SeasonEntryModel;
import com.seqirus.uk.core.dataObjects.OrderSummary;

/**
 * @author 614269
 *
 */
public interface SeqirusOrdersService
{

	/**
	 * @param customerId
	 * @param season
	 * @return
	 */
	List<OrderSummary> getOrders(String customerId, String season);

	/**
	 * @return
	 */
	SeasonEntryModel getSeasonEntry();
}
