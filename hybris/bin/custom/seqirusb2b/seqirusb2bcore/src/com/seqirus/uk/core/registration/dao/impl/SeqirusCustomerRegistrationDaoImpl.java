/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.uk.core.registration.dao.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import javax.annotation.Resource;

import com.seqirus.core.model.SeasonEntryModel;
import com.seqirus.core.model.SeqirusB2BTempCustomerModel;
import com.seqirus.uk.core.registration.dao.SeqirusCustomerRegistrationDao;


public class SeqirusCustomerRegistrationDaoImpl implements SeqirusCustomerRegistrationDao
{

	@Resource
	FlexibleSearchService flexibleSearchService;



	private static final String GET_ACCOUNT_QUERY = "SELECT {b.pk} FROM {B2BUnit as b} where {uid} = ?account";
	private static final String GET_COUNTRYBYNAME_QUERY = "SELECT {pk} FROM {Country} where UPPER({name})Like UPPER(?countryName)";
	private static final String GET_CUSTOMER_QUERY = "SELECT {pk} FROM {SeqirusB2BTempCustomer} where {userId} = ?uid";

	//private static final String GET_ACCOUNT_QUERY = "SELECT {pk} FROM {B2BUnit} where {uid} = ?account AND {name}=?name";
	private static final String ACCOUNT = "account";
	private static final String GET_ALL_CUSTOMERS = "SELECT {C:PK} from {B2BCustomer AS C} ";
	private static final String GET_WELCOMEEMAIL_CUSTOMERS = "SELECT {C:PK} from {B2BCustomer AS C} where {welcomeCustomerSoldToId}!='' ";

	@Override
	public B2BUnitModel getPartnerOrgByB2BUnit(final String account)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(GET_ACCOUNT_QUERY);

		fQuery.addQueryParameter("account", account);
		//fQuery.setResultClassList(Collections.singletonList(B2BUnitModel.class));
		//System.out.println("account value in daoimpl" + account);
		final SearchResult<B2BUnitModel> resultList = getFlexibleSearchService().search(fQuery);
		if (null != resultList)
		{

			/*
			 * System.out.println("account value inside method of daoimpl " + account);
			 * System.out.println("getcount value inside method of daoimpl " + resultList.getCount());
			 * System.out.println("getResult value inside method of daoimpl " + resultList.getResult());
			 */
			final List<B2BUnitModel> result = resultList.getResult();
			if (null != result && !result.isEmpty())
			{
				return result.get(0);
			}

		}
		return null;
	}


	@Override
	public CountryModel getCountryByName(final String countryName)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(GET_COUNTRYBYNAME_QUERY);
		fQuery.addQueryParameter("countryName", countryName);
		final SearchResult<CountryModel> resultList = getFlexibleSearchService().search(fQuery);
		if (null != resultList)
		{
			final List<CountryModel> result = resultList.getResult();
			if (null != result && !result.isEmpty())
			{
				return result.get(0);
			}
		}
		return null;
	}


	@Override
	public SeqirusB2BTempCustomerModel fetchCustModel(final String uid)
	{

		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(GET_CUSTOMER_QUERY);

		fQuery.addQueryParameter("uid", uid);

		final SearchResult<SeqirusB2BTempCustomerModel> resultList = getFlexibleSearchService().search(fQuery);
		//List<SeqirusB2BTempCustomerModel> customerModel = new ArrayList<SeqirusB2BTempCustomerModel>();
		if (null != resultList)
		{
			final List<SeqirusB2BTempCustomerModel> result = resultList.getResult();
			return result.isEmpty() ? null : result.get(0);


		}
		return null;
	}

	@Override
	public SeasonEntryModel seasonEntry()
	{

		final StringBuilder buildQuery = new StringBuilder();
		buildQuery.append("SELECT { ").append(SeasonEntryModel.PK).append(" } FROM { ").append(SeasonEntryModel._TYPECODE)
				.append("}");
		final SearchResult<SeasonEntryModel> result = getFlexibleSearchService()
				.search(new FlexibleSearchQuery(buildQuery.toString()));

		return result.getResult().get(0);
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	@Override
	public List<B2BCustomerModel> getAllCustomers()
	{
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(GET_ALL_CUSTOMERS);

		final SearchResult<B2BCustomerModel> searchResult = flexibleSearchService.search(searchQuery);

		return searchResult.getResult();

	}


	@Override
	public List<B2BCustomerModel> getWelcomeEmailCustomers()
	{
		List<B2BCustomerModel> welcomeEmailCustomers = null;
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(GET_WELCOMEEMAIL_CUSTOMERS);
		final SearchResult<B2BCustomerModel> searchResult = flexibleSearchService.search(searchQuery);
		if (null != searchResult)
		{
			welcomeEmailCustomers = searchResult.getResult();
			return welcomeEmailCustomers.isEmpty() ? null : welcomeEmailCustomers;
		}
		return welcomeEmailCustomers;
	}
}
