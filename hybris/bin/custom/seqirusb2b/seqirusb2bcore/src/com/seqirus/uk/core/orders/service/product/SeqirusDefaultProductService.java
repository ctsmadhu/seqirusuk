/**
 *
 */
package com.seqirus.uk.core.orders.service.product;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.ProductDao;
import de.hybris.platform.product.impl.DefaultProductService;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author 614269
 *
 */
public class SeqirusDefaultProductService extends DefaultProductService implements SeqirusProductService
{
	@Autowired
	private ProductDao productDao;

	@Override
	public ProductModel getProductDataForCode(final String code)
	{
		ProductModel productModel = null;
		final List<ProductModel> products = productDao.findProductsByCode(code);
		if (CollectionUtils.isNotEmpty(products))
		{
			productModel = products.get(0);
		}
		return productModel;
	}

}
