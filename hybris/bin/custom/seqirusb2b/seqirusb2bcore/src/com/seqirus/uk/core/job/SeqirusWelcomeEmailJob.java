/**
 *
 */
package com.seqirus.uk.core.job;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.seqirus.core.model.WelcomeEmailtoCustomersCronJobModel;
import com.seqirus.uk.core.event.WelcomeEmailToCustomerEvent;
import com.seqirus.uk.core.registration.service.SeqirusCustomerRegistrationService;


/**
 * @author 700196
 *
 */
public class SeqirusWelcomeEmailJob extends AbstractJobPerformable<WelcomeEmailtoCustomersCronJobModel>
{
	private static final Logger LOG = Logger.getLogger(SeqirusWelcomeEmailJob.class);
	private SeqirusCustomerRegistrationService seqirusCustomerRegistrationService;

	@Autowired
	private EventService eventService;

	@Autowired
	private ModelService modelService;

	@Override
	public PerformResult perform(final WelcomeEmailtoCustomersCronJobModel cronJob)
	{
		try
		{
			final List<B2BCustomerModel> welcomeEmailCustomers = seqirusCustomerRegistrationService
					.getWelcomeEmailCustomers();
			if (null != welcomeEmailCustomers && CollectionUtils.isNotEmpty(welcomeEmailCustomers))
			{
				LOG.info("WelcomeCustomerList..." + welcomeEmailCustomers.size());
				for (final B2BCustomerModel customersModel : welcomeEmailCustomers)
				{
					if (null != customersModel)
					{
						eventService.publishEvent(initializeEvent(new WelcomeEmailToCustomerEvent(), customersModel, cronJob));
					}
				}
			}
			else
			{
				LOG.info("WelcomeCustomersLis is empty");
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception occured : ", e);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}
		LOG.info("Welcome Email Action Completed");
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	/**
	 * @param welcomeEmailToCustomersEvent
	 * @param seqirusExistingCustomersModel
	 * @param cronJob
	 * @return
	 */
	private AbstractEvent initializeEvent(final WelcomeEmailToCustomerEvent event,
			final B2BCustomerModel customer, final WelcomeEmailtoCustomersCronJobModel cronJob)
	{
		event.setBaseStore(cronJob.getBaseStore());
		event.setSite(cronJob.getCmsSite());
		event.setCustomer(customer);
		event.setLanguage(cronJob.getSessionLanguage());
		event.setCurrency(cronJob.getSessionCurrency());
		return event;
	}

	/**
	 * @return the seqirusCustomerRegistrationService
	 */
	protected SeqirusCustomerRegistrationService getSeqirusCustomerRegistrationService()
	{
		return seqirusCustomerRegistrationService;
	}

	/**
	 * @param seqirusCustomerRegistrationService
	 *           the seqirusCustomerRegistrationService to set
	 */

	public void setSeqirusCustomerRegistrationService(final SeqirusCustomerRegistrationService seqirusCustomerRegistrationService)
	{
		this.seqirusCustomerRegistrationService = seqirusCustomerRegistrationService;
	}
}
