/**
 *
 */
package com.seqirus.uk.core.dataObjects;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author 700196
 *
 */
public class JoinAccountAPILocation
{
	@JsonProperty(value = "PartnerID")
	public String partnerId;
	@JsonProperty(value = "OrgName")
	public String orgName;
	@JsonProperty(value = "AddressLine_1")
	public String addressLine1;
	@JsonProperty(value = "AddressLine_2")
	public String addressLine2;
	@JsonProperty(value = "City")
	public String city;
	@JsonProperty(value = "State")
	public String state;
	@JsonProperty(value = "ZipCode")
	public String zipCode;
	@JsonProperty(value = "Country")
	public String country;
	@JsonProperty(value = "PartnerFunction")
	public String partnerFunction;
	@JsonProperty(value = "NHSCode")
	public String nhsCode;
	@JsonProperty(value = "OrgRegistrationNumber")
	public String registrationNumber;
	@JsonProperty(value = "VATNumber")
	public String vatNumber;
	@JsonProperty(value = "BusinessType")
	public String businessType;
	@JsonProperty(value = "SoldToContact")
	public JoinAccountLocationContact soldToContact;
	@JsonProperty(value = "BillToContact")
	public JoinAccountLocationContact billToContact;
	@JsonProperty(value = "PayerContact")
	public JoinAccountLocationContact payerContact;
	@JsonProperty(value = "ShippingContact")
	public List<JoinAccountLocationContact> shipToContact;
	@JsonProperty(value = "License")
	public JoinAccountAPIShipLicense shipToLicense;

	/**
	 * @return the partnerId
	 */
	public String getPartnerId()
	{
		return partnerId;
	}

	/**
	 * @param partnerId
	 *           the partnerId to set
	 */
	public void setPartnerId(final String partnerId)
	{
		this.partnerId = partnerId;
	}

	/**
	 * @return the orgName
	 */
	public String getOrgName()
	{
		return orgName;
	}

	/**
	 * @param orgName
	 *           the orgName to set
	 */
	public void setOrgName(final String orgName)
	{
		this.orgName = orgName;
	}



	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1()
	{
		return addressLine1;
	}

	/**
	 * @param addressLine1
	 *           the addressLine1 to set
	 */
	public void setAddressLine1(final String addressLine1)
	{
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2()
	{
		return addressLine2;
	}

	/**
	 * @param addressLine2
	 *           the addressLine2 to set
	 */
	public void setAddressLine2(final String addressLine2)
	{
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *           the city to set
	 */
	public void setCity(final String city)
	{
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * @param state
	 *           the state to set
	 */
	public void setState(final String state)
	{
		this.state = state;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode()
	{
		return zipCode;
	}

	/**
	 * @param zipCode
	 *           the zipCode to set
	 */
	public void setZipCode(final String zipCode)
	{
		this.zipCode = zipCode;
	}



	/**
	 * @return the country
	 */
	public String getCountry()
	{
		return country;
	}

	/**
	 * @param country
	 *           the country to set
	 */
	public void setCountry(final String country)
	{
		this.country = country;
	}

	/**
	 * @return the partnerFunction
	 */
	public String getPartnerFunction()
	{
		return partnerFunction;
	}

	/**
	 * @param partnerFunction
	 *           the partnerFunction to set
	 */
	public void setPartnerFunction(final String partnerFunction)
	{
		this.partnerFunction = partnerFunction;
	}

	/**
	 * @return the nhsCode
	 */
	public String getNhsCode()
	{
		return nhsCode;
	}

	/**
	 * @param nhsCode
	 *           the nhsCode to set
	 */
	public void setNhsCode(final String nhsCode)
	{
		this.nhsCode = nhsCode;
	}

	/**
	 * @return the registrationNumber
	 */
	public String getRegistrationNumber()
	{
		return registrationNumber;
	}

	/**
	 * @param registrationNumber
	 *           the registrationNumber to set
	 */
	public void setRegistrationNumber(final String registrationNumber)
	{
		this.registrationNumber = registrationNumber;
	}

	/**
	 * @return the vatNumber
	 */
	public String getVatNumber()
	{
		return vatNumber;
	}

	/**
	 * @param vatNumber
	 *           the vatNumber to set
	 */
	public void setVatNumber(final String vatNumber)
	{
		this.vatNumber = vatNumber;
	}

	/**
	 * @return the businessType
	 */
	public String getBusinessType()
	{
		return businessType;
	}

	/**
	 * @param businessType
	 *           the businessType to set
	 */
	public void setBusinessType(final String businessType)
	{
		this.businessType = businessType;
	}

	/**
	 * @return the soldToContact
	 */
	public JoinAccountLocationContact getSoldToContact()
	{
		return soldToContact;
	}

	/**
	 * @param soldToContact
	 *           the soldToContact to set
	 */
	public void setSoldToContact(final JoinAccountLocationContact soldToContact)
	{
		this.soldToContact = soldToContact;
	}

	/**
	 * @return the shipToContact
	 */
	public List<JoinAccountLocationContact> getShipToContact()
	{
		return shipToContact;
	}

	/**
	 * @param shipToContact
	 *           the shipToContact to set
	 */
	public void setShipToContact(final List<JoinAccountLocationContact> shipToContact)
	{
		this.shipToContact = shipToContact;
	}

	/**
	 * @return the shipToLicense
	 */
	public JoinAccountAPIShipLicense getShipToLicense()
	{
		return shipToLicense;
	}

	/**
	 * @param shipToLicense
	 *           the shipToLicense to set
	 */
	public void setShipToLicense(final JoinAccountAPIShipLicense shipToLicense)
	{
		this.shipToLicense = shipToLicense;
	}

	/**
	 * @return the billToContact
	 */
	public JoinAccountLocationContact getBillToContact()
	{
		return billToContact;
	}

	/**
	 * @param billToContact
	 *           the billToContact to set
	 */
	public void setBillToContact(final JoinAccountLocationContact billToContact)
	{
		this.billToContact = billToContact;
	}

	/**
	 * @return the payerContact
	 */
	public JoinAccountLocationContact getPayerContact()
	{
		return payerContact;
	}

	/**
	 * @param payerContact
	 *           the payerContact to set
	 */
	public void setPayerContact(final JoinAccountLocationContact payerContact)
	{
		this.payerContact = payerContact;
	}



}
