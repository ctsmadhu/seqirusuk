package com.seqirus.uk.core.dataObjects;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class InvoiceLandingResponse {
	@JsonProperty(value = "invoices")
	public List<InvoiceListLandingResponse> invoices;

	/**
	 * @return the invoices
	 */
	public List<InvoiceListLandingResponse> getInvoices()
	{
		return invoices;
	}

	/**
	 * @param invoices the invoices to set
	 */
	public void setInvoices(final List<InvoiceListLandingResponse> invoices)
	{
		this.invoices = invoices;
	}


}
