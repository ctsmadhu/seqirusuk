package com.seqirus.uk.core.dataObjects;

import java.util.List;

import com.seqirus.uk.facades.cutomer.data.SeqirusOrderQuantityData;


public class ChartandTableData
{


	/**
	 * @return the tableData
	 */
	public List<ShipmentTableData> getTableData()
	{
		return tableData;
	}
	/**
	 * @param tableData the tableData to set
	 */
	public void setTableData(final List<ShipmentTableData> tableData)
	{
		this.tableData = tableData;
	}

	public SeqirusOrderQuantityData getChartData()
	{
		return chartData;
	}
	/**
	 * @param chartData the chartData to set
	 */
	public void setChartData(final SeqirusOrderQuantityData chartData)
	{
		this.chartData = chartData;
	}
	 List<ShipmentTableData> tableData;
	public SeqirusOrderQuantityData chartData;

}

