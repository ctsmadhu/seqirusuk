package com.seqirus.core.dataimport.batch.task;

import de.hybris.platform.acceleratorservices.dataimport.batch.converter.ImpexRowFilter;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 *
 * This file SeqirusAddressRegionImpexRowFilter.java is used for row skip for blank/ Invalid values in the feeds.
 *
 */
public class SeqirusAddressRegionImpexRowFilter implements ImpexRowFilter
{

	private static final Logger LOG = Logger.getLogger(SeqirusAddressRegionImpexRowFilter.class);

	private static final String STRING_HASH = "#";
	private static final String STRING_SLASH = "/";
	private static final String STRING_HYPHEN = "-";


	private String countryIsoCodeFeildRowIndex;
	private String regionIsoCodeFeildRowIndex;

	/**
	 * this method is used for validate RegionIso Code fields data.. i.e. (US/NY) etc.
	 *
	 * @param row
	 * @return boolean
	 */
	@Override
	public boolean filter(final Map<Integer, String> row)
	{
		boolean isValid = true;
		final Integer regionIsoCodeFeildIndex = Integer.valueOf(regionIsoCodeFeildRowIndex);
		final String regionIsoCodeValue = row.get(regionIsoCodeFeildIndex);

		final Integer countryIsoCodeFeildIndex = Integer.valueOf(countryIsoCodeFeildRowIndex);
		final String countryIsoCodeValue = row.get(countryIsoCodeFeildIndex);

		String modifiedValue = StringUtils.EMPTY;

		if (StringUtils.contains(regionIsoCodeValue, STRING_SLASH))
		{
			if (StringUtils.endsWith(regionIsoCodeValue.trim(), STRING_SLASH)
					|| StringUtils.endsWith(regionIsoCodeValue.trim(), STRING_HASH))
			{
				LOG.info("ignoring region value :: " + regionIsoCodeValue.trim());
				modifiedValue = StringUtils.EMPTY;
				isValid = false;

			}
			else if (StringUtils.isEmpty(countryIsoCodeValue) || (StringUtils.isNotEmpty(countryIsoCodeValue)
					&& !StringUtils.startsWith(regionIsoCodeValue.trim(), countryIsoCodeValue.trim())))
			{
				LOG.info("ignoring region value as countryIso value donot matches with regionIso value, countryIso:: "
						+ countryIsoCodeValue + ", regionIso::" + regionIsoCodeValue.trim());
				modifiedValue = StringUtils.EMPTY;
				isValid = false;
			}
			else
			{
				modifiedValue = StringUtils.replace(regionIsoCodeValue, STRING_SLASH, STRING_HYPHEN);

				//setting the correct value to region Iso type
				row.put(regionIsoCodeFeildIndex, modifiedValue);
				isValid = true;
			}
		}
		else
		{
			LOG.info("ignoring current row as region value :: " + regionIsoCodeValue);
			modifiedValue = StringUtils.EMPTY;
			isValid = false;
		}

		return isValid;

	}

	/**
	 * @return the regionIsoCodeFeildRowIndex
	 */
	public String getRegionIsoCodeFeildRowIndex()
	{
		return regionIsoCodeFeildRowIndex;
	}

	/**
	 * @param regionIsoCodeFeildRowIndex
	 *           the regionIsoCodeFeildRowIndex to set
	 */
	@Required
	public void setRegionIsoCodeFeildRowIndex(final String regionIsoCodeFeildRowIndex)
	{
		this.regionIsoCodeFeildRowIndex = regionIsoCodeFeildRowIndex;
	}

	/**
	 * @return the countryIsoCodeFeildRowIndex
	 */
	public String getCountryIsoCodeFeildRowIndex()
	{
		return countryIsoCodeFeildRowIndex;
	}

	/**
	 * @param countryIsoCodeFeildRowIndex
	 *           the countryIsoCodeFeildRowIndex to set
	 */
	@Required
	public void setCountryIsoCodeFeildRowIndex(final String countryIsoCodeFeildRowIndex)
	{
		this.countryIsoCodeFeildRowIndex = countryIsoCodeFeildRowIndex;
	}


}
