package com.seqirus.uk.core.dataObjects;

public class TrackingDetail {
	String id;
	String link;

	public TrackingDetail(final String id, final String link) {
		super();
		this.id = id;
		this.link = link;
	}

	public String getId() {
		return id;
	}

	public String getLink() {
		return link;
	}

	@Override
	public String toString() {
		return "TrackingDetail [id=" + id + ", link=" + link + "]";
	}




}
