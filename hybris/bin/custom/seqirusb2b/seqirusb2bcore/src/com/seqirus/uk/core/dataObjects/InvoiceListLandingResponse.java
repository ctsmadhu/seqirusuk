/**
 *
 */
package com.seqirus.uk.core.dataObjects;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author 700196
 *
 */
public class InvoiceListLandingResponse
{
	@JsonProperty(value = "invoiceNumber")
	public String invoiceNumber;
	@JsonProperty(value = "invoiceType")
	public String invoiceType;
	@JsonProperty(value = "invoiceTypeDescription")
	public String invoiceTypeDescription;
	@JsonProperty(value = "salesOrderNumber")
	public String salesOrderNumber;
	@JsonProperty(value = "salesItem")
	public String salesItem;
	@JsonProperty(value = "amoutWithTax")
	public double amoutWithTax;
	@JsonProperty(value = "invoiceDate")
	public String invoiceDate;
	@JsonProperty(value = "dueDate")
	public String dueDate;
	@JsonProperty(value = "clearingDate")
	public String clearingDate;
	@JsonProperty(value = "currency")
	public String currency;
	@JsonProperty(value = "paymentTerm")
	public String paymentTerm;
	@JsonProperty(value = "paymentTermDescription")
	public String paymentTermDescription;
	@JsonProperty(value = "status")
	public String status;
	@JsonProperty(value = "originalInvoice")
	public String originalInvoice;
	@JsonProperty(value = "discount")
	public float discount;
	@JsonProperty(value = "amountwithDiscount")
	public double amountwithDiscount;
	@JsonProperty(value = "discountDate")
	public String discountDate;
	@JsonProperty(value = "discountPercentage")
	public String discountPercentage;
	@JsonProperty(value = "discountDays")
	public String discountDays;
	@JsonProperty(value = "netDays")
	public String netDays;
	/**
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}
	/**
	 * @param invoiceNumber the invoiceNumber to set
	 */
	public void setInvoiceNumber(final String invoiceNumber)
	{
		this.invoiceNumber = invoiceNumber;
	}
	/**
	 * @return the invoiceType
	 */
	public String getInvoiceType()
	{
		return invoiceType;
	}
	/**
	 * @param invoiceType the invoiceType to set
	 */
	public void setInvoiceType(final String invoiceType)
	{
		this.invoiceType = invoiceType;
	}
	/**
	 * @return the invoiceTypeDescription
	 */
	public String getInvoiceTypeDescription()
	{
		return invoiceTypeDescription;
	}
	/**
	 * @param invoiceTypeDescription the invoiceTypeDescription to set
	 */
	public void setInvoiceTypeDescription(final String invoiceTypeDescription)
	{
		this.invoiceTypeDescription = invoiceTypeDescription;
	}
	/**
	 * @return the salesOrderNumber
	 */
	public String getSalesOrderNumber()
	{
		return salesOrderNumber;
	}
	/**
	 * @param salesOrderNumber the salesOrderNumber to set
	 */
	public void setSalesOrderNumber(final String salesOrderNumber)
	{
		this.salesOrderNumber = salesOrderNumber;
	}
	/**
	 * @return the salesItem
	 */
	public String getSalesItem()
	{
		return salesItem;
	}
	/**
	 * @param salesItem the salesItem to set
	 */
	public void setSalesItem(final String salesItem)
	{
		this.salesItem = salesItem;
	}

	/**
	 * @return the invoiceDate
	 */
	public String getInvoiceDate()
	{
		return invoiceDate;
	}
	/**
	 * @param invoiceDate the invoiceDate to set
	 */
	public void setInvoiceDate(final String invoiceDate)
	{
		this.invoiceDate = invoiceDate;
	}
	/**
	 * @return the dueDate
	 */
	public String getDueDate()
	{
		return dueDate;
	}
	/**
	 * @param dueDate the dueDate to set
	 */
	public void setDueDate(final String dueDate)
	{
		this.dueDate = dueDate;
	}
	/**
	 * @return the clearingDate
	 */
	public String getClearingDate()
	{
		return clearingDate;
	}
	/**
	 * @param clearingDate the clearingDate to set
	 */
	public void setClearingDate(final String clearingDate)
	{
		this.clearingDate = clearingDate;
	}
	/**
	 * @return the currency
	 */
	public String getCurrency()
	{
		return currency;
	}
	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}
	/**
	 * @return the paymentTerm
	 */
	public String getPaymentTerm()
	{
		return paymentTerm;
	}
	/**
	 * @param paymentTerm the paymentTerm to set
	 */
	public void setPaymentTerm(final String paymentTerm)
	{
		this.paymentTerm = paymentTerm;
	}
	/**
	 * @return the paymentTermDescription
	 */
	public String getPaymentTermDescription()
	{
		return paymentTermDescription;
	}
	/**
	 * @param paymentTermDescription the paymentTermDescription to set
	 */
	public void setPaymentTermDescription(final String paymentTermDescription)
	{
		this.paymentTermDescription = paymentTermDescription;
	}
	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(final String status)
	{
		this.status = status;
	}
	/**
	 * @return the originalInvoice
	 */
	public String getOriginalInvoice()
	{
		return originalInvoice;
	}
	/**
	 * @param originalInvoice the originalInvoice to set
	 */
	public void setOriginalInvoice(final String originalInvoice)
	{
		this.originalInvoice = originalInvoice;
	}
	/**
	 * @return the discount
	 */
	public float getDiscount()
	{
		return discount;
	}
	/**
	 * @param discount the discount to set
	 */
	public void setDiscount(final float discount)
	{
		this.discount = discount;
	}


	/**
	 * @return the amoutWithTax
	 */
	public double getAmoutWithTax()
	{
		return amoutWithTax;
	}

	/**
	 * @param amoutWithTax
	 *           the amoutWithTax to set
	 */
	public void setAmoutWithTax(final double amoutWithTax)
	{
		this.amoutWithTax = amoutWithTax;
	}

	/**
	 * @return the amountwithDiscount
	 */
	public double getAmountwithDiscount()
	{
		return amountwithDiscount;
	}
	/**
	 * @param amountwithDiscount the amountwithDiscount to set
	 */
	public void setAmountwithDiscount(final double amountwithDiscount)
	{
		this.amountwithDiscount = amountwithDiscount;
	}
	/**
	 * @return the discountDate
	 */
	public String getDiscountDate()
	{
		return discountDate;
	}
	/**
	 * @param discountDate the discountDate to set
	 */
	public void setDiscountDate(final String discountDate)
	{
		this.discountDate = discountDate;
	}
	/**
	 * @return the discountPercentage
	 */
	public String getDiscountPercentage()
	{
		return discountPercentage;
	}
	/**
	 * @param discountPercentage the discountPercentage to set
	 */
	public void setDiscountPercentage(final String discountPercentage)
	{
		this.discountPercentage = discountPercentage;
	}
	/**
	 * @return the discountDays
	 */
	public String getDiscountDays()
	{
		return discountDays;
	}
	/**
	 * @param discountDays the discountDays to set
	 */
	public void setDiscountDays(final String discountDays)
	{
		this.discountDays = discountDays;
	}
	/**
	 * @return the netDays
	 */
	public String getNetDays()
	{
		return netDays;
	}
	/**
	 * @param netDays the netDays to set
	 */
	public void setNetDays(final String netDays)
	{
		this.netDays = netDays;
	}


}
