package com.seqirus.uk.core.dataObjects;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Holds overall details of a order for Order History module including all order material and shipping details.
 */
public class OrderSummary
{
	//Constants for POType
	public static final String PO_TYPE_RPLC = "Replacements";
	public static final String PO_TYPE_SHRT = "Shortage";
	public static final String PO_TYPE_OVER = "Overage";

	String orderID;
	Date orderDate;
	String status;
	double totalCost;
	AddressDetail address;
	String shipToID;
	String soldToPartnerID;
	List<ProductDetail> products;
	List<ProductDetailAllItemNumber> productsAllItem; // added in sprint17 to handle negative value
	List<ShipmentDetail> shipments;
	int maxItemNumber;
	String seasonInPre;
	BigDecimal estimatedCost;
	String poNumber;
	Set<String> poTypeSet = new HashSet<String>();
	int notificationCount ;
	int shipNotification ;
	int paymentNotification ;
	int shipLocationNotification;
	int totalFluad;
	int totalFlucelvax;
	int totalAfluria;
	String partnerName;

	public OrderSummary(final String orderID, final Date date, final double totalCost, final String shiptoID, final String soldTo,
			final String status, final String poNumber)
	{
		super();
		this.orderID = orderID;
		this.orderDate = date;
		this.totalCost = totalCost;
		this.shipToID = shiptoID;
		this.soldToPartnerID = soldTo;
		this.status = status;
		products = new ArrayList<ProductDetail>();
		productsAllItem = new ArrayList<ProductDetailAllItemNumber>();
		shipments = new ArrayList<ShipmentDetail>();
		this.poNumber = poNumber;
	}


	public void addProduct(final String materialID, final String itemNumber, final int totalQty, final int itemQty)
	{
		final ProductDetail pd = new ProductDetail(materialID, itemNumber, totalQty);
		pd.setItemQty(itemQty);
		products.add(pd);
	}

	public void addItemProduct(final String materialID, final String itemNumber, final int itemQty, final String wave,
			final Date promisedDate)
	{
		final ProductDetailAllItemNumber pd = new ProductDetailAllItemNumber(materialID, itemNumber, itemQty, wave, promisedDate);

		productsAllItem.add(pd);
	}

	public void addProduct(final String materialID, final String itemNumber, final int qty)
	{
		products.add(new ProductDetail(materialID, itemNumber, qty));
	}


	public void setShipToAddress(final String addressLine1, final String addressLine2, final String city, final String state,
			final String zipCode)
	{
		address = new AddressDetail(addressLine1, addressLine2, city, state, zipCode);
	}

	//returns number of shipments
	public int getShipmentCount()
	{
		return shipments.size();
	}

	//returns total ordered qty
	public int getTotalOrderedQty()
	{
		int total = 0;

		for (final ProductDetail p : products)
		{
			total = total + p.getQuantity();
		}
		return total;
	}

	//returns total shipped qty
	public int getTotalShippedQty()
	{

		int total = 0;
		for (final ShipmentDetail p : shipments)
		{
			total = total + p.getTotalShippedQty();
		}
		return total;
	}

	//returns all ordered material ID
	public List<String> getAllOrderedMaterials()
	{
		final List orderedMaterial = new ArrayList();

		for (final ProductDetail p : products)
		{
			orderedMaterial.add(p.getMaterialID());
		}
		return orderedMaterial;
	}

	public ShipmentDetail getShipmentByPosition(final int index)
	{
		if (shipments.size() < index)
		{
			return null;
		}
		return shipments.get(index - 1);
	}

	public int getMaterialOrderedQty(final String materialID)
	{

		for (final ProductDetail p : products)
		{
			if (p.getMaterialID().equals(materialID))
			{
				return p.getQuantity();
			}
		}
		return 0;
	}

	public int getMaterialShippedQty(final String materialID)
	{

		int qty = 0;
		for (final ShipmentDetail p : shipments)
		{
			qty += p.getMaterialShippedQty(materialID);
		}
		return qty;
	}

	public ProductDetail getMaterialByID(final String materialID)
	{
		for (final ProductDetail p : products)
		{
			if (p.getMaterialID().equals(materialID))
			{
				return p;
			}
		}
		return null;
	}

	public void setShipToID(final String shipToID)
	{
		this.shipToID = shipToID;
	}



	public void addShippingDetails(final ShipmentDetail shippingDetails)
	{
		this.shipments.add(shippingDetails);
	}


	public int getMaxItemNumber()
	{
		return maxItemNumber;
	}


	public void setMaxItemNumber(final int maxItemNumber)
	{
		this.maxItemNumber = maxItemNumber;
	}


	public String getOrderID()
	{
		return orderID;
	}


	/**
	 * @return the productsAllItem
	 */
	public List<ProductDetailAllItemNumber> getProductsAllItem()
	{
		return productsAllItem;
	}


	/**
	 * @param productsAllItem
	 *           the productsAllItem to set
	 */
	public void setProductsAllItem(final List<ProductDetailAllItemNumber> productsAllItem)
	{
		this.productsAllItem = productsAllItem;
	}


	/**
	 * @return the poTypeRplc
	 */
	public static String getPoTypeRplc()
	{
		return PO_TYPE_RPLC;
	}


	/**
	 * @return the poTypeShrt
	 */
	public static String getPoTypeShrt()
	{
		return PO_TYPE_SHRT;
	}


	/**
	 * @return the poTypeOver
	 */
	public static String getPoTypeOver()
	{
		return PO_TYPE_OVER;
	}


	public Date getOrderDate()
	{
		return orderDate;
	}


	public String getStatus()
	{
		return status;
	}


	public double getTotalCost()
	{
		return totalCost;
	}


	public AddressDetail getShipToAddress()
	{
		return address;
	}

	public String getShipToID()
	{
		return shipToID;
	}


	public List<ProductDetail> getProducts()
	{
		return products;
	}


	/**
	 * @return the totalFluad
	 */
	public int getTotalFluad()
	{
		return totalFluad;
	}


	/**
	 * @param totalFluad the totalFluad to set
	 */
	public void setTotalFluad(final int totalFluad)
	{
		this.totalFluad = totalFluad;
	}


	/**
	 * @return the totalFlucelvax
	 */
	public int getTotalFlucelvax()
	{
		return totalFlucelvax;
	}


	/**
	 * @param totalFlucelvax the totalFlucelvax to set
	 */
	public void setTotalFlucelvax(final int totalFlucelvax)
	{
		this.totalFlucelvax = totalFlucelvax;
	}


	/**
	 * @return the totalAfluria
	 */
	public int getTotalAfluria()
	{
		return totalAfluria;
	}


	/**
	 * @param totalAfluria the totalAfluria to set
	 */
	public void setTotalAfluria(final int totalAfluria)
	{
		this.totalAfluria = totalAfluria;
	}


	public AddressDetail getAddress()
	{
		return address;
	}


	public String getSoldToPartnerID()
	{
		return soldToPartnerID;
	}


	public List<ShipmentDetail> getShipments()
	{
		return shipments;
	}


	public void setStatus(final String status)
	{
		this.status = status;
	}

	/**
	 * @return the seasonInPre
	 */
	public String getSeasonInPre()
	{
		return seasonInPre;
	}


	/**
	 * @param seasonInPre
	 *           the seasonInPre to set
	 */
	public void setSeasonInPre(final String seasonInPre)
	{
		this.seasonInPre = seasonInPre;
	}


	/**
	 * @return the contract
	 */
	public boolean isContract()
	{
		return contract;
	}


	/**
	 * @param contract
	 *           the contract to set
	 */
	public void setContract(final boolean contract)
	{
		this.contract = contract;
	}


	private boolean contract;



	/**
	 * @return the estimatedCost
	 */
	public BigDecimal getEstimatedCost()
	{
		return estimatedCost;
	}


	/**
	 * @param estimatedCost
	 *           the estimatedCost to set
	 */
	public void setEstimatedCost(final BigDecimal estimatedCost)
	{
		this.estimatedCost = estimatedCost;
	}

	@Override
	public String toString()
	{
		return "OrderSummary [orderID=" + orderID + ", orderDate=" + orderDate + ", status=" + status + ", totalCost=" + totalCost
				+ ",\n address=" + address + ",\n shipToID=" + shipToID + ", soldToPartnerID=" + soldToPartnerID
				+ ",\n orderedproducts=" + products + ",\n orderedproductsAllItem=" + productsAllItem + ",\n shipments=" + shipments
				+ ", \nmaxItemNumber=" + maxItemNumber
				+ ", seasonInPre=" + seasonInPre + ", poNumber=" + poNumber + ",partnerName=" + partnerName + ", contract=" + contract + "]\n";
	}


	/**
	 * @return the poNumber
	 */
	public String getPoNumber()
	{
		return poNumber;
	}

	public void setTotalCost(final double totalCost)
	{
		this.totalCost = totalCost;
	}

	public boolean isOrderReplacement()
	{
		return poTypeSet.contains(PO_TYPE_RPLC);
	}

	public boolean isOrderOverage()
	{
		return poTypeSet.contains(PO_TYPE_OVER);
	}

	public boolean isOrderShortage()
	{
		return poTypeSet.contains(PO_TYPE_SHRT);
	}

	public void addPOType(final String poType)
	{
		poTypeSet.add(poType);
	}

	/**
	 * @return the notificationCount
	 */
	public int getNotificationCount()
	{
		return notificationCount;
	}


	/**
	 * @param notificationCount the notificationCount to set
	 */
	public void setNotificationCount(final int notificationCount)
	{
		this.notificationCount = notificationCount;
	}


	/**
	 * @return the shipNotification
	 */
	public int getShipNotification()
	{
		return shipNotification;
	}


	/**
	 * @param shipNotification the shipNotification to set
	 */
	public void setShipNotification(final int shipNotification)
	{
		this.shipNotification = shipNotification;
	}


	/**
	 * @return the paymentNotification
	 */
	public int getPaymentNotification()
	{
		return paymentNotification;
	}


	/**
	 * @param paymentNotification the paymentNotification to set
	 */
	public void setPaymentNotification(final int paymentNotification)
	{
		this.paymentNotification = paymentNotification;
	}


	/**
	 * @return the shipLocationNotification
	 */
	public int getShipLocationNotification()
	{
		return shipLocationNotification;
	}

	/**
	 * @return the partnerName
	 */
	public String getPartnerName()
	{
		return partnerName;
	}

	/**
	 * @param partnerName
	 *           the partnerName to set
	 */
	public void setPartnerName(final String partnerName)
	{
		this.partnerName = partnerName;
	}



	/**
	 * @param shipLocationNotification the shipLocationNotification to set
	 */
	public void setShipLocationNotification(final int shipLocationNotification)
	{
		this.shipLocationNotification = shipLocationNotification;
	}


	/**
	 * Checks whether the order is fully paid
	 *
	 * @return boolean
	 */
	public boolean isFullyPaid()
	{
		int paidCount = 0;
		final int shipmentCount = shipments.size();
		for (final ShipmentDetail p : shipments)
		{
			if (p.isInvoicesPaid())
			{
				paidCount++;
			}
		}
		if (paidCount > 0 && paidCount == shipmentCount)
		{
			return true;
		}
		return false;

	}


}
