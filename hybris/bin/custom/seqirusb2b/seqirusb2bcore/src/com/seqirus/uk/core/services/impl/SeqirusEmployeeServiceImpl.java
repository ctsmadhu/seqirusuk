/**
 *
 */
package com.seqirus.uk.core.services.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.PasswordEncoderConstants;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.seqirus.uk.core.event.AbstractEmployeeEvent;
import com.seqirus.uk.core.event.SeqirusEmployeePwdEvent;
import com.seqirus.uk.core.services.SeqirusEmployeeService;


/**
 * @author 172553
 *
 */
public class SeqirusEmployeeServiceImpl implements SeqirusEmployeeService
{
	private long tokenValiditySeconds;
	private final String passwordEncoding = PasswordEncoderConstants.DEFAULT_ENCODING;

	@Autowired
	private UserService userService;

	@Autowired
	private SecureTokenService secureTokenService;

	@Autowired
	private ModelService modelService;

	@Autowired
	private EventService eventService;

	@Autowired
	private BaseStoreService baseStoreService;

	@Autowired
	private BaseSiteService baseSiteService;

	@Autowired
	private CommonI18NService commonI18NService;


	@Override
	public void forgottenPassword(final EmployeeModel employeeModel)
	{
		validateParameterNotNullStandardMessage("employeeModel", employeeModel);
		final long timeStamp = getTokenValiditySeconds() > 0L ? new Date().getTime() : 0L;
		final SecureToken data = new SecureToken(employeeModel.getUid(), timeStamp);
		final String token = secureTokenService.encryptData(data);
		employeeModel.setToken(token);
		modelService.save(employeeModel);
		eventService.publishEvent(initializeEvent(new SeqirusEmployeePwdEvent(token), employeeModel));

	}


	@Override
	public void updatePassword(final String token, final String newPassword) throws TokenInvalidatedException
	{
		Assert.hasText(token, "The field [token] cannot be empty");
		Assert.hasText(newPassword, "The field [newPassword] cannot be empty");

		final SecureToken data = secureTokenService.decryptData(token);
		if (getTokenValiditySeconds() > 0L)
		{
			final long delta = new Date().getTime() - data.getTimeStamp();
			if (delta / 1000 > getTokenValiditySeconds())
			{
				throw new IllegalArgumentException("token expired");
			}
		}

		final EmployeeModel employee = userService.getUserForUID(data.getData(), EmployeeModel.class);
		if (employee == null)
		{
			throw new IllegalArgumentException("user for token not found");
		}
		if (!token.equals(employee.getToken()))
		{
			throw new TokenInvalidatedException();
		}
		employee.setToken(null);
		employee.setLoginDisabled(false);
		modelService.save(employee);

		userService.setPassword(data.getData(), newPassword, getPasswordEncoding());
		eventService.publishEvent(initializeEvent(new SeqirusEmployeePwdEvent(), employee));
	}

	protected long getTokenValiditySeconds()
	{
		return tokenValiditySeconds;
	}

	@Required
	public void setTokenValiditySeconds(final long tokenValiditySeconds)
	{
		if (tokenValiditySeconds < 0)
		{
			throw new IllegalArgumentException("tokenValiditySeconds has to be >= 0");
		}
		this.tokenValiditySeconds = tokenValiditySeconds;
	}

	/**
	 * @return the passwordEncoding
	 */
	public String getPasswordEncoding()
	{
		return passwordEncoding;
	}


	protected AbstractEmployeeEvent initializeEvent(final AbstractEmployeeEvent event, final EmployeeModel employeeModel)
	{
		event.setBaseStore(baseStoreService.getCurrentBaseStore());
		event.setSite(baseSiteService.getCurrentBaseSite());
		event.setEmployee(employeeModel);
		event.setLanguage(commonI18NService.getCurrentLanguage());
		event.setCurrency(commonI18NService.getCurrentCurrency());
		return event;
	}

}
