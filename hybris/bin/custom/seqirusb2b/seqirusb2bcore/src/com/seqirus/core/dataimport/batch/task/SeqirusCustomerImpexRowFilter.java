package com.seqirus.core.dataimport.batch.task;

import de.hybris.platform.acceleratorservices.dataimport.batch.converter.ImpexRowFilter;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 *
 * This file SeqirusCustomerImpexRowFilter.java is used for row skip logic password is update by hybris.
 *
 */
public class SeqirusCustomerImpexRowFilter implements ImpexRowFilter
{
	private static final Logger LOG = Logger.getLogger(SeqirusCustomerImpexRowFilter.class);

	private String isDeletedFeildRowIndex;
	private String isDeletedFeildPermissibleValue;

	/**
	 * this method is used for custom behavior to make customer logically deleted as per feed's data.
	 *
	 * @param row
	 * @return boolean
	 */
	@Override
	public boolean filter(final Map<Integer, String> row)
	{
		final Integer isDeletedRowIndex = Integer.valueOf(isDeletedFeildRowIndex);
		final String isDeletedValue = row.get(isDeletedRowIndex);

		if (StringUtils.isNotEmpty(isDeletedValue)
				&& StringUtils.equalsIgnoreCase(isDeletedValue.trim(), isDeletedFeildPermissibleValue))
		{
			LOG.info("Logically deleting b2bUnit type (setting active to false)");
			row.put(isDeletedRowIndex, "false");
		}
		else
		{
			row.put(isDeletedRowIndex, "true");
		}

		return true;
	}

	/**
	 * @return the isDeletedFeildRowIndex
	 */
	public String getIsDeletedFeildRowIndex()
	{
		return isDeletedFeildRowIndex;
	}

	/**
	 * @param isDeletedFeildRowIndex
	 *           the isDeletedFeildRowIndex to set
	 */
	@Required
	public void setIsDeletedFeildRowIndex(final String isDeletedFeildRowIndex)
	{
		this.isDeletedFeildRowIndex = isDeletedFeildRowIndex;
	}

	/**
	 * @return the isDeletedFeildPermissibleValue
	 */
	public String getIsDeletedFeildPermissibleValue()
	{
		return isDeletedFeildPermissibleValue;
	}

	/**
	 * @param isDeletedFeildPermissibleValue
	 *           the isDeletedFeildPermissibleValue to set
	 */
	@Required
	public void setIsDeletedFeildPermissibleValue(final String isDeletedFeildPermissibleValue)
	{
		this.isDeletedFeildPermissibleValue = isDeletedFeildPermissibleValue;
	}

}
