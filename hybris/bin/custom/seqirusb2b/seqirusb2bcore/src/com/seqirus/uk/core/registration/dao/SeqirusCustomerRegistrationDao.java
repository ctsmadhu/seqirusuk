/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.uk.core.registration.dao;


import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.c2l.CountryModel;

import java.util.List;

import com.seqirus.core.model.SeasonEntryModel;
import com.seqirus.core.model.SeqirusB2BTempCustomerModel;

/**
 * @author 845332
 *
 */

/**
 * Data Access Object for looking up items related to the Customer's account.
 */
public interface SeqirusCustomerRegistrationDao
{
	B2BUnitModel getPartnerOrgByB2BUnit(final String account);

	CountryModel getCountryByName(final String countryName);

	List<B2BCustomerModel> getAllCustomers();
	/**
	 * @param uid
	 * @return
	 */
	SeqirusB2BTempCustomerModel fetchCustModel(String uid);

	/**
	 * @return SeasonEntryModel
	 */
	SeasonEntryModel seasonEntry();

	/**
	 * @return
	 */
	List<B2BCustomerModel> getWelcomeEmailCustomers();
}
