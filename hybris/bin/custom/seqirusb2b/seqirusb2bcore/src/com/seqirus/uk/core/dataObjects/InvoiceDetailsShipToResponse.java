/**
 *
 */
package com.seqirus.uk.core.dataObjects;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author 700196
 *
 */
public class InvoiceDetailsShipToResponse
{
	@JsonProperty(value = "id")
	public String id;
	@JsonProperty(value = "name")
	public String name;
	@JsonProperty(value = "street")
	public String street;
	@JsonProperty(value = "additionalStreet")
	public String additionalStreet;
	@JsonProperty(value = "city")
	public String city;
	@JsonProperty(value = "country")
	public String country;
	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(final String id)
	{
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(final String name)
	{
		this.name = name;
	}
	/**
	 * @return the street
	 */
	public String getStreet()
	{
		return street;
	}
	/**
	 * @param street the street to set
	 */
	public void setStreet(final String street)
	{
		this.street = street;
	}
	/**
	 * @return the additionalStreet
	 */
	public String getAdditionalStreet()
	{
		return additionalStreet;
	}
	/**
	 * @param additionalStreet the additionalStreet to set
	 */
	public void setAdditionalStreet(final String additionalStreet)
	{
		this.additionalStreet = additionalStreet;
	}
	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(final String city)
	{
		this.city = city;
	}
	/**
	 * @return the country
	 */
	public String getCountry()
	{
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(final String country)
	{
		this.country = country;
	}


}
