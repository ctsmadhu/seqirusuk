package com.seqirus.uk.core.dataObjects;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/*
 * Holds Shipment(Delivery) details for an order
 */

public class ShipmentDetail
{
	List<TrackingDetail> trackingURLs;
	List<ProductDetail> shippedIems = new ArrayList();
	String status;
	Date date;
	Date PGIDate;
	String number;
	//	List<String> invoiceNumbers;
	Map<String, PaymentDetail> paymentDetails;
	boolean creditsAvailable = false;

	public ShipmentDetail()
	{
		shippedIems = new ArrayList<ProductDetail>();
		trackingURLs = new ArrayList<TrackingDetail>();
		//	invoiceNumbers = new ArrayList<String>();
		paymentDetails = new HashMap<String, PaymentDetail>();
	}

	public ShipmentDetail(final String deliveryStatus, final Date shipmentDate, final String deliveryNumber,final Date ShipDate)
	{
		this();
		status = deliveryStatus;
		date = shipmentDate;
		number = deliveryNumber;
		PGIDate =ShipDate;
	}

	/**
	 * @return the date
	 */
	public Date getDate()
	{
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(final Date date)
	{
		this.date = date;
	}

	/**
	 * @return the pGIDate
	 */
	public Date getPGIDate()
	{
		return PGIDate;
	}

	public void addShipmentProduct(final String materialID, final String itemNumber, final int qty)
	{
		shippedIems.add(new ProductDetail(materialID, itemNumber, qty));
	}

	public void addTracking(final String id, final String link)
	{
		trackingURLs.add(new TrackingDetail(id, link));
	}


	/*
	 * public void addInvoice(final String invoiceNumber) { invoiceNumbers.add(invoiceNumber); }
	 */

	public void addInvoice(final String invoiceNumber)
	{
		paymentDetails.put(invoiceNumber, null);
	}


	public List<String> getShippedMaterials()
	{
		final List orderedMaterial = new ArrayList();

		for (final ProductDetail p : shippedIems)
		{
			orderedMaterial.add(p.getMaterialID());
		}
		return orderedMaterial;
	}

	public int getTotalShippedQty()
	{
		int total = 0;

		for (final ProductDetail p : shippedIems)
		{
			total = total + p.getQuantity();
		}
		return total;
	}

	public int getMaterialShippedQty(final String materialID)
	{
       int qty = 0;
		for (final ProductDetail p : shippedIems)
		{
			if (p.getMaterialID().equals(materialID))
			{
				qty +=p.getQuantity();
			}
		}
		return qty;
	}


	public List<TrackingDetail> getTrackingURLs()
	{
		return trackingURLs;
	}

	public List<ProductDetail> getShippedIems()
	{
		return shippedIems;
	}

	public String getStatus()
	{
		return status;
	}




	public List<PaymentDetail> getPaymentDetails()
	{
		return new ArrayList<PaymentDetail>(paymentDetails.values());
	}

	public List<String> getInvoiceNumbers()
	{
		return new ArrayList<String>(paymentDetails.keySet());
	}

	public boolean doesMaterialExist(final String materialID, final String itemNumber)
	{
		for (final ProductDetail p : shippedIems)
		{
			if (p.getMaterialID().equals(materialID) && p.getItemNumber().equals(itemNumber))
			{
				return true;
			}
		}
		return false;
	}

	public void setPaymentDetail(final String invoiceNumber, final PaymentDetail pd)
	{
		paymentDetails.put(invoiceNumber, pd);
	}


	@Override
	public String toString()
	{
		return "ShipmentDetail [trackingURLs=" + trackingURLs + ", status=" + status + ", PGIDate=" + PGIDate + ", date=" + date + ", number=" + number
				+ ",\n invoiceNumbers=" + getInvoiceNumbers() + ", \n shippedIems=" + shippedIems + " PaymentDetails :"
				+ getPaymentDetails() + "]\n";
	}

	public boolean isCreditsAvailable()
	{
		return creditsAvailable;
	}

	public void setCreditsAvailable(final boolean creditsAvailable)
	{
		this.creditsAvailable = creditsAvailable;
	}

	/**
	 * Checks whether the shipment invoice is paid or not.
	 *
	 * @return boolean
	 */
	public boolean isInvoicesPaid()
	{
		if (paymentDetails.isEmpty())
		{
			return false;
		}
		for (final PaymentDetail payment : paymentDetails.values())
		{
			/*
			 * if(null!=payment){ if (!CustomerOrderHistoryService.PAYMENT_STATUS_PAID.equals(payment.getStatus())) {
			 * return false; } }
			 */
		}
		return true;
	}


}
