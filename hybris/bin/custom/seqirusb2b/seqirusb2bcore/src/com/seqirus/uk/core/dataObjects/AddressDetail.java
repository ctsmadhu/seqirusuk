/**
 *
 */
package com.seqirus.uk.core.dataObjects;

/**
 * Holds address details for Customer
 */
public class AddressDetail
{
	String addressLine1;

	/**
	 * @param state
	 *           the state to set
	 */
	public void setState(final String state)
	{
		this.state = state;
	}

	String addressLine2;
	String city;
	String state;
	String zipCode;

	public AddressDetail(final String addressLine1, final String addressLine2, final String city, final String state,
			final String zipCode)
	{
		super();
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
	}

	public String getAddressLine1()
	{
		return addressLine1;
	}

	public String getAddressLine2()
	{
		return addressLine2;
	}

	public String getCity()
	{
		return city;
	}

	public String getState()
	{
		return state;
	}

	public String getZipCode()
	{
		return zipCode;
	}

	@Override
	public String toString()
	{
		return "AddressDetail [addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", city=" + city + ", state="
				+ state + ", zipCode=" + zipCode + "]";
	}


}
