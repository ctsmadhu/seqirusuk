/**
 *
 */
package com.seqirus.uk.core.event;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author 813784
 *
 */
public class UpdateRegistrationEventListener extends AbstractAcceleratorSiteEventListener<SeqirusCustomerUpdateRegistrationEvent>
{
	private ModelService modelService;
	private BusinessProcessService businessProcessService;

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


	@Override
	protected void onSiteEvent(final SeqirusCustomerUpdateRegistrationEvent updateRregisterEvent)
	{
		final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel = (StoreFrontCustomerProcessModel) getBusinessProcessService()
				.createProcess(
						"updateRegistrationEmailProcess-" + updateRregisterEvent.getCustomer().getUid() + "-"
								+ System.currentTimeMillis(),
						"updateRegistrationEmailProcess");
		storeFrontCustomerProcessModel.setSite(updateRregisterEvent.getSite());
		storeFrontCustomerProcessModel.setCustomer(updateRregisterEvent.getCustomer());
		storeFrontCustomerProcessModel.setLanguage(updateRregisterEvent.getLanguage());
		storeFrontCustomerProcessModel.setCurrency(updateRregisterEvent.getCurrency());
		storeFrontCustomerProcessModel.setStore(updateRregisterEvent.getBaseStore());
		final EmployeeModel employee = updateRregisterEvent.getEmployee();
		if (null != employee)
		{
			//cc address
			final List<String> ccAddresses = new ArrayList<>();
			ccAddresses.add(employee.getUid());
			storeFrontCustomerProcessModel.setCcAddresses(ccAddresses);
		}
		getModelService().save(storeFrontCustomerProcessModel);
		getBusinessProcessService().startProcess(storeFrontCustomerProcessModel);
	}

	@Override
	protected SiteChannel getSiteChannelForEvent(final SeqirusCustomerUpdateRegistrationEvent event)
	{
		final BaseSiteModel site = event.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return site.getChannel();
	}

}
