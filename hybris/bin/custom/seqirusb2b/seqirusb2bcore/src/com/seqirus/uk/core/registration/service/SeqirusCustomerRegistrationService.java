/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.uk.core.registration.service;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.model.consent.ConsentModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;

import com.seqirus.core.model.SeasonEntryModel;
import com.seqirus.core.model.SeqirusB2BTempCustomerModel;
import com.seqirus.uk.core.dataObjects.JoinAccountAPIResponse;

/**
 * @author 845332
 *
 */


public interface SeqirusCustomerRegistrationService
{
	B2BUnitModel getOrgNameByB2BUnit(final String b2bUnitId);

	CountryModel getCountryByName(final String countryName);

	SeqirusB2BTempCustomerModel fetchCustModel(final String uid);

	List<ConsentModel> fetchConstModel(final CustomerModel customer);

	/**
	 * @return SeasonEntryModel
	 */
	SeasonEntryModel getSeasonEntry();

	List<B2BCustomerModel> getAllCustomers();

	/**
	 * @param accountNumber
	 * @param zipCode
	 * @return
	 */
	JoinAccountAPIResponse getCustomerData(String accountNumber, String zipCode);

	/**
	 * @return List<SeqirusExistingCustomersModel>
	 */
	List<B2BCustomerModel> getWelcomeEmailCustomers();
}
