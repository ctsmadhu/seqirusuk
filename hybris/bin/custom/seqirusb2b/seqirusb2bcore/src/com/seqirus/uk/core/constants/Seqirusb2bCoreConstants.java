/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.uk.core.constants;

/**
 * Global class for all Seqirusb2bCore constants. You can add global constants for your extension into this class.
 */
public final class Seqirusb2bCoreConstants extends GeneratedSeqirusb2bCoreConstants
{

	/** The Constant EXTENSIONNAME. */
	public static final String EXTENSIONNAME = "seqirusb2bcore";


	/**
	 * Instantiates a new seqirusb 2 b core constants.
	 */
	private Seqirusb2bCoreConstants()
	{
		//empty
	}

	/** The Constant QUOTE_BUYER_PROCESS. */
	// implement here constants used by this extension
	public static final String QUOTE_BUYER_PROCESS = "quote-buyer-process";

	/** The Constant QUOTE_SALES_REP_PROCESS. */
	public static final String QUOTE_SALES_REP_PROCESS = "quote-salesrep-process";

	/** The Constant QUOTE_USER_TYPE. */
	public static final String QUOTE_USER_TYPE = "QUOTE_USER_TYPE";

	/** The Constant QUOTE_SELLER_APPROVER_PROCESS. */
	public static final String QUOTE_SELLER_APPROVER_PROCESS = "quote-seller-approval-process";

	/** The Constant QUOTE_TO_EXPIRE_SOON_EMAIL_PROCESS. */
	public static final String QUOTE_TO_EXPIRE_SOON_EMAIL_PROCESS = "quote-to-expire-soon-email-process";

	/** The Constant QUOTE_EXPIRED_EMAIL_PROCESS. */
	public static final String QUOTE_EXPIRED_EMAIL_PROCESS = "quote-expired-email-process";

	/** The Constant QUOTE_POST_CANCELLATION_PROCESS. */
	public static final String QUOTE_POST_CANCELLATION_PROCESS = "quote-post-cancellation-process";

	/** The Constant INVOICES_DATA_FLAG. */
	public static final String INVOICES_DATA_FLAG = "invoice.request.data.flag";

	/** The Constant GET_API_URL. */
	public static final String GET_API_URL = "muelsoft.api.url";

	public static final String JOINACCOUNT_API_URL = "joinaccount.muelsoft.api.url";

	/** The Constant GET_SEQIRUS_ORGANIZATION_NUMBER. */
	public static final String GET_SEQIRUS_ORGANIZATION_NUMBER = "seqirus.organization.number";

	/** The Constant DEFAULT_ORGANIZATION_NUMBER. */
	public static final String DEFAULT_ORGANIZATION_NUMBER = "0603";

	public static final String ORDER_STATUS_ALL = "orderStatus";

	/** The Constant CUST_NUMBER_KEY. */
	public static final String CUST_NUMBER_KEY = "customerId";

	/** The Constant FROM_DATE_KEY. */
	public static final String FROM_DATE_KEY = "fromDate";

	/** The Constant TO_DATE_KEY. */
	public static final String TO_DATE_KEY = "toDate";

	/** The Constant RETURNS_KEY. */
	public static final String RETURNS_KEY = "returns";

	/** The Constant API_CERTIFICATE_LOCATION. */
	public static final String API_CERTIFICATE_LOCATION = "sap.certification.loc";

	/** The Constant QUESTION_MARK. */
	public static final String QUESTION_MARK = "?";

	/** The Constant AMPERSAND. */
	public static final String AMPERSAND = "&";

	/** The Constant EQUAL_SIGN. */
	public static final String EQUAL_SIGN = "=";

	/** The Constant SLASH_SIGN. */
	public static final String SLASH_SIGN = "/";

	/** The Constant JSON. */
	public static final String JSON = "JSON";

	/** The Constant BYTE. */
	public static final String BYTE = "BYTE";

	/** The Constant API_RESPONSE. */
	public static final String API_RESPONSE = "APIRESPONSE";

	/** The Constant FILE_RESPONSE. */
	public static final String FILE_RESPONSE = "FILERESPONSE";

	/** The Constant DOCUMENTS. */
	public static final String DOCUMENTS = "documents";

	/** The Constant INVOICDETAILS. */
	public static final String INVOICDETAILS = "InvoiceDetails";

	/** The Constant REQUEST_GET. */
	public static final String REQUEST_GET = "GET";

	/** The Constant DEFAULT_SEASONENTRY. */
	public static final String DEFAULT_SEASONENTRY = "seqirus.default.season";

	/** The Constant DEFAULT_SEASONENTRY. */
	public static final String ASHFIELD_PRESENCE = "seqirus.ashfield.presence";

	public static final String ASHFIELD_ALLOWED_SEASONS = "seqirus.ashfield.allowed.seasons";

	/** The Constant CLIENT_SECRET. */
	public static final String CLIENT_SECRET = "client_secret";

	/** The Constant CLIENT_ID. */
	public static final String CLIENT_ID = "client_id";

	/** The Constant CONTENT_TYPE_KEY. */
	public static final String CONTENT_TYPE_KEY = "Content-Type";

	/** The Constant ACCEPT_TYPE_KEY. */
	public static final String ACCEPT_TYPE_KEY = "Accept";

	/** The Constant CONTENT_TYPE. */
	public static final String CONTENT_TYPE = "application/json";

	/** The Constant API_JKS. */
	public static final String API_JKS = "JKS";

	/** The Constant API_CHANGEIT. */
	public static final String API_CHANGEIT = "changeit";

	/** The Constant API_TLS. */
	public static final String API_TLS = "TLS";

	/** The Constant CLIENT_ID_VALUE. */
	public static final String CLIENT_ID_VALUE = "8edee0d5f7544799835710937c82cf22";

	/** The Constant CLIENT_SECRET_VALUE. */
	public static final String CLIENT_SECRET_VALUE = "d35A22b0796c4dC6a895b0824784F7aB";

	public static final String SHIPPING_ADDRESS = "ShippingAddress";
	public static final String BILLING_ADDRESS = "BillingAddress";
	public static final String PAY_ADDRESS = "PayAddress";

	public static final String ZIPCODE_VALIDATION = "zipcode.validation.check";

	public static final String ZIPCODE = "zipCode";

	public static final String ZIPCODEVALIDATION = "zipCodeValidationRequired";
}
