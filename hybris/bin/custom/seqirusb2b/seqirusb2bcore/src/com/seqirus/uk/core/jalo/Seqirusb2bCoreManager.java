/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.uk.core.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.seqirus.uk.core.constants.Seqirusb2bCoreConstants;
import com.seqirus.uk.core.setup.CoreSystemSetup;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 * 
 */
public class Seqirusb2bCoreManager extends GeneratedSeqirusb2bCoreManager
{
	public static final Seqirusb2bCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (Seqirusb2bCoreManager) em.getExtension(Seqirusb2bCoreConstants.EXTENSIONNAME);
	}
}
