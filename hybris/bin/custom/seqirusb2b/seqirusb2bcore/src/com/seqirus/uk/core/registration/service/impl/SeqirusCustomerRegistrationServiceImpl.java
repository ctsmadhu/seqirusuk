/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.uk.core.registration.service.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.consent.dao.ConsentDao;
import de.hybris.platform.commerceservices.model.consent.ConsentModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.seqirus.core.model.SeasonEntryModel;
import com.seqirus.core.model.SeqirusB2BTempCustomerModel;
import com.seqirus.uk.core.constants.Seqirusb2bCoreConstants;
import com.seqirus.uk.core.dataObjects.JoinAccountAPIResponse;
import com.seqirus.uk.core.dataObjects.RequestParameters;
import com.seqirus.uk.core.registration.dao.SeqirusCustomerRegistrationDao;
import com.seqirus.uk.core.registration.service.SeqirusCustomerRegistrationService;
import com.seqirus.uk.core.services.SeqirusAPIService;


/**
 * @author 845332
 *
 */
public class SeqirusCustomerRegistrationServiceImpl extends SeqirusAPIService implements SeqirusCustomerRegistrationService
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(SeqirusCustomerRegistrationServiceImpl.class);

	private SeqirusCustomerRegistrationDao seqirusCustomerRegistrationDao;

	@Resource
	private ConsentDao consentDao;

	@Resource
	SearchRestrictionService searchRestrictionService;

	/** The configuration service. */
	@Autowired
	protected ConfigurationService configurationService;

	/** The certificate path. */
	protected String certificatePath;



	@Override
	public B2BUnitModel getOrgNameByB2BUnit(final String b2bUnitId)
	{//Call retrieve B2B Unit name from  result object
	 //System.out.println("account value in serviceimpl " + b2bUnitId);
		B2BUnitModel b2bunitmodel = null;
		if (null != b2bUnitId)
		{
			//System.out.println("account value inside method of serviceimpl " + b2bUnitId);

			searchRestrictionService.disableSearchRestrictions();
			b2bunitmodel = seqirusCustomerRegistrationDao.getPartnerOrgByB2BUnit(b2bUnitId);
			searchRestrictionService.enableSearchRestrictions();

		}
		return b2bunitmodel;
	}

	@Override
	public CountryModel getCountryByName(final String countryName)
	{
		if (StringUtils.isNotBlank(countryName))
		{
			return seqirusCustomerRegistrationDao.getCountryByName(countryName);
		}
		return null;
	}

	@Override
	public SeqirusB2BTempCustomerModel fetchCustModel(final String uid)
	{
		return seqirusCustomerRegistrationDao.fetchCustModel(uid);
	}

	@Override
	public SeasonEntryModel getSeasonEntry()
	{
		return seqirusCustomerRegistrationDao.seasonEntry();

	}


	protected SeqirusCustomerRegistrationDao getSeqirusCustomerRegistrationDao()
	{
		return seqirusCustomerRegistrationDao;
	}

	@Required
	public void setSeqirusCustomerRegistrationDao(final SeqirusCustomerRegistrationDao seqirusCustomerRegistrationDao)
	{
		this.seqirusCustomerRegistrationDao = seqirusCustomerRegistrationDao;
	}

	/**
	 * @return the searchRestrictionService
	 */
	public SearchRestrictionService getSearchRestrictionService()
	{
		return searchRestrictionService;
	}

	/**
	 * @param searchRestrictionService
	 *           the searchRestrictionService to set
	 */
	public void setSearchRestrictionService(final SearchRestrictionService searchRestrictionService)
	{
		this.searchRestrictionService = searchRestrictionService;
	}

	@Override
	public List<ConsentModel> fetchConstModel(final CustomerModel customer)
	{
		return consentDao.findAllConsentsByCustomer(customer);
	}

	@Override
	public List<B2BCustomerModel> getAllCustomers()
	{
		return seqirusCustomerRegistrationDao.getAllCustomers();
	}

	@Override
	public JoinAccountAPIResponse getCustomerData(final String accountNumber, final String zipCode)
	{
		final String zipCodeValidation = configurationService.getConfiguration()
				.getString(Seqirusb2bCoreConstants.ZIPCODE_VALIDATION);
		final Map<String, String> parameter = new HashMap<>();
		parameter.put(Seqirusb2bCoreConstants.ZIPCODE, zipCode);
		if (StringUtils.isNotBlank(zipCode))
		{
			parameter.put(Seqirusb2bCoreConstants.ZIPCODEVALIDATION, "true");
		}
		else
		{
			parameter.put(Seqirusb2bCoreConstants.ZIPCODEVALIDATION, "false");
		}
		certificatePath = configurationService.getConfiguration()
				.getString(Seqirusb2bCoreConstants.API_CERTIFICATE_LOCATION);
		return (JoinAccountAPIResponse) processRequest(
				new RequestParameters(parameter, prepareJoinAccountAPIURL(parameter, accountNumber), StringUtils.EMPTY,
						Seqirusb2bCoreConstants.REQUEST_GET, certificatePath, Seqirusb2bCoreConstants.API_RESPONSE),
				JoinAccountAPIResponse.class);
	}

	/**
	 * @param parameter
	 * @param accountNumber
	 * @return
	 */
	private String prepareJoinAccountAPIURL(final Map<String, String> parameter, final String accountNumber)
	{
		final String endpointURL = configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.JOINACCOUNT_API_URL);
		final String organizationId = configurationService.getConfiguration()
				.getString(Seqirusb2bCoreConstants.GET_SEQIRUS_ORGANIZATION_NUMBER);
		final StringBuilder sb = new StringBuilder();
		sb.append(endpointURL).append(accountNumber).append(Seqirusb2bCoreConstants.SLASH_SIGN).append(organizationId);
		if (!parameter.isEmpty())
		{
			sb.append(Seqirusb2bCoreConstants.QUESTION_MARK);
		}
		String ampChar = "";
		for (final String s : parameter.keySet())
		{
			sb.append(ampChar).append(s).append(Seqirusb2bCoreConstants.EQUAL_SIGN).append(parameter.get(s));
			ampChar = Seqirusb2bCoreConstants.AMPERSAND;
		}
		return sb.toString();
	}

	@Override
	public List<B2BCustomerModel> getWelcomeEmailCustomers()
	{
		return seqirusCustomerRegistrationDao.getWelcomeEmailCustomers();
	}

	/**
	 * Gets the certificate path.
	 *
	 * @return the certificatePath
	 */
	public String getCertificatePath()
	{
		return certificatePath;
	}

	/**
	 * Sets the certificate path.
	 *
	 * @param certificatePath
	 *           the certificatePath to set
	 */
	public void setCertificatePath(final String certificatePath)
	{
		this.certificatePath = certificatePath;
	}



}

/*
 * @Override public B2BUnitModel getOrgNameByB2BUnit(final String b2bUnitId) {//Call retrieve B2B Unit name from result
 * object System.out.println("account value in serviceimpl " + b2bUnitId); if (null != b2bUnitId) {
 * System.out.println("account value inside method of serviceimpl " + b2bUnitId);
 *
 * searchRestrictionService.disableSearchRestrictions(); B2BUnitModel a=
 * seqirusCustomerRegistrationDao.getPartnerOrgByB2BUnit(b2bUnitId);
 * searchRestrictionService.enableSearchRestrictions(); //return a; } return null; }
 */
