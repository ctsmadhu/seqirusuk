/**
 *
 */
package com.seqirus.uk.core.dataObjects;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * The Class ReturnsAndCreditsListResponse.
 *
 * @author Avaneesh Chauhan
 */
public class ReturnsAndCreditsListResponse
{

	/** The invoice number. */
	@JsonProperty(value = "invoiceNumber")
	public String invoiceNumber;

	/** The invoice type. */
	@JsonProperty(value = "invoiceType")
	public String invoiceType;

	/** The invoice type description. */
	@JsonProperty(value = "invoiceTypeDescription")
	public String invoiceTypeDescription;

	/** The sales item. */
	@JsonProperty(value = "salesItem")
	public String salesItem;

	/** The amout with tax. */
	@JsonProperty(value = "amoutWithTax")
	public double amoutWithTax;

	/** The invoice date. */
	@JsonProperty(value = "invoiceDate")
	public String invoiceDate;

	/** The due date. */
	@JsonProperty(value = "dueDate")
	public String dueDate;

	/** The clearing date. */
	@JsonProperty(value = "clearingDate")
	public String clearingDate;

	/** The currency. */
	@JsonProperty(value = "currency")
	public String currency;

	/** The status. */
	@JsonProperty(value = "status")
	public String status;

	/**
	 * Gets the invoice number.
	 *
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}

	/**
	 * Sets the invoice number.
	 *
	 * @param invoiceNumber
	 *           the invoiceNumber to set
	 */
	public void setInvoiceNumber(final String invoiceNumber)
	{
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * Gets the invoice type.
	 *
	 * @return the invoiceType
	 */
	public String getInvoiceType()
	{
		return invoiceType;
	}

	/**
	 * Sets the invoice type.
	 *
	 * @param invoiceType
	 *           the invoiceType to set
	 */
	public void setInvoiceType(final String invoiceType)
	{
		this.invoiceType = invoiceType;
	}

	/**
	 * Gets the invoice type description.
	 *
	 * @return the invoiceTypeDescription
	 */
	public String getInvoiceTypeDescription()
	{
		return invoiceTypeDescription;
	}

	/**
	 * Sets the invoice type description.
	 *
	 * @param invoiceTypeDescription
	 *           the invoiceTypeDescription to set
	 */
	public void setInvoiceTypeDescription(final String invoiceTypeDescription)
	{
		this.invoiceTypeDescription = invoiceTypeDescription;
	}

	/**
	 * Gets the sales item.
	 *
	 * @return the salesItem
	 */
	public String getSalesItem()
	{
		return salesItem;
	}

	/**
	 * Sets the sales item.
	 *
	 * @param salesItem
	 *           the salesItem to set
	 */
	public void setSalesItem(final String salesItem)
	{
		this.salesItem = salesItem;
	}

	/**
	 * Gets the invoice date.
	 *
	 * @return the invoiceDate
	 */
	public String getInvoiceDate()
	{
		return invoiceDate;
	}

	/**
	 * Sets the invoice date.
	 *
	 * @param invoiceDate
	 *           the invoiceDate to set
	 */
	public void setInvoiceDate(final String invoiceDate)
	{
		this.invoiceDate = invoiceDate;
	}

	/**
	 * Gets the due date.
	 *
	 * @return the dueDate
	 */
	public String getDueDate()
	{
		return dueDate;
	}

	/**
	 * Sets the due date.
	 *
	 * @param dueDate
	 *           the dueDate to set
	 */
	public void setDueDate(final String dueDate)
	{
		this.dueDate = dueDate;
	}

	/**
	 * Gets the clearing date.
	 *
	 * @return the clearingDate
	 */
	public String getClearingDate()
	{
		return clearingDate;
	}

	/**
	 * Sets the clearing date.
	 *
	 * @param clearingDate
	 *           the clearingDate to set
	 */
	public void setClearingDate(final String clearingDate)
	{
		this.clearingDate = clearingDate;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public String getCurrency()
	{
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency
	 *           the currency to set
	 */
	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *           the status to set
	 */
	public void setStatus(final String status)
	{
		this.status = status;
	}

	/**
	 * Gets the amout with tax.
	 *
	 * @return the amoutWithTax
	 */
	public double getAmoutWithTax()
	{
		return amoutWithTax;
	}

	/**
	 * Sets the amout with tax.
	 *
	 * @param amoutWithTax
	 *           the amoutWithTax to set
	 */
	public void setAmoutWithTax(final double amoutWithTax)
	{
		this.amoutWithTax = amoutWithTax;
	}

}
