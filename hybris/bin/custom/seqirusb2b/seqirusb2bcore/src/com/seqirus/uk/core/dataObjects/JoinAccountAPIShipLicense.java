/**
 *
 */
package com.seqirus.uk.core.dataObjects;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author 700196
 *
 */
public class JoinAccountAPIShipLicense
{
	@JsonProperty(value = "LicenseNumber")
	public String licenseNumber;
	@JsonProperty(value = "Name")
	public String licenseName;
	@JsonProperty(value = "StateofIssueLicense")
	public String stateOfIssue;
	@JsonProperty(value = "ExpirationDate")
	public String expirationDate;
	@JsonProperty(value = "AddressLine_1")
	public String addressLine1;
	@JsonProperty(value = "AddressLine_2")
	public String addressLine2;
	@JsonProperty(value = "City")
	public String city;
	@JsonProperty(value = "State")
	public String state;
	@JsonProperty(value = "ZipCode")
	public String zipCode;

	/**
	 * @return the licenseNumber
	 */
	public String getLicenseNumber()
	{
		return licenseNumber;
	}

	/**
	 * @param licenseNumber
	 *           the licenseNumber to set
	 */
	public void setLicenseNumber(final String licenseNumber)
	{
		this.licenseNumber = licenseNumber;
	}

	/**
	 * @return the licenseName
	 */
	public String getLicenseName()
	{
		return licenseName;
	}

	/**
	 * @param licenseName
	 *           the licenseName to set
	 */
	public void setLicenseName(final String licenseName)
	{
		this.licenseName = licenseName;
	}

	/**
	 * @return the stateOfIssue
	 */
	public String getStateOfIssue()
	{
		return stateOfIssue;
	}

	/**
	 * @param stateOfIssue
	 *           the stateOfIssue to set
	 */
	public void setStateOfIssue(final String stateOfIssue)
	{
		this.stateOfIssue = stateOfIssue;
	}

	/**
	 * @return the expirationDate
	 */
	public String getExpirationDate()
	{
		return expirationDate;
	}

	/**
	 * @param expirationDate
	 *           the expirationDate to set
	 */
	public void setExpirationDate(final String expirationDate)
	{
		this.expirationDate = expirationDate;
	}

	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1()
	{
		return addressLine1;
	}

	/**
	 * @param addressLine1
	 *           the addressLine1 to set
	 */
	public void setAddressLine1(final String addressLine1)
	{
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2()
	{
		return addressLine2;
	}

	/**
	 * @param addressLine2
	 *           the addressLine2 to set
	 */
	public void setAddressLine2(final String addressLine2)
	{
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *           the city to set
	 */
	public void setCity(final String city)
	{
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * @param state
	 *           the state to set
	 */
	public void setState(final String state)
	{
		this.state = state;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode()
	{
		return zipCode;
	}

	/**
	 * @param zipCode
	 *           the zipCode to set
	 */
	public void setZipCode(final String zipCode)
	{
		this.zipCode = zipCode;
	}



}
