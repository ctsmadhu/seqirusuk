/**
 *
 */
package com.seqirus.uk.core.invoice.service.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.seqirus.uk.core.constants.Seqirusb2bCoreConstants;
import com.seqirus.uk.core.dataObjects.InvoiceDetailsResponse;
import com.seqirus.uk.core.dataObjects.InvoiceLandingRequest;
import com.seqirus.uk.core.dataObjects.InvoiceLandingResponse;
import com.seqirus.uk.core.dataObjects.RequestParameters;
import com.seqirus.uk.core.dataObjects.ReturnsAndCreditsRequest;
import com.seqirus.uk.core.dataObjects.ReturnsAndCreditsResponse;
import com.seqirus.uk.core.exceptions.SeqirusCustomException;
import com.seqirus.uk.core.invoice.service.SeqirusInvoiceService;
import com.seqirus.uk.core.services.SeqirusAPIService;


/**
 * The Class SeqirusInvoiceServiceImpl.
 *
 * @author 700196
 */
public class SeqirusInvoiceServiceImpl extends SeqirusAPIService implements SeqirusInvoiceService
{

	private static final Logger LOGGER = Logger.getLogger(SeqirusInvoiceServiceImpl.class);
	/** The configuration service. */
	@Autowired
	protected ConfigurationService configurationService;

	/** The certificate path. */
	protected String certificatePath;


	/**
	 * Retrieve invoices list.
	 *
	 * @param request
	 *           the request
	 * @return InvoiceLandingResponse
	 * @throws SeqirusCustomException
	 *            the seqirus custom exception
	 */
	@Override
	public InvoiceLandingResponse retrieveInvoicesList(final InvoiceLandingRequest request) throws SeqirusCustomException
	{
		final Map<String, String> parameter = new HashMap<>();
		parameter.put(Seqirusb2bCoreConstants.CUST_NUMBER_KEY, request.getCustomerNumber());
		parameter.put(Seqirusb2bCoreConstants.FROM_DATE_KEY, request.getFromDate());
		parameter.put(Seqirusb2bCoreConstants.TO_DATE_KEY, request.getToDate());
		parameter.put(Seqirusb2bCoreConstants.RETURNS_KEY, StringUtils.EMPTY);
		certificatePath = configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.API_CERTIFICATE_LOCATION);
		return (InvoiceLandingResponse) processRequest(
				new RequestParameters(parameter, prepareInvoiceLandingURL(parameter), StringUtils.EMPTY,
						Seqirusb2bCoreConstants.REQUEST_GET, certificatePath, Seqirusb2bCoreConstants.API_RESPONSE),
				InvoiceLandingResponse.class);
	}


	/**
	 * Gets the returns and credits list.
	 *
	 * @param requestPayload
	 *           the request payload
	 * @return the returns and credits list
	 * @throws SeqirusCustomException
	 *            the seqirus custom exception
	 */
	@Override
	public ReturnsAndCreditsResponse getReturnsAndCreditsList(final ReturnsAndCreditsRequest requestPayload)
			throws SeqirusCustomException
	{
		final Map<String, String> parameter = new HashMap<>();
		parameter.put(Seqirusb2bCoreConstants.CUST_NUMBER_KEY, requestPayload.getCustomerNumber());
		parameter.put(Seqirusb2bCoreConstants.FROM_DATE_KEY, requestPayload.getFromDate());
		parameter.put(Seqirusb2bCoreConstants.TO_DATE_KEY, requestPayload.getToDate());
		parameter.put(Seqirusb2bCoreConstants.RETURNS_KEY, requestPayload.getReturns());
		certificatePath = configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.API_CERTIFICATE_LOCATION);
		return (ReturnsAndCreditsResponse) processRequest(
				new RequestParameters(parameter, prepareInvoiceLandingURL(parameter), StringUtils.EMPTY,
						Seqirusb2bCoreConstants.REQUEST_GET, certificatePath, Seqirusb2bCoreConstants.API_RESPONSE),
				ReturnsAndCreditsResponse.class);
	}



	/**
	 * Retrieve invoice details.
	 *
	 * @param invoiceNumber
	 *           the invoice number
	 * @return InvoiceDetailsResponse
	 * @throws SeqirusCustomException
	 *            the seqirus custom exception
	 */
	@Override
	public InvoiceDetailsResponse retrieveInvoiceDetails(final String invoiceNumber) throws SeqirusCustomException
	{
		certificatePath = configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.API_CERTIFICATE_LOCATION);
		return (InvoiceDetailsResponse) processRequest(
				new RequestParameters(null, prepareRequestURL(invoiceNumber, Seqirusb2bCoreConstants.INVOICDETAILS),
						StringUtils.EMPTY, Seqirusb2bCoreConstants.REQUEST_GET, certificatePath, Seqirusb2bCoreConstants.API_RESPONSE),
				InvoiceDetailsResponse.class);
	}

	/**
	 * Implemented INvoice download API call.
	 *
	 * @param invoiceNumber
	 *           the invoice number
	 * @return InputStream
	 * @throws SeqirusCustomException
	 *            the seqirus custom exception
	 */
	public InputStream downloadInvoice(final String invoiceNumber) throws SeqirusCustomException
	{
		certificatePath = configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.API_CERTIFICATE_LOCATION);
		final RequestParameters input = new RequestParameters(null, prepareRequestURL(invoiceNumber, StringUtils.EMPTY),
				invoiceNumber + ".pdf", null, certificatePath, Seqirusb2bCoreConstants.API_RESPONSE);
		input.setResponseFormat(Seqirusb2bCoreConstants.BYTE);
		final byte[] b = (byte[]) processRequest(input, null);
		return new ByteArrayInputStream(b);
	}

	/**
	 * Prepare invoice landing URL.
	 *
	 * @param parameter
	 *           the parameter
	 * @return String
	 */
	private String prepareInvoiceLandingURL(final Map<String, String> parameter)
	{
		final String endpointURL = configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.GET_API_URL);
		final String organizationId = configurationService.getConfiguration()
				.getString(Seqirusb2bCoreConstants.GET_SEQIRUS_ORGANIZATION_NUMBER);
		final StringBuilder sb = new StringBuilder();
		sb.append(endpointURL).append(organizationId);
		if (!parameter.isEmpty())
		{
			sb.append(Seqirusb2bCoreConstants.QUESTION_MARK);
		}
		String ampChar = "";
		for (final String s : parameter.keySet())
		{
			sb.append(ampChar).append(s).append(Seqirusb2bCoreConstants.EQUAL_SIGN).append(parameter.get(s));
			ampChar = Seqirusb2bCoreConstants.AMPERSAND;
		}
		LOGGER.info("Invoice Landing URL..." + sb.toString());
		return sb.toString();
	}

	/**
	 * Prepare request URL.
	 *
	 * @param invoiceNumber
	 *           the invoice number
	 * @param type
	 *           the type
	 * @return String
	 */
	private String prepareRequestURL(final String invoiceNumber, final String type)
	{
		final String endpointURL = configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.GET_API_URL);
		final String organizationId = configurationService.getConfiguration()
				.getString(Seqirusb2bCoreConstants.GET_SEQIRUS_ORGANIZATION_NUMBER);
		final StringBuilder sb = new StringBuilder();
		if (type.equals("InvoiceDetails"))
		{
			sb.append(endpointURL).append(organizationId).append(Seqirusb2bCoreConstants.SLASH_SIGN).append(invoiceNumber);
		}
		else
		{
			sb.append(endpointURL).append(organizationId).append(Seqirusb2bCoreConstants.SLASH_SIGN).append(invoiceNumber)
					.append(Seqirusb2bCoreConstants.SLASH_SIGN).append(Seqirusb2bCoreConstants.DOCUMENTS);
		}
		return sb.toString();
	}

	/**
	 * Gets the certificate path.
	 *
	 * @return the certificatePath
	 */
	public String getCertificatePath()
	{
		return certificatePath;
	}

	/**
	 * Sets the certificate path.
	 *
	 * @param certificatePath
	 *           the certificatePath to set
	 */
	public void setCertificatePath(final String certificatePath)
	{
		this.certificatePath = certificatePath;
	}


}
