package com.seqirus.core.dataimport.batch.task;

import de.hybris.platform.acceleratorservices.dataimport.batch.converter.ImpexRowFilter;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 *
 * This file SeqirusBlankMandatoryFeildImpexRowFilter.java is used for row skip for blank/ Invalid values in the feeds.
 *
 */
public class SeqirusBlankMandatoryFeildImpexRowFilter implements ImpexRowFilter
{
	private static final Logger LOG = Logger.getLogger(SeqirusBlankMandatoryFeildImpexRowFilter.class);

	private String mandatoryFeildRowIndex;
	private String type;


	/**
	 * this method is used for validate mandatory field data..
	 *
	 * @param row
	 * @return boolean
	 */
	@Override
	public boolean filter(final Map<Integer, String> row)
	{
		boolean isValid = true;
		final Integer mandatoryFeildIndex = Integer.valueOf(mandatoryFeildRowIndex);
		final String mandatoryFeildValue = row.get(mandatoryFeildIndex);

		if (StringUtils.isEmpty(mandatoryFeildValue))
		{
			LOG.info("feild value is blank for itemType::" + type);
			isValid = false;
		}

		return isValid;
	}


	/**
	 * @return the mandatoryFeildRowIndex
	 */
	public String getMandatoryFeildRowIndex()
	{
		return mandatoryFeildRowIndex;
	}

	/**
	 * @param mandatoryFeildRowIndex
	 *           the mandatoryFeildRowIndex to set
	 */
	@Required
	public void setMandatoryFeildRowIndex(final String mandatoryFeildRowIndex)
	{
		this.mandatoryFeildRowIndex = mandatoryFeildRowIndex;
	}

	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}


	/**
	 * @param type
	 *           the type to set
	 */
	@Required
	public void setType(final String type)
	{
		this.type = type;
	}


}
