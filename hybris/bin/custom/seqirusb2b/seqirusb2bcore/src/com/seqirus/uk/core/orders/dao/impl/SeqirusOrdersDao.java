/**
 *
 */
package com.seqirus.uk.core.orders.dao.impl;

import com.seqirus.core.model.SeasonEntryModel;


/**
 * @author 614269
 *
 */
public interface SeqirusOrdersDao
{
	SeasonEntryModel seasonEntry();
}
