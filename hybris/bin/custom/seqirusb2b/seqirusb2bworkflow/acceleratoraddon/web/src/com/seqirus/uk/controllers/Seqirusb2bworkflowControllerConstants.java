/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.uk.controllers;

public interface Seqirusb2bworkflowControllerConstants
{
	// implement here controller constants used by this extension
}
