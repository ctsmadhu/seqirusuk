/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.uk.facades.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.workflow.WorkflowTemplateService;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.seqirus.core.model.SeqirusB2BTempCustomerModel;
import com.seqirus.uk.constants.Seqirusb2bworkflowConstants;
import com.seqirus.uk.data.B2BRegistrationData;
import com.seqirus.uk.exceptions.CustomerAlreadyExistsException;
import com.seqirus.uk.facades.B2BRegistrationFacade;
import com.seqirus.uk.facades.B2BRegistrationWorkflowFacade;
import com.seqirus.uk.model.B2BRegistrationModel;
import com.seqirus.uk.services.B2BRegistrationService;


/**
 * Default implementation of {@link B2BRegistrationFacade}
 */
public class DefaultB2BRegistrationFacade implements B2BRegistrationFacade
{

	private static final Logger LOG = Logger.getLogger(DefaultB2BRegistrationFacade.class);

	private CMSSiteService cmsSiteService;

	private CommonI18NService commonI18NService;

	private ModelService modelService;

	private BaseStoreService baseStoreService;

	private UserService userService;

	private B2BRegistrationWorkflowFacade b2bRegistrationWorkflowFacade;

	private WorkflowTemplateService workflowTemplateService;

	private B2BRegistrationService b2bRegistrationService;


	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	/**
	 * @param cmsSiteService
	 *           the cmsSiteService to set
	 */
	@Required
	public void setCmsSiteService(final CMSSiteService cmsSiteService)
	{
		this.cmsSiteService = cmsSiteService;
	}

	/**
	 * @param commonI18NService
	 *           the commonI18NService to set
	 */
	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @param b2bRegistrationWorkflowFacade
	 *           the b2bRegistrationWorkflowFacade to set
	 */
	@Required
	public void setB2bRegistrationWorkflowFacade(final B2BRegistrationWorkflowFacade b2bRegistrationWorkflowFacade)
	{
		this.b2bRegistrationWorkflowFacade = b2bRegistrationWorkflowFacade;
	}

	/**
	 * @param workflowTemplateService
	 *           the workflowTemplateService to set
	 */
	@Required
	public void setWorkflowTemplateService(final WorkflowTemplateService workflowTemplateService)
	{
		this.workflowTemplateService = workflowTemplateService;
	}

	/**
	 * @return the b2bRegistrationService
	 */
	public B2BRegistrationService getB2bRegistrationService()
	{
		return b2bRegistrationService;
	}

	/**
	 * @param b2bRegistrationService
	 *           the b2bRegistrationService to set
	 */
	@Required
	public void setB2bRegistrationService(final B2BRegistrationService b2bRegistrationService)
	{
		this.b2bRegistrationService = b2bRegistrationService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.seqirus.uk.facades.B2BRegistrationFacade#register(com.seqirus.uk .data .B2BRegistrationData)
	 */
	@Override
	public void register(final B2BRegistrationData data) throws CustomerAlreadyExistsException
	{

		final Transaction tx = Transaction.current();
		tx.begin();

		boolean success = false;

		try
		{

			// Check if a user using the same email exist, if so we need to abort the current operation!
			/*
			 * final boolean userExists = userService.isUserExisting(data.getEmail()); if (userExists) { if
			 * (LOG.isDebugEnabled()) { LOG.debug(String.format("user with uid '%s' already exists!", data.getEmail())); }
			 * throw new CustomerAlreadyExistsException(String.format("User with uid '%s' already exists!",
			 * data.getEmail())); }
			 */

			// Save the registration model so that it is accessible to the workflow actions. The registration model will be deleted as part of the cleanup
			// of the workflow.
			final B2BRegistrationModel registration = toRegistrationModel(data);
			modelService.save(registration);

			// Save the customer. At this point, the customer is saved to generate emails and initiate the workflow. This customer will be deleted as part of the
			// cleanup of the workflow IF he is rejected. If approved, the customer will be deleted by the "approve workflow action" and will be re-created
			// as a B2BCustomer and assigned to the proper B2BUnit. At this stage, we can't create a B2BCustomer since we don't even have a B2BUnit (organization!).
			//final CustomerModel customer = toCustomerModel(data);
			//modelService.save(customer);
			final String workflowName = data.getWorkflowName();
			final WorkflowTemplateModel workflowTemplate = workflowTemplateService.getWorkflowTemplateForCode(workflowName);

			if (LOG.isDebugEnabled())
			{
				LOG.debug(String.format("Created WorkflowTemplateModell using name '%s'", workflowName));
			}

			// Start the workflow
			b2bRegistrationWorkflowFacade.launchWorkflow(workflowTemplate, registration);

			tx.commit();
			success = true;

		}
		finally
		{
			if (!success)
			{
				tx.rollback();
			}
		}

	}

	/**
	 * Converts a {@link B2BRegistrationData} into a {@link CustomerModel}. Only keeps the most important fields to
	 * generate emails, the rest is ignored as this customer is to be deleted as part of the workflow execution
	 *
	 * @param data
	 *           The registration data
	 * @return An unsaved instance of {@link CustomerModel}
	 */
	protected CustomerModel toCustomerModel(final B2BRegistrationData data)
	{

		final CustomerModel model = modelService.create(CustomerModel.class);

		model.setName(WordUtils.capitalizeFully(data.getName()));
		model.setUid(data.getEmail());
		model.setLoginDisabled(true);
		model.setSessionLanguage(commonI18NService.getCurrentLanguage());
		model.setSessionCurrency(commonI18NService.getCurrentCurrency());

		// Title is optional
		/*
		 * if (StringUtils.isNotBlank(data.getTitleCode())) { final TitleModel title =
		 * userService.getTitleForCode(data.getTitleCode()); model.setTitle(title); }
		 */

		return model;

	}

	/**
	 * Converts a {@link B2BRegistrationData} into a {@B2BRegistrationModel}
	 *
	 * @param data
	 *           The registration data
	 * @return An unsaved instance of type {@B2BRegistrationModel}
	 */
	protected B2BRegistrationModel toRegistrationModel(final B2BRegistrationData data)
	{

		final B2BRegistrationModel model = modelService.create(B2BRegistrationModel.class);
		//Add Customer Information from logged in user
		final UserModel currentUser = userService.getCurrentUser();
		if (currentUser != null && !userService.isAnonymousUser(currentUser))
		{
			final B2BCustomerModel customer = (B2BCustomerModel) currentUser;
			model.setEmail(customer.getUid());
			model.setName(customer.getName());
			model.setTelephone(customer.getPhone());
			model.setTelephoneExtension(customer.getPhoneExt());
			model.setPosition(customer.getJobTitle());
		}

		final String workFlowName = data.getWorkflowName();
		final B2BUnitModel b2bUnit = null;
		if (StringUtils.equalsIgnoreCase(workFlowName, Seqirusb2bworkflowConstants.Workflows.JOIN_ACCOUNT_WORKFLOW))
		{
			//Join Account - Get from Frontend
			model.setAccountNumber(data.getAccountNumber());
			//model.setCompanyName(data.getCompanyName());
			model.setCompanyAddressPostalCode(data.getCompanyAddressPostalCode());
			model.setAccessCode(data.getAccessCode());
			//model.setReferralEmail(data.getReferralEmail());
		}
		else
		{
			// New Account - Fetch from registration model
			final List<SeqirusB2BTempCustomerModel> regModels = b2bRegistrationService
					.getRegistrationDetailsOfCustomer(data.getEmail());
			SeqirusB2BTempCustomerModel regModel = null;
			if (CollectionUtils.isNotEmpty(regModels))
			{
				regModel = regModels.get(0);
				if (null != regModel)
				{
					if (null != regModel.getCompanyInfo())
					{
						model.setCompanyInfo(regModel.getCompanyInfo());
					}
					if (null != regModel.getPayingAddress())
					{
						model.setPayingAddress(regModel.getPayingAddress());
					}
					if (null != regModel.getInvoiceAddress())
					{
						model.setInvoiceAddress(regModel.getInvoiceAddress());
					}
					if (CollectionUtils.isNotEmpty(regModel.getShippingAddress()))
					{
						model.setShippingAddress(regModel.getShippingAddress());
					}
				}
			}
		}
		// Get these from current context
		model.setBaseStore(baseStoreService.getCurrentBaseStore());
		model.setCmsSite(cmsSiteService.getCurrentSite());
		model.setCurrency(commonI18NService.getCurrentCurrency());
		model.setLanguage(commonI18NService.getCurrentLanguage());

		return model;

	}

	@Override
	public void updateAccount(final B2BRegistrationModel model, final String workflowName)
	{
		LOG.info("Triggering " + workflowName + " for " + model.getEmail());
		final Transaction tx = Transaction.current();
		tx.begin();

		boolean success = false;

		try
		{
			// Get these from current context
			model.setBaseStore(baseStoreService.getCurrentBaseStore());
			model.setCmsSite(cmsSiteService.getCurrentSite());
			model.setCurrency(commonI18NService.getCurrentCurrency());
			model.setLanguage(commonI18NService.getCurrentLanguage());
			modelService.save(model);
			final WorkflowTemplateModel workflowTemplate = workflowTemplateService.getWorkflowTemplateForCode(workflowName);

			if (LOG.isDebugEnabled())
			{
				LOG.debug(String.format("Created WorkflowTemplateModell using name '%s'", workflowName));
			}

			// Start the workflow
			b2bRegistrationWorkflowFacade.launchWorkflow(workflowTemplate, model);

			tx.commit();
			success = true;

		}
		finally
		{
			if (!success)
			{
				tx.rollback();
			}
		}
	}

}
