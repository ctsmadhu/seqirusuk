/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.uk.facades;

import com.seqirus.uk.data.B2BRegistrationData;
import com.seqirus.uk.exceptions.CustomerAlreadyExistsException;
import com.seqirus.uk.model.B2BRegistrationModel;


/**
 * Facade responsible for everything related to B2B registration
 */
public interface B2BRegistrationFacade
{

	/**
	 * Initiates the registration process for B2B. This method will first validate the submitted data, check if a user or
	 * a company to the given name already exists, persist the registration request (as a model) and initiate the
	 * workflow so that the registration request either gets approved OR rejected.
	 *
	 * @param data
	 *           The registration data
	 */
	public void register(B2BRegistrationData data) throws CustomerAlreadyExistsException;

	/**
	 * Initiates workflow so that the account update request either gets approved OR rejected.
	 * 
	 * @param model
	 * @param workflowName
	 */
	public void updateAccount(B2BRegistrationModel model, String workflowName);


}
