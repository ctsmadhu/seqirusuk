$(document).ready(function(){
    moment().format();

    
    $("#order-details-calendar").fullCalendar({
        header: {
            left: 'prev, title, next ',
            center: '',
            right: ''
        },
        views: {
            week: {
                titleFormat: 'MMMM',
            },
            month: {
                titleFormat: 'MMMM',
            },
            
        },
        
        //height: 'parent',
        //timeZone: 'Europe/Istanbul',
        fixedWeekCount: false,
        defaultDate: moment(),
        dayOfMonthFormat: 'ddd DD/MM',

        columnFormat: {
            month: 'dd', // Monday, Wednesday, etc
            
        },

        
        
        dayRender: function (date, cell) {
           
            var month = date.format("MMMM");
            var strFirstThree = month.substring(0,3);
            var day = date.format("D");
            if ($(window).width() < 767){
                cell.html("<div class='fcalen__month-name'>" + day + " " + strFirstThree + "</div>");
                
                
            } else {
                cell.html("<div class='fcalen__month-name'>" + day + " " + strFirstThree + "</div>");
            }
            $(window).resize(function() {
            if ($(window).width() < 767){
                cell.html("<div class='fcalen__month-name'>" + day + " " + strFirstThree + "</div>");
                
                
            } else {
                cell.html("<div class='fcalen__month-name'>" + day + " " + strFirstThree + "</div>");
            }

            });

            $(window).resize(function() {
                if ($(window).width() < 768){
                    $('.fc-day-header').each(function(i, obj) {
                        var mobday = $(this).text();
                        console.log(mobday);
                        $(this).text(mobday.substring(0,3));
                       
                    });
                    
                    
                } if ($(window).width() > 768){
                    $('.fc-day-header').each(function(i, obj) {
                        var mobday = $(this).text();
                        console.log(mobday);
                        $(this).text(mobday);
                       
                    });
                  }
    
            });

            if ($(window).width() > 768){
                $('.fc-day-header').each(function(i, obj) {
                    var mobday = $(this).text();
                    console.log(mobday);
                    $(this).text(mobday);
                   
                });
              }

              if ($(window).width() < 768){
                $('.fc-day-header').each(function(i, obj) {
                    var mobday = $(this).text();
                    console.log(mobday);
                    $(this).text(mobday.substring(0,3));
                   
                });
              }  
            
            var today = $.fullCalendar.moment().utc();
           // alert(today);
            // var end = $.fullCalendar.moment().add(21, 'days');
            // var prev = $.fullCalendar.moment().subtract(14, 'days');
            var td = today.day();
       
        if (td == 0){
        var weekStart = today.clone().startOf('isoWeek').add(1, 'isoWeek').isoWeekday(0);
        var weekEnd = today.clone().endOf('isoWeek').add(4, 'isoWeek').isoWeekday(6);
        }
       
        else{
        var weekStart = today.clone().startOf('isoWeek').subtract(1, 'days');
        var weekEnd = today.clone().endOf('isoWeek').add(3, 'isoWeek').subtract(1, 'days');
        }
            
            var prevWeek = today.clone().startOf('isoWeek').subtract(2, 'isoWeek').subtract(1, 'days');
            if (date >= weekStart && date <= weekEnd) {
                cell.addClass("active");
            }
            
            else {
                cell.addClass("non-active");
                cell.parent().parent().parent().parent().parent().addClass("no-event-show");
                
            }
        },
        /*Ui end remove comment Start*/
        events: [{
                title: '45 DOSES EXPECTED TO SHIP',
                start: '2020-11-20T01:00:00',
                end: '2020-11-26T23:00:00',
                description: '<table class="table table-striped table-condensed"><tr><td>Flucelvax</td><td>1000</td></tr><tr><td>Fluad</td><td>2000</td></tr><tr><td>Afluria</td><td>500</td></tr><tr><td>rapivab</td><td>200</td></tr></table>'
                },
            {
                // title  : 'delivered',
                start: '2020-12-04',
                className: "cal-delivered",
                icon: "check-circle-o" // Add here your icon name
              },
            {
                // title  : 'scheduled',
                start: '2020-12-18',
                className: "cal-delivered",
                icon: "arrow-circle-o-right" // Add here your icon name
                },
            {
                // title  : 'delivered',
                start: '2020-08-19',
                className: "cal-delivered",
                icon: "truck" // Add here your icon name
               },
            {
                // title  : 'expected delivery',
                start: '2020-08-12',
                className: "cal-delivered",
                icon: "truck" // Add here your icon name
                },
            {
                title: '500 UNITS EXPECTED TO SHIP',
                start: '2020-08-09T01:00:00',
                end: '2020-08-15T23:00:00',
                description: '<table class="table table-striped table-condensed"><tr><td>Flucelvax</td><td>1000</td></tr><tr><td>Fluad</td><td>2000</td></tr><tr><td>Afluria</td><td>500</td></tr></table>'
                }
                // more events here
            ],
        /*Ui end remove comment end*/
         /*Hybris end remove the comment
            events: getEvents(),*/
        eventRender: function (event, element) {
            if (event.icon) {
                element.find(".fc-title").prepend("<i class='fa fa-" + event.icon + "'></i>");
            }
            $(element).popover({
                title: function () {
                    return "<B>" + event.title + "</B>";
                },
                placement: 'top',
                html: true,
                container: 'body',
                /* optional */
                trigger: 'click',
                animation: 'true',
                content: function () {
                    return "<B>" + event.description + "</B>";
                },
                container: 'body'
            }).click(function (e) {
                $(this).addClass("fc-event-select");
                $(this).removeClass("fc-event-default fc-normal");
                $('.fc-event').not(this).addClass("fc-event-default");
                $('.fc-event').not(this).removeClass("fc-event-select");
                //$('.popover').not(this).hide(); /* optional, hide other popovers */
                $(this).popover('show'); /* show popover now it's setup */
                // e.preventDefault();
            });
        }
     });
    $('#calendar').fullCalendar({

        
        header: {
            left: 'prev, title, next ',
            center: '',
            right: ''
        },
        views: {
            week: {
                titleFormat: 'MMMM',
            },
            month: {
                titleFormat: 'MMMM',
            },
            
        },
       
        //height: 'parent',
        //timeZone: 'Europe/Istanbul',
        fixedWeekCount: false,
        defaultDate: moment(),
        dayOfMonthFormat: 'ddd DD/MM',

        columnFormat: {
            month: 'dd', // Monday, Wednesday, etc
            
        },

        
        
        dayRender: function (date, cell) {
           
            var month = date.format("MMMM");
            var strFirstThree = month.substring(0,3);
            var day = date.format("D");
            if ($(window).width() < 767){
                cell.html("<div class='fcalen__month-name'>" + day + " " + strFirstThree + "</div>");
                
                
            } else {
                cell.html("<div class='fcalen__month-name'>" + day + " " + strFirstThree + "</div>");
            }
            $(window).resize(function() {
            if ($(window).width() < 767){
                cell.html("<div class='fcalen__month-name'>" + day + " " + strFirstThree + "</div>");
                
                
            } else {
                cell.html("<div class='fcalen__month-name'>" + day + " " + strFirstThree + "</div>");
            }

            });

            $(window).resize(function() {
                if ($(window).width() < 768){
                    $('.fc-day-header').each(function(i, obj) {
                        var mobday = $(this).text();
                        console.log(mobday);
                        $(this).text(mobday.substring(0,3));
                       
                    });
                    
                    
                } if ($(window).width() > 768){
                    $('.fc-day-header').each(function(i, obj) {
                        var mobday = $(this).text();
                        console.log(mobday);
                        $(this).text(mobday);
                       
                    });
                  }
    
            });

            if ($(window).width() > 768){
                $('.fc-day-header').each(function(i, obj) {
                    var mobday = $(this).text();
                    console.log(mobday);
                    $(this).text(mobday);
                   
                });
              }

              if ($(window).width() < 768){
                $('.fc-day-header').each(function(i, obj) {
                    var mobday = $(this).text();
                    console.log(mobday);
                    $(this).text(mobday.substring(0,3));
                   
                });
              }  
            
            var today = $.fullCalendar.moment().utc();
           // alert(today);
            // var end = $.fullCalendar.moment().add(21, 'days');
            // var prev = $.fullCalendar.moment().subtract(14, 'days');
            var td = today.day();
       
        if (td == 0){
        var weekStart = today.clone().startOf('isoWeek').add(1, 'isoWeek').isoWeekday(0);
        var weekEnd = today.clone().endOf('isoWeek').add(4, 'isoWeek').isoWeekday(6);
        }
       
        else{
        var weekStart = today.clone().startOf('isoWeek').subtract(1, 'days');
        var weekEnd = today.clone().endOf('isoWeek').add(3, 'isoWeek').subtract(1, 'days');
        }
            
            var prevWeek = today.clone().startOf('isoWeek').subtract(2, 'isoWeek').subtract(1, 'days');
            if (date >= weekStart && date <= weekEnd) {
                cell.addClass("active");
            }
            
            else {
                cell.addClass("non-active");
                cell.parent().parent().parent().parent().parent().addClass("no-event-show");
                
            }
        },
        /*Ui end remove comment Start*/
        events: [{
                title: '45 DOSES EXPECTED TO SHIP',
                start: '2020-11-20T01:00:00',
                end: '2020-11-26T23:00:00',
                description: '<table class="table table-striped table-condensed"><tr><td>Flucelvax</td><td>1000</td></tr><tr><td>Fluad</td><td>2000</td></tr><tr><td>Afluria</td><td>500</td></tr><tr><td>rapivab</td><td>200</td></tr></table>'
                },
            {
                // title  : 'delivered',
                start: '2020-12-12',
                className: "cal-delivered",
                icon: "check-circle-o" // Add here your icon name
              },
            {
                // title  : 'scheduled',
                start: '2020-12-15',
                className: "cal-delivered",
                icon: "arrow-circle-o-right" // Add here your icon name
                },
            {
                // title  : 'delivered',
                start: '2020-08-19',
                className: "cal-delivered",
                icon: "truck" // Add here your icon name
               },
            {
                // title  : 'expected delivery',
                start: '2020-08-12',
                className: "cal-delivered",
                icon: "truck" // Add here your icon name
                },
            {
                title: '500 UNITS EXPECTED TO SHIP',
                start: '2020-08-09T01:00:00',
                end: '2020-08-15T23:00:00',
                description: '<table class="table table-striped table-condensed"><tr><td>Flucelvax</td><td>1000</td></tr><tr><td>Fluad</td><td>2000</td></tr><tr><td>Afluria</td><td>500</td></tr></table>'
                }
                // more events here
            ],
        /*Ui end remove comment end*/
         /*Hybris end remove the comment
            events: getEvents(),*/
        eventRender: function (event, element) {
            if (event.icon) {
                element.find(".fc-title").prepend("<i class='fa fa-" + event.icon + "'></i>");
            }
            $(element).popover({
                title: function () {
                    return "<B>" + event.title + "</B>";
                },
                placement: 'top',
                html: true,
                container: 'body',
                /* optional */
                trigger: 'click',
                animation: 'true',
                content: function () {
                    return "<B>" + event.description + "</B>";
                },
                container: 'body'
            }).click(function (e) {
                $(this).addClass("fc-event-select");
                $(this).removeClass("fc-event-default fc-normal");
                $('.fc-event').not(this).addClass("fc-event-default");
                $('.fc-event').not(this).removeClass("fc-event-select");
                //$('.popover').not(this).hide(); /* optional, hide other popovers */
                $(this).popover('show'); /* show popover now it's setup */
                // e.preventDefault();
            });
        }
    });

    
    

    setTimeout(function () {
        $('.hideatcancel').css("opacity", "1");
    }, 1000);
    
    $(document).on("click", ".fc-icon-right-single-arrow", function () {
        $(".popover").popover('hide');
    });
    $(document).on("click", ".fc-icon-left-single-arrow", function () {
        $(".popover").popover('hide');
    });
    $(document).on('click', function (e) {
        $('[data-toggle="popover"],[data-original-title]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).addClass("fc-normal");
                $(this).removeClass("fc-event-select");
                (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false // fix for BS 3.3.6
            }
        });
    });
    
  
});
