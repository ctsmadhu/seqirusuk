$(document).ready(function(){
  $('.tablinks, .pagi-link').on('click', function () {
     $(".tab-section").hide();
     $(".img-no, .active-img").hide();
     $('#' + $(this).data('section-id')).show();
     $('#' + $(this).data('img-id')).show();
});

$(".Nextbutton").on("click", function() {
    $('#businessPost').each(function() {
        if (checkpostBusiness()) {
            setTimeout(function() {
                $('.postcodeerror1').empty('');
            }, 10);
        } else {
            $('.postcodeerror1').html('Please Enter Valid Postcode');
        }
    });
});

 $(".bannerClose").click(function () {
        $(".cookie_banner").hide();
        });
  
  $("body").tooltip({ selector: '[data-toggle=tooltip]' });
  /* Non amnadotory fields checking logic starts here.****This should be top section of the JS*/

  $(".Nextbutton, .Nextbutton5, .Nextbutton2,.removebutton, .Nextbutton3, .prevStepbtn").click(function() {
          $('html, body').animate({
              scrollTop: $(".accountcontentArea").offset().top
          }, 1000);
  });
  function CheckDyn(){
    if (checkshipping && dynamicValidate){
      $(".nxt5_5").addClass("enableNext");
     // $(".nxt5_5").prop("type", "button");
    }
    else{
     $(".nxt5_5").removeClass("enableNext");
    // $(".nxt5_5").prop("type", "button");
    }
  }
  $(".orgreginum").on("keyup", function () {
        var nhscount = $(this).val().length;
          if (nhscount < 7 && nhscount > 0) {
           $('.zeroerrmsg').html('Should not be less than 7 character');
          }
          else{
          $('.zeroerrmsg').html('');
          }
      });
  
  $('#vatNumber').inputmask("GB999999999");
  $("#vatNumber").on("keyup", function () {
    var phoneunmasklength = $("#vatNumber").inputmask("unmaskedvalue").length;
    if (phoneunmasklength < 9 && phoneunmasklength> 0) {
      
      $(".vaterror").html("Please enter in the format GB123456789");
      return false;
    } 
    else {
      $('.vaterror').html("");
      return true;
    }
  });

  function checklcn(){
    $('.shipLicense').next().html() =='';
  }

  
  $(".emailcheck_step1").on('keyup keypress blur', function(e) {

   var userinput = $(this).val();
   var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i
   
  if (pattern.test(userinput) || userinput.length ==0) {
   $(this).next().html("").show();        
  }
  else{
   $(this).next().html("Please enter valid email").show();
  }
});
  

$('.phone-not-man').inputmask("99999999999",{"placeholder": ""});
$(".phone-not-man").on("keyup",function(){
   $(this).next().show();
   var phoneunmasklength = $(this).inputmask('unmaskedvalue').length;
   if(phoneunmasklength<10 && phoneunmasklength>0)
   {
     $(this).next().empty().append("Please enter a valid UK format Telephone");
   }
   else{
     $(this).next().empty();
   } 
 });

//join act page
/*$(".selectiveselect").on("keyup",function(){
  customalerts();
$('#joinform').validator();  
});*/

  $(".post_not_man").on('keyup keypress blur', function(e) {
       $(this).next().show();
     /*$(".post_not_man").inputmask("Regex",{regex:"([A-Za-z0-9]){3}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){4}([ ]){1}([A-Za-z0-9]){3}"});
     var nhscount = $(this).val().length;
     
       if(nhscount < 7 && nhscount>0)
       {
         $(this).next().empty().append("Must be in A1B 2C3 format");
         return false;
       }
       else
       {
         $(this).next().empty();
         return true;
       }*/
        
        
        $(".post_not_man").inputmask("Regex",{regex:"([A-Za-z0-9]){2}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){3}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){4}([ ]){1}([A-Za-z0-9]){3}"});
          var nhscount = $(this).val().length;
          var thirdchar = $(this).val().slice(2,3);
          var fourthchar = $(this).val().slice(3,4);
          var fifthchar = $(this).val().slice(4,5);
            if(nhscount == 6 && thirdchar == " ") 
            {
              $(this).next().empty();
              return true;
            }
            else if(nhscount == 7 && fourthchar == " ")
            {
              $(this).next().empty();
              return true;
            }
            else if(nhscount == 8 && fifthchar == " ")
            {
              $(this).next().empty();
              return true;
            }
            else
            {
              $(this).next().empty().append("Please enter valid Post Code");
              return false;
            }
  });

  /* Non amnadotory fields checking logic ends here*/
  
  
     
     
   //accnumbermask
     $(":input").inputmask();

         

       $('#viewAllFaqs').on('click', function () {
       $("#supportPageId").hide();
      $("#faqId").show();
       });
       $('.faqlinks').on('click', function () {
           var faqlists=$(this).attr("id").split("-")[1];
           $(".faq-section").hide();
           $("#faq-section-"+faqlists).show();      
           
       });


       $(".link-1").on('click', function(){
           $(".faqlinks").removeClass("sublinks-active");
           $(".link-1").addClass("sublinks-active");
         });

       $(".link-2").on('click', function(){
           $(".faqlinks").removeClass("sublinks-active");
           $(".link-2").addClass("sublinks-active");
         });
       
         $(".link-3").on('click', function(){
           $(".faqlinks").removeClass("sublinks-active");
           $(".link-3").addClass("sublinks-active");
         });
         $(".link-4").on('click', function(){
           $(".faqlinks").removeClass("sublinks-active");
           $(".link-4").addClass("sublinks-active");
         });
         $(".link-5").on('click', function(){
           $(".faqlinks").removeClass("sublinks-active");
           $(".link-5").addClass("sublinks-active");
         });
         $(".link-6").on('click', function(){
           $(".faqlinks").removeClass("sublinks-active");
           $(".link-6").addClass("sublinks-active");
         });
         $(".link-7").on('click', function(){
           $(".faqlinks").removeClass("sublinks-active");
           $(".link-7").addClass("sublinks-active");
         });
         $(".link-8").on('click', function(){
           $(".faqlinks").removeClass("sublinks-active");
           $(".link-8").addClass("sublinks-active");
         });
       
     $('ul.topnav li a').click(function(){
     $('li a').removeClass("active-1");
     $(this).addClass("active-1");
     });
     if($(".abtmaincontent .aboutheadingtop").hasClass("aboutpg"))
     {
     $('li a').removeClass("active-1");
     $('ul.topnav li:first-child a').addClass('active-1');
     }
     else if($(".supportsection").hasClass("supportpg"))
     {
     $('li a').removeClass("active-1");
     $('ul.topnav li:nth-child(2) a').addClass('active-1');
     }

     //$('.phonemask').inputmask("(+44) 9999-999999");
     
     //Address Lookup
     var cc_object = new clickToAddress({
            accessToken: '252fa-0e273-cf8ab-03341',
            countryMatchWith: "text",
          enabledCountries: ["United Kingdom"]
      });
      var uri=window.location.toString();
      if (uri.length > 0 && uri.indexOf('/businessDetails')>=0 || uri.indexOf('/profile')>=0) {
      cc_object.attach({
            search:     'lookup',
            town:       'city',
            postcode:   'postalCode',
            country:     'country',
            line_1:     'buildingStreet',
            line_2:     'additionalStreet' 
            },
            {
           onResultSelected: function(c2a, elements, address){
              $("#businessAddress,#businessStreet,#businessCity,#businessPost,#BussinessCountry").blur(); 
              $("#businessAddress,#businessStreet,#businessCity,#businessPost,#BussinessCountry").keypress();
              $("#edit_company_address,#edit_company_address2,#edit_company_city,#edit_company_zip,#edit_company_country").blur(); 
              $("#edit_company_address,#edit_company_address2,#edit_company_city,#edit_company_zip,#edit_company_country").keypress();
            }
      });

      cc_object.attach({
            search:     'payingContactInfo.addressLookUp',
            town:       'payingContactInfo.city',
            postcode:   'payingContactInfo.postalCode',
            country:     'payingContactInfo.country',
            line_1:     'payingContactInfo.addressLine1',
            line_2:     'payingContactInfo.addressLine2' 
      },
      {
            onResultSelected: function(c2a, elements, address){
              $("#step1address,#step1street,#step1City,#step1post,#step1country").blur();  
              $("#step1address,#step1street,#step1City,#step1post,#step1country").keypress();  
              $('#step1post').next().html('');
              $("#payAddr1,#payAddr2,#payCity,#payZip,#payCountry").blur();  
              $("#payAddr1,#payAddr2,#payCity,#payZip,#payCountry").keypress(); 
            }
      });
     /*cc_object.attach({
            search:     'billingContactInfo.addressLookUp',
            town:       'billingContactInfo.city',
            postcode:   'billingContactInfo.postalCode',
            country:     'billingContactInfo.country',
            line_1:     'billingContactInfo.addressLine1',
            line_2:     'billingContactInfo.addressLine2' 
            },
            {
            onResultSelected: function(c2a, elements, address){
                $("#step2address,#step2street,#step2City,#step2post,#step2country").blur();  
              }         
      });*/
      cc_object.attach({
            search:     'invoicingContractInfo.addressLookUp',
            town:       'invoicingContractInfo.city',
            postcode:   'invoicingContractInfo.postalCode',
           country:     'invoicingContractInfo.country',
            line_1:     'invoicingContractInfo.addressLine1',
            line_2:     'invoicingContractInfo.addressLine2' 
            },
            {
            onResultSelected: function(c2a, elements, address){
                $("#invoicingaddress,#invoicingstreet,#invoicingCity,#invoicingpost,#invoicingcountry").blur(); 
                $("#invoicingaddress,#invoicingstreet,#invoicingCity,#invoicingpost,#invoicingcountry").keypress();  
                $("#invAddr1,#invAddr2,#invCity,#invPostal,#invCountry").blur(); 
                $("#invAddr1,#invAddr2,#invCity,#invPostal,#invCountry").keypress(); 
              }   
      });
      cc_object.attach({
            search:     "shippingLocations[0].addressLookUp",
            town:       'shippingLocations[0].city',
            postcode:   'shippingLocations[0].postalCode',
            country:     'shippingLocations[0].country',
            line_1:     'shippingLocations[0].addressLine1',
            line_2:     'shippingLocations[0].addressLine2' 
      },
            {
            onResultSelected: function(c2a, elements, address){
              $("#shippingaddress,#shippingstreet,#shippingcity,#shippingpostcode,#shippingcountry,.post_code5").blur();  
              $(".add-ship .post_code5").next().html(''); 
              $("#shipAddr1,#shipAddr2,#shipCity,#shipPostal,#shipCountry,.post_code5").blur();
              }         
            
      });
      }
      
      

var businesstype = "default";
$("#businesstype").on("change",function(){
  $('#BussinesslastNHSCode,#shippingNHS,#shippingLicensenumber').val("");
  $('#BussinesslastNHSCode,#shippingNHS').inputmask("remove");
  if($("#businesstype").val()=='GENERAL_PRACTICE'){
    $("#BussinesslastNHSCode,#shippingNHS").attr("maxlength",6);
    $("#BussinesslastNHSCode,#shippingNHS").attr("maxlength",7);
    $('.customNHSerror').html('');
    businesstype = "gp";
   
  }
  else if($("#businesstype").val()=='PHARMACY')
  {
    $("#BussinesslastNHSCode,#shippingNHS").attr("maxlength",5);
    $("#BussinesslastNHSCode,#shippingNHS").attr("maxlength",7);
    $('.customNHSerror').html('');
    businesstype = "pharma";
  }
  else if($("#businesstype").val()=='HOSPITAL_PUBLIC')
  {
    $("#shippingLicensenumber").attr("maxlength",7);
    $("#BussinesslastNHSCode").attr("maxlength",6);
    $('.nhs_shp').attr("maxlength",6);
    $('.customNHSerror').html('');
    businesstype = "hospital_public";
  }
  else if($("#businesstype").val()=='HOSPITAL_PRIVATE')
  {
    $("#shippingLicensenumber").attr("maxlength",7);
    $("#BussinesslastNHSCode").attr("maxlength",6);
    $('.nhs_shp').attr("maxlength",6);
    $('.customNHSerror').html('');
    businesstype = "hospital_private";
  }
  else if($("#businesstype").val()=='NURSING_HOMES')
  {
    $("#shippingLicensenumber").attr("maxlength",7);
    $("#BussinesslastNHSCode").attr("maxlength",6);
    $('.nhs_shp').attr("maxlength",6);
    $('.customNHSerror').html('');
    businesstype = "nursing_homes";
  }
  else if($("#businesstype").val()=='CORRECTIONAL_FACILITY')
  {
    $("#shippingLicensenumber").attr("maxlength",7);
    $("#BussinesslastNHSCode").attr("maxlength",6);
    $('.nhs_shp').attr("maxlength",6);
    $('.customNHSerror').html('');
    businesstype = "correctional_facility";
  }
  else if($("#businesstype").val()=='WHOLESALER')
  {
    $("#shippingLicensenumber").attr("maxlength",7);
    $("#BussinesslastNHSCode").attr("maxlength",6);
    $('.nhs_shp').attr("maxlength",6);
    $('.customNHSerror').html('');
    businesstype = "wholesaler";
  }
  else if($("#businesstype").val()=='OTHER')
  {
    $("#shippingLicensenumber").attr("maxlength",8);
    $("#BussinesslastNHSCode").attr("maxlength",6);
    $('.nhs_shp').attr("maxlength",6);
    $('.customNHSerror').html('');
    businesstype = "other";
  }
  else
  {
    businesstype = "default";
  }
});







var nhscodetext = $("#BussinesslastNHSCode").val();

$("#BussinesslastNHSCode").on("keyup",function(e){
  if(businesstype == "gp")
  {
  //  alert(1);
  $("#BussinesslastNHSCode").inputmask("Regex",{regex:"[A-Za-z]{1}([0-9]){5}}"});
  var nhscount = $("#BussinesslastNHSCode").val().length;
  //console.log(nhscount);
    if(nhscount < 6 && nhscount>0)
    {
     // $(".customNHSerror").show();
      $(".customNHSerror").html("Please provide a valid NHS code e.g. Y02906");
    }
    else
    {
      $('.customNHSerror').html('');
    }
  }
  else if(businesstype == "pharma")
  {
  //  alert(2);
    //$('#BussinesslastNHSCode').inputmask("F9999");
   
    $("#BussinesslastNHSCode").inputmask("Regex",{regex:"[F|f]([A-Za-z]|[0-9]){4}}"});

  


    var nhscount = $("#BussinesslastNHSCode").val().length;
   
      if(nhscount < 5 )
      {
       // $(".customNHSerror").show();
        $(".customNHSerror").html("Please provide a valid NHS code e.g. F1J4D");
        if(e.keyCode==8 && nhscount==0){
          $('.customNHSerror').html('');  
        }
      }
      else {
       $('.customNHSerror').html('');  
            

      }
     
  }

});   

$("#shippingNHS").on("keyup",function(e){
  if(businesstype == "gp")
  {
  $("#shippingNHS").inputmask("Regex",{regex:"[A-Za-z]{1}([0-9]){5}}"});
  var nhscount = $("#shippingNHS").val().length;
    if(nhscount < 6 && nhscount>0)
    {
     // $(".customNHSerror2").show();
      $(".customNHSerror2").html("Please provide a valid NHS code e.g. Y02906");
    }
    else
    {
      $('.customNHSerror2').html('');
    }
  }
  else if(businesstype == "pharma")
  
  {
    $("#shippingNHS").inputmask("Regex",{regex:"[F|f]([A-Za-z]|[0-9]){4}}"});
    var nhscount = $("#shippingNHS").val().length;
      if(nhscount < 5)
      {
       // $(".customNHSerror2").show();
        $(".customNHSerror2").html("Please provide a valid NHS code e.g. F1J4D");
         if(e.keyCode==8 && nhscount==0){
          $('.customNHSerror2').html('');  
        }
      }
      else
      {
        $('.customNHSerror2').html('');
      }
  }

});   

var nhs_global = true;
function chknhs(nhsparam){
  //nhs_global = false;
  $("#shippingNHS"+nhsparam).on("keyup keypress",function(e){
    if(businesstype == "gp")
    {
    $("#shippingNHS"+nhsparam).inputmask("Regex",{regex:"[A-Za-z]{1}([0-9]){5}}"});
    var nhscount = $("#shippingNHS"+nhsparam).val().length;
      if(nhscount < 6 && nhscount>0)
      {
       // $(".customNHSerror2").show();
        $("#nhs_err_shipping"+nhsparam).html("Please provide a valid NHS code e.g. Y02906");
        $(this).siblings('.help-block').addClass("shipphner");
        nhs_global = false;
      }
      else
      {
        $('#nhs_err_shipping'+nhsparam).html('');
        $(this).siblings('.help-block').removeClass("shipphner");
        nhs_global = true;
      }
    }
    else if(businesstype == "pharma")
    
    {
      $("#shippingNHS"+nhsparam).inputmask("Regex",{regex:"[F|f]([A-Za-z]|[0-9]){4}}"});
      var nhscount = $("#shippingNHS"+nhsparam).val().length;
        if(nhscount < 5)
        {
         // $(".customNHSerror2").show();
          $("#nhs_err_shipping"+nhsparam).html("Please provide a valid NHS code e.g. F1J4D");
          $(this).siblings('.help-block').addClass("shipphner");
          nhs_global = false;
           if(e.keyCode==8 && nhscount==0){
            $('#nhs_err_shipping'+nhsparam).html('');  
            $(this).siblings('.help-block').removeClass("shipphner");
            nhs_global = true;
          }
        }
        else
        {
          $('#nhs_err_shipping'+nhsparam).html('');
          $(this).siblings('.help-block').removeClass("shipphner");
          nhs_global = true;
        }
    }
    
    //return nhs_global;
    
  
  }); 
}


$('ul.topnav li a').click(function(){  
$('li a').removeClass("active-1");
$(this).addClass("active-1");
});

/*$('#Businessphone').inputmask("(+44) 9999-999999");
$("#Businessphone").on("keyup",function(){
  //alert();
  var phoneunmasklength = $("#Businessphone").inputmask("unmaskedvalue").length;
  if(phoneunmasklength<10)
  {
    $("#message").empty().append("Requires 10 digits");
  }
  else{
    $("#message").empty();
  }
  
});*/



//$('#shippingphoneNumber').inputmask("(+44) 9999-999999");
$("#shippingphoneNumber").on("keyup",function(){
  //alert();
  var phoneunmasklength1 = $("#shippingphoneNumber").val().length;
  //console.log(phoneunmasklength1);
  if(phoneunmasklength1<10)
  {
    $("#message5").empty().append("Please enter a valid UK format Telephone");
  }
  else{
    $("#message5").empty();
}
});

$('#section-4 .Nextbutton_without_validation').on('click', function () {
    $('.accountslist li:last-child').removeClass('no-click');
  });
  $('#section-4 .skiplink').on('click', function () {
    $('.accountslist li:last-child').removeClass('no-click');
  });

$('.add-ship').on('keyup keypress blur', '.emailcheck', function(e) {

        var userinput = $(this).val();
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i
        
       if (pattern.test(userinput)) {
          $(this).next().html("").show();  
          emailCheck = 1;        
       }
      else{
        $(this).next().html("Please enter valid email").show();
        emailCheck = 0;    
        
       }
      });


      $('.add-ship').on('keyup keypress blur', '.phonemask', function(e) {
        //alert();
        var phoneunmasklength1 = $(this).val().length;
        //console.log(phoneunmasklength1);
        if(phoneunmasklength1<10)
        {
          $(this).next().empty().append("Please enter a valid UK format Telephone");
        }
        else{
          $(this).next().empty();
        }
       
      });


  /*Validation code starts*/

/* Non Mamnadotory screen validation  starts here*/

  $('#section-2 .spcl-fld').on('keyup', function(e) { 
    var flag = false;
    $('#section-2 .spcl-fld').each(function() {
      if($(this).parent().find('.help-block').html()!="")
      {
        flag = true;
      } 
    });  
    if(!flag)
      {
        $('#section-2 .Nextbutton_without_validation').removeAttr('disabled'); 
      } 
      else{
        $("#section-2 .Nextbutton_without_validation").attr('disabled', 'disabled');
         }
  });

  $('#section-3 .spcl-fld').on('keyup', function(e) { 
    var flag = false;
    $('#section-3 .spcl-fld').each(function() {
      if($(this).parent().find('.help-block').html()!="")
      {
        flag = true;
      } 
    });  
    if(!flag)
      {
        $('#section-3 .Nextbutton_without_validation').removeAttr('disabled'); 
      } 
      else{
        $("#section-3 .Nextbutton_without_validation").attr('disabled', 'disabled');
         }
  });

  $('#section-4 .spcl-fld').on('keyup', function(e) { 
    var flag = false;
    $('#section-4 .spcl-fld').each(function() {
      if($(this).parent().find('.help-block').html()!="")
      {
        flag = true;
      } 
    });  
    if(!flag)
      {
        $('#section-4 .Nextbutton_without_validation').removeAttr('disabled'); 
      } 
      else{
        $("#section-4 .Nextbutton_without_validation").attr('disabled', 'disabled');
         }
  });


/* Non Mamnadotory screen validation ends here*/

$('.required-fld-form1,#vatNumber,#registrationNumber, #BussinesslastNHSCode').on('keyup change blur', function(e) {    
    var userinput = $('.email_input').val(); 
    var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i   
    var Phoneno1 = $('#BussinessPhone').val();     
    var validationFlag=false;
      $('.required-fld-form1').each(function() {
       if($(this).val() == '' && $(this).attr("id")!='otherbusinessType'){
                $(".nxt1_1").removeClass("enableNext");
                $(".nxt1_1").prop("type", "submit");
                $('#form1').validator(); 
                $('.accountslist li:first-child').addClass('no-click');
                $('.accountslist li:nth-child(2)').addClass('no-click');
                $('.accountslist li:last-child').addClass('no-click');
                validationFlag=true;
        }else if($(this).val() == '' && $("#businesstype").children("option:selected").text()=='Other' && $('#otherbusinessType').val() == '' && $('.customNHSerror').html()==""){
            $(".nxt1_1").removeClass("enableNext");
            $(".nxt1_1").prop("type", "submit");
                $('#form1').validator(); 
                $('.accountslist li:first-child').addClass('no-click'); 
                $('.accountslist li:nth-child(2)').addClass('no-click');
                $('.accountslist li:last-child').addClass('no-click');
                validationFlag=true;
                
        }
      });
    if(!validationFlag){
     $('.required-fld-form1').each(function() {
        if($(this).val() != '' && ($('.customNHSerror').html() ==''  || $('#BussinesslastNHSCode').val()=='') && ($('.vaterror').html() ==''  || $('#vatNumber').val()=='') && ($(".zeroerrmsg").html() == "" || $('#registrationNumber').val() == '') && checkpostBusiness() && $(this).attr("id")!='otherbusinessType' && $('#trading').val() != '' && $('#businesstype').val() != '' &&  pattern.test(userinput) && $("#message").html()=="" && Phoneno1 != '' ){ 
              $(".nxt1_1").addClass("enableNext");
                $('.accountslist li:first-child').removeClass('no-click');   
                $(".nxt1_1").prop("type", "button");
        }else if($('.required-fld-form1').val() != '' && ($('.customNHSerror').html() ==''  || $('#BussinesslastNHSCode').val()=='') && ($('.vaterror').html() =='' || $('#vatNumber').val()=='') && ($(".zeroerrmsg").html() == "" || $('#registrationNumber').val() == '') && checkpostBusiness() &&  $("#businesstype").children("option:selected").text()=='Other' && $('#otherbusinessType').val() != '' &&  pattern.test(userinput) && $("#message").html()=="" && Phoneno1 != '' ){ 
              $(".nxt1_1").addClass("enableNext");
              $('.accountslist li:first-child').removeClass('no-click'); 
              $(".nxt1_1").prop("type", "button");
        }else{
              $(".nxt1_1").removeClass("enableNext");
              $(".nxt1_1").prop("type", "submit");
                $('#form1').validator(); 
                $('.accountslist li:first-child').addClass('no-click'); 
                $('.accountslist li:nth-child(2)').addClass('no-click')
                $('.accountslist li:last-child').addClass('no-click')
          }
       });
      }
  });
  
  
  
  $('.required-fld-form2,.phone-not-man, #addresslookupPaying').on('keyup change blur', function(e) {    
          var userinput = $('.emailcheck2').val(); 
          var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i
          var validationFlag=false;
          $('.required-fld-form2').each(function() {
                   if($(this).val() == ''){
                      $(".nxt2_2").removeClass("enableNext");
                   $('.accountslist li:nth-child(2)').addClass('no-click'); 
                   $('.accountslist li:last-child').addClass('no-click'); 
                   $(".nxt2_2").prop("type", "submit");
                      $('#form1').validator(); 
                      validationFlag=true;
                 }
           });    
          if(!validationFlag){
            $('.required-fld-form2').each(function() {
              if($('required-fld-form2').val() != '' && checkpostPayer()  &&  pattern.test(userinput) && $("#message2").html()=="" ){ 
                $(".nxt2_2").addClass("enableNext");
                $('.accountslist li:first-child').removeClass('no-click'); 
                $('.accountslist li:nth-child(2)').removeClass('no-click'); 
                $(".nxt2_2").prop("type", "button");
              }else{
                $(".nxt2_2").removeClass("enableNext");
                $('.accountslist li:nth-child(2)').addClass('no-click'); 
                  $('.accountslist li:last-child').addClass('no-click');
                  $(".nxt2_2").prop("type", "submit");
                $('#form1').validator(); 
              }
            });
          }
        });
  
  $('.required-fld-form3, .phone-not-man,.emailcheck_step1, #addresslookupInvoice').on('keyup change blur', function(e) {    
          var userinput = $('.emailcheck3').val(); 
          var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i   
            var validationFlag=false;
                $('.required-fld-form3').each(function() {
                         if($(this).val() == '' ){
                            $('.accountslist li:last-child').addClass('no-click'); 
                        $(".nxt3_3").removeClass("enableNext");
                        $(".nxt3_3").prop("type", "submit");
                        $('#form1').validator(); 
                        validationFlag=true;
                       }
                 });
            if(!validationFlag){
            $('.required-fld-form3').each(function() {
              if($(this).val() != '' && checkpostInvoice()  &&  pattern.test(userinput) && $("#message3").html()=="" && $("#addmail").html()=="" ){ 
                  $(".nxt3_3").addClass("enableNext");
                $('.accountslist li:last-child').removeClass('no-click');
                $('.accountslist li:nth-child(2)').removeClass('no-click');
                $(".nxt3_3").prop("type", "button");
              }else{
                $('.accountslist li:last-child').addClass('no-click'); 
                $(".nxt3_3").removeClass("enableNext");
                $(".nxt3_3").prop("type", "submit");
                $('#form1').validator(); 
              }
            });
            }
        });


var checkshipping = false;
  $('.required-fld-form5,#addresslookupShipping,.nhserrdynamic,.nhs_shp').on('keyup change blur', function(e) {
    var userinput = $('.email-input5').val(); 
    var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i  
    var Phoneno5 = $('#shippingphoneNumber').val();  
    
    var validationFlag=false;
    
    
    $('.required-fld-form5').each(function() {
             if($(this).val() == '' ){
                $('.accountslist li:nth-child(2)').removeClass('no-click'); 
                 // $(".nxt5_5").removeClass("enableNext");
                  $('.addship').attr('disabled','disabled');
                  $(".nxt5_5").prop("type", "submit");
                  $('#form1').validator(); 
                  validationFlag=true;
                  checkshipping = false;
           }
     });
      if(!validationFlag){
          $('.required-fld-form5').each(function() {
            
            if($(this).val() != ''  && ($('.customNHSerror2').html() =='' || $('#shippingNHS').val()=='') && checkpost() && pattern.test(userinput) && $("#message5").html()=="" &&  checklicence() && Phoneno5 != '' && $('#shippingLicensenumber').val() != '' && $('#shippingpostcode').val() != '' ){
                //  $(".nxt5_5").addClass("enableNext");
                  $('.addship').removeAttr('disabled');
                  $('.lst-tab').removeClass('no-click');
                  $('.accountslist li:last-child').removeClass('no-click');  
                 $(".nxt5_5").prop("type", "button");
                  checkshipping = true;
            }  
            
            else{
              $('.accountslist li:nth-child(2)').removeClass('no-click'); 
             $(".nxt5_5").prop("type", "button");
             // $(".nxt5_5").removeClass("enableNext");
              $('.addship').attr('disabled','disabled');
              $('#form1').validator();
              checkshipping = false;
             }
          });  
        }
    });

$('.nxt1_1').on('click', function () {
    $('.required-fld-form1').trigger('blur');
   $("#section-2 .help-block").hide();
   });
  
  $('.nxt2_2').on('click', function () {
        $("#section-2 .help-block").show();
         $("#section-4 .help-block").hide();
         $('.required-fld-form2').trigger('blur');
         });
  $('.nxt3_3').on('click', function () {
         $("#section-4 .help-block").show();
         $('.required-fld-form3').trigger('blur');
         //$("#section-5 .help-block").hide();
         });
         
   $('.nxt5_5').on('click', function () {
    $("#section-5 .with-errors").show();
   });
  

   function checkValidationShip(){
   $('.required-fld-form5,#addresslookupShipping,.nhserrdynamic,.nhs_shp').on('keyup change blur', function() {
    CheckDyn();
   
   });
  
  
  }


  checkValidationShip();

  $('.Nextbutton').on('click', function () {
    $('#form1').validator();
    if($(".Nextbutton").hasClass("enableNext"))
    {
       var sectionid = $(this).attr("id").split("_")[1];
       $("#emailerr").hide();
        $(".tab-section").hide();
        $(".img-no, .active-img").hide();
        $("#section-"+sectionid).show();
        $("#img-"+sectionid).show();
       
    }
   else{
     $('#form1').validator(); 
    }
   });
  
  $('.Nextbutton2').on('click', function () {
          $('#form1').validator();
          if($(".Nextbutton2").hasClass("enableNext"))
          {
             var sectionid = $(this).attr("id").split("_")[1];
             $("#emailerr2").hide();
              $(".tab-section").hide();
              $(".img-no, .active-img").hide();
           $("#section-"+sectionid).show();
              $("#img-"+sectionid).show();
          }
          else{
          $('#form1').validator(); 
          }
          $('#step1Email').each(function () {
                  if ($(this).val() != '' && $("#emailerr2").html()==""){
                    $(this).next().html('');
                  }
                  else{
                    $(this).next().html('Please enter valid email');
                  }
                });
         });
  
  $('.Nextbutton3').on('click', function () {
          $('#form1').validator();
         if($(".Nextbutton3").hasClass("enableNext"))
          {
             var sectionid = $(this).attr("id").split("_")[1];
             $("#emailerr").hide();
              $(".tab-section").hide();
              $(".img-no, .active-img").hide();
              $("#section-"+sectionid).show();
              $("#img-"+sectionid).show();
          $("#section-5 .with-errors").html('');
          }
          else{
        $('#form1').validator(); 
          }
          $('#invoicingEmail').each(function () {
                  if ($(this).val() != '' && $("#emailerr3").html()==""){
                    $(this).next().html('');
                  }
                  else{
                    $(this).next().html('Please enter valid email');
                  }
                });
         });

   $('.Nextbutton5').on('click', function () {
    $('#form1').validator();
    if($(".Nextbutton5").hasClass("enableNext"))
    {
      $(".sub-8").removeClass("sub-active");
      var sectionid = $(this).attr("id").split("_")[1];
      $("#emailerr").hide();
        $(".tab-section").hide();
        $(".img-no, .active-img").hide();
        $("#section-"+sectionid).show();
        $("#img-"+sectionid).show();
    }
    else{
     $('#form1').validator(); 
    }
   });

  /*Validation code ends*/

  function valid_postcode(postcode) {
    postcode = postcode.replace(/\s/g, "");
    var regex = /^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([AZa-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z]))))[0-9][A-Za-z]{2})$/i;
    return regex.test(postcode);
   }

  /* $('.post_code').on('keyup blur', function(e) {
    var postcode = $(this).val();  
    
    if (valid_postcode(postcode)) {
      
      $(this).next().html("").show();

    } else {
      
      $(this).next().html("Please Enter Valid UK Postcode").show();
    }

  }); */



  var emailCheck = 0;

  $(".emailcheck").on('keyup keypress blur', function(e) {

    var userinput = $(this).val();
    var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i
    
   if (pattern.test(userinput) && userinput!='') {
         $('#emailerr5').html("").show();  
      emailCheck = 1;        
   }
   else{
         $('#emailerr5').html("Please enter valid email").show();
    emailCheck = 0;    
    
   }
  });
  
  $(".emailcheck3").on('keyup keypress blur', function(e) {

          var userinput = $(this).val();
          var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i
          
         if (pattern.test(userinput) && userinput!='') {
            $('#emailerr3').html("").show();  
            emailCheck = 1;        
         }
         else{
          $('#emailerr3').html("Please enter valid email").show();
          emailCheck = 0;    
          
         }
  });

  $(".emailcheck2").on('keyup keypress blur', function(e) {

          var userinput = $(this).val();
          var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i
          
         if (pattern.test(userinput) && userinput!='') {
            $('#emailerr2').html("").show();  
            emailCheck = 1;        
         }
         else{
          $('#emailerr2').html("Please enter valid email").show();
          emailCheck = 0;    
          
         }
        });
  
  

  $(".emailcheck1").on('keyup keypress blur', function(e) {

    var userinput = $(this).val();
    var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i
    
   if (pattern.test(userinput) && userinput!='') {
      $('#emailerr').html("").show();  
      emailCheck = 1;        
   }
   else{
    $('#emailerr').html("Please enter valid email").show();
    emailCheck = 0;    
    
   }
  });
  
  
  //join account email check
  /*$(".emailcheckaccount").on('keyup keypress blur', function(e) {

    var userinput = $(this).val();
    var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i
    
   if (pattern.test(userinput) && userinput!='') {
      $('#emailerr').html("").show();  
           
   }
   
  });*/


  $('.phonemask').inputmask("99999999999",{"placeholder": ""});
  $(".phonemask").on("keyup blur",function(){
  $('#message5').html("").show();
  var phoneunmasklength = $(".phonemask").inputmask('unmaskedvalue').length;
  if(phoneunmasklength<10 && phoneunmasklength>0 )
  {
    $("#message5").empty().append("Please enter a valid UK format Telephone");
  }
  else{
    $("#message5").empty();
  }

});
  
  $('.add-ship').on('keyup keypress blur', '.phonemask', function (e) {
  $('.phonemask').inputmask("99999999999",{"placeholder": ""});
  $(".phonemask").on("keyup blur",function(){
  $('#message5').html("").show();     
  var phoneunmasklength = $(".phonemask").inputmask('unmaskedvalue').length;
  if(phoneunmasklength<10)
  {
    $("#message5").empty().append("Please enter a valid UK format Telephone");
  }
  else{
    $("#message5").empty();
  }
      });
  });

  $('.phonemask1').inputmask("99999999999",{"placeholder": ""});
  $(".phonemask1").on("keyup blur",function(){
  var phoneunmasklength = $(".phonemask1").inputmask('unmaskedvalue').length;
  if(phoneunmasklength<10 )
  {
    $("#message").empty().append("Please enter a valid UK format Telephone");
  }
  else{
    $("#message").empty();
  }

});

/* $(".step2, .step4").on('click', function () {
          $('.addship').attr('disabled', 'disabled');

});*/


$(".sub-2").on('click', function(){
      if ($(".sub-2").hasClass("enableNext")){
        $(".tablinks").removeClass("sub-active");
        $(".tab-3-sub .sub-6").addClass("sub-active");
         $(".tab-2").removeClass("stepTwo").addClass("listing");
         $(".tab-3").removeClass("listing-pending").addClass("stepThree")

      }/*else{
        $(".tablinks").removeClass("sub-active");
        $(".tab-3-sub .sub-6").addClass("sub-active");
      }*/
});

$(".sub-5").on('click', function(){
      if ($(".sub-5").hasClass("enableNext")){
        $(".tablinks").removeClass("sub-active");
       
         $(".tab-3").removeClass("stepThree").addClass("listing");
         $(".tab-4").removeClass("listing-pending").addClass("stepFour")

      }/*else{
        $(".tablinks").removeClass("sub-active");
        $(".tab-3-sub .sub-6").addClass("sub-active");
      }*/
});

$(".sub-6").on('click', function(){
        $(".tablinks").removeClass("sub-active");
        $(".tab-3-sub .sub-6").addClass("sub-active");
        $(".tab-2").removeClass("stepTwo").addClass("listing");
         $(".tab-3").removeClass("listing-pending").addClass("stepThree");
        $(".tab-4").removeClass("stepFour").addClass("listing-pending");
});

$(".sub-3").on('click', function(){
  if ($(".sub-3").hasClass("enableNext")){
        $(".tablinks").removeClass("sub-active");
        $(".tab-3-sub .sub-7").addClass("sub-active");
  }
});

$(".sub-7").on('click', function(){
        $(".tablinks").removeClass("sub-active");
        $(".tab-3-sub .sub-7").addClass("sub-active");
        $(".tab-2").removeClass("stepTwo").addClass("listing");
         $(".tab-3").removeClass("listing-pending").addClass("stepThree");
         $(".tab-4").removeClass("stepFour").addClass("listing-pending");
});

$(".sub-4").on('click', function(){
      if ($(".sub-4").hasClass("enableNext")){
              $('.accountslist li:nth-child(3)').removeClass('no-click');   
              $(".tablinks").removeClass("sub-active");
              $(".tab-3-sub .sub-8").addClass("sub-active");
      }
});

/*$(".sub-8").on('click', function(){
              $('.accountslist li:nth-child(3)').removeClass('no-click');   
              $(".tablinks").removeClass("sub-active");
              $(".tab-3-sub .sub-8").addClass("sub-active");
});*/

/*$(".sub-3-spcl").on('click', function(){
  $(".tablinks").removeClass("sub-active");
  $(".tab-3-sub .sub-3").addClass("sub-active");
  
});*/

/*$(".sub-5").on('click', function(){
      if ($(".sub-5").hasClass("enableNext")){
              $(".tablinks").removeClass("sub-active");
              $(".tab-3-sub .sub-8").addClass("sub-active");
      }             
});*/

$(".sub-8").on('click', function(){
            $(".tablinks").removeClass("sub-active");
            $(".tab-3-sub .sub-8").addClass("sub-active");
            $(".tab-2").removeClass("stepTwo").addClass("listing");
            $(".tab-3").removeClass("listing-pending").addClass("stepThree");
             $(".tab-4").removeClass("stepFour").addClass("listing-pending");
});

$(".tab-2").on('click', function(){
        $(".tablinks").removeClass("sub-active");
        $(".tab-2").removeClass("listing").addClass("stepTwo"); 
        $(".tab-3").removeClass("stepThree").addClass("listing-pending");
        $(".tab-4").removeClass("stepFour").addClass("listing-pending");
      });
$(".tab-4").on('click', function(){
        //$(".tablinks").removeClass("sub-active");
        $(".tab-4").removeClass("listing-pending").addClass("stepFour"); 
        $(".tab-3").removeClass("stepThree listing-pending").addClass("listing");
        $(".tab-2").removeClass("stepTwo").addClass("listing");
      });
/*$(".sub-remove").on('click', function(){
  $(".tablinks").removeClass("sub-active");
  
});*/

/*$(".tab-3-sub").on('click', function(){
  
   $(".tab-3").removeClass("listing-pending").addClass("stepThree");
   $(".tab-2").removeClass("stepTwo").addClass("listing");
   $(".tab-4").removeClass("stepFour").addClass("listing-pending");
});


$(".tab-4").on('click', function(){
  $(".tablinks").removeClass("sub-active");
  $(".tab-4").removeClass("listing-pending").addClass("stepFour"); 
  $(".tab-3").removeClass("stepThree listing-pending").addClass("listing");
  $(".tab-2").removeClass("stepTwo").addClass("listing");
});*/




/* Shippinglocation add function Start */
/*var shippinglocationItration = 0;
$('.Addbutton').click(function (e) {
  $(this).attr('disabled','disabled');
    shippinglocationItration++;
    var appendhtml = $('div.add-shipping:first').clone().addClass('remove-part').attr("id", 'locationaddress' +
        shippinglocationItration);
   $('.add-ship').append(appendhtml);
    var formName = $(appendhtml).find('form').attr("id");
    var FormID = $(appendhtml).find('form').attr("id", formName + shippinglocationItration);
   $(appendhtml).find('form').attr("id", formName + shippinglocationItration);
    $(appendhtml).find('input').each(function (index) {
        $(this).val("");
        var idvalue = $(this).attr("id");
        $(this).attr("id", idvalue + shippinglocationItration);
    })
    var total_element = $(".add-shipping").length;
    if (total_element > 0) {
        $(".remove-part .no-show").show();
    }
    $(FormID).validator('update');
    
});*/

/* Shippinglocation add function Start */
var shippinglocationItration = 0;
var shippingCount=0;
shippingCount=1+shippinglocationItration;

//$("#shippingLocations").val(shippingCount);
$('.Addbutton').click(function (e) {
      $(this).attr('disabled', 'disabled');
    shippinglocationItration++;
    var appendhtml = $('div.add-shipping:first').clone().addClass('remove-part').attr("id",
        'locationaddress' + shippinglocationItration);
    $('.add-ship').append(appendhtml);
    var formName = $(appendhtml).find('form').attr("id");
    var FormID = $(appendhtml).find('form').attr("id", formName + shippinglocationItration);
    $(appendhtml).find('form').attr("id", formName + shippinglocationItration);
         /* $('.customLicenseerror').each(function (index){
            if (index > 0) { 
                  $(this).attr("id","shipLocation"+(shippingCount));
            }
          });*/
    $(appendhtml).find('input').each(function (index) {
        $(this).val("");
        var idvalue = $(this).attr("id");
        $(this).attr("id", idvalue + shippinglocationItration);
        if(shippinglocationItration>=1){
            var namevalue = $(this).attr("name");
            if(namevalue!=undefined){
            var trimName = namevalue.replace("shippingLocations[0]","");
                  $(appendhtml).find('.customLicenseerror').each(function (index){
                        $(this).attr("id","shipLocation"+(shippinglocationItration));
                  });
                  $(appendhtml).find('.customNHSerror2').each(function (index){
                    $(this).attr("id","nhs_err_shipping"+(shippinglocationItration));
              });
            $(this).attr("name", "shippingLocations[" +shippinglocationItration +"]" + trimName);
            $(this).attr("cc_applied","false");
                  }
            phoneshipvlidation(shippinglocationItration);
            postshipvalidtion(shippinglocationItration);
           checklicenceship(shippinglocationItration);
            shipemailcheck(shippinglocationItration);
            chknhs(shippinglocationItration);
            }
           shippingCount=1+shippinglocationItration;
            $("#shippingLocations").val(shippingCount);
    })
    
    var total_element = $(".add-shipping").length;
    if (total_element > 0) {
        $(".remove-part .no-show-ship").css("display", "inline-block");
      }
  $(FormID).validator('update');
  $('#form1').validator('update');
  $('.add-ship .shipLicense').next().removeClass('customLicenseerror');
  $('.add-ship .post_code5').next().removeClass('postcodeerror');  
  $('.add-ship .nhserrdynamic').siblings('.customNHSerror2').removeClass('customNHSerror2');
  $('.Nextbutton5').removeClass('enableNext');
  $('.Nextbutton5').prop("type", "submit");
  //$('.addship').attr('disabled', 'disabled');
  //$('.phonemask').inputmask("(+44) 9999-999999");
  $(".numberonly").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
           //display error message
           //$(".errmsg").html("Please enter numbers only").show().fadeOut("slow");
                  return false;
      }
      });
  cc_object.attach({
            search:     'shippingLocations['+(shippingCount-1)+'].addressLookUp',
            town:       'shippingLocations['+(shippingCount-1)+'].city',
            postcode:   'shippingLocations['+(shippingCount-1)+'].postalCode',
            country:     'shippingLocations['+(shippingCount-1)+'].country',
            line_1:     'shippingLocations['+(shippingCount-1)+'].addressLine1',
            line_2:     'shippingLocations['+(shippingCount-1)+'].addressLine2' 
            },
            {
              onResultSelected: function(c2a, elements, address){
                postCodeValidation(shippingCount-1);
                $("#shippingaddress"+(shippingCount-1)).blur();
                $("#shippingcity"+(shippingCount-1)).blur(); 
                $("#shippingpostcode"+(shippingCount-1)).blur(); 
                $("#shippingaddress"+(shippingCount-1)).blur(); 
                $("#shippingcountry"+(shippingCount-1)).blur(); 
                $("#shippingpostcode"+(shippingCount-1)).keypress(); 
                $("#shippingpostcode"+(shippingCount-1)).next().html(''); 
              }   
      });
    $(".Nextbutton5").on("click",function(){
        if(shippostflag){
        setTimeout(function(){
          $("#shippingpostcode"+(shippingCount-1)).trigger("blur");
          $("#shippingpostcode"+(shippingCount-1)).next().html(''); 
        }, 10);
        }
        else{
          $("#shippingpostcode"+(shippingCount-1)).trigger("blur");
          $("#shippingpostcode"+(shippingCount-1)).next().html('Please Enter Valid Postcode');
        }
      
      });
});

$('.rvw_prev').on('click', function () {
    $('.nxt5_5').addClass("enableNext");
  });

$('#shipping-container').on('click', '.removebutton', function() {

    $(this).parent().parent().remove();
    $('#form1').validator('update');
    // $('.addship').removeAttr('disabled');
    $('.Nextbutton5').prop("type", "button");
    $(".nxt5_5").addClass("enableNext");
   // $(".required-fld-form5").blur();

    $('.add-ship .required-fld-form5, .add-shipping .required-fld-form5').each(function() {
        if ($(this).val() != '' && ($('.customNHSerror2').html() == '' || $('#shippingNHS').val() == '') && checkpost() && pattern.test(userinput) && $("#message5").html() == "" && checklicence() && Phoneno5 != '' && $('#shippingLicensenumber').val() != '' && $('#shippingpostcode').val() != '') {
            $('.Nextbutton5').prop("type", "button");
            $(".nxt5_5").addClass("enableNext");
            $('.addship').removeAttr('disabled');
            $('#form1').validator();
        } else {
            $(".nxt5_5").removeClass("enableNext");
            $('.addship').attr('disabled', 'disabled');
            $(".nxt5_5").prop("type", "submit");
            $('#form1').validator();
        }
    });

    $('html, body').animate({
        scrollTop: $(".accountcontentArea").offset().top
    }, 1000);

});

var shipphoneflag = false;
  function phoneshipvlidation(phoneid) {
    //$('#shippingphoneNumber' + phoneid).inputmask("(+44) 9999-999999");
    $("#shippingphoneNumber" + phoneid).on("keyup", function () {
      //alert();
      var phoneunmasklength1 = $("#shippingphoneNumber" + phoneid).val().length;
      if (phoneunmasklength1 < 10) {
        $(this).next().empty().append("Please enter a valid UK format Telephone");
        $(this).next().addClass("shipphner");
        
      } else {
        $(this).next().empty();
        $(this).next().removeClass("shipphner");
        // return true;
        
      }
    });
  }

  
   /*  $(".phnship").on("keyup", function () {
      
      if($('.phnship').siblings('#message5').html()==''){
        shipphoneflag = true;
      }
      else{
        shipphoneflag = false;
      }
      console.log('static'+ shipphoneflag);
      
      
    }); */



$('.add-ship').on('keyup blur', '.textonly', function (e) {
      $(function () {
          $('.textonly').keydown(function (e) {
            if (e.ctrlKey || e.altKey) {
              e.preventDefault();
            } else {
              var key = e.keyCode;
              if (!((key == 8) || (key == 9) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                e.preventDefault();
              }
            }
          });
        });
});

/* Ship post  */
var shippostflag = false;
function postshipvalidtion(postid) {
  $("#shippingpostcode" + postid).on("keyup blur", function () {
            $("#shippingpostcode"+ postid).inputmask("Regex", {
              regex: "([A-Za-z0-9]){2}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){3}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){4}([ ]){1}([A-Za-z0-9]){3}"
            });
            var nhscount = $("#shippingpostcode"+ postid).val().length;
            var thirdchar = $("#shippingpostcode"+ postid).val().slice(2, 3);
            var fourthchar = $("#shippingpostcode"+ postid).val().slice(3, 4);
            var fifthchar = $("#shippingpostcode"+ postid).val().slice(4, 5);
            if (nhscount == 6 && thirdchar == " ") {
              $(this).next().empty();
              $(this).next().removeClass("shipphner");
               shippostflag = true;
            } else if (nhscount == 7 && fourthchar == " ") {
              $(this).next().empty();
              $(this).next().removeClass("shipphner");
              shippostflag = true;
            } else if (nhscount == 8 && fifthchar == " ") {
              $(this).next().empty();
              $(this).next().removeClass("shipphner");
              shippostflag = true;
            } else {
              $(this).next().empty().append("Please enter valid Post Code");
              $(this).next().addClass("shipphner");
               shippostflag = false;
            }
      });
}

var shippostflagFetchify = false;
function postCodeValidation(postid) {
            $("#shippingpostcode"+ postid).inputmask("Regex", {
              regex: "([A-Za-z0-9]){2}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){3}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){4}([ ]){1}([A-Za-z0-9]){3}"
            });
            var nhscount = $("#shippingpostcode"+ postid).val().length;
            var thirdchar = $("#shippingpostcode"+ postid).val().slice(2, 3);
            var fourthchar = $("#shippingpostcode"+ postid).val().slice(3, 4);
            var fifthchar = $("#shippingpostcode"+ postid).val().slice(4, 5);
            if (nhscount == 6 && thirdchar == " ") {
              $(this).next().empty();
              $(this).next().removeClass("shipphner");
              shippostflagFetchify = true;
            } else if (nhscount == 7 && fourthchar == " ") {
              $(this).next().empty();
              $(this).next().removeClass("shipphner");
              shippostflagFetchify = true;
            } else if (nhscount == 8 && fifthchar == " ") {
              $(this).next().empty();
              $(this).next().removeClass("shipphner");
              shippostflagFetchify = true;
            } else {
              $(this).next().empty().append("Please enter valid Post Code");
              $(this).next().addClass("shipphner");
              shippostflagFetchify = false;
            }
}


/* License validation start */
var shiplicenceflag = false;
function checklicenceship(shipid) {
  $("#shippingLicensenumber" + shipid).on("keyup", function () {
    if (businesstype == "gp") {
      $("#shippingLicensenumber" + shipid).inputmask("Regex", {
        regex: "([0-9]){7}}"
      });
      var nhscount = $("#shippingLicensenumber" + shipid).val().length;
      if (nhscount < 7) {
        $('#shipLocation'+shipid).empty().append("Please provide your 7 digit license");
        $(this).siblings('.help-block').addClass("shipphner");
        shiplicenceflag = false;
      //  return false;
      } else {
        $('#shipLocation'+ shipid).empty();
        $(this).siblings('.help-block').removeClass("shipphner");
        shiplicenceflag = true;
      //  return true;
      }
    } else if (businesstype == "pharma" || businesstype == "hospital_public" || businesstype == "hospital_private" || businesstype == "correctional_facility") {
      $("#shippingLicensenumber" + shipid).inputmask("Regex", {
        regex: "2([0-9]){6}|[0-9]{4}"
      });
      var nhscount = $("#shippingLicensenumber" + shipid).val().length;
      var firstnum = $("#shippingLicensenumber" + shipid).val().slice(0, 1);
      if (nhscount < 4) {
        $('#shipLocation'+ shipid).empty().append("Must be 4 digits for Northern Ireland");
        $(this).siblings('.help-block').addClass("shipphner");
        shiplicenceflag = false;
        //return false;
      } else if (nhscount > 4 && nhscount < 7 && firstnum == 2) {
        $('#shipLocation'+ shipid).empty().append("Please enter 7 digit license starts with 2");
        $(this).siblings('.help-block').addClass("shipphner");
        shiplicenceflag = false;
       // return false;
      } else {
        $('#shipLocation'+ shipid).empty();
        $(this).siblings('.help-block').removeClass("shipphner");
        shiplicenceflag = true;
       // return true;
      }
    }

    else if (businesstype == "nursing_homes" || businesstype == "wholesaler") {
      $("#shippingLicensenumber" + shipid).inputmask("Regex", {
        regex: "[0-9]{7}"
      });
      var nhscount = $("#shippingLicensenumber" + shipid).val().length;
      if (nhscount < 7) {
        $('#shipLocation'+ shipid).empty().append("Must be 7 digits");
        $(this).siblings('.help-block').addClass("shipphner");
        shiplicenceflag = false;
       return false;
      } else {
        $('#shipLocation'+ shipid).empty();
        $(this).siblings('.help-block').removeClass("shipphner");
        shiplicenceflag = true;
       return true;
      }
    }

    else{
      $('#shipLocation'+ shipid).empty();
      $(this).siblings('.help-block').removeClass("shipphner");
        shiplicenceflag = true;
    } 

    


    /*else if (businesstype == "care" || businesstype == "wholesale") {
      $("#shippingLicensenumber" + shipid).inputmask("Regex", {
        regex: "[0-9]{7}"
      });
      var nhscount = $("#shippingLicensenumber" + shipid).val().length;
      if (nhscount < 7) {
        $(".customLicenseerror").empty().append("Must be 7 digits");
        shiplicenceflag = false;
       return false;
      } else {
        $('.customLicenseerror').empty();
        shiplicenceflag = true;
       return true;
      }
    } else if (businesstype == "other") {
      $("#shippingLicensenumber" + shipid).inputmask("Regex", {
     regex: "[0-9]{8}"
      });
      var nhscount = $("#shippingLicensenumber" + shipid).val().length;
      if (nhscount < 8) {
        $(".customLicenseerror").empty().append("Must be 8 digits");
        shiplicenceflag = false;
       return false;
      } else {
        $('.customLicenseerror').empty();
        shiplicenceflag = true;
       return true;
      }
    }*/
  });
}

/* check Ship email Starts*/
$("#shippingEmail").on('keyup', function (e) {
        var userinput = $("#shippingEmail").val();
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i
        if (pattern.test(userinput)) {
            $("#shippingEmail").next().html("").show();
            //shipemailcheckflag= true;
            return true;
        } else {
            $("#shippingEmail").next().html("Please enter valid email").show();
            //shipemailcheckflag= false;
            return false;
        }
});

var shipemailcheckflag= false;
function shipemailcheck(shipemailid){
$("#shippingEmail"+shipemailid).on('keyup', function (e) {
        var userinput = $("#shippingEmail"+shipemailid).val();
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i
        if (pattern.test(userinput)) {
            $("#shippingEmail"+shipemailid).next().html("").show();
            $(this).next().removeClass("shipphner");
            shipemailcheckflag= true;
        } else {
            $("#shippingEmail"+shipemailid).next().html("Please enter valid email").show();
            $(this).next().addClass("shipphner");
            shipemailcheckflag= false;
        }
});
}

var dynamicValidate = true;

  $('.add-ship').on('keyup blur', '.required-fld-form5, .address-look-ship, .nhserrdynamic, .phnship, .nhs_shp', function (e) {
    var validationFlag=false;
    dynamicValidate = false;
    
      
       
          if($('.add-ship').find(".help-block").hasClass("shipphner")){
            shipphoneflag = false;
          }
          else{
            shipphoneflag = true;
          }
         

        $('.add-ship .required-fld-form5').each(function() {
                 if($(this).val() == '' ){
                            /* $('.Nextbutton5').removeClass('enableNext');*/
                        $('.Nextbutton5').prop("type", "submit"); 
                        $('.addship').attr('disabled', 'disabled'); 
                      validationFlag=true;
                      dynamicValidate = false;
               }
         });
    if(!validationFlag){    
        $('.add-ship .required-fld-form5,.add-ship .nhserrdynamic').each(function () {
          
          if ($('.add-ship .required-fld-form5').val() != '' && shipphoneflag && (shippostflag || shippostflagFetchify) &&  nhs_global && shiplicenceflag && shipemailcheckflag) {
           
            $('.addship').removeAttr('disabled');
            $('.Nextbutton5').prop("type", "button");
            /* $('.Nextbutton5').addClass('enableNext'); */
            dynamicValidate = true;
          } else {
           /*  $('.Nextbutton5').removeClass('enableNext');*/
            $('.Nextbutton5').prop("type", "button"); 
            $('.addship').attr('disabled', 'disabled');
            dynamicValidate = false;
          }
        });
    }
   

    CheckDyn();
   
  });
  



$('#shipping-container').on('keydown', '.textonly', function (e) {
    if (e.ctrlKey || e.altKey) {
      e.preventDefault();
    } else {
      var key = e.keyCode;
      if (!((key == 8) || (key == 9) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
        e.preventDefault();
      }
    }
  });



//Organization Registration number validation
$(".numberwithoutzero").on("keypress", function () {
  checkSpcialChar(event);

});

function checkSpcialChar(event){
  if(!((event.keyCode >= 65) && (event.keyCode <= 90) || (event.keyCode >= 97) && (event.keyCode <= 122) || (event.keyCode >= 48) && (event.keyCode <= 57))){
     event.returnValue = false;
     return;
  }
  event.returnValue = true;
}



$('.Nextbutton5').on('click', function () {
    var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]{2,}\.[A-Z]{2,4}\b$/i
    
    $('.email-input5').each(function () {
      var userinput = $(this).val();
      //if ($(this).val() != ''){
        if (pattern.test(userinput) && userinput!='') {
        $(this).next().html('').show();
      }
      else{
        $(this).next().html('Please enter valid email').show();
      }
    });
  });


$('.Nextbutton5').on('click', function () {
    $('.shipLicense').each(function () {
      if ($(this).val().length == 4 || $(this).val().length == 7){
        $(this).next().html('').show();
      }
      else{
        $(this).next().html('Please provide your 7 digit license').show();
      }
    });
  });

$('.step5').on('click', function () {
          setTimeout(function() {
            $('.shipLicense').trigger('blur');
          }, 10);
});

  $('.Nextbutton5').on('click', function () {
    $('.phonemask').each(function () {
      if ($(this).val() != ''){
        $(this).next().html('').show();
     }
      else{
        $(this).next().html('Please enter a valid UK format Telephone').show();
      }
    });
  });
  
  $('.Nextbutton5').on('click', function () {
          $('.shipLicense').each(function () {
            if ($(this).val() != ''){
             //$(this).parent().find('.customLicenseerror').html('').show();
            }
            else{
              //$(this).next().html('Please enter Licecnse Number').show();
              $(this).parent().find('.customLicenseerror').html('Please enter valid License Number').show();
            }
          });
        });
  
  $('.Nextbutton').on('click', function () {
          $('#BussinessPhone').each(function () {
            if ($(this).val() != '' && $("#message").html()==""){
              $(this).next().html('').show();
            }
            else{
              $(this).next().html('Please enter a valid UK format Telephone').show();
            }
          });
        });
  
  $('.Nextbutton').on('click', function () {
          $('#Bussinessemail').each(function () {
            if ($(this).val() != '' && $("#emailerr").html()==""){
              $(this).next().html('').show();
            }
            else{
              $(this).next().html('Please enter valid email').show();
            }
          });
        });

  $(".Nextbutton2").on("click",function(){

          if(checkpostPayer()){
          setTimeout(function(){
          $('#step1post').next().html('');
          }, 10);
          }
          else{
            $('#step1post').next().html('Please Enter Valid Postcode');
          }
        
  });       
  
  /*$('.Nextbutton2').on('click', function () {
          $('#step1Email').each(function () {
            if ($(this).val() != '' && $("#emailerr2").html()==""){
              $(this).next().html('');
            }
            else{
              $(this).next().html('Please enter valid email');
            }
          });
        });*/
  
 /* $('.Nextbutton3').on('click', function () {
          $('#invoicingEmail').each(function () {
            if ($(this).val() != '' && $("#emailerr3").html()==""){
              $(this).next().html('');
            }
            else{
              $(this).next().html('Please enter valid email');
            }
          });
        });*/



  $(function () {
          $('.textonly').keydown(function (e) {
            if (e.ctrlKey || e.altKey) {
              e.preventDefault();
            } else {
              var key = e.keyCode;
              if (!((key == 8) || (key == 9) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                e.preventDefault();
              }
            }
          });
        });

$(".numberonly").keypress(function (e) {
  //if the letter is not digit then display error and don't type anything
  if (e.which != 8 && e.which != 0  && (e.which < 48 || e.which > 57)) {
     //display error message
     //$(this).next().html("Please enter numbers only").show().delay(3000).fadeOut();

            return false;
}

});

$(".with_space").keypress(function (e) {
    if (!(e.which == 32) && !(e.which > 47 && e.which < 58) && !(e.which > 64 && e.which < 91) && !(e.which > 96 && e.which < 123)) {
      return false;
    }
  });

/* License validation start */

function checklicence(){
  if(businesstype == "gp")
  {
 $("#shippingLicensenumber").inputmask("Regex",{regex:"([0-9]){7}}"});
  var nhscount = $("#shippingLicensenumber").val().length;
    if(nhscount < 7)
    {
      $('.customLicenseerror').empty().append("Please provide your 7 digit license");
      return false;
    }
    else
    {
      $('.customLicenseerror').empty();
      return true;
    }
  }
  else if(businesstype == "pharma" || businesstype == "hospital_public" || businesstype == "hospital_private" || businesstype == "correctional_facility" )
  {
    $("#shippingLicensenumber").inputmask("Regex",{regex:"2([0-9]){6}|[0-9]{4}"});
    var nhscount = $("#shippingLicensenumber").val().length;
    var firstnum = $("#shippingLicensenumber").val().slice(0,1);
      if(nhscount < 4)
      {
        $('.customLicenseerror').empty().append("Please enter a valid license number");
        return false;
       
      }
      else if(nhscount > 4 && nhscount < 7 && firstnum ==2) 
      {
        $('.customLicenseerror').empty().append("Please enter 7 digit license starts with 2");
        return false;
      }
      else
     {
        $('.customLicenseerror').empty();
        return true;
      }
  }
  
  else if(businesstype == "nursing_homes" || businesstype == "wholesaler")
  {
    $("#shippingLicensenumber").inputmask("Regex",{regex:"[0-9]{7}"});
    var nhscount = $("#shippingLicensenumber").val().length;
    
      if(nhscount < 7)
      {
        $(".customLicenseerror").empty().append("Must be 7 digits");
        return false;
      }
      else
      {
        $('.customLicenseerror').empty();
        return true;
      }
  }
  
  
  else{
        $('.customLicenseerror').empty();
      return true;
  }

  
  /*else if(businesstype == "care" || businesstype == "wholesale")
  {
    $("#shippingLicensenumber").inputmask("Regex",{regex:"[0-9]{7}"});
    var nhscount = $("#shippingLicensenumber").val().length;
    
      if(nhscount < 7)
      {
        $(".customLicenseerror").empty().append("Must be 7 digits");
        return false;
      }
      else
      {
        $('.customLicenseerror').empty();
        return true;
      }
  }
  else if(businesstype == "other")
  {
    $("#shippingLicensenumber").inputmask("Regex",{regex:"[0-9]{8}"});
    var nhscount = $("#shippingLicensenumber").val().length;
    
      if(nhscount < 8)
      {
        $(".customLicenseerror").empty().append("Must be 8 digits");
        return false;
      }
      else
      {
        $('.customLicenseerror').empty();
        return true;
      }
  }*/

}



$("#shippingLicensenumber").on("keyup",function(){
  
  checklicence();

  });   


function checkpost(){
    $("#shippingpostcode").inputmask("Regex",{regex:"([A-Za-z0-9]){2}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){3}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){4}([ ]){1}([A-Za-z0-9]){3}"});
    var nhscount = $("#shippingpostcode").val().length;
    var thirdchar = $("#shippingpostcode").val().slice(2,3);
    var fourthchar = $("#shippingpostcode").val().slice(3,4);
    var fifthchar = $("#shippingpostcode").val().slice(4,5);
      if(nhscount == 6 && thirdchar == " ") 
      {
        $(".postcodeerror").empty();
        return true;
      }
      else if(nhscount == 7 && fourthchar == " ")
      {
        $(".postcodeerror").empty();
        return true;
      }
      else if(nhscount == 8 && fifthchar == " ")
      {
        $(".postcodeerror").empty();
        return true;
      }
      else
      {
        $(".postcodeerror").empty().append("Please enter valid Post Code");
        return false;
      }
      
    }


function checkpostPayer(){
    $("#step1post").inputmask("Regex",{regex:"([A-Za-z0-9]){2}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){3}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){4}([ ]){1}([A-Za-z0-9]){3}"});
    var nhscount = $("#step1post").val().length;
    var thirdchar = $("#step1post").val().slice(2,3);
  var fourthchar = $("#step1post").val().slice(3,4);
    var fifthchar = $("#step1post").val().slice(4,5);
      if(nhscount == 6 && thirdchar == " ") 
      {
        $(".postcodeerror1").empty();
        return true;
      }
      else if(nhscount == 7 && fourthchar == " ")
      {
        $(".postcodeerror1").empty();
        return true;
      }
      else if(nhscount == 8 && fifthchar == " ")
      {
        $(".postcodeerror1").empty();
        return true;
      }
      else
      {
        $(".postcodeerror1").empty().append("Please enter valid Post Code");
        return false;
      }
      
    }

function checkpostInvoice(){
    $("#invoicingpost").inputmask("Regex",{regex:"([A-Za-z0-9]){2}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){3}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){4}([ ]){1}([A-Za-z0-9]){3}"});
    var nhscount = $("#invoicingpost").val().length;
    var thirdchar = $("#invoicingpost").val().slice(2,3);
    var fourthchar = $("#invoicingpost").val().slice(3,4);
    var fifthchar = $("#invoicingpost").val().slice(4,5);
      if(nhscount == 6 && thirdchar == " ") 
      {
        $(".postcodeerror1").empty();
        return true;
      }
      else if(nhscount == 7 && fourthchar == " ")
      {
        $(".postcodeerror1").empty();
        return true;
      }
      else if(nhscount == 8 && fifthchar == " ")
      {
        $(".postcodeerror1").empty();
        return true;
      }
      else
      {
        $(".postcodeerror1").empty().append("Please enter valid Post Code");
        return false;
      }
      
    }

function checkpostBusiness(){
    $("#businessPost").inputmask("Regex",{regex:"([A-Za-z0-9]){2}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){3}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){4}([ ]){1}([A-Za-z0-9]){3}"});
    var nhscount = $("#businessPost").val().length;
    var thirdchar = $("#businessPost").val().slice(2,3);
    var fourthchar = $("#businessPost").val().slice(3,4);
    var fifthchar = $("#businessPost").val().slice(4,5);
      if(nhscount == 6 && thirdchar == " ") 
      {
        $(".postcodeerror1").empty();
        return true;
      }
      else if(nhscount == 7 && fourthchar == " ")
      {
        $(".postcodeerror1").empty();
       return true;
      }
      else if(nhscount == 8 && fifthchar == " ")
      {
        $(".postcodeerror1").empty();
        return true;
      }
      else
      {
        $(".postcodeerror1").empty().append("Please enter valid Post Code");
        return false;
      }
      
    }


  
  $("#shippingpostcode").on("keyup",function(){
    checkpost();
    
  });
  
        
        $("#businessPost").on("keyup blur",function(){
          checkpostBusiness(); 
        });
        
                               
    //abandon popup in registration starts                              
    if ($("body").hasClass("page-register")) {
    var urlLink1;
    $("a:not(.tablinks, .pagi-link)").addClass('showPopupclick');
    $("a.showPopupclick").click(function (e) {
        urlLink1 = $(this).attr("href");
                                
        $("#myModaluserAllow").show();
        e.preventDefault();
                                
    });
    $('#cotinueRegistration').click(function (e) {
$("#myModaluserAllow").hide();
});
$(".abdnlink").hover(function(){
	$(".confirmreg").removeClass("active");
});
$('#leaveLink').click(function (e) {
window.location.href = urlLink1;
$("#myModaluserAllow").hide();
});
                                }
                                
 
 //join act abandon
                              
    if ($("body").hasClass("page-joinAccount")) {
    var urlLink1;
    $("a:not(.tablinks, .pagi-link)").addClass('showPopupclick');
    $("a.showPopupclick").click(function (e) {
        urlLink1 = $(this).attr("href");
                                
        $("#myModaluserAllow").show();
        e.preventDefault();
                                
    });
    $('#cotinueRegistration').click(function (e) {
$("#myModaluserAllow").hide();
});
$(".abdnlink").hover(function(){
	$(".confirmreg").removeClass("active");
});
$('#leaveLink').click(function (e) {
window.location.href = urlLink1;
$("#myModaluserAllow").hide();
});
                                }
                                                               
                                

});


$('.edit-profile').on('click', function () {
    $(this).fadeOut();
    $(".profile-details").fadeOut("fast", function(){
       $(".profile-details-edit").fadeIn("fast");
   });
   });

   $('.close-profile-edit').on('click', function () {   
    $(".profile-details-edit").fadeOut("fast", function(){
       $(".profile-details").fadeIn("fast");
       $(".edit-profile").fadeIn();
   });
       
   });
   
 /*  function customalerts()
{
     if($("#orgName").val()!="" && $("#postcode").val()=="")
            {
                  $("#orgName").attr("required",true);
                 $("#postcode").attr("required",false);
                  
                  $(".postErrortext").empty();
                  $("#postcode").trigger("blur");
                  //return true;
            }
            else if($("#postcode").val()!="" && $("#orgName").val()=="")
            {
                  $("#postcode").attr("required",true);
                  $("#orgName").attr("required",false);
  
                  $(".orgErrortext").empty();
                  $("#orgName").trigger("blur");
                  //return true;
            }
            else if($("#postcode").val()=="" && $("#orgName").val()=="")
            {
                  $("#postcode").attr("required",true);
                  $("#orgName").attr("required",true);
                  //$(".orgErrortext").empty().append("");
                  //$(".orgErrortext").empty().append("");
                  //$('#joinform').validator();
                  if($("#postcode").val()=="" && $("#orgName").val()!="")
                  {
                        $("#postcode").trigger("blur");
                  }
                  else if($("#orgName").val()=="" && $("#postcode").val()!="")
                  {
                        $("#orgName").trigger("blur");
                  }
                  else if($("#postcode").val()=="" && $("#orgName").val()=="")
                  {
                        $(".selectiveselect").trigger("blur");
                  }
                  
                  
                  //return false;   
            }
            else
            {     
                  $("#postcode").attr("required",false);
                  $("#orgName").attr("required",false);
                  $(".postErrortext").empty();
                  $(".orgErrortext").empty();
                  //return true;
            }
}*/


//join account postcode js
function checkpostjoint(){

        $("#postcode").inputmask("Regex",{regex:"([A-Za-z0-9]){2}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){3}([ ]){1}([A-Za-z0-9]){3}|([A-Za-z0-9]){4}([ ]){1}([A-Za-z0-9]){3}"});
            var nhscount = $("#postcode").val().length;
            var thirdchar = $("#postcode").val().slice(2,3);
            var fourthchar = $("#postcode").val().slice(3,4);
            var fifthchar = $("#postcode").val().slice(4,5);

          if(nhscount == 6 && thirdchar == " ")  {

            $(".jointcodeerror1").empty();
                  return true;

          }
                  else if(nhscount == 7 && fourthchar == " ") {
            $(".jointcodeerror1").empty();
                  return true;

          }

          else if(nhscount == 8 && fifthchar == " "){
                  $(".jointcodeerror1").empty();
                 return true;

          }else{
            $(".jointcodeerror1").empty().append("Please enter valid postcode");
                  return false;
          }
        }

  $("#postcode").on("keyup",function(){
         checkpostjoint();
     });

//join account accnum js
$("#accountnumber").on("keyup",function(){   
      var jaccountcount = $("#accountnumber").val().length;
      if(jaccountcount < 8 )

      {
            $(".lengtherror").show();
            $(".lengtherror").text("Please provide a valid account number");

      }

      else

      {
            $('.lengtherror').hide();

      }
});

//join account accesscode js
$("#accesscode").on("keyup",function(){   
      var jaccesscount = $("#accesscode").val().length;
      if(jaccesscount < 8 )

      {
            $(".lengtherroraccess").show();
            $(".lengtherroraccess").text("Please provide a valid access code");

      }

      else

      {
            $('.lengtherroraccess').hide();

      }
});
//join account postcode-icons
$("#postcode").keyup(function() {
    if ($(this).val() == '') { // check if value changed
        
        $("#failureIcon3").hide();
            $("#successIcon3").hide();
            $("#successDiv").hide();
            $("#successSubDiv").hide();
            $("#failureDiv").hide();
            $("#failureSubDiv").hide();
            $("#joinActDiv").hide();
            
            
    }

});
//join account accesscode-icons
$("#accesscode").keyup(function() {
    if ($(this).val() == '') { // check if value changed
        
        $("#failureIcon5").hide();
            $("#successIcon5").hide();
            $("#successDiv").hide();
            $("#successSubDiv").hide();
            $("#failureDiv").hide();
            $("#failureSubDiv").hide();
            $("#joinActDiv").hide();
    }

});
//join account accnum-icons
$("#accountnumber").keyup(function() {
    if ($(this).val() == '') { // check if value changed
        
        $("#failureIcon1").hide();
            $("#successIcon1").hide();
            $("#successDiv").hide();
           $("#successSubDiv").hide();
            $("#failureDiv").hide();
            $("#failureSubDiv").hide();
            $("#joinActDiv").hide();
    }

});
//join account orgemail-icons

$(document).ready(function() {
    $('.referencepanel').hide();
    $('.pull-me').click(function() {
       $('.referencepanel').slideToggle('slow');          
       $('p i').toggleClass("fa-angle-up");
  });
    });
      
      
//Resource page JS starts from here----  
        $('.down-slide').on('click', function() {
        $(this).siblings('.toggle-resource').toggleClass('fa-chevron-circle-down fa-chevron-circle-up');
        $('#' + $(this).data('panel-id')).slideToggle();
        $(this).text($(this).text() == 'View Resources' ? 'Hide Resources' : 'View Resources');
        });

        $('.left-nav-wrap ul li a').click(function(event) {
        $('.left-nav-wrap ul li a.active').removeClass('active');
          $(this).addClass('active');
          event.preventDefault();
        });  

        $("a").on('click', function(event) {
        if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
        scrollTop: $(hash).offset().top
        }, 800, function(){
        window.location.hash = hash;
       });
       } 
       });

       $(window).on('scroll', function () {
        var scrollBottom = $(document).height() - $(window).height() - $(window).scrollTop();
        if (scrollBottom <= 100) {
            $(".left-side").fadeOut();
        }
        if (scrollBottom > 100) {
            $(".left-side").fadeIn();
        }
        }).trigger('scroll');

        $(window).on('scroll', function () {
            if ($(window).scrollTop() > 350) {
                $(".left-side").addClass("flot-menu");
            } else {
                $(".left-side").removeClass("flot-menu");

            }
        });

        $(window).on('scroll', function() {
            $("div.section").each(function() {
                var id = $(this).attr("id");
                if (isScrolledIntoView("#" + id)) {
                    
                    $('a[href="#' + id + '"]').parent().addClass("active").siblings().removeClass("active");
                }
            });
          });
          
          function isScrolledIntoView(elem) {
            var docViewTop = $(window).scrollTop();
            var docViewBottom = docViewTop + $(window).height();
            var elemTop = $(elem).offset().top;
            var elemBottom = elemTop + $(elem).height();
            return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
          }

     //Resource page JS ends from here----      
      
      
   //alerthome starts
$('.closebutton').on('click', function()  {
$(".alertbar1").hide();
$(".closebutton").hide();
});








