$(document).ready(function(){
	
	if(localStorage.getItem("pagenav") == 1)
	{
		$("#supportPageId").hide();
       $("#faqId").show();
       $("#faqsection-4").click();
       localStorage.removeItem("pagenav");
	}
	$(".returnsredtxt").on('click',function(){
	localStorage.setItem("pagenav",1);
	//alert(localStorage.getItem("pagenav"));
		window.location.href="/faqs";
		//$("#viewAllFaqs").trigger('click');
		
		
		
	});
	
    var uri = window.location.toString();
	if (window.location.pathname!='/invoicesDetails' && uri.length > 0 && uri.indexOf('/returnsandcredit')>=0) {
	
    var orderTable=$('#returnsTable').dataTable({
    	"ajax": {
			"url": ACC.config.contextPath + "/returnsandcredit/getCredits",
			"type": "GET",
			 "dataSrc": "creditList",
		    },
		    "oLanguage": {
		        "sEmptyTable": "Credits not available"
		    },
        "columns": [
          { "data": "invoiceNumber"},
		  { render: function (data, type, row) {if(row.amoutWithTax=='0'){ return 'N/A' }else{return row.currency+row.amoutWithTax;}}},
          { "data": "invoiceDate" },
          { "data": "status" },
        ],
          "pagingType":"full_numbers",
          "pageLength": 10,
          "lengthChange": false,
          "info":false,
          "autoWidth":false,
          "language": {
          "search": "",
          "paginate": {
            "first": "<<",
            "previous": "<",
            "next": ">",
            "last": ">>",
          }
      },
        "initComplete": function(settings, json) {
        	$("#returnsTable thead tr th:nth-child(even)").css("background","none");
        	if($("#returnsTable tr:first-child td:first-child").html()=="N/A" || $("#returnsTable tr").length<11)
        	{
        		$("#returnsTable_paginate").hide();
        	}
        	else
        	{
        		$("#returnsTable_paginate").show();
        	}

        }
      });
      orderTable= $("#returnsTable_filter input").attr("placeholder", "Search by order #, etc");
      }
 });
  



  
  


