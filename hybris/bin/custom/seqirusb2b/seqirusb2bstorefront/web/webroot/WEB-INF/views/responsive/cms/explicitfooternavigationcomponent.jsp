<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer"
	tagdir="/WEB-INF/tags/responsive/common/footer"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>



<div class="col-xs-12 footerdiv">
	<div class="col-xs-12 footertopsection">
		<div class="col-md-2 col-xs-12 footerlogosection">
			<div class="footerlogo"></div>
			<div class="footersiteview">
			<div class="linkbtton"></div>
				<a class="footer-anchorlink" href="https://www.seqirus.com/" target="blank">seqirus.co.uk </a>
			</div>
		</div>
		<div class="col-md-2 col-xs-12 marTop-30 footerCustomer">
			<div class="footertermsheader" style="color: #555555;">Customer Service</div>
			<p class="footerpara-1 footermar">Our Customer Service Team is available 08.00 - 17.00</p>
			<div class="footertermsheader" style="color: #555555;">Call</div>
			<div class="footerterm footermar"><a href="tel:0845 745 1500" class="footerterms">0845 745 1500</a></div>
			<div class="footertermsheader" style="color: #555555;">Email</div>
			<div class="footerterm footermar"><a href="mailto:service.uk@seqirus.com" class="footerterms">service.uk@seqirus.com</a></div>
		</div>
		<div class="col-md-2 col-xs-12 marTop-30">
	               <div class="footertermsheader" style="color: #555555;">Legal</div>
	               <div class="footerterm marTop-25" ><a href="#" class="footerterms" target="_blank">Terms of Use</a></div>
	               <div class="footerterm"><a href="#" class="footerterms" target="_blank">Terms of Sale</a></div>
	               <div class="footerterm"><a href="#" class="footerterms" target="_blank">Privacy Policy</a></div>          
	           
        </div>
        
	
		<div class="col-md-5 col-xs-12 footertermsheader marTop-30">
			<div
				style="font-size: 14px; margin-bottom: 20px;">
				<spring:theme code="footer.text1" />
			</div>
			<div>
				<p class="footerpara-1">
					<spring:theme code="footer.text2" />
					<a href="https://www.mhra.gov.uk/yellowcard" class="footerredlink" target="_blank"><spring:theme
							code="footer.link1" /> </a>
					<spring:theme code="footer.text3" />
				</p>
				<p class="footerpara-2 marTop-25">
					<spring:theme code="footer.text4" />
					<a href="#" class="footerredlink" target="_blank"><spring:theme
							code="footer.link2" /></a>
					<spring:theme code="footer.text5" />
				</p>
			</div>
		</div>
	</div>
	<c:if test="${cmsPage.uid eq 'homepagelanding'}">
	<div class="copyrighttext col-xs-12 col-md-11">
         	<spring:theme code="logoutstatefooter.copyright.text.line1" />
    </div>
    </c:if>
</div>
