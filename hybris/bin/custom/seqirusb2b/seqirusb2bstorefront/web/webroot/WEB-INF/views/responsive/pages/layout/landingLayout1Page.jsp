<%-- <%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">

	<div class="post_login">

		<div class="container-fluid body-section">
			<div class="container">
				
				<div class="row">
					<div class="col-md-12 marBottom40">
						<h2 class="contactheader">
							<cms:pageSlot position="Section1" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</h2>
					</div>
				</div>

				<div class="container dashboard-body">

					<div class="col-md-6 no-pad">
						<div class="blkfirst">
							<div class="col-md-12 no-pad">
								<table class="table dashboard-heading-table">
									<tr>
										<td class="heading-txt">Orders</td>
										<td><a href="#" class="view_order_link">View All
												Orders</a></td>
									</tr>
								</table>
							</div>

							<div class="col-md-12 white-background">
								<div class="box-heading-dashboard">You have 2 orders
									pending</div>
								<div class="dashboard-table-wrapper">
									<table class="table dashboard-table">
										<tr>
											<td>Order#123456789</td>
											<td><a href="#" class="view_order_link">View Order</a></td>
										</tr>
										<tr>
											<td>Order#123456789</td>
											<td><a href="#" class="view_order_link">View Order</a></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-6 no-pad">
						<div class="blklast">
							<div class="col-md-12 no-pad">
								<table class="table dashboard-heading-table">
									<tr>
										<td class="heading-txt">Invoices</td>
										<td><a href="#" class="view_order_link">View All
												Invoices</a></td>
									</tr>
								</table>
							</div>

							<div class="col-md-12 white-background">
								<div class="box-heading-dashboard">You have 3 Invoices
									pending</div>
								<div class="dashboard-table-wrapper">
									<table class="table dashboard-table">
										<tr>
											<td>Invoice#123456789</td>
											<td><a href="#" class="view_order_link">View Order</a></td>
										</tr>
										<tr>
											<td>Invoice#123456789</td>
											<td><a href="#" class="view_order_link">View Order</a></td>
										</tr>
										<tr>
											<td>Invoice#123456789</td>
											<td><a href="#" class="view_order_link">View Order</a></td>
										</tr>
									</table>

								</div>
							</div>
						</div>

					</div>


					<div class="col-md-6 no-pad">
						<div class="blklast">
							<div class="col-md-12 no-pad">
								<table class="table dashboard-heading-table">
									<tr>
										<td class="heading-txt">Notifications <span>2</span></td>
										<td><a href="#" class="view_order_link">View All
												Notifications</a></td>
									</tr>
								</table>
							</div>

							<div class="col-md-12 dashboard-notification no-pad">

								<div class="notification-block">
									<cms:pageSlot position="Section3A" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
								<div class="notification-block">
									<cms:pageSlot position="Section3B" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
								<div class="notification-block">
									<cms:pageSlot position="Section3C" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
								<div class="notification-block">
									<cms:pageSlot position="Section3D" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>

							</div>
						</div>

					</div>


					<div class="col-md-6 no-pad">
						<div class="blklast">
							<div class="col-md-12 no-pad">
								<table class="table dashboard-heading-table">
									<tr>
										<td class="heading-txt">Support</td>
										<td><a href="${contextPath}/support"
											class="view_order_link">More Support</a></td>
									</tr>
								</table>
							</div>

							<div class="col-md-12 no-pad dashboard-support">

								<div class="col-md-6 col-sm-6 col-xs-12 support-left">
									<cms:pageSlot position="Section2B" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12 support-right">
									<cms:pageSlot position="Section2C" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>

							</div>
						</div>

					</div>

				</div>

			</div>
		</div>

	</div>
</template:page> --%>