<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="userProfileInfo"
	tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="userCompanyInfo"
	tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="userAccountInfo"
	tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="userLocationsInfo"
	tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<template:page pageTitle="${pageTitle}">
<div class="container-fluid body-section">

	<div class="hiddenYear" id="${presentSeason}"></div>
		
		<div class="totalOrderFluadTiv10x" id="${totalOrderFluadTiv10x}"></div>
		<div class="totalOrderFluadTiv1x" id="${totalOrderFluadTiv1x}"></div>
		<div class="totalOrderFluadQiv10x" id="${totalOrderFluadQiv10x}"></div>
		<div class="totalOrderFluadQiv1x" id="${totalOrderFluadQiv1x}"></div>
		<div class="totalOrderFlucelvaxQiv10x" id="${totalOrderFlucelvaxQiv10x}"></div>
		<div class="totalOrderFlucelvaxQiv1x" id="${totalOrderFlucelvaxQiv1x}"></div>
		<div class="totalOrderAdjuvantedTiv10x" id="${totalOrderAdjuvantedTiv10x}"></div>
		<div class="totalOrderAdjuvantedTiv1x" id="${totalOrderAdjuvantedTiv1x}"></div>
		<div class="totalOrderFlucelvax1x" id="${totalOrderFlucelvax1x}"></div>
		<div class="totalOrderFlucelvax10x" id="${totalOrderFlucelvax10x}"></div>
		
		<div class="totalOrder" id="${totalOrder}"></div>
				
		
		<div class="totalShipFluadTiv10x" id="${totalShipFluadTiv10x}"></div>
		<div class="totalShipFluadTiv1x" id="${totalShipFluadTiv1x}"></div>
		<div class="totalShipFluadQiv10x" id="${totalShipFluadQiv10x}"></div>
		<div class="totalShipFluadQiv1x" id="${totalShipFluadQiv1x}"></div>
		<div class="totalShipFlucelvaxQiv10x" id="${totalShipFlucelvaxQiv10x}"></div>
		<div class="totalShipFlucelvaxQiv1x" id="${totalShipFlucelvaxQiv1x}"></div>
		<div class="totalShipAdjuvantedTiv10x" id="${totalShipAdjuvantedTiv10x}"></div>
		<div class="totalShipAdjuvantedTiv1x" id="${totalShipAdjuvantedTiv1x}"></div>
		<div class="totalShipFlucelvax1x" id="${totalShipFlucelvax1x}"></div>
		<div class="totalShipFlucelvax10x" id="${totalShipFlucelvax10x}"></div>
		
		
		<div class="totalShip" id="${totalShip}"></div>

      <div class="container" style="width:1170px">
         <nav aria-label="supportbreadcrumb" >
            <ol class="supportbreadcrumb">
            <c:url value="/" var="homeUrl" />
               <li class="supportbreadcrumb-item active"><a href="${homeUrl}" class="blacktext"><spring:theme code="breadcrumb.home" /></a></li>
               <li class="supportbreadcrumb-item" aria-current="page">Orders</li>
            </ol>
         </nav>
         <div class="row">
            <div class="col-md-12 marBottom40">
               <h2 class="contactheader">ORDERS</h2>  
            </div>
            
            <div class="col-md-3 left-order-section" id="dropdownOrders">
               <div class="search-sec">
               <select class="form-control selectpicker" id="ordersDropdown" >
               <c:forEach items="${seasonList}" var="seasons" varStatus="seasonLoop">
               <option value="${seasons.orderSeason}" ${(seasons.orderSeason eq presentSeason) ? 'selected="selected"' : ''}>${seasons.orderSeason}</option>

                 </c:forEach>
                 </select>
               </div>
               <div class="order-left-sec">
                  <div class="order-title"><a href="${contextPath}/orders?&season=${presentSeason}"><i class="fa fa-angle-right" aria-hidden="true"></i> <span>All Orders</span></a></div>
                  <div class="search-order-sec">
                     <div class="form-group">
                        <div class="input-group spcl-addon">  
                        <div class="input-group-addon"><span class="fa-search fa"></span></div>
                        <input class="form-control" placeholder="Search Orders" id="search-orders" type="text">
                        </div>
                        
                     </div>
                  </div>

                  <div class="left-navigation-order">
                     <div class="customer-scroll" id="custom-scrollbar">
                     <c:forEach items="${shipAddressIds}" var="shipAddressId" varStatus="ship">
                     <div class="menu_container">

                        <!--Location 1-->
                        <div class="menu-title">

 <c:forEach items="${orders}" var="activeOrder" varStatus="activeOrderLoop">
		 <c:if test="${activeOrder.shipToID == shipAddressId}">
		 <c:set var= "locationName" value="${activeOrder.partnerName}"/>
		 </c:if>
		 </c:forEach>
          Location ${ship.index+1} - ${locationName}

	<i class="fa fa-chevron-up" aria-hidden="true"></i></div>
                       
                        <div class="expand-menu" id="expandOrder">
                        <c:forEach items="${orders}" var="activeOrder" varStatus="activeOrderLoop">
                       <c:if test="${activeOrder.shipToID == shipAddressId}">
                       <input type="hidden" id="orderStatus${activeOrderLoop.index}" value="${activeOrder.status}"/>
                           <div class="menu-box ordertoOpen" id="expandOrder_${activeOrderLoop.index}" data-searchby="${activeOrder.orderID}">
                           <div class="left-gap"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                           <div class="right-area">
                              <div class="order-status">${activeOrder.status}</div>
                              <c:choose>
                              <c:when test="${activeOrder.status eq 'Closed'}">
                             <div class="order-no" id="${activeOrder.orderID}">Order #${activeOrder.orderID}</div>
                              
                              </c:when>
                              <c:otherwise>
                               <div class="order-no error-ord" id="${activeOrder.orderID}">Order #${activeOrder.orderID}</div>
                              </c:otherwise>
                              
                              </c:choose>
                              <div class="order-date">Placed <fmt:formatDate pattern = "dd/MM/yyyy" value = "${activeOrder.orderDate}"/></div>
                           </div>
                           </div>
                           </c:if>
                        </c:forEach>
                        </div>
                        
                        <!--Location 2-->
                       <!-- <div class="menu-title">Location 2 - Hampshire<i class="fa fa-chevron-up" aria-hidden="true"></i></div>
                         <div class="expand-menu">
                           <div class="menu-box">
                           <div class="left-gap"></div>
                           <div class="right-area">
                              <div class="order-status">OPEN</div>
                              <div class="order-no error-ord">Order #45605645 <i class="fa fa-exclamation-circle" aria-hidden="true"></i></div>
                              <div class="order-date">Placed 29/06/2020</div>

                           </div>
                           </div>
                           <div class="menu-box">
                              <div class="left-gap"></div>
                              <div class="right-area">
                                 <div class="order-status">CLOSED</div>
                                 <div class="order-no">Order #45605645</div>
                                 <div class="order-date">Placed 29/06/2020</div>
   
                              </div>
                              </div>
                        </div> -->

                        <!--Location 3-->
                      <!--  <div class="menu-title">Location 3 - Amstartdam<i class="fa fa-chevron-up" aria-hidden="true"></i></div>
                        <div class="expand-menu">
                           <div class="menu-box">
                           <div class="left-gap"></div>
                           <div class="right-area">
                              <div class="order-status">OPEN</div>
                              <div class="order-no error-ord">Order #45605645 <i class="fa fa-exclamation-circle" aria-hidden="true"></i></div>
                              <div class="order-date">Placed 29/06/2020</div>

                           </div>
                           </div>
                           <div class="menu-box">
                              <div class="left-gap"></div>
                              <div class="right-area">
                                 <div class="order-status">CLOSED</div>
                                 <div class="order-no">Order #45605645</div>
                                 <div class="order-date">Placed 29/06/2020</div>
   
                              </div>
                              </div>
                        </div>-->


                     </div>
                     </c:forEach>
                  </div>

                  </div>

               </div>
            </div>
            
            
            <div class="col-md-9 order-right-section contentarea marBottom40">
            <div class="order-landing order-right-section-bg">
          <c:if test="${totalOrders==0}">
        <div class="container" >
    <div class="row marBottom40 margin-T20" >
           <div class="col-md-8 page-title ">YOU HAVE NO ORDERS PLACED <br/>IN THIS SEASON.</div>
           <!-- <div class="col-md-4 updated-status">Last Updated 2nd August  at 3:02am </div> -->
           
        <div class="row">
                  <div class="col-md-8">Can't find your order? Check that you are searching in the correct season. Otherwise, <a class="emaillink" href="mailto:service.uk@seqirus.com">contact support.</a></div>
               </div>
 </div>
 </div>
 </c:if>
             <c:if test="${totalOrder>0}"> 
               <div class="row">
                  <div class="col-md-5 page-title">ORDER STATUS</div>
                  <%-- <div class="col-md-7 updated-status">Last Updated 2nd August  at 3:02am <span>Refresh Content <a href="${contextPath}/orders?&season=${presentSeason}"><i class="fa fa-repeat" aria-hidden="true"></i></a></span></div> --%>
               </div>
               <div class="row">
                  <div class="col-md-8">This is a summary of all orders placed for this season. Use the filters to view fulfillment status by order and product. </div>
               </div>
               <div class="invoiceHeader col-xs-12 allOrderTableHeader">${presentSeason}</div>
               <div class="row order-filter">
                  <!-- <div class="col-md-2 filter-order-lebel">Filter By</div>
                  <div class="col-md-10 order-filter-drop">
                     <div class="order-drop-down">
                      <div class="col-md-3 no-pad order-status-filter"> 
                       <select class="form-control selectpicker" id="forcast-status">
                        <option style="pointer-events: none;">Orders</option>
                        <option>Filter Value1</option>
                        <option>Filter Value2</option>
                        <option>Filter Value3</option>
                        <option>Filter Value4</option>
                        </select>  
                     </div> 
                     <div class="col-md-9 clear-filter-order no-pad" style="display:none;">
                        <div class="form-group clr-fil">
                           <a href="#/" id="statusclearButton" class="btn btn-md btn-default">Clear All</a>
                       </div>
                     </div>
                     <div class="clear"></div>
                     </div>
                     <div class="clear"></div>
                     <div class="row" id="forcastordervalues" style="display:none;">
                        <div class="filter-title floatLeft margin-T20 marginPL10" id="filterdisplay">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                       
                        
                        --Multiple Filter Value Populated Start Area-
                        <div id="forcastordersfilter1" class="filter-value-wrap"></div> 
                        --Multiple Filter Value Populated Ends Area-                       
                        
                       
                        <div class="clear "></div>
                        
                    </div>

                  </div>  -->
                  
               </div>

               <div class="row marBottom40">
                  <div class="col-md-5 col-sm-3 col-xs-12">
                     <div id="circleGraphic"></div>
                 </div>
                 <div class="col-md-7 col-sm-9 col-xs-12">
                     
                     <c:if test="${not empty totalOrderFluadTiv10x && totalOrderFluadTiv10x!=0}">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Fluad <span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe 10 Pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">${totalShipFluadTiv10x}/${totalOrderFluadTiv10x}</div>
                     </c:if>
                     
                     
                     
                     <c:if test="${not empty totalOrderFluadTiv1x && totalOrderFluadTiv1x!=0}">
                      <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Fluad <span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe single pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">${totalShipFluadTiv1x}/${totalOrderFluadTiv1x}</div>
                     </c:if>
                     
                     
                     
                     <c:if test="${not empty totalOrderFluadQiv10x && totalOrderFluadQiv10x!=0}">
                      <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Fluad Tetra<span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe 10 Pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">${totalShipFluadQiv10x}/${totalOrderFluadQiv10x}</div>
                     </c:if>
                     
                     
                     
                     <c:if test="${not empty totalOrderFluadQiv1x && totalOrderFluadQiv1x!=0}">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Fluad Tetra<span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe single pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">${totalShipFluadQiv1x}/${totalOrderFluadQiv1x}</div>
                     </c:if>
                     
                     
                     
                     <c:if test="${not empty totalOrderFlucelvaxQiv10x && totalOrderFlucelvaxQiv10x!=0}">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Flucelvax Tetra<span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe 10 Pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">${totalShipFlucelvaxQiv10x}/${totalOrderFlucelvaxQiv10x}</div>
                     </c:if>
                     
                     
                     <c:if test="${not empty totalOrderFlucelvaxQiv1x && totalOrderFlucelvaxQiv1x!=0}">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Flucelvax Tetra<span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe single pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">${totalShipFlucelvaxQiv1x}/${totalOrderFlucelvaxQiv1x}</div>
                     </c:if>
                     
                     
                     <c:if test="${not empty totalOrderAdjuvantedTiv10x && totalOrderAdjuvantedTiv10x!=0}">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">aTIV <span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe 10 Pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">${totalShipAdjuvantedTiv10x}/${totalOrderAdjuvantedTiv10x}</div>
                     </c:if>
                     
                     
                     <c:if test="${not empty totalOrderAdjuvantedTiv1x && totalOrderAdjuvantedTiv1x!=0}">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">aTIV <span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe single pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">${totalShipAdjuvantedTiv1x}/${totalOrderAdjuvantedTiv1x}</div>
                     </c:if>
                     
                     
                     <c:if test="${not empty totalOrderFlucelvax1x && totalOrderFlucelvax1x!=0}">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Flucelvax Tetra<span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe single pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">${totalShipFlucelvax1x}/${totalOrderFlucelvax1x}</div>
                     </c:if>
                    
                    
                    <c:if test="${not empty totalOrderFlucelvax10x && totalOrderFlucelvax10x!=0}">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Flucelvax Tetra<span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe 10 Pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text">${totalShipFlucelvax10x}/${totalOrderFlucelvax10x}</div>
                     </c:if>
                   
                 </div>
               </div>

                <div class="row marBottom40">
                  <div class="col-md-12">
                        <!-- Table starts -->
                        <div class="tablecontainer ordersTablecontainer"> 
                            <table id="ordersTable" class="display">
                                <thead>
                                    <tr>
                                        <th>Orders</th>
                                        <th>Location</th>
                                       <!--  <th>Status</th>
                                        <th>Est. Delivery</th>
                                        <th></th> -->
                                    </tr>
                                </thead> 
                            </table>
                        </div>
                        <!-- Table Ends --> 
                    </div>

                  </div>

               
               <!-- 
               <div class="row">
                <div class="col-md-4 order-sub-title">Delivery Schedule</div>  
                <div class="col-md-8">This is a summary of all orders placed for this season.This calendar reflects the requested delivery weeks at time of order placement. The schedule will be updated once a named delivery date is confirmed.</div>
               </div>

               <div class="row order-filter">
                  <div class="col-md-2 filter-order-lebel">Filter By</div>
                  <div class="col-md-10">
                     <div class="order-drop-down">
                        <div class="col-md-3 no-pad forcast-select product-forcast-filter"> 
                           <select class="form-control selectpicker" id="product_filter">
                              <option style="pointer-events: none;">(2) Product</option>
                              <option>Filter Value1</option>
                              <option>Filter Value2</option>
                              <option>Filter Value3</option>
                              <option>Filter Value4</option>
                           </select> 
                        </div>
                        <div class="col-md-3 no-pad forcast-select order-forcast-filter"> 
                           <select class="form-control selectpicker" id="order_deliver_filter">
                              <option style="pointer-events: none;">Orders</option>
                              <option>Filter Value1</option>
                              <option>Filter Value2</option>
                              <option>Filter Value3</option>
                              <option>Filter Value4</option>
                           </select>
                        </div>
                       <div class="col-md-6 clear-filter-order-shipping no-pad" style="display:none;">
                        <div class="form-group floatLeft greyLine clr_botn">
                           <a href="#/" id="clearButton" class="btn btn-md btn-default">Clear All</a>
                       </div>
                       </div>
                       <div class="clear"></div>
                       </div>
                       <div class="clear"></div>
                       <div class="row" id="filteredvalues" style="display:none;">
                          <div class="filter-title floatLeft margin-T20 marginPL10" id="filterdisplay">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                         
                          
                         <!-----This is the three Filter Buttons will be started-
                        
                         <div id="forprimaryfilter" class="filter-block"></div>
                         <div id="forcityfilter" class="filter-block"></div>
                         <div id="forstatefilter" class="filter-block"></div>
                         
                          <!-----This is the three Filter Buttons will be ended                     
                          
                         
                          <div class="clear "></div>
                          
                      </div>

                  </div> 
                  
               </div>

               <div class="row">
                  <div class="col-md-12 col-xs-12 calender-order-page no-pad">
                  <div id="calendar">
                  <div class="delivery-info">
                    <table class="table table-condensed delivery-table">
                       <tr>
                          <td ><span>2/2</span> Shipments Delivered</td>
                          <td ><i class="fa fa-arrow-circle-o-right" style="color:green;"></i> Shipment Scheduled</td>
                          <td ><i class="fa fa-check-circle-o" style="color:red;"></i> Delivery Made</td>
                       </tr>
                     </table>
    
                   </div>
                  </div>
                  
                  </div>
                
                 
                </div> -->
            </div>
             </c:if>  
            <div class="order-details">
            
<c:forEach items="${orders}" var="activeOrder" varStatus="activeOrderLoop">
              <div class="orderHeader orderHeader${activeOrderLoop.index}">
               <div class="row marBottom40">
                  <div class="col-md-4 order-details-box">
                     <div class="title">Order #</div>
                     <div class="white-box" id="${activeOrder.orderID}">${activeOrder.orderID}<span>Placed On: <fmt:formatDate pattern = "dd/MM/yyyy" value = "${activeOrder.orderDate}" /> </span></div>

                  </div>
                  <div class="col-md-4 order-details-box">
                     <div class="title">Total Units</div>
                     <div class="white-box-table">
                        <table class="table table-condensed">
                           <tr>
                              <td>Flucelvax</td>
                              <td id="${activeOrder.totalFlucelvax}" >${activeOrder.totalFlucelvax}</td>
                           </tr>
                           <tr>
                              <td>Fluad</td>
                              <td id="${activeOrder.totalFluad}" >${activeOrder.totalFluad}</td>
                           </tr>
                            <tr>
                              <td>Adjuvanted</td>
                              <td id="${activeOrder.totalAfluria}" >${activeOrder.totalAfluria}</td>
                           </tr>
                         </table>
                     </div>

                  </div>
                  <div class="col-md-4 order-details-box">

                     <div class="title">Total Cost</div>
                     <div class="white-box">&#163;<fmt:formatNumber type = "number" maxFractionDigits = "2" minFractionDigits = "2" value="${activeOrder.totalCost}"/><span><a href="${contextPath}/invoices?season=${presentSeason}">See invoices for<br/> this Order</a></span></div>

                  </div>
               </div>

               <div class="row marBottom40">
                  <div class="col-md-4 order-white-rect">
                     <div class="title">P.O Number</div>
                     <div class="white-area">${activeOrder.poNumber}</div>

                  </div>
                  <div class="col-md-8 order-white-rect">
                     <div class="title">Shipping Location</div>
                     <div class="shipping-loc">
                     <div class="col-md-5 grey">${activeOrder.partnerName}</div>   
                     <div class="col-md-7 shipp-address"> <c:if test="${not empty activeOrder.address.addressLine2 && not empty activeOrder.address.addressLine1 }">${activeOrder.address.addressLine2},&nbsp;${activeOrder.address.addressLine1},&nbsp;${activeOrder.address.city},&nbsp;${activeOrder.address.state}<br/>${activeOrder.address.zipCode}
                              </c:if>
                              <c:if test="${ empty activeOrder.address.addressLine2}">${activeOrder.address.addressLine1},&nbsp;${activeOrder.address.city},&nbsp;${activeOrder.address.state}<br/>${activeOrder.address.zipCode}
                              </c:if>   
                              <c:if test="${ empty activeOrder.address.addressLine1}">${activeOrder.address.addressLine2},&nbsp;${activeOrder.address.city},&nbsp;${activeOrder.address.state}<br/>${activeOrder.address.zipCode}
                              </c:if></div>    
                     </div>

                  </div>
               </div>
               </div>
              </c:forEach>
               <div class="order-right-section-bg">

               <div class="row">
                  <div class="col-md-5 page-title">ORDER STATUS</div>
                  <div class="col-md-7 updated-status"></div>
               </div>
               <div class="row marBottom40">
                  <div class="col-md-8">This is a summary of all orders placed for this season. Use the filters to view fulfillment status by order and product. </div>
               </div>
               <div class="invoiceHeader col-xs-12">${presentSeason}</div>

               <div class="row marBottom40">
                  <div class="col-md-5 col-sm-3 col-xs-12">
                     <div id="circleGraphic1"></div>
                 </div>
                 <div class="col-md-7 col-sm-9 col-xs-12">                     
                     
                     <div id="fluadTiv10PxId">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Fluad  <span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe 10 Pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text" id="fluadTiv10Px">0/0</div>
                     </div>
                     
                     <div id="fluadTiv1PxId">
                      <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Fluad  <span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe single pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text" id="fluadTiv1Px">0/0</div>
                     </div>
                     
                     
                     <div id="fluadQiv10PxId">
                       <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Fluad  Tetra<span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe 10 Pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text" id="fluadQiv10Px">0/0</div>
                     </div>                     
                     
                         <div id="fluadQiv1PxId">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Fluad  Tetra<span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe single pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text" id="fluadQiv1Px">0/0</div>
                     </div>                     
                     
                     <div id="flucelvaxQiv10PxId">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Flucelvax Tetra <span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe 10 Pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text" id="flucelvaxQiv10Px">0/0</div>
                     </div>
                     
                                          
                     <div id="flucelvaxQiv1PxId">
                      <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Flucelvax Tetra <span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe single pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text" id="flucelvaxQiv1Px">0/0</div>
                     </div>
                                          
                     <div id="adjuvantedTiv10PxId">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">aTIV  <span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe 10 Pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text" id="adjuvantedTiv10Px">0/0</div>
                      </div>                     
                     
                     <div id="adjuvantedTiv1PxId">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">aTIV <span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe single pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text" id="adjuvantedTiv1Px">0/0</div>
                     </div>
                     
                     
                     <div id="flucelvax1PxId">
                          <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Flucelvax Tetra <span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe single pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text" id="flucelvax1Px">0/0</div>
                    </div>
                                         
                     
                     <div id="flucelvax10PxId">
                          <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="prodcuttitle-text">Flucelvax Tetra <span class="prodcuttitle-Subtext">- 0.5mL prefilled syringe 10 Pack</span></div>
                     </div>
                     <div class="col-md-9 col-sm-6 col-xs-12">
                         <div class='container-prog'>
                             <div class='progress-bar-vertical'>
                                 <div class='progress-now-vertical'></div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12 value-text" id="flucelvax10Px">0/0</div>
                     </div>
                     
                                          
                 </div>
               </div>

                <div class="row marBottom40">
                  <div class="col-md-12">
                        <!-- Table starts -->
                        <div class=" tablecontainer2 ordersTablecontainer"> 
                            <table id="ordersTable1" class="display">
                                <thead>
                                    <tr>
                                        <th>Orders</th>
                                        <th>Location</th>
                                        <!-- <th>Status</th>
                                        <th>Est. Delivery</th>
                                        <th></th> -->
                                    </tr>
                                </thead> 
                            </table>
                        </div>
                        <!-- Table Ends --> 
                    </div>

                  </div>

               
<!-- 
               <div class="row">
                <div class="col-md-4 order-sub-title">Delivery Schedule</div>  
                <div class="col-md-8">This is a summary of all orders placed for this season.This calendar reflects the requested delivery weeks at time of order placement. The schedule will be updated once a named delivery date is confirmed.</div>
               </div>

               <div class="row order-filter order-filter-details">
                  <div class="col-md-2 filter-order-lebel">Filter By</div>
                  <div class="col-md-10">
                     <div class="order-drop-down">
                        <div class="col-md-3 no-pad forcast-select product-forcast-filter"> 
                           <select class="form-control selectpicker" id="product_filter_order_details">
                              <option style="pointer-events: none;">(2) Product</option>
                              <option>Filter Value1</option>
                              <option>Filter Value2</option>
                              <option>Filter Value3</option>
                              <option>Filter Value4</option>
                           </select> 
                        </div>
                        <div class="col-md-3 no-pad forcast-select order-forcast-filter"> 
                           <select class="form-control selectpicker" id="order_deliver_filter_order_details">
                              <option style="pointer-events: none;">Orders</option>
                              <option>Filter Value1</option>
                              <option>Filter Value2</option>
                              <option>Filter Value3</option>
                              <option>Filter Value4</option>
                           </select>
                        </div>
                       <div class="col-md-6 clear-filter-order-shipping no-pad" style="display:none;">
                        <div class="form-group floatLeft greyLine clr_botn">
                           <a href="#/" id="clearButton" class="btn btn-md btn-default">Clear All</a>
                       </div>
                       </div>
                       <div class="clear"></div>
                       </div>
                       <div class="clear"></div>
                       <div class="row" id="filteredvalues" style="display:none;">
                          <div class="filter-title floatLeft margin-T20 marginPL10" id="filterdisplay">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                         
                          
                         ---This is the three Filter Buttons will be started--
                        
                         <div id="forprimaryfilter" class="filter-block"></div>
                         <div id="forcityfilter" class="filter-block"></div>
                         <div id="forstatefilter" class="filter-block"></div>
                         
                          ---This is the three Filter Buttons will be ended--                     
                          
                         
                          <div class="clear "></div>
                          
                      </div>

                  </div> 
                  
               </div> -->

               <!-- <div class="row">
                  <div class="col-md-12 col-xs-12 calender-order-page no-pad">
                  <div id="order-details-calendar">
                  <div class="delivery-info">
                    <table class="table table-condensed delivery-table">
                       <tr>
                          <td ><span>2/2</span> Shipments Delivered</td>
                          <td ><i class="fa fa-arrow-circle-o-right" style="color:green;"></i> Shipment Scheduled</td>
                          <td ><i class="fa fa-check-circle-o" style="color:red;"></i> Delivery Made</td>
                       </tr>
                     </table>
    
                   </div>
                  </div>
                  
                  </div>
                
                 
                </div> -->
               </div>
            </div>
            </div> 
            </div>
            
         </div>

         
         
      </div>
</template:page>