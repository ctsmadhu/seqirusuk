<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<template:page pageTitle="${pageTitle}">


<div class="alertbar1">
  
          <cms:pageSlot position="Section1Z1" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					
					      
</div>

	<div class="container-fluid maincontent">
		<div class="row">
			<div class="col-md-12">
				<div class="container-fluid maincontent homepage-background">
					<cms:pageSlot position="Section2E" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					<div class="row marBottom40">
						<div class="col-md-10 col-xs-10 homepagegreyshadow">
							<div class="container">
								<div class="row">
									<cms:pageSlot position="Section2B" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
									<cms:pageSlot position="Section2D" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<!-- 	<div class="container-fluid">
      <div class="row redalertbar">
         <div class="col-md-9 redalertpad">By using our website, you agree to our use of cookies to analyze website traffic and improve your experience on our website. 
         Learn more about the types of cookies we use by reviewing our updated Privacy Policy.</div>
         <div class="col-md-3 redalertpad">
            <div><a class="whiteagreebtn" href="#">Agree</a></div>
         </div>
   </div>
</div> -->
</template:page>