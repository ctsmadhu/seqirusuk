<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/responsive/nav/breadcrumb"%>

<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:theme code="resetPwd.title" var="pageTitle"/>

<template:page pageTitle="${pageTitle}">
	<div id="breadcrumb" class="col-sm-12 col-md-12">
			<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}" />
	</div>
	<div class="container-fluid maincontent col-xs-12">
			<div class="contentArea col-xs-11">
				<div class="leftcontentArea col-xs-12 col-md-4">
					<div class="leftHeader col-xs-11">
				        LOG IN
				     </div>
				     <div class="leftsubHeader col-xs-11">
				        Access orders, view invoices, and find relevant resources for your clinics.
				      </div>				
				</div>
				<div class="rightcontentArea col-xs-12 col-md-8" id="rightSection_1">
					<user:updatePwd/>
				</div>
			</div>
		</div>
</template:page>
