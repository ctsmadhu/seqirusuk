<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">


   <!--Body code starts -->
<nav aria-label="breadcrumb">
			<ol class="breadcrumb padLeft79">
				<c:url value="/" var="homeUrl" />
				<li class="breadcrumb-item active"><a href="${homeUrl}"
					class="blacktext"><spring:theme code="breadcrumb.home" /></a></li>
				 <li class="breadcrumb-item" aria-current="page"><spring:theme
						code="breadcrumb.about" /></li>
			</ol>
		</nav>

        <!--Cookie Policy Logout code starts -->
        <main role="main" class="cookiePolicyLogout col-xs-12 no-padding" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cookiePolicyLogout_parent_header no-padding-left">
                <div class="cookiePolicyLogout_left_header">COOKIE POLICY</div>
            </div>
            <div class="clearboth"></div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 cookiePolicyLogout_leftside no-padding-right">
                <div class="cookiePolicyLogout_leftText1">Your Cookie Settings</div>
                <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12 cookiePolicyLogout_leftText">

                    <div class="cookiePolicyLogout_leftText2">You can opt into each cookie category (except essential cookies) by clicking on the categories below.</div>

				 <c:set var="cookiePreferences" value="${cookie['cookie-preferences'].getValue()}"/>
				  <c:set var="cookieNotification" value="${cookie['cookie-notification'].getValue()}"/>
				  <c:choose>
				 	<c:when test="${cookieNotification == 'ACCEPTED' && cookiePreferences == 'NOT_ACCEPTED'}">
	                    <label class="container">Essential Only
	                        <input id="essentialCookie" type="radio" name="cookieRadio" value="Essential Only" checked="checked">
	                        <span class="checkmark"></span>
	                    </label>
                    </c:when>
                    <c:otherwise>
                    	<label class="container">Essential Only
	                        <input id="essentialCookie" type="radio" name="cookieRadio" value="Essential Only">
	                        <span class="checkmark"></span>
	                    </label>
                    </c:otherwise>
                  </c:choose>
                  <c:choose>
				 	<c:when test="${cookieNotification == 'ACCEPTED' && cookiePreferences == 'ACCEPTED'}">
	                    <label class="container">Accept All Cookies
	                        <input id="allCookie" type="radio" name="cookieRadio" value="Accept All Cookies" checked="checked">
	                        <span class="checkmark"></span>
	                    </label>
                    </c:when>
                    <c:otherwise>
                    	<label class="container">Accept All Cookies
                        <input id="allCookie" type="radio" name="cookieRadio" value="Accept All Cookies">
                        <span class="checkmark"></span>
                    </label>
                     </c:otherwise>
                  </c:choose>
                    <div class="clearboth"></div>
                    <button class="cookiePolicyLogoutBtn active">Save</button>
                    <div class="clearboth"></div>
                </div>
            </div>

<cms:pageSlot position="Section6" var="feature">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 cookiePolicyLogout_rightside no-padding-left">
                <div class="cookiePolicyLogout_label padding-B30">${feature.title}</div>
                <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12 cookiePolicyLogout_rightSubpart">

                    <div class="cookiePolicyLogout_section" id="cookiePolicyLogout_section1">

                        <div class="cookiePolicyLogout_text1 padding-B30">${feature.content}</div>


                    </div>
                </div>
            </div>
            </cms:pageSlot>
<div class="clearboth"></div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cookiePolicyLogout_tableLayout">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="cookiePolicyLogout_lowerText1 padding-B30">Managing your cookie options:</div>
                    <div class="cookiePolicyLogout_lowerLayout">
                    <cms:pageSlot position="Section7" var="feature">
                        <div class="cookiePolicyLogout_lowerText2 padding-B30">${feature.title}</div>
                        <div class="cookiePolicyLogout_lowerText3 padding-B30">${feature.content}</div>
                        </cms:pageSlot>
                        <div><table class="cookiePolicyLogout_table">
                                <tr>
                                    <th>Cookie Name</th>
                                    <th>Purpose</th>
                                    <th>Retention & Additional Information</th>
                                </tr>
                                <tr>
                                    <td>AMCV_{Profile ID}<br>ucid<br>gig_bootstrap_3_{ID}<br>hasGmid<br>gmid<br>gig_canary_ve<br>gig_canary<br>acceleratorSecureGUID<br>anonymous-consents<br>ROUTE<br>JESSIONID</td>
                                    <td>Cookies set by the system that control your profile once you log in.</td>
                                    <td>Category: System, Essential<br>Duration: 1 year<br>Owner: gigya.com</td>
                                </tr>
                                <tr>
                                    <td>remember-me</td>
                                    <td>Remembers your details for easier login</td>
                                    <td>Category: System, Essential<br>Duration: 1 year<br>Owner: gigya.com</td>
                                </tr>
                                <tr>
                                    <td>visitor_type</td>
                                    <td>Verifies that your are a healthcare provider eligible to use the site.</td>
                                    <td>Category: System, Essential<br>Duration: 1 year<br>Owner: flu360.co.uk</td>
                                </tr>
                                <tr>
                                <td>cookie-preferences<br>cookie-notification</td>
                                <td>Determines if the cookie banner should be displayed.</td>
                                <td>Category: System, Essential<br>Duration: 1 year<br>Owner: flu360.co.uk</td>
                                </tr>
                            </table></div>
<cms:pageSlot position="Section8" var="feature">
                        <div class="cookiePolicyLogout_lowerText4 padding-B30 padding-T30">${feature.title}</div>
                        <div class="cookiePolicyLogout_lowerText5 padding-B30">${feature.content}</div>
                        </cms:pageSlot>
                        <table class="cookiePolicyLogout_table">
                            <tr>
                                <th>Cookie Name</th>
                                <th>Purpose</th>
                                <th>Retention & Additional Information</th>
                            </tr>
                            <tr>
                                <td>_ga<br>_gat<br>_gid<br>AMP_TOKEN<br>_gac_<property-id><br>_gac_gb_<container-id><br>_ga_<container-id><br>_utma<br>_utmb<br>_utmc<br>_utmz<br>_utmt<br>_utmv<br>jtitl<br>orgt<br>bust</td>
                                <td>Google Analytics cookies gather information on the number of visits, pages viewed, the duration of time spent on pages and other site statistics.<br><br>You can disable all Google Analytics on your web browser using the Google Analytics Opt-Out Browser Add-on.<br><br><a href="https://policies.google.com/technologies/cookies" target="_blank">Learn more about how Google uses cookies and their privacy policy.</a></td>
                                <td>Category: Website Analytics<br>Duration: upto 2 years<br>3rd Party: google.com</td>
                            </tr>

                        </table>
<cms:pageSlot position="Section9" var="feature">
                        <div class="cookiePolicyLogout_lowerText6 padding-B30 padding-T30">${feature.title}</div>
                        <div class="cookiePolicyLogout_lowerText7 padding-B30">${feature.content}</div>
                        </cms:pageSlot>
                        <table class="cookiePolicyLogout_table">
                            <tr>
                                <th>Cookie Name</th>
                                <th>Purpose</th>
                                <th>Retention & Additional Information</th>
                            </tr>
                            <tr>
                                <td>1P_JAR</td>
                                <td>Gathers information about how the end user uses the website and any advertising that the end user may have seen before visiting the said website<br><br>This website does not contain advertising and this cookie may not be relevant.</td>
                                <td>Category: Targeting/Advertising<br>Duration: 30 days<br>3rd Party: google.com</td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>

        </main>

        <!--Cookie Policy Logout code ends -->

	</template:page>