<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>

<spring:htmlEscape defaultHtmlEscape="true"/>


    <div class="section1_header col-xs-12"><spring:theme code="text.account.profile.resetPassword"/></div>
    <form:form method="post" modelAttribute="updatePwdForm">
            <div class="col-xs-12 formContent">
                <div class="passDiv">
                    <formElement:formPasswordBox idKey="password" labelKey="updatePwd.pwd" 
                    							 inputCSS="form-control password-strength" path="pwd" mandatory="true"/>                                               
                </div>
                <div class="passConfirmdiv">
                    <formElement:formPasswordBox idKey="updatePwd.checkPwd" labelKey="updatePwd.checkPwd" 
                                                 inputCSS="form-control" path="checkPwd" mandatory="true"/>
                </div>
                <div class="col-xs-12 prevnmail">                   
                        <button type="submit" class="logInbtn">
                            <spring:theme code="updatePwd.submit"/>
                        </button>          
                </div>
            </div>
    </form:form>   
