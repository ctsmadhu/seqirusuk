<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="blkfirst">
                    
                    <div class="heading-txt">Notification Preferences</div>  
                  <div class="col-md-12 white-background-profile">
                    <div class="col-md-12 no-pad email-section">
                        <table class="table dashboard-heading-table profile-heading-table">
                            <tr>
                                <td>
                                    <div class="profile-field">Email Notifications</div>
						<div id="company_email_main">
							<sm>
							<em><cms:pageSlot position="Section1E" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot></em></sm>
						</div>
					</td>
                                    <td><a href="javascript:void(0)" class="view_order_link">
                                        <label class="switch" for="checkbox">
                                            <input type="checkbox" id="checkbox" />
                                            <div class="slider round"></div>
                                          </label>

                                    </a>
                                </td>
                            </tr>
                            
                        </table>
                    </div>

                    

                  </div>
</div>

