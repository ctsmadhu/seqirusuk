<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<template:page pageTitle="${pageTitle}">

	<div class="container-fluid portalbody-section">
		<nav aria-label="portalbreadcrumb">
			<ol class="portalbreadcrumb portalpadLeft60">
				<c:url value="/" var="homeUrl" />
				<li class="portalbreadcrumb-item active"><a href="${homeUrl}"
					class="blacktext"><spring:theme code="breadcrumb.home" /></a></li>
				<li class="portalbreadcrumb-item" aria-current="page">Portal
					Usage Agreement</li>
			</ol>
		</nav>
		<div class="portalpageHeader">POLICIES</div>
		<div class="container portal-body">
			<div class="col-md-12 no-pad">
				<div class="bllkfirst white-cotte">
					<div class="col-md-12 no-pad blue-back">
						<span class="portalheading-txt">Consent Management</span>
					</div>
					<div
						class="col-md-12 white-background-portal white-background-portal-consent">
						<div class="col-md-12 marBottom20">
							<div class="col-md-6 portallefttxt">Below is a record of the 'Terms of Use' and 'Privacy Policy' agreements to which you have provided consent.</div>
							<div class="col-md-6 portalrighttxt">
								<a class="linksbtton"
									href="<spring:theme code="portal.termsofuse" />"
									target="_blank"></a> View Current Terms of Use <a
									class="linksbtton"
									href="<spring:theme code="portal.privacy" />" target="_blank"></a>
								View Current Privacy Policy
							</div>
						</div>
						<table class="portalagreement">
							<tr>
								<th>Policy</th>
								<th>Version date</th>
								<th>Language</th>
								<th>Consent Provision</th>
							</tr>
							<c:forEach var="constdata" items="${constData}">
								<tr>
									<td>${constdata.name}</td>
									<td><spring:theme code="portal.versiondate" /></td>
									<td>${portlaLanguage}</td>
									<td>Accepted: <fmt:formatDate
											pattern="hh:mm: a z, dd/MM/yyyy"
											value="${constdata.consentGivenDate}" /></td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-12 no-pad">
				<div class="bllkfirst white-cotte">
					<div class="col-md-12 no-pad blue-back">
						<span class="portalheading-txt">Prescribing Information and Summary of Product Characteristics</span>
					</div>
					<div
						class="col-md-12 white-background-portal white-background-portal-consent portalbody-txt">
						<cms:pageSlot position="Section1" var="feature">
							<div class="col-md-3">
								<div class="grey-Blocks">
									<div class="grey-Labels">
										<!-- <div class="red-labels">Fluad</div> -->
										<img class="portalBoximage"
											src="${feature.productMediaPdf.url}" />
									</div>
									<div class="inner-Content">
										<div class="portal-blocks">
											<p class="Portalboxheading">${feature.title}</p>
											<p class="Portalresource-para"><a href="https://labeling.seqirus.com/API/UK/FluAd-Tetra/EN/FluAd-Tetra-Prescribing-Information.pdf" target="_blank">${feature.content}</a></p>
											<!-- <div class="linksbtton"></div>
                                 Prescribing Info -->
										</div>
									</div>
								</div>
							</div>
						</cms:pageSlot>
						<cms:pageSlot position="Section3" var="feature">
							<div class="col-md-3">
								<div class="grey-Blocks">
									<div class="grey-Labels">
										<!-- <div class="red-labels">Fluad</div> -->
										<img class="portalBoximage"
											src="${feature.productMediaPdf.url}" />
									</div>
									<div class="inner-Content">
										<div class="portal-blocks">
											<p class="Portalboxheading">${feature.title}</p>
											<p class="Portalresource-para"><a href="https://www.medicines.org.uk/emc/product/11679/smpc" target="_blank">${feature.content}</a></p>
											<!-- <div class="linksbtton"></div>
                                 SmPC
 -->
										</div>
									</div>
								</div>
							</div>
						</cms:pageSlot>
						<cms:pageSlot position="Section4" var="feature">
							<div class="col-md-3">
								<div class="grey-Blocks">
									<div class="grey-Labels">
										<!-- <div class="red-labls">Flucelvax</div> -->
										<img class="portalBoximage"
											src="${feature.productMediaPdf.url}" />
									</div>
									<div class="inner-Content">
										<div class="portal-blocks">
											<p class="Portalboxheading">${feature.title}</p>
											<p class="Portalresource-para"><a href="https://labeling.seqirus.com/API/UK/Flucelvax-Tetra/EN/Flucelvax-Tetra-Prescribing-Information.pdf" target="_blank">${feature.content}</a></p>
											<!-- <div class="linksbtton"></div>
                                 Prescribing Info -->
										</div>
									</div>
								</div>
							</div>
						</cms:pageSlot>
						<cms:pageSlot position="Section5" var="feature">
							<div class="col-md-3">
								<div class="grey-Blocks">
									<div class="grey-Labels">
										<!-- <div class="red-labels">Fluad</div> -->
										<img class="portalBoximage"
											src="${feature.productMediaPdf.url}" />
									</div>
									<div class="inner-Content">
										<div class="portal-blocks">
											<p class="Portalboxheading">${feature.title}</p>
											<p class="Portalresource-para"><a href="https://www.medicines.org.uk/emc/product/9753/smpc" target="_blank">${feature.content}</a></p>
											<!-- <div class="linksbtton"></div>
                                 SmPC -->
										</div>
									</div>
								</div>
							</div>
						</cms:pageSlot>
					</div>
				</div>
			</div>
			<!--- <div class="col-md-12 no-pad">
               <div class="bllkfirst white-cotte">
                  <div class="col-md-12 no-pad blue-back">
                     <span class="portalheading-txt">Consent Management</span>
                  </div>
                  <div class="col-md-6 white-background-portal white-background-portal-consent portalbody-txt">
                     This website uses cookies to personalise your experience and measure portal usage.<br>
                     You can manage your consent to the access for this portal <a href="#">here</a><br><br>
                     1. Adverse Events Reporting and Medical Enquiries
                  </div>
                  <div class="col-md-6 white-background-portal white-background-portal-consent portalbody-txt">
                     2. Adverse Events should be reported .Reporting forms and information can be found at <span class="red-txt"><a href="http://www.mhra.gov.uk/yellowcard" target="blank">www.mhra.gov.uk/yellowcard</a></span>.Adverse events should also be reported to Seqirus UK Limited on 01748 828816.<br><br>
                     In the case of a product complaint,Please notify the seqirus customer service team on 0845 745 1500 or <span class="red-txt"><a href="mailto:ukptc@seqirus.com">ukptc@seqirus.com</a></span> and provide a full description of the nature of the complaint,contact details for complaint follow-up,and the batch number(s) of the vaccine(s) concerned.  
                  </div>
               </div>
            </div>-->
		</div>
	</div>

</template:page>