<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<c:forEach items="${component.resourceContentList}" var="comp">
 <c:if test="${empty comp.media.url}">
	<tr>
		<td>${comp.paragraphcontent}</td>
		<td> <a href="${comp.mediaForpdf.downloadurl}">Download PDF</a></td>
			
			
	</tr>
</c:if>	
</c:forEach>