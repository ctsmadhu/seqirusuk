<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>

<div id="ShippingLocation" class="tabcontent ShippingLocation">
<input type="hidden"  id="shippingLocations" value="0"/>
               <div class="section1_header col-xs-12">
                  <spring:theme code="form.register.shipping.header" />
               </div>
               <div class="section1_subheader col-xs-12">
                  <spring:theme code="form.register.shipping.subHeader" />
                  <br>   <input type="radio" id="shippingInfo2" name="shippinginfo" value="<spring:theme code="form.register.shipping.radioText" />">
                  <label class="radioLabel" for="shippingInfo2"><spring:theme code="form.register.shipping.radioText" /></label>
               </div>
               <div class="col-xs-12 shippingformContent shipping-container" id="shipping-container">
                  <div class="add-shipping">
                     
                        <div class="row">
                           <div class="col-xs-12 col-md-12">
                              <div class="form-group col-md-4">
                                 <label for="shippingFirstName"><spring:theme code="form.register.invoice.firstName" /><span class="asteriskred">*</span></label><br/>
                                 <input  class="form-control textonly required-fld-form5" autocomplete="no" data-error="Please enter First name" name="shippingLocations[0].firstName" id="shippingFirstName" placeholder="Edward"  type="text" required/>                              
                                 <div class="help-block with-errors"></div>
                              </div>
                              <div class="form-group col-md-4">
                                 <label for="shippingLastName"><spring:theme code="form.register.invoice.lastName" /><span class="asteriskred">*</span></label><br/>
                                 <input  class="form-control textonly required-fld-form5" autocomplete="no" data-error="Please enter Surname" id="shippingLastName" name="shippingLocations[0].lastName" placeholder="Newgate"  type="text" required/> 
                                 <div class="help-block with-errors"></div>
                              </div>
                           </div>
                        </div> 
                        <div class="row">
                           <div class="col-xs-12 col-md-12">
                              <div class="form-group col-md-3">
                                 <label for="shippingEmail"><spring:theme code="form.register.invoice.email" /><span class="asteriskred">*</span></label><br/>
                                 <input  class="form-control required-fld-form5 email-input5 emailcheck" autocomplete="no" data-error="Please enter valid email" id="shippingEmail" name="shippingLocations[0].email" placeholder="email@example.co.uk"  type="email" required/>                             
                                 <!-- <div class="help-block with-errors"></div> -->
                                 <div class="help-block" id="emailerr5"></div>
                              </div>
                              <div class="form-group col-md-3">
                                 <label for="shippingphoneNumber"><spring:theme code="form.register.invoice.phoneNum" /><span class="asteriskred">*</span></label><br/>
                                 <input  class="form-control phonemask required-fld-form5 phnship"  autocomplete="no" data-error="Please enter a valid UK format Telephone" name="shippingLocations[0].phone" id="shippingphoneNumber" maxlength="11"  type="text" required/> 
                                 <div class="help-block" id="message5"></div>
                              </div>
                              <div class="form-group col-md-2">
                                 <label for="shippingphoneext"><spring:theme code="form.register.invoice.phoneExt" /></label><br/>
                                 <input  class="form-control numberonly" id="shippingphoneext" name="shippingLocations[0].phoneExt"  maxlength="4" type="text"/>  
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-12 col-md-12">
                           	  <div class="form-group col-md-4">
                                 <label for="shippingorgname"><spring:theme code="form.register.shipping.deliveryLocation" /><span class="asteriskred">*</span></label><br/>
                                 <input class="form-control required-fld-form5 with_space" autocomplete="no"  data-error="Please enter Delivery Location Name" name="shippingLocations[0].organizationName" id="shippingorgname" data-toggle="tooltip" data-placement="bottom"  title="<spring:theme code="form.register.business.orgName" />" placeholder="Practice, Pharmacy or Business Name"  type="text" required/>
                                 <div class="help-block with-errors"></div>
                              </div>
                              <div class="form-group col-md-4">
                                 <label for="shippingstreet"><spring:theme code="form.register.shipping.mobileNumber" /></label><br/>
                                 <input  class="form-control numberonly" autocomplete="no" id="mobileNumber"  name="shippingLocations[0].mobilePhone"  type="text" maxlength="11"/> 
                              </div>
                             </div>
                            </div>  
                        <div class="row">
                        <div class="col-md-5 col-xs-12">
                           <div class="form-group col-md-12">
                              <label for="Address">Address Lookup</label>
                              <div class="input-group spcl-addon">  
                              <div class="input-group-addon"><i class="fa fa-search"></i></div>
                              <input class="form-control address-look-ship" style="margin-bottom:0px;" id="addresslookupShipping" name="shippingLocations[0].addressLookUp"  placeholder="Search" type="text">
                              </div>
                             
                           </div>
                        </div>
                     </div>
                         
                        <div class="row">
                           <div class="col-xs-12 col-md-12">
                              <div class="form-group col-md-4">
                                 <label for="shippingaddress"><spring:theme code="form.register.invoice.addr" /><span class="asteriskred">*</span></label><br/>
                                 <input class="form-control required-fld-form5" autocomplete="no" data-error="Please enter Address" id="shippingaddress" name="shippingLocations[0].addressLine1" placeholder="Building Number & Street"  type="text" required/>
                                 <div class="help-block with-errors"></div>
                              </div>
                              <div class="form-group col-md-4">
                                 <label for="shippingstreet"><spring:theme code="form.register.invoice.additionalStreet" /></label><br/>
                                 <input  class="form-control" autocomplete="no" id="shippingstreet" placeholder="Building or Office name/number" name="shippingLocations[0].addressLine2"  type="text"/> 
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-12 col-md-12">
                              <div class="form-group col-md-3">
                                 <label for="shippingcity"><spring:theme code="form.register.invoice.city" /><span class="asteriskred">*</span></label><br/>
                                 <input  class="form-control textonly required-fld-form5" autocomplete="no"  data-error="Please enter City" id="shippingcity" name="shippingLocations[0].city" placeholder="City"  type="text" required/>                             
                                 <div class="help-block with-errors"></div>
                              </div>
                              <div class="form-group col-md-3">
                                 <label for="shippingpostcode"><spring:theme code="form.register.invoice.postCode" /><span class="asteriskred">*</span></label><br/>
                                 <input class="form-control required-fld-form5 post_code post_code5" autocomplete="no" data-error="Please enter valid Post Code" id="shippingpostcode" name="shippingLocations[0].postalCode" placeholder="SL16 8AA"  type="text" required/> 
                                  <div class="help-block with-errors postcodeerror" ></div>
                              </div>
                              <div class="form-group col-md-4">
                                 <label for="shippingcountry"><spring:theme code="form.register.invoice.country" /><span class="asteriskred">*</span></label><br/>
                                 <input  class="form-control textonly required-fld-form5" autocomplete="no"  data-error="Please enter Country" id="shippingcountry" name="shippingLocations[0].country" placeholder="Country"  type="text" required/>  
                                 <div class="help-block with-errors"></div>
                              </div>
                           </div>
                        </div> 
                        <div class="row">
                           <div class="col-xs-12 col-md-12">
                              <div class="form-group col-md-3">
                                 <label for="shippingLicensenumber"><spring:theme code="form.register.invoice.licNum" /><span class="asteriskred">*</span></label><br/>
                                 <input  class="form-control numberonly required-fld-form5 shipLicense" data-toggle="tooltip" data-placement="bottom"  title="<spring:theme code="form.register.shipping.licNumDesc" />"  autocomplete="no" data-error="Please enter valid License Number"  id="shippingLicensenumber" name="shippingLocations[0].licenseNumber"   placeholder="e.g. GMC or GPhC licence"  type="text" maxlength="7" required/> 
                                <!--  <div class="help-block with-errors"></div> -->  
                                 <div class="help-block customLicenseerror"></div>
                              </div>
                              <div class="form-group col-md-3">
                                 <label for="shippingLicensename"><spring:theme code="form.register.invoice.licName" /><span class="asteriskred">*</span></label><br/>
                                 <input class="form-control textonly required-fld-form5"  autocomplete="no" id="shippingLicensename" name="shippingLocations[0].licenseName" data-error="Please enter License Name" placeholder="Jane Smith"  type="text" required/> 
                                 <div class="help-block with-errors"></div>
                              </div>
                              <div class="form-group col-md-3">
                                 <label for="shippingNHS"><spring:theme code="form.register.invoice.nhsCode" /></label><br/>
                                 <input  class="form-control nhs_shp" id="shippingNHS" autocomplete="no" data-toggle="tooltip" data-placement="bottom"  title="<spring:theme code="form.register.shipping.nhsCodeDesc" />" name="shippingLocations[0].nhsCode" type="text" maxlength="7"/>  
                              	 <div class="help-block customNHSerror2"></div>
                              </div>
                           </div>
                        </div>
                       <input type="hidden"  id="shippingLocations" value="0"/> 
                       <input type="hidden"  id="deletionFlag" name="shippingLocations[0].deletionFlag" value=""/> 
                         
                   
                     <div class="col-md-12 col-xs-12">
                        <%-- <button  class="removebutton no-show-removeButton btn btn-default" ><spring:theme code="form.register.invoice.remove" /></button> --%>
                        <div  class="removebutton no-show-ship btn-default" ><spring:theme code="form.register.invoice.remove" /></div>
                     </div>
                     <div class="col-md-12 dotted-horizontal"></div>
                  </div>
                  <div class="add-ship"></div>
                  <div class="col-md-12 col-xs-12">
                     <%-- <button   class="Addbutton btn btn-default addship" disabled="disabled"><spring:theme code="form.register.invoice.adding" /></button> --%>
                     <button type="button" class="Addbutton btn addship btn-default" disabled="disabled"><spring:theme code="form.register.invoice.adding" /></button>
                  </div>
               </div>
                             
              
              
              
               <div class="col-xs-12 shippingprevios">
                  <div class="prevStepbtn pagi-link sub-7" data-section-id="section-4" data-img-id="img-2">< <spring:theme code="form.register.prev" /></div>
                  <%-- <button class="Nextbutton nxt2"><spring:theme code="form.register.next" /></button>
                  <div hidden="" id="step5Next" class="step5 nxt2_2 NextbuttonSubmit pagi-link tab-4 sub-5 sub-remove" data-section-id="section-6" data-img-id="img-4"><spring:theme code="form.register.next" /></div> --%>
                  <!-- <button  class="step5 nxt5_5 Nextbutton5 tab-4 sub-4 sub-remove" id="gotosection_6" >Next</button> -->
                  <button type="submit"  class="step5 nxt5_5 Nextbutton5 tab-4 sub-5 sub-remove" id="gotosection_6">Next</button>
                  <%-- <span class="skiptext" id="step5Skip1"><a class="skiplink tab-4 pagi-link" href="javaScript:void(0);" data-section-id="section-6" data-img-id="img-4" id="step5Skip"><spring:theme code="form.register.skip" /></a></span> --%>
               </div>
            </div> 
               
               

