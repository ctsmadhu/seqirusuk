<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="blkfirst">
                    <div class="heading-txt">Seqirus Account Information</div>  
                    <div class="col-md-12 white-background-profile marBottom40">
                     <div class="col-md-4">
                        <div class="profile-lebel">Account Number</div>
                        <div class="profile-field">${soldToAcc}</div>

                     </div> 

                     <div class="col-md-7">
                        <div class="profile-lebel">Account Contact Info</div>
                        <%-- <div class="profile-field">${soldToEmail}<br/>${soldToPh}&nbsp;&nbsp;&nbsp;Ext. ${soldToExt}</div> --%>
                        <c:if test="${not empty soldToExt}"><div class="profile-field">${soldToEmail}<br/>${soldToPh}&nbsp;&nbsp;&nbsp;Ext. ${soldToExt}</div></c:if>
                        <c:if test="${empty soldToExt}"><div class="profile-field">${soldToEmail}<br/>${soldToPh}&nbsp;&nbsp;&nbsp;</div></c:if>

                     </div>    
                    </div>

                </div>
<div class="clearfix1"></div>