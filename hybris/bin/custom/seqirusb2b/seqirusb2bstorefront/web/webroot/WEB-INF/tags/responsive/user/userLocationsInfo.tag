<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

 <div class="blklast">
                    <div class="heading-txt" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Invoice address may be different to delivery address"><spring:theme code="edit.profile.locations.myLocations" /></div>
                    <div class="col-md-12 white-background-profile">
                        <div class="col-md-12">
                            <div class="col-md-6 col-xs-12 heading-txt-bold"></div>
                            <div class="col-md-6 col-xs-12 align-right-txt"><input type="button" class="Addbutton_profile" value="+ Add New Location"></div>

                        </div>
                        <div class="clearfix1"></div>
                        <div class="col-md-12 no-pad data-table-section">
                            
                            <div class="tablecontainer proTablecontainer">  
                              <div class="center" id="content">
                                <table id="proTable" class="display">
                               
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th><spring:theme code="edit.profile.locations.type" /></th>
                                            <th><spring:theme code="edit.profile.locations.name" /></th>
                                            <th><spring:theme code="edit.profile.locations.address" /></th>
                                            <th><spring:theme code="edit.profile.locations.status" /></th>
                                        </tr>
                                         
                                    </thead>   
                                            
                                </table>
                              </div>
                            </div>

                          
                          
                        </div>
	<c:forEach items="${tableDataList}" var="tableData" >	
<c:set var="shippingAvailable" value="" ></c:set>	
<c:set var="payingAvailable" value="" ></c:set>	
<c:set var="invoicingAvailable" value="" ></c:set>
<c:if test="${tableData.type eq 'Shipping' }">	
<c:set var="shippingAvailable" value="true" ></c:set>	
</c:if>	
<c:if test="${tableData.type eq 'Paying' }">	
<c:set var="payingAvailable" value="true" ></c:set>	
</c:if>	
<c:if test="${tableData.type eq 'Invoicing' }">	
<c:set var="invoicingAvailable" value="true" ></c:set>	
</c:if>	
</c:forEach>
                        <div class="col-md-12 add-ship-section shippingformContent-profile rounded-bg">
                        <div class="col-md-12 ship-radio-buttons" id="selectAddr">
                                              
                                <input class="selectShipTo" type="radio"  name="radiolocation11" checked="checked">
                                <label class="radioLabelsShip" for="shipping-location"><spring:theme code="edit.profile.locations.shippingLoc" /></label>
                                <c:choose><c:when test="${empty payingAvailable}">
                                <input class="selectPayTo" type="radio" name="radiolocation12">
                                </c:when>
                                <c:otherwise>
                                <input class="selectPayTo" type="radio" disabled name="radiolocation12">
                                </c:otherwise>
                                 </c:choose>
                                
                                <label class="radioLabelsPayl" for="paying-location"><spring:theme code="edit.profile.locations.payingLoc" /></label>
                                <c:choose><c:when test="${empty invoicingAvailable}">
                                <input type="radio" class="selectInvoice" name="radiolocation13">
                                </c:when>
                                <c:otherwise>
                                <input type="radio" class="selectInvoice" disabled name="radiolocation13">
                                </c:otherwise>
                                 </c:choose>
                                
                                <label class="radioLabelsInv" for="invoicing-location"><spring:theme code="edit.profile.locations.invLocation" /></label>
                            
                            </div>
                            <div class="show-radioShip">
                           <form:form role="form" modelAttribute="customerRegistrationForm" id="edit_shiplocation" data-toggle="validator" method="POST">
                           <div class="row"> <div class="col-md-12"><div class="profile-field"><spring:theme code="edit.profile.locations.contactInfo" /></div></div> </div>
                            <div class="row">
                            <div class="col-xs-12 col-md-5 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.fName" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_shipping textonly" autocomplete='no' data-error="Please enter First name" name="shippingLocations[0].firstName" id="shipFName" value="" placeholder="Edward" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             <div class="col-xs-12 col-md-5 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.lName" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_shipping textonly" autocomplete='no' data-error="Please enter Surname" name="shippingLocations[0].lastName" id="shipLName" value="" placeholder="Newgate" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             </div>
                             
                             <div class="row">
                             <div class="col-xs-12 col-md-5 form-group">
                                 <label for="gmc"><spring:theme code="edit.profile.locations.email" /><span class="asteriskred">*</span></label><br>
                                <input type="email" class="edit_rqrd_shipping" pattern=".*@[\w.-]+\.[a-zA-Z0-9.-]{2,6}" autocomplete='no' data-error="Please enter valid Email" name="shippingLocations[0].email" id="shipEmail" value="" placeholder="email@example.co.uk" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             <div class="col-xs-12 col-md-4 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.phone" /><span class="asteriskred">*</span></label><br>
                                <input type="text"  class="edit_rqrd_shipping numberonly" autocomplete='no' data-error="Please enter a valid UK format Telephone" name="shippingLocations[0].phone" id="shipPhone" value="" placeholder="" class="phonemask" maxlength="11" required>
                                <div class="help-block with-errors" id="message_ship"></div>
                             </div>
                             <div class="col-xs-12 col-md-3">
                                <label for="gmc"><spring:theme code="edit.profile.locations.ext" /></label><br>
                                <input type="text" class="numberonly" name="shippingLocations[0].phone" id="shipPhoneExt" value="" maxlength="4" placeholder="1234">
                             </div>
                             </div>
                            
                            <div class="row"> <div class="col-md-12"><div class="profile-field"><spring:theme code="edit.profile.locations.location" /></div></div> </div>
                              
                              <div class="row">
                              <div class="col-xs-12 col-md-7 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.orgName" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_shipping" autocomplete='no' data-error="Please enter Organisation name" name="shippingLocations[0].organizationName" id="shipOrg" value="" placeholder="Practice, Pharmacy or Business Name" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             </div>
                             <div class="row">
		                      <div class="col-xs-12 col-md-7 form-group">
		                           <div class="form-group">
		                              <label for="Address">Address Lookup</label>
		                              <div class="input-group spcl-addon">  
		                              <div class="input-group-addon" style="background:#fff !important;"><i class="fa fa-search"></i></div>
		                              <input class="form-control" style="margin-bottom:0px;" id="addresslookupbusiness" name="shippingLocations[0].addressLookUp" placeholder="Search" type="text">
		                              </div>
		                             
		                           </div>
		                        </div> 
		                        </div> 
		                            
                              <div class="row">
                             <div class="col-xs-12 col-md-7 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.street" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_shipping" autocomplete='no' data-error="Please enter Address" name="shippingLocations[0].addressLine1" id="shipAddr1" value="" placeholder="Building Number & Street" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             <div class="col-xs-12 col-md-5">
                                <label for="gmc"><spring:theme code="edit.profile.locations.additionalStreet" /></label><br>
                                <input type="text" name="shippingLocations[0].addressLine2" id="shipAddr2" value="" placeholder="Building or Office name/number">
                             </div>
                             </div>
                             <div style="clear:both"></div>
                            
                             <div class="row">
                             <div class="col-xs-12 col-md-4 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.city" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_shipping textonly" autocomplete='no' data-error="Please enter City" name="shippingLocations[0].city" id="shipCity" value="" placeholder="City" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             <div class="col-xs-12 col-md-4 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.postalcode" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_shipping" autocomplete='no' data-error="Please enter valid UK Post Code" name="shippingLocations[0].postalCode" id="shipPostal" value="" placeholder="SL16 8AA" required>
                                <div class="help-block with-errors postcodeerror_edit_ship"></div>
                             </div>
                              <div class="col-xs-12 col-md-4 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.country" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_shipping textonly" autocomplete='no' data-error="Please enter Country" name="shippingLocations[0].country" id="shipCountry" value="" placeholder="Country" required>
                               <div class="help-block with-errors"></div>
                             </div>
                             </div>
                             <div style="clear:both"></div>
                             <div class="row"><div class="col-md-12"><div class="profile-field"><spring:theme code="edit.profile.locations.licenseInfo" /></div></div> </div>
                             <div class="row">
                             <div class="col-xs-12 col-md-4 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.licenseName" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_shipping textonly" autocomplete='no' data-error="Please enter License name" name="shippingLocations[0].licenseName" id="shipLicName" value="" placeholder="Jane Smith" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             <div class="col-xs-12 col-md-4 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.licenseNum" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_shipping" autocomplete='no' data-error="Please enter License Number" name="shippingLocations[0].licenseNumber" id="shipLicNum" value="" maxlength="7" placeholder="e.g. GMC or GPhC licence" required>
                                <div class="help-block with-errors editshipLicenserror"></div>
                             </div>
                             <div class="col-xs-12 col-md-4 form-group">
                                <label for="gmc"><spring:theme code="form.register.invoice.nhsCode" /></label><br>
                                <input type="text" name="shippingLocations[0].nhsCode" id="shipLicNhs" value=""maxlength="7">
                                <div class="help-block with-errors editshipNHSerror"></div>
                             </div>
                             </div>
					<input type="hidden" name="shippingLocations[0].addressID" id="shipAddressId" value="" >
					<input type="hidden" name="shipLocationSubmitFlag" id="shipLocationSubmitFlag" value="">

                             <div class="col-xs-12 col-md-2"></div>
                             <div class="col-xs-12 col-md-4 middle-align">
                                <a href="javascript:void(0)" class="cancel-btn close-shipping" draggable="false"><spring:theme code="edit.profile.locations.cancel" /></a>
                             </div>
                             <div class="col-xs-12 col-md-6 middle-align">
                                <button type="submit" class="new-location Nextbutton-profile ff" disabled="disabled" ><spring:theme code="edit.profile.locations.update" /></button>
                             </div>
                             </form:form>
                        </div>

                    <div class="show-radioInv">
                        <form:form role="form" modelAttribute="customerRegistrationForm" id="edit_invSection" data-toggle="validator" method="POST">
                            <div class="col-md-12"><div class="profile-field"><spring:theme code="edit.profile.locations.contactInfo" /></div></div> 
                            <div class="col-xs-12 col-md-5 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.fName" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="rqrd_edit_inv textonly" name="invoicingContractInfo.firstName" data-error="Please enter First Name" id="invName" value="" placeholder="Edward" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             <div class="col-xs-12 col-md-5 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.lName" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="rqrd_edit_inv textonly" name="invoicingContractInfo.lastName" id="invLName"  value="" data-error="Please enter Surname" placeholder="Newgate" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             <div class="clearfix"></div>
                             <div class="col-xs-12 col-md-5 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.jobTitle" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="rqrd_edit_inv textonly" name="invoicingContractInfo.jobTitle" id="invJobtitle" value="" data-error="Please enter Job Title" placeholder="Ex. Vaccine Buyer" required>
                                <div class="help-block with-errors"></div> 
                             </div>
                             <div class="col-xs-12 col-md-6 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.email" /><span class="asteriskred">*</span></label><br>
                                <input type="email" class="rqrd_edit_inv" name="invoicingContractInfo.email" id="invEmail" value="" pattern=".*@[\w.-]+\.[a-zA-Z0-9.-]{2,6}" data-error="Please enter valid Email" placeholder="email@example.co.uk" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             <div class="clearfix"></div>
                             <div class="col-xs-12 col-md-6 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.phone" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="rqrd_edit_inv numberonly" name="invoicingContractInfo.phone" id="invPhone" class="phonemask" value="" data-error="Please enter a valid UK format Telephone" placeholder="" maxlength="11" required>
                                <div class="help-block with-errors" id="message_phninv"></div>
                             </div>
                             <div class="col-xs-12 col-md-6">
                                <label for="gmc"><spring:theme code="edit.profile.locations.ext" /></label><br>
                                <input type="text" class="numberonly" name="invoicingContractInfo.phoneExt" id="invPhoneExt" maxlength="4" value="" placeholder="1234">
                             </div>
                             <div class="clearfix"></div>
                              <div class="col-md-12"><div class="profile-field"><spring:theme code="edit.profile.locations.location" /></div></div> 
                              <div class="col-xs-12 col-md-6 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.orgName" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="rqrd_edit_inv" name="invoicingContractInfo.organizationName" id="invOrgName" value="" data-error="Please enter Organisation Name" placeholder="Practice, Pharmacy or Business Name" required>
                                <div class="help-block with-errors"></div>
                             </div>
		                           
		                         <div class="col-xs-12 col-md-7 form-group">
		                           <div class="form-group">
		                              <label for="Address">Address Lookup</label>
		                              <div class="input-group spcl-addon">  
		                              <div class="input-group-addon" style="background:#fff !important;"><i class="fa fa-search"></i></div>
		                              <input class="form-control" style="margin-bottom:0px;" id="addresslookupbusiness" name="invoicingContractInfo.addressLookUp" placeholder="Search" type="text">
		                              </div>
		                             
		                           </div>
		                        </div>
		                      
                             <div class="col-xs-12 col-md-7 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.street" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="rqrd_edit_inv" name="invoicingContractInfo.addressLine1" id="invAddr1" value="" data-error="Please enter Address" placeholder="Building Number & Street" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             <div class="col-xs-12 col-md-5">
                                <label for="gmc"><spring:theme code="edit.profile.locations.additionalStreet" /></label><br>
                                <input type="text" name="invoicingContractInfo.addressLine2" id="invAddr2" value="" placeholder="Building or Office name/number">
                             </div>
                             <div style="clear:both"></div>
                             <div class="col-xs-12 col-md-4 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.city" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="rqrd_edit_inv textonly" name="invoicingContractInfo.city" id="invCity" value="" data-error="Please enter City" placeholder="City" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             <div class="col-xs-12 col-md-4 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.postalcode" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="rqrd_edit_inv" name="invoicingContractInfo.postalCode" id="invPostal" value="" data-error="Please enter valid UK Post Code" placeholder="SL16 8AA" required>
                                <div class="help-block with-errors invpostcodeerror_edit"></div>
                             </div>
                             <div class="col-xs-12 col-md-4 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.country" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="rqrd_edit_inv textonly" name="invoicingContractInfo.country" id="invCountry" value="" data-error="Please enter Country" placeholder="Country" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             
                             <div style="clear:both"></div>
					            <input type="hidden" value="" name="invoicingContractInfo.addressID" id="invAddrId">
					            <input type="hidden" name="invoiceContactSubmitFlag" id="invoiceContactSubmitFlag" value="">
                             <div class="col-xs-12 col-md-2"></div>
                             <div class="col-xs-12 col-md-4 middle-align">
                                <a href="javascript:void(0)" class="cancel-btn close-shipping" draggable="false"><spring:theme code="edit.profile.locations.cancel" /></a>
                             </div>
                             <div class="col-xs-12 col-md-6 middle-align">
                                <button type="submit" class="Nextbutton-profile myproinvoiceUpdate" ><spring:theme code="edit.profile.locations.update" /></button>
                             </div>
                             </form:form>
                        </div>
 
            <div class="show-radioPay">

                     <form:form role="form" modelAttribute="customerRegistrationForm" id="edit_paying" data-toggle="validator" method="POST">
                            <div class="col-md-12"><div class="profile-field"><spring:theme code="edit.profile.locations.contactInfo" /></div></div> 
                            <div class="col-xs-12 col-md-5 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.fName" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_paying textonly" name="payingContactInfo.firstName" autocomplete='no' data-error="Please enter First name" id="payFName" value="" placeholder="Edward" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             <div class="col-xs-12 col-md-5 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.lName" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_paying textonly" name="payingContactInfo.lastName" autocomplete='no' data-error="Please enter Surname" id="payLName" value="" placeholder="Newgate" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             <div class="clearfix"></div>
                             <div class="col-xs-12 col-md-5 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.jobTitle" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_paying textonly" name="payingContactInfo.jobTitle" autocomplete='no' data-error="Please enter Job title" id="payJobTitle" value="" placeholder="Ex. Vaccine Buyer" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             <div class="col-xs-12 col-md-6 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.email" /><span class="asteriskred">*</span></label><br>
                                <input type="email" class="edit_rqrd_paying" pattern=".*@[\w.-]+\.[a-zA-Z0-9.-]{2,6}" name="payingContactInfo.email" autocomplete='no' data-error="Please enter valid Email" id="payEmail" value="" placeholder="email@example.co.uk" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             <div class="clearfix"></div>
                             <div class="col-xs-12 col-md-6 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.phone" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_paying numberonly" name="payingContactInfo.phone" autocomplete='no' data-error="Please enter a valid UK format Telephone" id="payPhone" value="" required class="phonemask" placeholder="" maxlength="11">
                                <div class="help-block with-errors" id="message_paying"></div>
                             </div>
                             <div class="col-xs-12 col-md-6">
                                <label for="gmc"><spring:theme code="edit.profile.locations.ext" /></label><br>
                                <input type="text" class="numberonly" name="payingContactInfo.phoneExt"  id="payPhoneExt" value="" maxlength="4" placeholder="1234">
                                
                             </div>
                             <div class="clearfix"></div>
                             <div class="col-md-12"><div class="profile-field"><spring:theme code="edit.profile.locations.location" /></div></div> 
                             <div class="col-xs-12 col-md-6 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.orgName" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_paying" name="payingContactInfo.organizationName" autocomplete='no' data-error="Please enter Organisation name" id="payOrg" value="" placeholder="Practice, Pharmacy or Business Name" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             
		                       <div class="col-xs-12 col-md-7 form-group">
		                           <div class="form-group">
		                              <label for="Address">Address Lookup</label>
		                              <div class="input-group spcl-addon">  
		                              <div class="input-group-addon" style="background:#fff !important;"><i class="fa fa-search"></i></div>
		                              <input class="form-control" style="margin-bottom:0px;" id="addresslookupbusiness" name="payingContactInfo.addressLookUp" placeholder="Search" type="text">
		                              </div>
		                             
		                           </div>
		                        </div>    
                             
                             <div class="col-xs-12 col-md-7 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.street" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_paying" name="payingContactInfo.addressLine1" autocomplete='no' data-error="Please enter Address" id="payAddr1" value="" placeholder="Building Number & Street" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             <div class="col-xs-12 col-md-5">
                                <label for="gmc"><spring:theme code="edit.profile.locations.additionalStreet" /></label><br>
                                <input type="text"  name="payingContactInfo.addressLine2" id="payAddr2" value="" placeholder="Building or Office name/number">
                             </div>
                             <div style="clear:both"></div>
                             <div class="col-xs-12 col-md-4 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.city" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_paying textonly" name="payingContactInfo.city" autocomplete='no' data-error="Please enter City" id="payCity" value="" placeholder="City" required>
                                <div class="help-block with-errors"></div>
                             </div>
                              <div class="col-xs-12 col-md-4 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.postalcode" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_paying" name="payingContactInfo.postalCode" id="payZip" autocomplete='no' data-error="Please enter valid UK Post Code" value="" placeholder="SL16 8AA" required>
                                <div class="help-block with-errors postcodeerror_edit_paying"></div>
                             </div>
                             <div class="col-xs-12 col-md-4 form-group">
                                <label for="gmc"><spring:theme code="edit.profile.locations.country" /><span class="asteriskred">*</span></label><br>
                                <input type="text" class="edit_rqrd_paying textonly" name="payingContactInfo.country" autocomplete='no' data-error="Please enter Country" id="payCountry" value="" placeholder="Country" required>
                                <div class="help-block with-errors"></div>
                             </div>
                             
                       <div style="clear:both"></div>      
					<input type="hidden" name="payingContactInfo.addressID" value="" id="payAddrID" >
					<input type="hidden" name="payerContactSubmitFlag" id="payerContactSubmitFlag" value="">
					

                             <div class="col-xs-12 col-md-2"></div>
                             <div class="col-xs-12 col-md-4 middle-align">
                                <a href="javascript:void(0)" class="cancel-btn close-shipping" draggable="false"><spring:theme code="edit.profile.locations.cancel" /></a>
                             </div>
                             <div class="col-xs-12 col-md-6 middle-align">
                                <button type="submit" class="Nextbutton-profile update_paying" ><spring:theme code="edit.profile.locations.update" /></button>
                             </div>
                              </form:form> 
                        </div>
                      
                      
            </div>
					
                       
                       
                                                   
                        
                       
                       
                            
                        <div class="rounded-bg confirm-msg">
                            <div class="col-md-12 confirm-txt align-center-txt"><spring:theme code="edit.profile.locations.finishText" /></div>
                            <div class="clearfix1"></div>
                            <div class="col-md-6 col-md-offset-3"><button type="button" class="Nextbutton-profile finish-btn"><spring:theme code="edit.profile.locations.finish" /></button></div>
                        </div>
                        <c:if test="${percentageCalculation eq '33'}">
                        <c:set var="percent" value="34"></c:set>
                        <c:set var="percentCircle" value="over25 34"></c:set>
                        </c:if>
                        <c:if test="${percentageCalculation eq '66'}">
                        <c:set var="percent" value="67"></c:set>
                        <c:set var="percentCircle" value="over50 p67"></c:set>
                        </c:if>
                        <c:if test="${percentageCalculation eq '99' || percentageCalculation ge '99'}">
                        <c:set var="percent" value="100"></c:set>
                        <c:set var="percentCircle" value=" over50 p100"></c:set>
                        </c:if>
                        <c:if test="${percentageCalculation eq '0'}">
                        <c:set var="percent" value="0"></c:set>
                        <c:set var="percentCircle" value="p0"></c:set>
                        </c:if>
                        <div class="col-md-7"></div>
                        <div class="col-md-5 no-pad">
                            <div class="profile-progress">
                                <div class="profile-field margin-top-field">${percent}% Complete</div>
                               
                        <div class="progress-circle ${percentCircle}">
                                    <div class="left-half-clipper">
                                       <div class="first50-bar"></div>
                                       <div class="value-bar"></div>
                                    </div>
                                </div>
                                
                            </div>


                            
                        </div>
    
    
                      </div>


                </div>

