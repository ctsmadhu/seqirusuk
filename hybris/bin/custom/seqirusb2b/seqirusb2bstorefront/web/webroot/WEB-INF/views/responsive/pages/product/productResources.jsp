<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<template:page pageTitle="${pageTitle}">
	<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
		<div class="container-fluid productsbody-section">
			<nav aria-label="productsbreadcrumb">
				<c:url value="/" var="homeUrl" />
				<ol class="productsbreadcrumb portalpadLeft60">
					<li class="productsbreadcrumb-item active"><a
						href="${homeUrl}" class="blacktext"><spring:theme
								code="breadcrumb.home" /></a></li>
					<li class="productsbreadcrumb-item" aria-current="page"><spring:theme
							code="breadcrumb.products.resources" /></li>
				</ol>
			</nav>
			<div class="productspageHeader">
				<spring:theme code="product.resources.header" />
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="productheading-txt">
						<spring:theme code="product.resources.title" />
					</div>
					<cms:pageSlot position="Section7" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					<cms:pageSlot position="Section8" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="blclkfirst productswhite-cotte">
						<div class="col-md-12 productsblue-back">
							<span class="productsheading-txt"><spring:theme
									code="product.resources.resources" /></span>
						</div>

						<!-----New Product Resource page design starts here -->
						<div class="col-md-12 white-background-products-consent productsbody-txt product_resource_wrap">
							<div class="col-md-3">
								<div class="Pgrey-Blocks">
									<!-- <div class="Pgrey-Labels">
										<div class="Rred-labls">Flucelvax</div>
									</div> -->
									<cms:pageSlot position="Section9" var="component">
										<div class="Pinner-Content">
											<div class="products-blocks">
												<p class="boxheading">${component.title}</p>
												<p class="starmark">x x x x x x x x x x x x x x x x x x</p>
												<p class="italic_dsgn">${component.text2}</p>
												<p class="resource-para">${component.content}</p>
												<div class="downlaodbg"><a class="downlodlink" href="<spring:theme code="product.resources.fluimmunisation" />" target="_blank">
													Read More <i class="fas fa-external-link-alt downloadarrow"></i></a>
												</div>
											</div>
										</div>
									</cms:pageSlot>
								</div>
							</div>
							<div class="col-md-3">
								<div class="Pgrey-Blocks">
									<!-- <div class="Pgrey-Labels">
										<div class="Rred-labls">Flucelvax</div>
									</div> -->
									<cms:pageSlot position="Section10" var="component">
										<div class="Pinner-Content">
											<div class="products-blocks">
												<p class="boxheading">${component.title}</p>
												<p class="starmark">x x x x x x x x x x x x x x x x x x</p>
												<p class="italic_dsgn">${component.text2}</p>
												<p class="resource-para">${component.content}</p>
												<div class="downlaodbg"><a class="downlodlink" href="<spring:theme code="product.resources.greenbook" />" target="_blank">
													Read More <i class="fas fa-external-link-alt downloadarrow"></i></a>
												</div>
											</div>
										</div>
									</cms:pageSlot>
								</div>
							</div>
							<div class="col-md-3">
								<div class="Pgrey-Blocks">
									<!-- <div class="Pgrey-Labels">
										<div class="Rred-labls">Flucelvax</div>
									</div> -->
									<cms:pageSlot position="Section11" var="component">
										<div class="Pinner-Content">
											<div class="products-blocks">
												<p class="boxheading">${component.title}</p>
												<p class="starmark">x x x x x x x x x x x x x x x x x x</p>
												<p class="italic_dsgn">${component.text2}</p>
												<p class="resource-para">${component.content}</p>
												<div class="downlaodbg"><a class="downlodlink" href="<spring:theme code="product.resources.influenzaimmunisation" />" target="_blank">
													Read More <i class="fas fa-external-link-alt downloadarrow"></i></a>
												</div>
											</div>
										</div>
									</cms:pageSlot>
								</div>
							</div>
							<div class="col-md-3">
								<div class="Pgrey-Blocks">
									<!-- <div class="Pgrey-Labels">
										<div class="Rred-labls">Flucelvax</div>
									</div> -->
									<cms:pageSlot position="Section12" var="component">
										<div class="Pinner-Content">
											<div class="products-blocks">
												<p class="boxheading">${component.title}</p>
												<p class="starmark">x x x x x x x x x x x x x x x x x x</p>
												<p class="italic_dsgn">${component.text2}</p>
												<p class="resource-para">${component.content}</p>
												<div class="downlaodbg"><a class="downlodlink" href="<spring:theme code="product.resources.elearning" />" target="_blank">
													Read More <i class="fas fa-external-link-alt downloadarrow"></i></a>
												</div>
											</div>
										</div>
									</cms:pageSlot>
								</div>
							</div>
							<!-- <div class="col-md-11 textCenter marTop30">
								<a href="${contextPath}/resourcelanding"><button class="productsbutton">
									<spring:theme code="product.resources.allresources" />
								</button></a>
							</div> -->
						</div>

						<!-----New Product Resource page design ends here -->
					</div>
				</div>
			</div>
			<cms:pageSlot position="Section6" var="component">
				<div class="row">
					<div class="col-md-12">
						<div class="referencebodysec">
							<div class="productsheading-txt referncepad">${component.title}</div>
							<div class="referencepanel white-background-products-consent">
							<div class="col-xs-12 xhLine">xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</div>
								<div class="referencebody">${component.content}</div>
							</div>
							<p class="slide">
							<div class="pull-me">
								<p>
									<i class="fa fa-angle-down pull-topfa" aria-hidden="true"></i>
								</p>
							</div>
							</p>
						</div>
					</div>
				</div>
			</cms:pageSlot>
		</div>
	</sec:authorize>
</template:page>