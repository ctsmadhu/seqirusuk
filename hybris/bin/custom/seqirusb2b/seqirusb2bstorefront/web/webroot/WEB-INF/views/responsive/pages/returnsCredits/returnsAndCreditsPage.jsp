<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="breadcrumb"
	tagdir="/WEB-INF/tags/responsive/nav/breadcrumb"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">
	<div class="post_login">
		<div class="container-fluid body-returns">
			<div class="container">
				<nav aria-label="returnsbreadcrumb">
					<ol class="returnsbreadcrumb">
						<li class="returnsbreadcrumb-item active"><a href="${contextPath}/dashboard"
							class=""><spring:theme code="breadcrumb.home" text="Home" /></a></li>
						<li class="returnsbreadcrumb-item" aria-current="page"><spring:theme
								code="breadcrumb.financial" text="Financial" /></li>
						<li class="returnsbreadcrumb-item" aria-current="page"><spring:theme
								code="breadcrumb.returnsandcredits" text="Returns & Credits" /></li>
					</ol>
				</nav>
				<div class="row">
					<div class="col-md-12">
						<cms:pageSlot position="ReturnsAndCreditsHeadingSlot" var="feature" element="div" class="returnsheader">
								<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
				<div class="col-md-12" style="padding-left: 0px;">
					<cms:pageSlot position="ReturnsHeadingSlot" var="feature" element="div" class="returnssubheader1">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="returnsheader2 returnsleftsection">
							<cms:pageSlot position="CurrentSeasonSlot" var="feature" element="div">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
					<div class="returnsheader2 returnsrightsection">
								<cms:pageSlot position="EndOfSeasonSlot" var="feature" element="div">
									<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
					</div>
					<div class="col-md-12" style="display:flex;flex-wrap:wrap;">
						
						<div class="returns-detailssection returns-seasontxt returnsleftsection">
							<cms:pageSlot position="LeftSlotForReturn" var="feature" element="div" >
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
						<div class="returns-detailssection returns-seasontxt returnsrightsection">
							<cms:pageSlot position="RightSlotForReturn" var="feature" element="div">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 row">
						<div class="col-md-12">
						
							<cms:pageSlot position="CreditsHeadingSlot" var="feature" element="div" class="returnssubheader1">
									<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
						<div class="col-md-6">
						<cms:pageSlot position="CreditHistoryHeadingSlot" var="feature" element="div" class="returnssubheader2">
									<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
						<div class="col-md-6" style="padding-right:0px;">
							<div class="returnssubheader2">
								<span>
								<%-- 	<a class="returnsredtxt">
									       <spring:theme code="credits.howtoapply.text" text="How will a credit be applied to my account?" />
									 </a> --%>
								 </span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12" style="padding-left:0;">
					<div class="col-xs-12 returnsBody">
						<div class="col-md-6 credittextmar">
							<div class="returnssubheader5">
								<spring:theme code="credits.account.title"
									text="Customer Account" />
							</div>
							<c:choose>
								<c:when test="${not empty creditsData}">
									<div class="creditsvalue">${creditsData.accountName}</div>
								</c:when>
								<c:otherwise>
									<div class="creditsvalue"></div>
								</c:otherwise>
							</c:choose>
						</div>
						<div class="col-md-6 credittextmar">
							<div class="returnssubheader5">
								<spring:theme code="credits.availability.title"
									text="Available Credits" />
							</div>
							<div class="creditsvalue">
								<c:choose>
									<c:when test="${not empty creditsData.creditList && creditsData.creditList[0].invoiceNumber!='N/A'}">
	                           			${creditsData.creditList[0].currency}${creditsData.totalAvailableCredit}
	                           		</c:when>
	                          	 	<c:otherwise>
	                          	 		&#163;${creditsData.totalAvailableCredit}
	                          		</c:otherwise>
	                           </c:choose>
							</div>
						</div>
						<div class="returnstablecontainer returnsTablecontainer">
							<table id="returnsTable" class="display">
								<thead>
									<tr>
										<th><spring:theme code="credits.table.heading1"
												text="Credit Note #" /></th>
										<th><spring:theme code="credits.table.heading2"
												text="Credit Amount" /></th>
										<th><spring:theme code="credits.table.heading3"
												text="Date Issued" /></th>
										<th><spring:theme code="credits.table.heading4"
												text="Status" /></th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</template:page>
