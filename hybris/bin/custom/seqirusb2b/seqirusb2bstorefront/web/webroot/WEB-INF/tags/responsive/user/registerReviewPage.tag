<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
 
 
 <!-- Review and confirm starts -->
   <div id="reviewConfirm" class="tabcontent">
      <div class="section1_header col-xs-12">
         <spring:theme code="form.register.review.header" />
      </div>
      <div class="section1_subheader col-xs-12">
         <spring:theme code="form.register.review.subHeader" />
      </div>
      <div class="row padLeft30 padBottom50">
         <div class="col-md-3">
            <div class="form-group">
               <label class="reviewHeader control-label" for="userProfile"><spring:theme code="form.register.review.userProfile" /></label>
               <div>
                  <label id="" class="reviewPara">${userName} </label>
                  <label id="" class="reviewPara">${jobTitle} </label>
                  <label class="reviewPara">  ${userId}</label>
                  <label id="" class="reviewPara">${phoneNum} </label>
               </div>
            </div>
         </div>
         <div class="col-md-5">
            <div class="reviewHeader"><spring:theme code="form.register.review.businessDetails" /></div>
            
               <div class="form-group col-md-6 reviewcolumns no-pad">
                  <label class="reviewsubheading"><spring:theme code="form.register.review.orgName" /></label>
                  <div ><label class="reviewPara control-label" id="compName"></label></div>
                  <label class="reviewsubheading"><spring:theme code="form.register.review.reg" /></label>
                  <div  class="reviewPara"><label class="reviewPara" id="regVal"></label></div>
                  <label class="reviewsubheading"><spring:theme code="form.register.review.vat" /></label>
                  <div class="reviewPara"><label class="reviewPara" id="vatNum" ></label></div>
                  <label class="reviewsubheading"><spring:theme code="form.register.review.trading" /></label>
                  <div class="reviewPara" ><label class="reviewPara" id="tradingVal" ></label></div>
                  
                  <%-- <label class="reviewsubheading"><spring:theme code="form.register.business.bildingStreet" /></label>
                  <div class="reviewPara" ><label class="reviewPara" id="businessAddr" ></label></div>
                  <label class="reviewsubheading"><spring:theme code="form.register.business.additionalStreet" /></label>
                  <div class="reviewPara" ><label class="reviewPara" id="businessStr" ></label></div> --%>
               </div>
               <div class="form-group col-md-6 reviewcolumns">
                  <label class="reviewsubheading"><spring:theme code="form.register.review.companyType" /></label>
                  <div  class="reviewPara" ><label class="reviewPara" id="companyTypeVal"></label></div>
                  <label class="reviewsubheading"><spring:theme code="form.register.review.businessType" /></label>
                  <div  class="reviewPara" ><label class="reviewPara" id="businessTypeVal"></label></div>
                  <%-- <label class="reviewsubheading"><spring:theme code="form.register.review.gmc" /></label>
                  <div class="reviewPara" ><label class="reviewPara" id="gmcVal" ></label></div> --%>
                  
                  <label class="reviewsubheading"><spring:theme code="form.register.business.address" /></label>
                  <div class="reviewPara" ><label class="reviewPara" id="businessAddr" ></label></div>
                  <div class="reviewPara" ><label class="reviewPara" id="businessStr" ></label></div>
                  <div class="reviewPara" ><label class="reviewPara" id="businessci" ></label></div>
                  <div class="reviewPara" ><label class="reviewPara" id="businesspo" ></label></div>
                  <div class="reviewPara" ><label class="reviewPara" id="businesscou" ></label></div>
                  
                 <%--  <label class="reviewsubheading"><spring:theme code="form.register.business.postCode" /></label>
                  <div class="reviewPara" ><label class="reviewPara" id="businesspo" ></label></div>
                  <label class="reviewsubheading"><spring:theme code="form.register.business.country" /></label>
                  <div class="reviewPara" ><label class="reviewPara" id="businesscou" ></label></div> --%>
 
               </div>
            
         </div>
         <div class="col-md-4">
            <div class="form-group" id="reviewPayerId"><label class="reviewHeader"><spring:theme code="form.register.review.payer" /></label>
            <div id="payerFullName"><label class="reviewPara" ></label></div>
            <div id="payerCompany" ><label class="reviewPara"></label></div>
            <div id="payerEmail" ><label class="reviewPara"  ></label></div>
            <div id="payerPhone" ><label class="reviewPara" ></label></div>  <br>                   
            <div id="payerAddress" ><label class="reviewPara" ></label></div>
            <div id="payerPostalCode" ><label class="reviewPara" ></label></div>
            <div id="payerCountry" ><label class="reviewPara" ></label></div>
            
         </div>
      </div>
         
         <%-- <div class="col-md-3">
            <div class="form-group" id="reviewPayerId"><label class="reviewHeader"><spring:theme code="form.register.review.payer" /></label>
            <div id="payerFullName"><label class="reviewPara" ></label></div>
            <div id="payerCompany" ><label class="reviewPara"></label></div>
            <div id="payerEmail" ><label class="reviewPara"  ></label></div>
            <div id="payerPhone" ><label class="reviewPara" ></label></div>  <br>                   
            <div id="payerAddress" ><label class="reviewPara" ></label></div>
            <div id="payerPostalCode" ><label class="reviewPara" ></label></div>
            <div id="payerCountry" ><label class="reviewPara" ></label></div>
            
         </div>
      </div> --%>
      </div>
      <div class="row padLeft30">
         <%-- <div class="col-md-3" >
            <div class="form-group" id="reviewBillingId">
            <label class="reviewHeader"><spring:theme code="form.register.review.billing" /></label>
            <div id="billingName" ><label class="reviewPara" ></label></div>
            <div id="billingCompany"><label class="reviewPara"></label></div>
            <div id="billingEmail"><label class="reviewPara" ></label></div>
            <div id="billingPhone"><label class="reviewPara"></label></div> <br>                   
            <div id="billingAddress" ><label class="reviewPara"></label></div>
            <div id="billingPostalCode" ><label class="reviewPara"></label></div>
            <div id="billingCountry" ><label class="reviewPara"></label></div>
            
         </div>
      </div> --%>
      
        
         <div class="col-md-4">
            <div class="form-group" id="reviewInvoiceId"><label class="reviewHeader"><spring:theme code="form.register.review.invoice" /></label>
            <div id="invoiceName"><label class="reviewPara"></label></div>
            <div id="invoiceCompany"><label class="reviewPara" ></label></div>
            <div id="invoiceEmail"><label class="reviewPara" ></label></div>
            <div id="invoicePhone"><label class="reviewPara" ></label></div> <br>                  
            <div id="invoiceAddress"><label class="reviewPara" ></label></div> 
            <div id="invoicePostalCode"><label class="reviewPara" ></label></div>
            <div id="invoiceCountry"><label class="reviewPara" ></label></div>
            
            
         </div>
      </div>
         <div class="col-md-3">
            <div class="form-group"><label class="reviewHeader"><spring:theme code="form.register.review.shipping" /></label>
            <div ><label class="reviewPara" id="totalShippingLocations"></label></div>
            <%-- <label class="editlink"><a href="#"><spring:theme code="form.register.review.editShipping" /></a></label> --%>
            <div class="prevStepbtn pagi-link tab-3-sub sub-8 last-prev" data-section-id="section-5" data-img-id="img-3">Edit Locations</div>
         </div>
      </div>
      </div>
      <div class="col-xs-12 previosnmail">
         <div class="prevStepbtn pagi-link tab-3-sub sub-8 rvw_prev" data-section-id="section-5" data-img-id="img-4">< <spring:theme code="form.register.prev" /></div>
         <button type="submit" class="NextbuttonAccount" ><spring:theme code="form.register.createAccount.reg" /></button>
         <!-- <span class="skiptext"><a class="skiplink pagi-link" href="JavaScript:Void(0);" data-section-id="section-1" data-img-id="img-1">Skip For Now</a></span> -->
      </div>
   </div>

<!-- Review and confirm ends-->