<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<template:page pageTitle="${pageTitle}">
<div class="container-fluid invoicebody-section">
         <nav aria-label="invoicebreadcrumb">
            <ol class="invoicebreadcrumb">
            	<c:url value="/" var="homeUrl" />
               <li class="invoicebreadcrumb-item active"><a href="${homeUrl}" class=""><spring:theme code="breadcrumb.home" /></a></li>
               <li class="invoicebreadcrumb-item active"><a href="${contextPath}/invoices" class="">Invoices </a></li>
               <li class="invoicebreadcrumb-item" aria-current="page">Invoice #${invoiceDetailsList.invoiceNumber}</li>
            </ol>
         </nav>
		 <div class="row">
			 <div class="col-md-12 invoicepadLeft31">
				<div class="col-md-6">
				   <div class="invoicepagesubHeader1">Invoice Number:</div>
				   <div class="invoicepageHeader">${invoiceDetailsList.invoiceNumber}</div>
				   <%-- <div class="invoicepagesubHeader2">${invoiceDetailsList.paymentTermDescription} From Invoice Issue</div> --%>
				</div>
				<div class="col-md-6">  
				   <div class="rightAlignment">             
				   <a class="invoicewhitebutton" href="${contextPath}/downloadInvoice?invoiceNumber=${invoiceDetailsList.invoiceNumber}"><spring:theme code="invoices.landing.downloadPdf"/></a>
				</div>
				</div>
			 </div>
		 </div>
		 <div class="row">
			 <div class="col-md-12 pulltopleft">
				<div class="col-md-3 nopadleftright">
				   <span class="invoicepagesubHeader1"><spring:theme code="invoices.landing.paymentdue"/></span>
				   <div class="invoicesection tealbg">
					  <div class="tealbgheader">${invoiceDetailsList.paymentDueDay}</div> 
					  <div class="tealbgsubheader">${invoiceDetailsList.paymentDueMnthYr}</div>
				   </div>
				</div>
				<div class="col-md-3 nopadleftright">
				   <span class="invoicepagesubHeader1"><spring:theme code="invoices.landing.invoiceDate"/></span>
				   <div class="invoicesection whiteboxbg">
					  <div class="whiteboxbgheader">${invoiceDetailsList.invoiceDate}</div>
				   </div>
				</div>
				<div class="col-md-3 nopadleftright">
				   <span class="invoicepagesubHeader1"><spring:theme code="invoices.landing.amountDue"/></span>
				   <div class="invoicesection whiteboxbg">
					  <div class="whiteboxbgheader">�<fmt:formatNumber type = "number" maxFractionDigits = "2" minFractionDigits = "2" value = "${invoiceDetailsList.amountDue}"/></div>
				   </div>
				</div>
			 </div>
		 </div>
		 <div class="row">
			 <div class="col-md-12 pulltopleft">
				<div class="col-md-3 nopadleftright">
				   <span class="invoiceboxheader"><spring:theme code="invoices.landing.salesOrderNumber"/></span>
				   <div class="whitesubbox">
					  <div class="whiteboxtext">${invoiceDetailsList.salesOrderNumber}</div>
				   </div>
				</div>
				<div class="col-md-3 nopadleftright">
				   <span class="invoiceboxheader"><spring:theme code="invoices.landing.deliveryOrderNumber"/></span>
				   <div class="whitesubbox">
					  <div class="whiteboxtext">${invoiceDetailsList.deliveryNumber}</div>
				   </div>
				</div>
				<div class="col-md-6 nopadleftright">
				   <span class="invoiceboxheader"><spring:theme code="invoices.landing.soldTo"/> &nbsp;${invoiceDetailsList.soldTo.soldToId}</span>
				   <div class="whitesubboxs">
					  <div class="invoicegreybg">
						 <div class="whiteboxtext"> ${invoiceDetailsList.soldTo.soldToName}</div>
					  </div>
					  <div class="invoicewhitebg">
						 <div class="whiteboxgreytext">${invoiceDetailsList.soldTo.soldToStreet} <br>${invoiceDetailsList.soldTo.soldToCity}</div>
					  </div>
				   </div>
				   
				</div>
			 </div>
		 </div>
		  <div class="row">
			 <div class="col-md-12 pulltopleft">
				<div class="col-md-3 nopadleftright">
				   <span class="invoiceboxheader"><spring:theme code="invoices.landing.poNumber"/></span>
				   <div class="whitesubbox">
					  <div class="whiteboxtext">${invoiceDetailsList.poNumber}</div>
				   </div>
				</div>
				<div class="col-md-3 nopadleftright">
				   <span class="invoiceboxheader"><spring:theme code="invoices.landing.poDate"/></span>
				   <div class="whitesubbox">
					  <div class="whiteboxtext">${invoiceDetailsList.poDate}</div>
				   </div>
				</div>
				<div class="col-md-6 nopadleftright">
				   <span class="invoiceboxheader"><spring:theme code="invoices.landing.shipTo"/> &nbsp;${invoiceDetailsList.shipTo.shipToId}</span>
				   <div class="whitesubboxs">
					  <div class="invoicegreybg">
						 <div class="whiteboxtext"> ${invoiceDetailsList.shipTo.shipToName}</div>
					  </div>
					  <div class="invoicewhitebg">
						 <div class="whiteboxgreytext">${invoiceDetailsList.shipTo.shipToStreet} <br>${invoiceDetailsList.shipTo.shipToCity}</div>
					  </div>
				   </div>
				</div>
			 </div>
		 </div>
		  <div class="row">
			 <div class="col-md-12 invoicebllkfirst">
				<div class="col-md-12  invoiceblue-back">
				   <span class="invoiceheading-txt"><spring:theme code="invoices.landing.productsInvoiced"/></span>
				</div>
				<div class="col-md-12 white-background-invoice ">
				   <table class="invoiceagreement">
				   	  <tr>
				   	  	 <th><spring:theme code="invoices.landing.materialNumber"/></th>
				   	  	 <th><spring:theme code="invoices.landing.productName"/></th>
						 <th><spring:theme code="invoices.landing.batch"/></th>
						 <%-- <th><spring:theme code="invoices.landing.productDesc"/></th> --%>
						 <th><spring:theme code="invoices.landing.doses"/></th>
						 <th><spring:theme code="invoices.landing.unitPrice"/></th>
						 <th><spring:theme code="invoices.landing.netAmount"/></th>
						 <th><spring:theme code="invoices.landing.vatAmount"/></th>
						 <th><spring:theme code="invoices.landing.total"/></th>
					  </tr>
				   <c:forEach var="invoiceProduct" items="${invoiceDetailsList.lineItems}">
					  <tr>
					  	 <td>${invoiceProduct.materialNumber}</td>
					     <td>${invoiceProduct.productName}</td>
						 <td>${invoiceProduct.batchNumber}</td>
						<%--  <td>${invoiceProduct.productDescription}</td> --%>
						 <td>${invoiceProduct.doses}</td>
						 <td>� <fmt:formatNumber type = "number" maxFractionDigits = "2" minFractionDigits = "2" value = "${invoiceProduct.unitPrice}"/></td>
						 <td>� <fmt:formatNumber type = "number" maxFractionDigits = "2" minFractionDigits = "2" value = "${invoiceProduct.netAmount}"/></td>
						 <td>� <fmt:formatNumber type = "number" maxFractionDigits = "2" minFractionDigits = "2" value = "${invoiceProduct.lineItemTax}"/></td>
						 <td>� <fmt:formatNumber type = "number" maxFractionDigits = "2" minFractionDigits = "2" value = "${invoiceProduct.totalCost}"/></td>
					  </tr>
					</c:forEach>
				   </table>
				   <div class="col-md-4 totalinvoice">
					  <table class="invoicetotal">
						 <tr>
							<td colspace="4" class="invoicelefttd"><spring:theme code="invoices.landing.subTotal"/></td>
							<td class="invoicerighttd">� <fmt:formatNumber type = "number" maxFractionDigits = "2" minFractionDigits = "2" value = "${invoiceDetailsList.subTotal}"/></td>
						 </tr>
						<%--  <tr>
							<td class="invoicelefttd"><spring:theme code="invoices.landing.vat"/></td>
							<td class="invoicerighttd">� <fmt:formatNumber type = "number" maxFractionDigits = "2" minFractionDigits = "2" value = "${invoiceDetailsList.vat}"/></td>
						 </tr> --%>
						 <tr>
							<td class="nopadding" colspan="2">
							   <hr class="totalhrline">
							   </hr>
							</td>
						 </tr>
						 <tr>
							<td class="invoiceleft"><spring:theme code="invoices.landing.total.gpb"/></td>
							<td class="invoiceright">� <fmt:formatNumber type = "number" maxFractionDigits = "2" minFractionDigits = "2" value = "${invoiceDetailsList.total}"/></td>
						 </tr>
					  </table>
				   </div>
				</div>
			 </div>
		</div>
      </div>
     </template:page> 