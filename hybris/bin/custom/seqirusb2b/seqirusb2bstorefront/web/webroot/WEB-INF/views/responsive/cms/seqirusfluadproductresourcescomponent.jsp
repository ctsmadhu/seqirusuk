<div class="col-md-6 marTop30">
	<div class="Rgrey-Blocks-1">
		<div class="Rgrey-Labels">
			<img class="boximages-1" src="${feature.fluadProductImage.url}" />
		</div>
		<div class="Rinner-Content">
			<div class="resource-blocks">
				<p class="resource-headings">${feature.title}</p>
				<p class="resource-para">${feature.content}</p>
				<%-- <div><a href="${component.productMediaPdf.URL}" target="_blank">
					<i class="fas fa-external-link-alt external-link"></i> <span
						class="linkHeadings">Discover More About FLUAD TETRA</span></a>
				</div> --%>
			</div>
		</div>
	</div>
</div>