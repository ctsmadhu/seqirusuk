<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="blkfirst">
	<div class="heading-txt"><spring:theme code="userprofile.heading" text="My Profile"/></div>
		<div class="col-md-12 white-background-profile marBottom40" style="padding-bottom: 0px;">
			<div class="col-md-12 no-pad">
			<c:set var="asagent" value="${agent.uid}" />
				<c:if test="${ empty asagent}">
					<table class="table dashboard-heading-table">
						<tr>
							<td style="background: #fff;"><a class="view_order_link edit-profile profile_edit_section"><spring:theme code="userprofile.editinfo" text="Edit Info" /></a></td>
						</tr>
					</table>
				</c:if>
			</div>

		<div class="profile-details">
			<div class="col-md-12 profile-lebel"><spring:theme code="userprofile.name" text="Name"/></div>
			<div class="col-md-12 profile-field" id="profile_name">${customerProfileData.firstName}&nbsp;${customerProfileData.lastName}</div>
			<div class="clearfix1"></div>

			<div class="col-md-12 profile-lebel"><spring:theme code="userprofile.jobtitle" text="Job Title"/></div>
			<div class="col-md-12 profile-field" id="profile_job">${customerProfileData.jobTitle}</div>
			<div class="clearfix1"></div>

			<div class="col-md-12 profile-lebel"><spring:theme code="userprofile.email" text="Email"/></div>
			<div class="col-md-12 profile-field" id="profile_email">${customerProfileData.uid}</div>
			<div class="clearfix1"></div>

			<div class="col-md-12 profile-lebel"><spring:theme code="userprofile.phone" text="Telephone"/></div>
			<div class="col-md-12 profile-field" id="profile_phone">${customerProfileData.phoneNumber}</div>
			<div class="clearfix1"></div>
		</div>

		<div class="profile-details-edit shippingformContent-profile greycolor">
			<cms:pageSlot position="LeftContentSlotEditProfile" var="feature" element="div" class="account-section-content col-xs-12">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</div>
	</div>

	<div class="heading-txt"><spring:theme code="userprofile.heading.accountpassword" text="Account Password"/></div>
	<div class="col-md-12 white-background-profile marBottom40">
		<div class="shippingformContent-profile greycolor">
			<div class="col-md-12 profile-lebel"><spring:theme code="userprofile.accountpassword.title" text="Use this link to change or reset your password." /></div>
			<div class="col-md-12">
				<a href="${contextPath}/changepassword"
					class="view_order_link update_password_link"><spring:theme code="userprofile.changepassword" text="Change Password"/></a>
			</div>
		</div>
	</div>
</div>
<div class="clearfix1"></div>