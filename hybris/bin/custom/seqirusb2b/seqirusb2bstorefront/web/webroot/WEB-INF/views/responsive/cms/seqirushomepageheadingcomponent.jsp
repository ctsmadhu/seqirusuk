<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="row primary-content">
	<div class="col-lg-12">
		<div class="headingtop">
			<h1 class="homeheading1">${feature.text1}</h1>
			<p class="headerpara">${feature.text2}</p>
			<div>
				<a class="createAccountbtn" href="${contextPath}/<spring:theme code="header.link.cdclogin" text='login' />"><spring:theme code="homepage.headingcomponent.logintext" text="SIGN UP/LOG IN" /></a>
			</div>
		</div>
	</div>
</div>