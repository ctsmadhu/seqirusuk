<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/responsive/nav/breadcrumb"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<spring:theme code="resetPwd.title" var="pageTitle" />

<template:page pageTitle="${pageTitle}">
	<div id="breadcrumb" class="col-sm-12 col-md-12">
		<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}" />
	</div>
	<div class="container-fluid maincontent col-xs-12">
		<div class="contentArea col-xs-11">
			<div class="leftcontentArea col-xs-12 col-md-4">
				<div class="leftHeader col-xs-11">LOG IN</div>
				<div class="leftsubHeader col-xs-11">Access orders, view
					invoices, and find relevant resources for your clinics.</div>
			</div>
			<div class="rightcontentArea col-xs-12 col-md-8" id="rightSection_1">
				<div class="section1_header col-xs-12">
					PASSWORD RESET<br />UNSUCCESSFUL
				</div>
				<div class="enterCodeheader col-xs-12"><spring:theme code="${accErrorMsgs}"/></div>
				<div class="col-xs-12 prevnmail">
					<div class="logInbtn"><a href="<c:url value='/liveassist'/>">Return To Livessist</a></div>
				</div>
			</div>
		</div>
	</div>
</template:page>
