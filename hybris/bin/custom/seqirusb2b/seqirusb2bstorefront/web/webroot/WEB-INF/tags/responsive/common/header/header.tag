<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>

<spring:htmlEscape defaultHtmlEscape="true" />


<cms:pageSlot position="TopHeaderSlot" var="feature">
<cms:component component="${feature}" />
</cms:pageSlot>
<header class="headerbar">
      <nav class="">
            <div class="row headerSection">
                  <div class="col-md-3 col-xs-12 mob-logo">
                        <a rel="home" href="/"> <cms:pageSlot position="SiteLogo"
                                    var="logo">
						<cms:component component="${logo}" element="div" class="logoimg" />
                              </cms:pageSlot> 
                        </a>
                  </div>
                  <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                        <cms:pageSlot position="BottomHeaderSlot" var="component"
                        element="div">
                        <cms:component component="${component}" />
                  </cms:pageSlot>
                  </sec:authorize>
                  <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                        <cms:pageSlot position="HeaderLinks" var="component"
                        element="div">
                        <cms:component component="${component}" />
                  </cms:pageSlot>
                  </sec:authorize>
            </div>
      </nav>
      
             <div id="myModaluserAllow" class="modal showmodel-abdonod-popup">

				<!-- Modal content -->
				<div class="modal-content">
					<div class="abdnHeader"> Are you sure you want to  leave this page?</div>
					<p class="abdnpara">If you leave now, your registration information will be lost. Without registering, you will not be able to access your flu360 account and access important information</p>
					
					<div class="">
					<button class="confirmreg active" type="button" id="cotinueRegistration">Continue sign up</button>
					
					</div>
					<div class="">
					 <div class="abdnlink" id="leaveLink">Leave and lose progress</div>

					</div>
				</div>

			</div> 
 
</header>


