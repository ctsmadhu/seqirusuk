<div class="row">
	<div class="container greymar-left161">
		<div class="row">
			<div class="col-md-6 col-xs-12 abtgreyboxMar imgboxfirst">
				<div>
					<img class="imgwidth aboutimg"
						src="${feature.invoiceTrackingImage.url}" alt="Resource" />
				</div>
			</div>
			<div class="col-md-6 col-xs-12 secondsection aboutsecondsec">
				<h3 class="aboutsecondary-header">${feature.text1}</h3>
				<p class="aboutheaderpara">${feature.text2}</p>
				<ul class="circle">${feature.text3}</ul>

			</div>
		</div>
	</div>
</div>
