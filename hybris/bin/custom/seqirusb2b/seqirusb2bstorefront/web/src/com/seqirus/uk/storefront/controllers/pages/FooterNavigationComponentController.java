/**
 *
 */
package com.seqirus.uk.storefront.controllers.pages;

import de.hybris.platform.acceleratorcms.model.components.FooterNavigationComponentModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.seqirus.uk.storefront.controllers.ControllerConstants;
import com.seqirus.uk.storefront.controllers.cms.AbstractAcceleratorCMSComponentController;


/**
 * @author 700196
 *
 */
@Controller("FooterNavigationComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.FooterNavigationComponent)
public class FooterNavigationComponentController extends AbstractAcceleratorCMSComponentController<FooterNavigationComponentModel>
{
	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "userService")
	private UserService userService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final FooterNavigationComponentModel component)
	{
		if (!userFacade.isAnonymousUser())
		{
   		final B2BCustomerModel account = (B2BCustomerModel) userService.getCurrentUser();
   		if (null != account && null != account.getDefaultB2BUnit() && StringUtils.isNotBlank(account.getDefaultB2BUnit().getUid())
   				&& account.getDefaultB2BUnit().getUid().equals("SeqirusUK"))
   		{
				model.addAttribute("footerHideStatus", "hide");
   		}
		}
		model.addAttribute("component", component);
	}

}

