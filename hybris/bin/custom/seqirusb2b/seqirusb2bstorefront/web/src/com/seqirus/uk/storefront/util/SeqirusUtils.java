/**
 *
 */
package com.seqirus.uk.storefront.util;

import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantOptionQualifierData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * @author 700196
 *
 */
public class SeqirusUtils
{
	protected static final String EXPLITCONSENT = "visitor_type";

	/**
	 * This method will return remember me cookie value.
	 *
	 * @param request
	 * @return cookie value.
	 */
	public static String getCookie(final HttpServletRequest request)
	{
		final Cookie[] cookies = request.getCookies();
		if (null != cookies)
		{
			for (final Cookie cookie : cookies)
			{
				if (cookie.getName().equalsIgnoreCase(EXPLITCONSENT))
				{
					return cookie.getValue();
				}
			}
		}
		return null;
	}

	/**
	 * get formulation category data
	 *
	 * @param variantOptionData
	 * @return
	 */
	public static Map<String, Collection<VariantOptionData>> getFormulationCategories(final VariantOptionData variantOptionData)
	{
		final Collection<VariantOptionQualifierData> formulations = variantOptionData.getVariantOptionQualifiers();
		final Map<String, Collection<VariantOptionData>> map = new HashMap();
		for (final VariantOptionQualifierData variantOptionQualifierData : formulations)
		{
			final String formulationName = variantOptionQualifierData.getValue();
			if (map.containsKey(formulationName))
			{
				map.get(formulationName).add(variantOptionData);
				removeDuplicate(map.get(formulationName));
			}
			else
			{
				final List<VariantOptionData> vodList = new ArrayList<>();
				vodList.add(variantOptionData);
				map.put(formulationName, vodList);

			}
		}
		return map;
	}

	/**
	 * Remove duplicates from the collection
	 *
	 * @param collection
	 */
	public static void removeDuplicate(final Collection<VariantOptionData> variantOptionDatas)
	{
		final List<VariantOptionData> result = new ArrayList<>();
		final Set<String> uniqueVODs = new HashSet<String>();

		for (final VariantOptionData vod : variantOptionDatas)
		{
			if (uniqueVODs.add(vod.getCode()))
			{
				result.add(vod);
			}
		}

	}

	public static void sortBySequenceId(final Map<String, Collection<VariantOptionData>> productsGroupByFormulation)
	{
		for (final String formulation : productsGroupByFormulation.keySet())
		{
			final List<VariantOptionData> variants = (List<VariantOptionData>) productsGroupByFormulation.get(formulation);
			Collections.sort(variants, new Comparator<VariantOptionData>()
			{
				@Override
				public int compare(final VariantOptionData arg0, final VariantOptionData arg1)
				{
					if (arg0.getSequenceId() != null && arg1.getSequenceId() != null)
					{
						return arg0.getSequenceId().compareTo(arg1.getSequenceId());
					}
					return 0;

				}
			});
		}
	}



}
