/**
 *
 */
package com.seqirus.uk.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Live Assist Controller for supporting sub domain
 *
 * @author 172553
 *
 */
@Controller
@RequestMapping("/liveassist")
public class SeqirusLiveAssistController extends AbstractPageController
{
	@RequestMapping(method = RequestMethod.GET)
	public String home(final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException, ServletException, IOException
	{

		final Cookie cookie = new Cookie("visitor_type", "healthcare");
		response.addCookie(cookie);
		return REDIRECT_PREFIX + "/homepage?asm=true";
	}
}
