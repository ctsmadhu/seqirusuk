/**
 *
 */
package com.seqirus.uk.storefront.controllers.pages;


import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.seqirus.core.model.SeasonEntryModel;
import com.seqirus.core.model.SeqirusSeasonModel;
import com.seqirus.facades.cart.SeqirusCheckoutFacade;
import com.seqirus.uk.core.constants.Seqirusb2bCoreConstants;
import com.seqirus.uk.core.dataObjects.ChartandTableData;
import com.seqirus.uk.core.dataObjects.OrderSummary;
import com.seqirus.uk.core.dataObjects.ProductDetail;
import com.seqirus.uk.core.dataObjects.ShipmentDetail;
import com.seqirus.uk.core.dataObjects.ShipmentTableData;
import com.seqirus.uk.core.orders.service.SeqirusOrdersService;
import com.seqirus.uk.core.orders.service.product.SeqirusProductService;
import com.seqirus.uk.facades.cutomer.data.SeqirusOrderQuantityData;
import com.seqirus.uk.facades.orders.SeqirusOrdersFacade;
import com.seqirus.uk.storefront.controllers.ControllerConstants;
import com.seqirus.uk.storefront.util.SeqirusUtils;

@Controller
@RequestMapping(value = "/orders")
public class SeqirusOrdersPageController extends AbstractSearchPageController
{
	private static final Logger LOGGER = Logger.getLogger(AccountPageController.class);

	@Resource(name = "commerceCategoryService")
	private CommerceCategoryService commerceCategoryService;

	@Resource(name = "productService")
	private ProductService productService;

	@Autowired
	private SeqirusProductService seqirusProductService;

	/**
	 *
	 * @param model
	 * @param redirectModel
	 * @return
	 * @throws CMSItemNotFoundException
	 *
	 *            METHOD TO GET ACCOUNT DASBOARD DETAILS.
	 */

	@Resource(name = "seqirusOrdersFacade")
	private SeqirusOrdersFacade seqirusOrdersFacade;

	@Resource(name = "userService")
	UserService userService;

	@Resource(name = "productVariantFacade")
	private ProductFacade productFacade;

	@Resource(name = "seqirusCheckoutFacade")
	private SeqirusCheckoutFacade seqirusCheckoutFacade;

	@Resource(name = "seqirusOrdersService")
	private SeqirusOrdersService seqirusOrdersService;

	@Resource
	private SessionService sessionService;

	@Autowired
	protected ConfigurationService configurationService;

	private static final String NO_FORMULATION = "NoFormulation";
	private static final String BASE_FLUADTIV10X = "FLUADTIV10X";
	private static final String BASE_FLUADTIV1X = "FLUADTIV1X";
	private static final String BASE_FLUADQIV10X = "FLUADQIV10X";
	private static final String BASE_FLUADQIV1X = "FLUADQIV1X";
	private static final String BASE_FLUCELVAXQIV10X = "FLUCELVAXQIV10X";
	private static final String BASE_FLUCELVAXQIV1X = "FLUCELVAXQIV1X";
	private static final String BASE_ADJUVANTEDTIV10X = "ADJUVANTEDTIV10X";
	private static final String BASE_ADJUVANTEDTIV1X = "ADJUVANTEDTIV1X";
	private static final String BASE_FLUCELVAX1X = "FLUCELVAX1X";
	private static final String BASE_FLUCELVAX10X = "FLUCELVAX10X";


	private static final String TOTAL_SHIP_FLUADTIV10X = "totalShipFluadTiv10x";
	private static final String TOTAL_SHIP_FLUADTIV1X = "totalShipFluadTiv1x";
	private static final String TOTAL_SHIP_FLUADQIV10X = "totalShipFluadQiv10x";
	private static final String TOTAL_SHIP_FLUADQIV1X = "totalShipFluadQiv1x";
	private static final String TOTAL_SHIP_FLUCELVAXQIV10X = "totalShipFlucelvaxQiv10x";
	private static final String TOTAL_SHIP_FLUCELVAXQIV1X = "totalShipFlucelvaxQiv1x";
	private static final String TOTAL_SHIP_ADJUVANTEDTIV10X = "totalShipAdjuvantedTiv10x";
	private static final String TOTAL_SHIP_ADJUVANTEDTIV1X = "totalShipAdjuvantedTiv1x";
	private static final String TOTAL_SHIP_FLUCELVAX1X = "totalShipFlucelvax1x";
	private static final String TOTAL_SHIP_FLUCELVAX10X = "totalShipFlucelvax10x";


	private static final String TOTAL_SHIP = "totalShip";
	private static final String TOTAL_ORDER_FLUADTIV10X = "totalOrderFluadTiv10x";
	private static final String TOTAL_ORDER_FLUADTIV1X = "totalOrderFluadTiv1x";
	private static final String TOTAL_ORDER_FLUADQIV10X = "totalOrderFluadQiv10x";
	private static final String TOTAL_ORDER_FLUADQIV1X = "totalOrderFluadQiv1x";
	private static final String TOTAL_ORDER_FLUCELVAXQIV10X = "totalOrderFlucelvaxQiv10x";
	private static final String TOTAL_ORDER_FLUCELVAXQIV1X = "totalOrderFlucelvaxQiv1x";
	private static final String TOTAL_ORDER_ADJUVANTEDTIV10X = "totalOrderAdjuvantedTiv10x";
	private static final String TOTAL_ORDER_ADJUVANTEDTIV1X = "totalOrderAdjuvantedTiv1x";
	private static final String TOTAL_ORDER_FLUCELVAX1X = "totalOrderFlucelvax1x";
	private static final String TOTAL_ORDER_FLUCELVAX10X = "totalOrderFlucelvax10x";
	private static final String TOTAL_ORDER_QTY = "totalOrder";


	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String orders(final Model model, final RedirectAttributes redirectModel,
			@RequestParam(value = "season", required = false) String season) throws CMSItemNotFoundException
	{

		final UserModel currentUser = userService.getCurrentUser();
		final B2BCustomerModel customer = (B2BCustomerModel) currentUser;
		if (null != customer.getDefaultB2BUnit())
		{
			final B2BUnitModel b2bUnit = customer.getDefaultB2BUnit();

			final SeasonEntryModel entry = fetchSeasonEntry();
			if (null == season)
			{
				season = configurationService.getConfiguration().getString(Seqirusb2bCoreConstants.DEFAULT_SEASONENTRY);
			}
			model.addAttribute("presentSeason", season);

			final List<OrderSummary> orders = seqirusOrdersFacade.getOrders(b2bUnit.getUid(), season);

			final B2BCustomerModel account = (B2BCustomerModel) userService.getCurrentUser();
			final String customerId = account.getDefaultB2BUnit().getUid();


			if (null != season)
			{
				getSessionService().setAttribute("season", season);
				model.addAttribute("presentSeason", season);

			}



			removeJunkOrder(orders);
			final List<OrderSummary> customerOrderlist = new ArrayList<OrderSummary>();
			final List<String> baseProduct = new ArrayList<String>();
			baseProduct.add("FLUADTIV10X");
			baseProduct.add("FLUADTIV1X");
			baseProduct.add("FLUADQIV10X");
			baseProduct.add("FLUADQIV1X");
			baseProduct.add("FLUCELVAXQIV10X");
			baseProduct.add("FLUCELVAXQIV1X");
			baseProduct.add("ADJUVANTEDTIV10X");
			baseProduct.add("ADJUVANTEDTIV1X");
			baseProduct.add("FLUCELVAX1X");
			baseProduct.add("FLUCELVAX10X");
			model.addAttribute("baseProduct", baseProduct);


			final Collection<VariantOptionData> varientdataInSeason = new ArrayList<VariantOptionData>();
			final List<VariantOptionData> varientdataSortedInSeason = new ArrayList<VariantOptionData>();
			final Collection<VariantOptionData> varientdataOrderableInSeason = new ArrayList<VariantOptionData>();
			final List<VariantOptionData> varientdataOrderableListInSeason = new ArrayList<VariantOptionData>();

			getProductsForSeason("Seqirus_InSeason", varientdataInSeason, varientdataSortedInSeason, varientdataOrderableInSeason,
					varientdataOrderableListInSeason);

			final Collection<VariantOptionData> varientdataPreSeason = new ArrayList<VariantOptionData>();
			final List<VariantOptionData> varientdataSortedPreSeason = new ArrayList<VariantOptionData>();
			final Collection<VariantOptionData> varientdataOrderablePreSeason = new ArrayList<VariantOptionData>();
			final List<VariantOptionData> varientdataOrderableListPreSeason = new ArrayList<VariantOptionData>();

			getProductsForSeason("Seqirus_PreSeason", varientdataPreSeason, varientdataSortedPreSeason,
					varientdataOrderablePreSeason, varientdataOrderableListPreSeason);


			// filter selected season order from API response returned for date range
			for (final OrderSummary co : orders)
			{

				final String varCode = co.getProducts().get(0).getMaterialID();

				// set in/pre season

				for (final VariantOptionData variantOptionData : varientdataPreSeason)
				{
					if (varCode.equalsIgnoreCase(variantOptionData.getCode()))
					{
						co.setSeasonInPre("Seqirus_PreSeason");
						break;
					}
				}
				for (final VariantOptionData variantOptionData : varientdataInSeason)
				{
					if (varCode.equalsIgnoreCase(variantOptionData.getCode()))
					{
						co.setSeasonInPre("Seqirus_InSeason");
						break;
					}
				}

				// set season  like 2019 2020
				final VariantProductModel product = (VariantProductModel) productService.getProductForCode(varCode);
				final String productSeason = product.getSeason();
				if (StringUtils.isNotEmpty(productSeason) && productSeason.equalsIgnoreCase(season))
				{
					customerOrderlist.add(co);
				}

				// set contract price
				if (null != co.getSeasonInPre() && co.getSeasonInPre().equalsIgnoreCase("Seqirus_InSeason"))
				{
					BigDecimal total = new BigDecimal(0);
					for (final ProductDetail productforPrice : co.getProducts())
					{

						for (final VariantOptionData variantOptionData : varientdataInSeason)
						{
							if (productforPrice.getMaterialID().equalsIgnoreCase(variantOptionData.getCode())
									&& null != variantOptionData.getPriceData() && null != variantOptionData.getPriceData().getValue())
							{
								total = total.add(variantOptionData.getPriceData().getValue()
										.multiply(BigDecimal.valueOf(productforPrice.getQuantity())));

							}
						}

					}
					co.setEstimatedCost(total);

					// set total quantity for each base product
					int fluadtiv10x = 0;
					int fluadtiv1x = 0;
					int fluadqiv10x = 0;
					int fluadqiv1x = 0;
					int flucelvaxqiv10x = 0;
					int flucelvaxqiv1x = 0;
					int adjuvantedtiv10x = 0;
					int adjuvantedtiv1x = 0;
					int flucelvax1x = 0;
					int flucelvax10x = 0;
					for (final String base : baseProduct)
					{
						int totalQty = 0;
						for (final VariantOptionData variantOptionData : varientdataInSeason)
						{

							VariantProductModel productModel;
							productModel = (VariantProductModel) seqirusProductService
									.getProductDataForCode(variantOptionData.getCode());

							if (base.equalsIgnoreCase(productModel.getBaseName()))
							{

								for (final ProductDetail productData : co.getProducts())
								{

									if (productModel.getCode().equalsIgnoreCase(productData.getMaterialID()))
									{

										totalQty = totalQty + productData.getQuantity();
									}
								}
							}
						}
						if (base.equalsIgnoreCase(BASE_FLUADTIV10X))
						{
							fluadtiv10x = totalQty;
						}
						if (base.equalsIgnoreCase(BASE_FLUADTIV1X))
						{
							fluadtiv1x = totalQty;
						}
						if (base.equalsIgnoreCase(BASE_FLUADQIV10X))
						{
							fluadqiv10x = totalQty;
						}
						if (base.equalsIgnoreCase(BASE_FLUADQIV1X))
						{
							fluadqiv1x = totalQty;
						}

						if (base.equalsIgnoreCase(BASE_FLUCELVAXQIV10X))
						{
							flucelvaxqiv10x = totalQty;
						}
						if (base.equalsIgnoreCase(BASE_FLUCELVAXQIV1X))
						{
							flucelvaxqiv1x = totalQty;
						}

						if (base.equalsIgnoreCase(BASE_ADJUVANTEDTIV10X))
						{
							adjuvantedtiv10x = totalQty;
						}
						if (base.equalsIgnoreCase(BASE_ADJUVANTEDTIV1X))
						{
							adjuvantedtiv1x = totalQty;

						}
						if (base.equalsIgnoreCase(BASE_FLUCELVAX1X))
						{
							flucelvax1x = totalQty;
						}
						if (base.equalsIgnoreCase(BASE_FLUCELVAX10X))
						{
							flucelvax10x = totalQty;
						}

					}
					co.setTotalFluad(fluadtiv10x + fluadtiv1x + fluadqiv10x + fluadqiv1x);
					co.setTotalFlucelvax(flucelvaxqiv10x + flucelvaxqiv1x + flucelvax1x + flucelvax10x);
					co.setTotalAfluria(adjuvantedtiv10x + adjuvantedtiv1x);

					// set quantity end
				}

				if (null != co.getSeasonInPre() && co.getSeasonInPre().equalsIgnoreCase("Seqirus_PreSeason"))
				{
					BigDecimal total = new BigDecimal(0);
					for (final ProductDetail productforPrice : co.getProducts())
					{

						for (final VariantOptionData variantOptionData : varientdataPreSeason)
						{
							if (productforPrice.getMaterialID().equalsIgnoreCase(variantOptionData.getCode())
									&& null != variantOptionData.getPriceData() && null != variantOptionData.getPriceData().getValue())
							{
								total = total.add(variantOptionData.getPriceData().getValue()
										.multiply(BigDecimal.valueOf(productforPrice.getQuantity())));

							}
						}

					}
					co.setEstimatedCost(total);


					// set total quantity for each base product
					int fluadtiv10x = 0;
					int fluadtiv1x = 0;
					int fluadqiv10x = 0;
					int fluadqiv1x = 0;
					int flucelvaxqiv10x = 0;
					int flucelvaxqiv1x = 0;
					int adjuvantedtiv10x = 0;
					int adjuvantedtiv1x = 0;
					int flucelvax1x = 0;
					int flucelvax10x = 0;
					for (final String base : baseProduct)
					{
						int totalBaseQty = 0;
						for (final VariantOptionData variantOptionData : varientdataPreSeason)
						{

							VariantProductModel productModel;
							productModel = (VariantProductModel) seqirusProductService
									.getProductDataForCode(variantOptionData.getCode());

							if (base.equalsIgnoreCase(productModel.getBaseName()))
							{

								for (final ProductDetail productdata : co.getProducts())
								{

									if (productModel.getCode().equalsIgnoreCase(productdata.getMaterialID()))
									{

										totalBaseQty = totalBaseQty + productdata.getQuantity();
									}
								}
							}
						}
						if (base.equalsIgnoreCase(BASE_FLUADTIV10X))
						{
							fluadtiv10x = totalBaseQty;
						}
						if (base.equalsIgnoreCase(BASE_FLUADTIV1X))
						{
							fluadtiv1x = totalBaseQty;
						}
						if (base.equalsIgnoreCase(BASE_FLUADQIV10X))
						{
							fluadqiv10x = totalBaseQty;
						}
						if (base.equalsIgnoreCase(BASE_FLUADQIV1X))
						{
							fluadqiv1x = totalBaseQty;
						}

						if (base.equalsIgnoreCase(BASE_FLUCELVAXQIV10X))
						{
							flucelvaxqiv10x = totalBaseQty;
						}
						if (base.equalsIgnoreCase(BASE_FLUCELVAXQIV1X))
						{
							flucelvaxqiv1x = totalBaseQty;
						}

						if (base.equalsIgnoreCase(BASE_ADJUVANTEDTIV10X))
						{
							adjuvantedtiv10x = totalBaseQty;
						}
						if (base.equalsIgnoreCase(BASE_ADJUVANTEDTIV1X))
						{
							adjuvantedtiv1x = totalBaseQty;

						}

						if (base.equalsIgnoreCase(BASE_FLUCELVAX1X))
						{
							flucelvax1x = totalBaseQty;
						}
						if (base.equalsIgnoreCase(BASE_FLUCELVAX10X))
						{
							flucelvax10x = totalBaseQty;
						}

					}
					co.setTotalFluad(fluadtiv10x + fluadtiv1x + fluadqiv10x + fluadqiv1x);
					co.setTotalFlucelvax(flucelvaxqiv10x + flucelvaxqiv1x + flucelvax1x + flucelvax10x);
					co.setTotalAfluria(adjuvantedtiv10x + adjuvantedtiv1x);


				}


			}



			//set ship state
			final List<AddressData> allAddressData = seqirusCheckoutFacade.populateSoldToLinkedAddresses();
			final List<AddressData> shippingAddressData = seqirusCheckoutFacade
					.populateLinkedAddressesForGivenType("shippingAddress", allAddressData);
			final Set<String> shipAddressIds = new HashSet<String>();
			int totalShip = 0;
			int totalShipFluadTiv10x = 0;
			int totalShipFluadTiv1x = 0;
			int totalShipFluadQiv10x = 0;
			int totalShipFluadQiv1x = 0;
			int totalShipFlucelvaxQiv10x = 0;
			int totalShipFlucelvaxQiv1x = 0;
			int totalShipAdjuvantedTiv10x = 0;
			int totalShipAdjuvantedTiv1x = 0;
			int totalShipFlucelvax1x = 0;
			int totalShipFlucelvax10x = 0;

			int totalOrder = 0;
			int totalOrderFluadTiv10x = 0;
			int totalOrderFluadTiv1x = 0;
			int totalOrderFluadQiv10x = 0;
			int totalOrderFluadQiv1x = 0;
			int totalOrderFlucelvaxQiv10x = 0;
			int totalOrderFlucelvaxQiv1x = 0;
			int totalOrderAdjuvantedTiv10x = 0;
			int totalOrderAdjuvantedTiv1x = 0;
			int totalOrderFlucelvax1x = 0;
			int totalOrderFlucelvax10x = 0;

			//	final Map< List<baseProduct>  , map<String , String>> shipmentMap = new HashMap<>();

			for (final OrderSummary co : customerOrderlist)
			{

				for (final AddressData shippingAddress : shippingAddressData)

				{

					if (shippingAddress.getSapCustomerId().equals(co.getShipToID()))
					{
						//this.fetchShippingParams(seqCart);
						if (null != shippingAddress.getRegion() && null != shippingAddress.getRegion().getIsocodeShort())
						{
							co.getAddress().setState(shippingAddress.getRegion().getIsocodeShort());
						}


					}
				}
				shipAddressIds.add(co.getShipToID());

				// set total order  quantities


				if (!co.getStatus().equalsIgnoreCase("Cancelled"))
				{
					totalOrder = totalOrder + co.getTotalOrderedQty();
					for (final String orderMaterial : co.getAllOrderedMaterials())
					{


						VariantProductModel productModel;
						productModel = (VariantProductModel) seqirusProductService.getProductDataForCode(orderMaterial);

						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADTIV10X))
						{

							totalOrderFluadTiv10x = totalOrderFluadTiv10x + co.getMaterialOrderedQty(orderMaterial);

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADTIV1X))
						{

							totalOrderFluadTiv1x = totalOrderFluadTiv1x + co.getMaterialOrderedQty(orderMaterial);

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADQIV10X))
						{

							totalOrderFluadQiv10x = totalOrderFluadQiv10x + co.getMaterialOrderedQty(orderMaterial);

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADQIV1X))
						{

							totalOrderFluadQiv1x = totalOrderFluadQiv1x + co.getMaterialOrderedQty(orderMaterial);

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAXQIV10X))
						{

							totalOrderFlucelvaxQiv10x = totalOrderFlucelvaxQiv10x + co.getMaterialOrderedQty(orderMaterial);

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAXQIV1X))
						{

							totalOrderFlucelvaxQiv1x = totalOrderFlucelvaxQiv1x + co.getMaterialOrderedQty(orderMaterial);

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_ADJUVANTEDTIV10X))
						{

							totalOrderAdjuvantedTiv10x = totalOrderAdjuvantedTiv10x + co.getMaterialOrderedQty(orderMaterial);

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_ADJUVANTEDTIV1X))
						{

							totalOrderAdjuvantedTiv1x = totalOrderAdjuvantedTiv1x + co.getMaterialOrderedQty(orderMaterial);

						}

						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAX1X))
						{

							totalOrderFlucelvax1x = totalOrderFlucelvax1x + co.getMaterialOrderedQty(orderMaterial);

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAX10X))
						{

							totalOrderFlucelvax10x = totalOrderFlucelvax10x + co.getMaterialOrderedQty(orderMaterial);

						}


					}


					// set total shipment quantities

					for (final ShipmentDetail ship : co.getShipments())
					{

						totalShip = totalShip + ship.getTotalShippedQty();
						final List<String> shipMaterials = ship.getShippedMaterials();

						final Set<String> shipMaterialsSet = new HashSet<String>();
						for (final String material : shipMaterials)
						{
							shipMaterialsSet.add(material);
						}
						for (final String material : shipMaterialsSet)

						{

							VariantProductModel productModel;
							productModel = (VariantProductModel) seqirusProductService.getProductDataForCode(material);

							if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADTIV10X))
							{

								totalShipFluadTiv10x = totalShipFluadTiv10x + co.getMaterialShippedQty(material);

							}
							if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADTIV1X))
							{

								totalShipFluadTiv1x = totalShipFluadTiv1x + co.getMaterialShippedQty(material);

							}
							if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADQIV10X))
							{

								totalShipFluadQiv10x = totalShipFluadQiv10x + co.getMaterialShippedQty(material);

							}
							if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADQIV1X))
							{

								totalShipFluadQiv1x = totalShipFluadQiv1x + co.getMaterialShippedQty(material);

							}
							if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAXQIV10X))
							{

								totalShipFlucelvaxQiv10x = totalShipFlucelvaxQiv10x + co.getMaterialShippedQty(material);

							}
							if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAXQIV1X))
							{

								totalShipFlucelvaxQiv1x = totalShipFlucelvaxQiv1x + co.getMaterialShippedQty(material);

							}
							if (productModel.getBaseName().equalsIgnoreCase(BASE_ADJUVANTEDTIV10X))
							{

								totalShipAdjuvantedTiv10x = totalShipAdjuvantedTiv10x + co.getMaterialShippedQty(material);

							}
							if (productModel.getBaseName().equalsIgnoreCase(BASE_ADJUVANTEDTIV1X))
							{

								totalShipAdjuvantedTiv1x = totalShipAdjuvantedTiv1x + co.getMaterialShippedQty(material);

							}
							if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAX1X))
							{

								totalShipFlucelvax1x = totalShipFlucelvax1x + co.getMaterialShippedQty(material);

							}
							if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAX10X))
							{

								totalShipFlucelvax10x = totalShipFlucelvax10x + co.getMaterialShippedQty(material);

							}


						}

					}

				}

			}

			//set ship end


			model.addAttribute(TOTAL_SHIP_FLUADTIV10X, totalShipFluadTiv10x);
			model.addAttribute(TOTAL_SHIP_FLUADTIV1X, totalShipFluadTiv1x);
			model.addAttribute(TOTAL_SHIP_FLUADQIV10X, totalShipFluadQiv10x);
			model.addAttribute(TOTAL_SHIP_FLUADQIV1X, totalShipFluadQiv1x);
			model.addAttribute(TOTAL_SHIP_FLUCELVAXQIV10X, totalShipFlucelvaxQiv10x);
			model.addAttribute(TOTAL_SHIP_FLUCELVAXQIV1X, totalShipFlucelvaxQiv1x);
			model.addAttribute(TOTAL_SHIP_ADJUVANTEDTIV10X, totalShipAdjuvantedTiv10x);
			model.addAttribute(TOTAL_SHIP_ADJUVANTEDTIV1X, totalShipAdjuvantedTiv1x);
			model.addAttribute(TOTAL_SHIP_FLUCELVAX1X, totalShipFlucelvax1x);
			model.addAttribute(TOTAL_SHIP_FLUCELVAX10X, totalShipFlucelvax10x);
			model.addAttribute(TOTAL_SHIP, totalShip);
			model.addAttribute(TOTAL_ORDER_FLUADTIV10X, totalOrderFluadTiv10x);
			model.addAttribute(TOTAL_ORDER_FLUADTIV1X, totalOrderFluadTiv1x);
			model.addAttribute(TOTAL_ORDER_FLUADQIV10X, totalOrderFluadQiv10x);
			model.addAttribute(TOTAL_ORDER_FLUADQIV1X, totalOrderFluadQiv1x);
			model.addAttribute(TOTAL_ORDER_FLUCELVAXQIV10X, totalOrderFlucelvaxQiv10x);
			model.addAttribute(TOTAL_ORDER_FLUCELVAXQIV1X, totalOrderFlucelvaxQiv1x);
			model.addAttribute(TOTAL_ORDER_ADJUVANTEDTIV10X, totalOrderAdjuvantedTiv10x);
			model.addAttribute(TOTAL_ORDER_ADJUVANTEDTIV1X, totalOrderAdjuvantedTiv1x);
			model.addAttribute(TOTAL_ORDER_FLUCELVAX1X, totalOrderFlucelvax1x);
			model.addAttribute(TOTAL_ORDER_FLUCELVAX10X, totalOrderFlucelvax10x);
			model.addAttribute(TOTAL_ORDER_QTY, totalOrder);

			String customerPriceGroup = null;
			String accountName = null;
			if (null != account.getDefaultB2BUnit().getUserPriceGroup())
			{
				customerPriceGroup = account.getDefaultB2BUnit().getUserPriceGroup().getCode();
				accountName = account.getName();
			}
			if (!customerOrderlist.isEmpty())
			{

				final ProductModel product = productService
						.getProductForCode(String.valueOf(customerOrderlist.get(0).getProducts().get(0).getMaterialID()));

				if (null != product.getEurope1Prices())
				{
					final Collection<PriceRowModel> priceRowModelList = product.getEurope1Prices();
					PriceRowModel priceRowModel = new PriceRowModel();
					for (final PriceRowModel prm : priceRowModelList)
					{
						if (StringUtils.isNotEmpty(customerPriceGroup) && null != prm.getUg()
								&& customerPriceGroup.equalsIgnoreCase(prm.getUg().getCode()))
						{
							priceRowModel = prm;
						}

						if (StringUtils.isNotEmpty(customerPriceGroup) && null != prm.getUser()
								&& accountName.equalsIgnoreCase(prm.getUser().getName()))
						{
							priceRowModel = prm;
						}
					}
					model.addAttribute("priceRowModel", priceRowModel);
				}
			}

			model.addAttribute("varientdataInSeasonOrderable", varientdataOrderableListInSeason);
			model.addAttribute("varientdataPreSeasonOrderable", varientdataOrderableListPreSeason);


			final Collection<VariantOptionData> varientdataInCurrentSeason = new ArrayList<VariantOptionData>();
			final Collection<VariantOptionData> varientdataPreCurrentSeason = new ArrayList<VariantOptionData>();

			for (final VariantOptionData variantOptionData : varientdataInSeason)
			{

				final VariantProductModel product = (VariantProductModel) productService
						.getProductForCode(variantOptionData.getCode());
				final String productSeason = product.getSeason();
				if (season.equalsIgnoreCase(productSeason))
				{
					varientdataInCurrentSeason.add(variantOptionData);
				}
			}

			for (final VariantOptionData variantOptionData : varientdataPreSeason)
			{

				final VariantProductModel product = (VariantProductModel) productService
						.getProductForCode(variantOptionData.getCode());
				final String productSeason = product.getSeason();
				if (season.equalsIgnoreCase(productSeason))
				{
					varientdataPreCurrentSeason.add(variantOptionData);
				}
			}

			final List<SeqirusSeasonModel> seasonList = (entry == null ? null : entry.getSeasonList());
			model.addAttribute("seasonList", seasonList);
			model.addAttribute("varientdataInSeason", varientdataInCurrentSeason);
			model.addAttribute("varientdataPreSeason", varientdataPreCurrentSeason);


			model.addAttribute("orders", customerOrderlist);
			model.addAttribute("totalOrders", customerOrderlist.size());
			sessionService.setAttribute("allOrders", orders);
			model.addAttribute("shipAddressIds", shipAddressIds);


		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.Account.ORDERS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ControllerConstants.Views.Pages.Account.ORDERS_CMS_PAGE));
		return getViewForPage(model);
	}



	private void getProductsForSeason(final String season, final Collection<VariantOptionData> varientdata,
			final List<VariantOptionData> varientdataSorted, final Collection<VariantOptionData> varientdataOrderable,
			final List<VariantOptionData> varientdataOrderableList)
	{
		// all orderable products
		final CategoryModel category = commerceCategoryService.getCategoryForCode(season);
		final List<ProductOption> options = new ArrayList<>(
				Arrays.asList(ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL, ProductOption.VARIANT_MATRIX_MEDIA,
						ProductOption.BASIC, ProductOption.URL, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
						ProductOption.GALLERY, ProductOption.CATEGORIES, ProductOption.REVIEW, ProductOption.PROMOTIONS,
						ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL, ProductOption.STOCK, ProductOption.VOLUME_PRICES,
						ProductOption.PRICE_RANGE, ProductOption.DELIVERY_MODE_AVAILABILITY));


		final List<ProductModel> products = category.getProducts();

		final List<ProductWrapperData> productDataList = new ArrayList<>();
		final List<Map<String, Collection<VariantOptionData>>> productFormulationsList = new ArrayList<Map<String, Collection<VariantOptionData>>>();

		for (final ProductModel productModel : products)
		{
			final ProductWrapperData pwd = new ProductWrapperData();
			ProductData productData = productFacade.getProductForCodeAndOptions(productModel.getCode(), options);
			pwd.setProductFormulations(groupProductsByFormulationOrder(productData));
			pwd.setMinPrice(minProductPrice(productData));
			pwd.setProductData(productData);
			productDataList.add(pwd);
			productFormulationsList.add(groupProductsByFormulationOrder(productData));
			productData = null;
		}



		for (final Map<String, Collection<VariantOptionData>> productFormulations : productFormulationsList)
		{
			for (final Map.Entry<String, Collection<VariantOptionData>> entry : productFormulations.entrySet())
			{

				varientdata.addAll(entry.getValue());
				varientdataSorted.addAll(entry.getValue());

			}

		}

		for (final VariantOptionData varient : varientdata)
		{
			if (varient.getOrderable())
			{
				varientdataOrderable.add(varient);
				varientdataOrderableList.add(varient);
			}
		}

		Collections.sort(varientdataSorted, new Comparator<VariantOptionData>()
		{

			public int compare(final VariantOptionData u1, final VariantOptionData u2)
			{
				if ((null == u1.getSequenceId()) || (null == u2.getSequenceId()))
				{
					return 0;
				}
				else
				{
					return u1.getSequenceId().compareTo(u2.getSequenceId());
				}
			}
		});


	}


	/**
	 * @param productData
	 * @return
	 */
	private Map<String, Collection<VariantOptionData>> groupProductsByFormulationOrder(final ProductData productData)
	{
		final List<VariantOptionData> variants = productData.getVariantOptions();

		final Map<String, Collection<VariantOptionData>> productsGroupByFormulation = new HashMap<>();

		if (null == variants || variants.isEmpty())
		{
			return productsGroupByFormulation;
		}

		for (final VariantOptionData variantOptionData : variants)
		{

			if (CollectionUtils.isNotEmpty(variantOptionData.getVariantOptionQualifiers()))
			{
				udpateproductAndFormulationMap(productsGroupByFormulation, SeqirusUtils.getFormulationCategories(variantOptionData));

			}
			else if (productsGroupByFormulation.containsKey(NO_FORMULATION))
			{
				productsGroupByFormulation.get(NO_FORMULATION).add(variantOptionData);
			}
			else
			{
				final List<VariantOptionData> vodList = new ArrayList<>();
				vodList.add(variantOptionData);
				productsGroupByFormulation.put(NO_FORMULATION, vodList);
			}
		}

		SeqirusUtils.sortBySequenceId(productsGroupByFormulation);
		final TreeMap<String, Collection<VariantOptionData>> sorted = new TreeMap<>();
		sorted.putAll(productsGroupByFormulation);
		return sorted;
	}

	/**
	 * @param productsGroupByFormulation
	 * @param formulationData
	 */
	private void udpateproductAndFormulationMap(final Map<String, Collection<VariantOptionData>> productsGroupByFormulation,
			final Map<String, Collection<VariantOptionData>> formulationData)
	{

		//Map<BlogKey, Blog> map = new HashMap<BlogKey, Blog>();
		for (final String formulation : formulationData.keySet())
		{
			if (productsGroupByFormulation.containsKey(formulation))
			{
				productsGroupByFormulation.get(formulation).addAll(formulationData.get(formulation));
				SeqirusUtils.removeDuplicate(productsGroupByFormulation.get(formulation));
			}
			else
			{
				productsGroupByFormulation.put(formulation, formulationData.get(formulation));
			}
		}

	}

	/**
	 * @param productData
	 * @return
	 */
	private BigDecimal minProductPrice(final ProductData productData)
	{
		final List<VariantOptionData> variants = productData.getVariantOptions();
		if (null != variants)
		{
			final List<BigDecimal> allVariantsPrices = new ArrayList<BigDecimal>();
			for (final VariantOptionData variantOptionData : variants)
			{
				if (null != variantOptionData.getPriceData())
				{
					allVariantsPrices.add(variantOptionData.getPriceData().getValue());
				}

			}
			if (allVariantsPrices.isEmpty())
			{
				return BigDecimal.ZERO;
			}
			return Collections.min(allVariantsPrices);
		}
		else
		{
			return BigDecimal.ZERO;
		}
	}

	/**
	 * @param entry
	 * @return
	 */
	private SeasonEntryModel fetchSeasonEntry()
	{
		SeasonEntryModel entry = null;
		if (null != seqirusOrdersService)
		{
			entry = seqirusOrdersService.getSeasonEntry();
		}
		return entry;
	}

	public class ProductWrapperData
	{
		private ProductData productData;
		private Map<String, Collection<VariantOptionData>> productFormulations;
		private BigDecimal minPrice;

		/**
		 * @return the productData
		 */
		public ProductData getProductData()
		{
			return productData;
		}

		/**
		 * @param productData
		 *           the productData to set
		 */
		public void setProductData(final ProductData productData)
		{
			this.productData = productData;
		}

		/**
		 * @return the minPrice
		 */
		public BigDecimal getMinPrice()
		{
			return minPrice;
		}

		/**
		 * @param minPrice
		 *           the minPrice to set
		 */
		public void setMinPrice(final BigDecimal minPrice)
		{
			this.minPrice = minPrice;
		}

		/**
		 * @return the productFormulations
		 */
		public Map<String, Collection<VariantOptionData>> getProductFormulations()
		{
			return productFormulations;
		}

		/**
		 * @param productFormulations
		 *           the productFormulations to set
		 */
		public void setProductFormulations(final Map<String, Collection<VariantOptionData>> productFormulations)
		{
			this.productFormulations = productFormulations;
		}

	}


	/**
	 *
	 * @param orderID
	 * @param season
	 * @return
	 */

	@RequestMapping(value = "/getChartandTableData", method =
	{ RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public ChartandTableData getChartandTableData(@RequestParam(value = "orderID", required = false)
	final String orderID, @RequestParam(value = "season", required = true)
	final String season, final HttpServletRequest request, final Model model, final HttpSession session)
	{
		final ChartandTableData chartandTableData = new ChartandTableData();
		final SeqirusOrderQuantityData orderQuantity = new SeqirusOrderQuantityData();

		// get active and closed orders
		final List<OrderSummary> allCustomerOrderlist = new ArrayList<OrderSummary>();
		List<OrderSummary> orders = new ArrayList<OrderSummary>();
		orders = (List<OrderSummary>) sessionService.getAttribute("allOrders");

		final Collection<VariantOptionData> varientdataInSeason = new ArrayList<VariantOptionData>();
		final List<VariantOptionData> varientdataSortedInSeason = new ArrayList<VariantOptionData>();
		final Collection<VariantOptionData> varientdataOrderableInSeason = new ArrayList<VariantOptionData>();
		final List<VariantOptionData> varientdataOrderableListInSeason = new ArrayList<VariantOptionData>();

		getProductsForSeason("Seqirus_InSeason", varientdataInSeason, varientdataSortedInSeason, varientdataOrderableInSeason,
				varientdataOrderableListInSeason);

		final Collection<VariantOptionData> varientdataPreSeason = new ArrayList<VariantOptionData>();
		final List<VariantOptionData> varientdataSortedPreSeason = new ArrayList<VariantOptionData>();
		final Collection<VariantOptionData> varientdataOrderablePreSeason = new ArrayList<VariantOptionData>();
		final List<VariantOptionData> varientdataOrderableListPreSeason = new ArrayList<VariantOptionData>();

		getProductsForSeason("Seqirus_PreSeason", varientdataPreSeason, varientdataSortedPreSeason, varientdataOrderablePreSeason,
				varientdataOrderableListPreSeason);

		int totalOrder = 0;
		int totalOrderFluadTiv10x = 0;
		int totalOrderFluadTiv1x = 0;
		int totalOrderFluadQiv10x = 0;
		int totalOrderFluadQiv1x = 0;
		int totalOrderFlucelvaxQiv10x = 0;
		int totalOrderFlucelvaxQiv1x = 0;
		int totalOrderAdjuvantedTiv10x = 0;
		int totalOrderAdjuvantedTiv1x = 0;
		int totalOrderFlucelvax1x = 0;
		int totalOrderFlucelvax10x = 0;

		for (final OrderSummary summary : orders)
		{

			if (!summary.getStatus().equalsIgnoreCase("Cancelled"))
			{
				// added for large customer


				// set total order  quantities

				totalOrder = totalOrder + summary.getTotalOrderedQty();

				for (final String orderMaterial : summary.getAllOrderedMaterials())
				{


					VariantProductModel productModel;
					productModel = (VariantProductModel) seqirusProductService.getProductDataForCode(orderMaterial);

					if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADTIV10X))
					{

						totalOrderFluadTiv10x = totalOrderFluadTiv10x + summary.getMaterialOrderedQty(orderMaterial);

					}
					if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADTIV1X))
					{

						totalOrderFluadTiv1x = totalOrderFluadTiv1x + summary.getMaterialOrderedQty(orderMaterial);

					}
					if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADQIV10X))
					{

						totalOrderFluadQiv10x = totalOrderFluadQiv10x + summary.getMaterialOrderedQty(orderMaterial);

					}
					if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADQIV1X))
					{

						totalOrderFluadQiv1x = totalOrderFluadQiv1x + summary.getMaterialOrderedQty(orderMaterial);

					}
					if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAXQIV10X))
					{

						totalOrderFlucelvaxQiv10x = totalOrderFlucelvaxQiv10x + summary.getMaterialOrderedQty(orderMaterial);

					}
					if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAXQIV1X))
					{

						totalOrderFlucelvaxQiv1x = totalOrderFlucelvaxQiv1x + summary.getMaterialOrderedQty(orderMaterial);

					}
					if (productModel.getBaseName().equalsIgnoreCase(BASE_ADJUVANTEDTIV10X))
					{

						totalOrderAdjuvantedTiv10x = totalOrderAdjuvantedTiv10x + summary.getMaterialOrderedQty(orderMaterial);

					}
					if (productModel.getBaseName().equalsIgnoreCase(BASE_ADJUVANTEDTIV1X))
					{

						totalOrderAdjuvantedTiv1x = totalOrderAdjuvantedTiv1x + summary.getMaterialOrderedQty(orderMaterial);

					}
					if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAX1X))
					{

						totalOrderFlucelvax1x = totalOrderFlucelvax1x + summary.getMaterialOrderedQty(orderMaterial);

					}
					if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAX10X))
					{

						totalOrderFlucelvax10x = totalOrderFlucelvax10x + summary.getMaterialOrderedQty(orderMaterial);

					}



				}

			}

			if (null != orderID && orderID.equals(summary.getOrderID()))
			{
				final String varCode = summary.getProducts().get(0).getMaterialID();

				// set in/pre season


				for (final VariantOptionData variantOptionData : varientdataPreSeason)
				{
					if (varCode.equalsIgnoreCase(variantOptionData.getCode()))
					{
						summary.setSeasonInPre("Seqirus_PreSeason");
						break;
					}
				}
				for (final VariantOptionData variantOptionData : varientdataInSeason)
				{
					if (varCode.equalsIgnoreCase(variantOptionData.getCode()))
					{
						summary.setSeasonInPre("Seqirus_InSeason");
						break;
					}
				}


				final List<String> baseProduct = new ArrayList<String>();
				baseProduct.add("FLUADTIV10X");
				baseProduct.add("FLUADTIV1X");
				baseProduct.add("FLUADQIV10X");
				baseProduct.add("FLUADQIV1X");
				baseProduct.add("FLUCELVAXQIV10X");
				baseProduct.add("FLUCELVAXQIV1X");
				baseProduct.add("ADJUVANTEDTIV10X");
				baseProduct.add("ADJUVANTEDTIV1X");
				baseProduct.add("FLUCELVAX1X");
				baseProduct.add("FLUCELVAX10X");
				model.addAttribute("baseProduct", baseProduct);


				Collection<VariantOptionData> varientdataseason = new ArrayList<VariantOptionData>();

				if (null != summary.getSeasonInPre() && summary.getSeasonInPre().equalsIgnoreCase("Seqirus_InSeason"))
				{

					varientdataseason = varientdataInSeason;
				}
				else if (null != summary.getSeasonInPre() && summary.getSeasonInPre().equalsIgnoreCase("Seqirus_PreSeason"))
				{

					varientdataseason = varientdataPreSeason;
				}

				for (final String base : baseProduct)
				{
					int totalQty = 0;
					for (final VariantOptionData variantOptionData : varientdataseason)
					{

						VariantProductModel productModel;
						productModel = (VariantProductModel) seqirusProductService.getProductDataForCode(variantOptionData.getCode());

						if (base.equalsIgnoreCase(productModel.getBaseName()))
						{

							for (final ProductDetail productData : summary.getProducts())
							{

								if (productModel.getCode().equalsIgnoreCase(productData.getMaterialID()))
								{

									totalQty = totalQty + productData.getQuantity();
								}
							}
						}
					}

					if (base.equalsIgnoreCase(BASE_FLUADTIV10X))
					{
						orderQuantity.setFluadTivOrderQuantity10x(totalQty);
					}
					if (base.equalsIgnoreCase(BASE_FLUADTIV1X))
					{
						orderQuantity.setFluadTivOrderQuantity1x(totalQty);
					}
					if (base.equalsIgnoreCase(BASE_FLUADQIV10X))
					{
						orderQuantity.setFluadQivOrderQuantity10x(totalQty);
					}
					if (base.equalsIgnoreCase(BASE_FLUADQIV1X))
					{
						orderQuantity.setFluadQivOrderQuantity1x(totalQty);
					}

					if (base.equalsIgnoreCase(BASE_FLUCELVAXQIV10X))
					{
						orderQuantity.setFlucelvaxQivOrderQuantity10x(totalQty);
					}
					if (base.equalsIgnoreCase(BASE_FLUCELVAXQIV1X))
					{
						orderQuantity.setFlucelvaxQivOrderQuantity1x(totalQty);
					}

					if (base.equalsIgnoreCase(BASE_ADJUVANTEDTIV10X))
					{
						orderQuantity.setAdjuvantedTivOrderQuantity10x(totalQty);
					}
					if (base.equalsIgnoreCase(BASE_ADJUVANTEDTIV1X))
					{
						orderQuantity.setAdjuvantedTivOrderQuantity1x(totalQty);

					}
					if (base.equalsIgnoreCase(BASE_FLUCELVAX1X))
					{
						orderQuantity.setFlucelvaxOrderQuantity1x(totalQty);
					}
					if (base.equalsIgnoreCase(BASE_FLUCELVAX10X))
					{
						orderQuantity.setFlucelvaxOrderQuantity10x(totalQty);
					}

				}
				// set quantity end


				  int totalShip = 0;
				  int totalShipFluadTiv10x = 0;
				  int totalShipFluadTiv1x = 0;
				  int totalShipFluadQiv10x = 0;
				  int totalShipFluadQiv1x = 0;
				  int totalShipFlucelvaxQiv10x = 0;
				  int totalShipFlucelvaxQiv1x = 0;
				  int totalShipAdjuvantedTiv10x = 0;
				  int totalShipAdjuvantedTiv1x = 0;
				  int totalShipFlucelvax1x = 0;
				  int totalShipFlucelvax10x = 0;

				for (final ShipmentDetail ship : summary.getShipments())
				{
					totalShip = totalShip + ship.getTotalShippedQty();
					final List<String> shipMaterials = ship.getShippedMaterials();

					final Set<String> shipMaterialsSet = new HashSet<String>();
					for (final String material : shipMaterials)
					{
						shipMaterialsSet.add(material);
					}
					for (final String material : shipMaterialsSet)

					{

						VariantProductModel productModel;
						productModel = (VariantProductModel) seqirusProductService.getProductDataForCode(material);

						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADTIV10X))
						{

							orderQuantity.setFluadTivShipQuantity10x(totalShipFluadTiv10x + ship.getMaterialShippedQty(material));
							totalShipFluadTiv10x = orderQuantity.getFluadTivShipQuantity10x();

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADTIV1X))
						{

							orderQuantity.setFluadTivShipQuantity1x(totalShipFluadTiv1x + ship.getMaterialShippedQty(material));
							totalShipFluadTiv1x = orderQuantity.getFluadTivShipQuantity1x();

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADQIV10X))
						{

							orderQuantity.setFluadQivShipQuantity10x(totalShipFluadQiv10x + ship.getMaterialShippedQty(material));
							totalShipFluadQiv10x = orderQuantity.getFluadQivShipQuantity10x();

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUADQIV1X))
						{

							orderQuantity.setFluadQivShipQuantity1x(totalShipFluadQiv1x + ship.getMaterialShippedQty(material));
							totalShipFluadQiv1x = orderQuantity.getFluadQivShipQuantity1x();

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAXQIV10X))
						{

							orderQuantity
									.setFlucelvaxQivShipQuantity10x(totalShipFlucelvaxQiv10x + ship.getMaterialShippedQty(material));
							totalShipFlucelvaxQiv10x = orderQuantity.getFlucelvaxQivShipQuantity10x();

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAXQIV1X))
						{

							orderQuantity.setFlucelvaxQivShipQuantity1x(totalShipFlucelvaxQiv1x + ship.getMaterialShippedQty(material));
							totalShipFlucelvaxQiv1x = orderQuantity.getFlucelvaxQivShipQuantity1x();

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_ADJUVANTEDTIV10X))
						{

							orderQuantity
									.setAdjuvantedTivShipQuantity10x(totalShipAdjuvantedTiv10x + ship.getMaterialShippedQty(material));
							totalShipAdjuvantedTiv10x = orderQuantity.getAdjuvantedTivShipQuantity10x();

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_ADJUVANTEDTIV1X))
						{

							orderQuantity
									.setAdjuvantedTivShipQuantity1x(totalShipAdjuvantedTiv1x + ship.getMaterialShippedQty(material));
							totalShipAdjuvantedTiv1x = orderQuantity.getAdjuvantedTivShipQuantity1x();

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAX1X))
						{

							orderQuantity.setFlucelvaxShipQuantity1x(totalShipFlucelvax1x + ship.getMaterialShippedQty(material));
							totalShipFlucelvax1x = orderQuantity.getFlucelvaxShipQuantity1x();

						}
						if (productModel.getBaseName().equalsIgnoreCase(BASE_FLUCELVAX10X))
						{

							orderQuantity.setFlucelvaxShipQuantity10x(totalShipFlucelvax10x + ship.getMaterialShippedQty(material));
							totalShipFlucelvax10x = orderQuantity.getFlucelvaxShipQuantity10x();

						}


					}

				}
				orderQuantity.setTotalShipQuantity(totalShip);

			}
		}

		// shipment table data

		final List<ShipmentTableData> tableDataList = new ArrayList<ShipmentTableData>();
		//final String redirectUrl = getValueFromPropertyFile(PREFIX_NARVAR_KEY);

		if (null == orderID || orderID.isEmpty())
		{
			// code block for all order shipment
			for (final OrderSummary summary : orders)
			{
				final ShipmentTableData tableData = new ShipmentTableData();
				tableData.setOrders(summary.getOrderID());
				tableData.setLocation(summary.getPartnerName());
				tableDataList.add(tableData);
				/*
				 * for (final ShipmentDetail ship : summary.getShipments()) { String dateInString = ""; final String pattern
				 * = "MM/dd/yyyy"; final ShipmentTableData tableData = new ShipmentTableData();
				 * tableData.setOrders(summary.getOrderID()); tableData.setLocation(summary.getPartnerName()); if (null ==
				 * ship.getStatus() || ship.getStatus().isEmpty()) { tableData.setStatus("YET TO PROCESS");
				 *
				 * } else { tableData.setStatus(ship.getStatus().toUpperCase()); } if (null != ship.getPGIDate()) {
				 * dateInString = new SimpleDateFormat(pattern).format(ship.getPGIDate()); } else { dateInString = "TBD"; }
				 *
				 * tableData.setDelivery(dateInString);
				 *
				 * }
				 */

			}
		}
		else
		{
			// code for single order shipment
			for (final OrderSummary summary : orders)
			{
				if (orderID.equalsIgnoreCase(summary.getOrderID()))
				{
					final ShipmentTableData tableData = new ShipmentTableData();
					tableData.setOrders(summary.getOrderID());
					tableData.setLocation(summary.getPartnerName());
					tableDataList.add(tableData);
					/*
					 * int count = 1; for (final ShipmentDetail ship : summary.getShipments()) { String dateInString = "";
					 * final String pattern = "MM/dd/yyyy"; final ShipmentTableData tableData = new ShipmentTableData();
					 * tableData.setOrders(String.valueOf(count)); tableData.setLocation(summary.getPartnerName()); if (null
					 * == ship.getStatus() || ship.getStatus().isEmpty()) { tableData.setStatus("YET TO PROCESS");
					 *
					 * } else { tableData.setStatus(ship.getStatus().toUpperCase()); } if (null != ship.getPGIDate()) {
					 * dateInString = new SimpleDateFormat(pattern).format(ship.getPGIDate()); } else { dateInString = "TBD";
					 * } // tableData.setDelivery(dateInString);
					 *
					 * tableData.setViewdetails( redirectUrl + (ship.getTrackingURLs().isEmpty() ? "" :
					 * ship.getTrackingURLs().get(0).getId()) + ORDER_NUMBER + summary.getOrderID());
					 *
					 * tableDataList.add(tableData); count++; }
					 */
				}
			}

		}
		// shipment table data end
		chartandTableData.setChartData(orderQuantity);
		chartandTableData.setTableData(tableDataList);
		return chartandTableData;

	}

	private void removeJunkOrder(final List<OrderSummary> customerOrderlist)
	{

		final Set<String> materialSet = new HashSet<String>();
		//Get All Unique codes available in all order
		for (final OrderSummary co : customerOrderlist)
		{
			for (final ProductDetail d : co.getProducts())
			{
				materialSet.add(d.getMaterialID());
			}
		}
		final List<String> availableProductCode = new ArrayList<String>();
		//Prepare Available Code list
		for (final String code : materialSet)
		{
			try
			{
				final VariantProductModel product = (VariantProductModel) productService.getProductForCode(code);
				availableProductCode.add(code);
			}
			catch (final Exception e)
			{
				LOGGER.error("exception while fetching product details", e);
			}
		}
		//Remove Unwanted orders
		final Iterator<OrderSummary> custItr = customerOrderlist.iterator();
		while (custItr.hasNext())
		{
			final OrderSummary co = custItr.next();
			final Iterator<ProductDetail> orderItr = co.getProducts().iterator();
			while (orderItr.hasNext())
			{
				final ProductDetail d = orderItr.next();
				if (!availableProductCode.contains(d.getMaterialID()))
				{
					orderItr.remove();
				}
			}
			if (co.getProducts().isEmpty())
			{
				custItr.remove();
			}
		}

	}


}
