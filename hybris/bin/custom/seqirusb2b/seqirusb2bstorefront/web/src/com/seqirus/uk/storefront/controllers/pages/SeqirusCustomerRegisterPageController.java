package com.seqirus.uk.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractRegisterPageController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Register Controller for Seqirus. Handles register for create account flow.
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/registercustomer")
public class SeqirusCustomerRegisterPageController extends AbstractRegisterPageController
{

	private static final Logger LOGGER = Logger.getLogger(SeqirusCustomerRegisterPageController.class);

	private HttpSessionRequestCache httpSessionRequestCache;

	private static final String REGISTRATION_CMS_PAGE = "registrationpage";
	private static final String HOME_CMS_PAGE = "homepage";

	private static final String REG_SUCCESS_PAGE = "registration-success";

	private static final String LOCALE_US = "en_GB";

	private static final String ADDR_LOOKUP_API_URL = "address.lookup.api.url";
	private static final String ADDR_LOOKUP_API_KEY = "address.lookup.api.key";

	private static final String EDIT = "edit";

	//updatePageTitle
	private static final String REG_CUSTOMER = "Registration Form | Seqirus US";
	private static final String CREATE_ACCOUNT = "Create Account | Seqirus US";
	private static final String EDIT_ACCOUNT = "Edit Registered Details | Seqirus US";
	private static final String METADATA_CONTENT = "By creating an account with Seqirus, you can see specific product pricing, make a purchase, and manage your shipments easily.";



	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Autowired
	UserService userService;

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("register");
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		if (httpSessionRequestCache.getRequest(request, response) != null)
		{
			return httpSessionRequestCache.getRequest(request, response).getRedirectUrl();
		}
		return "/";
	}


	@Resource(name = "httpSessionRequestCache")
	public void setHttpSessionRequestCache(final HttpSessionRequestCache accHttpSessionRequestCache)
	{
		this.httpSessionRequestCache = accHttpSessionRequestCache;
	}


	@RequestMapping(method = RequestMethod.GET)
	public String register(final RegisterForm form, final Model model, final BindingResult bindingResult,
			final HttpServletRequest request) throws CMSItemNotFoundException
	{
		if (LOGGER.isDebugEnabled())
		{
			LOGGER.debug("SeqirusRegisterPageController:register() ");
		}

		if (!userService.isAnonymousUser(userService.getCurrentUser()))
		{
			return REDIRECT_PREFIX + "/";
		}
		model.addAttribute("regCustomer", REG_CUSTOMER);
		return goToRegisterPage(model);
	}

	/**
	 * @param model
	 * @param registerForm
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	public String goToRegisterPage(final Model model) throws CMSItemNotFoundException
	{
		final RegisterForm registerForm = new RegisterForm();
		model.addAttribute("registerForm", registerForm);
		//model.addAttribute("addrApiUrl", configurationService.getConfiguration().getProperty(ADDR_LOOKUP_API_URL));
		//model.addAttribute("access_key", configurationService.getConfiguration().getProperty(ADDR_LOOKUP_API_KEY));
		model.addAttribute("country", getCountry());
		storeCmsPageInModel(model, getAccountRegisterPage());
		setUpMetaDataForContentPage(model, (ContentPageModel) getAccountRegisterPage());
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, METADATA_CONTENT);
		return getViewForPage(model);
		//return ControllerConstants.Views.Pages.Registration.RegistrationPage;
	}

	private String getCountry()
	{
		String countryCode = "";

		final BaseSiteModel currentBaseSite = baseSiteService.getCurrentBaseSite();
		if (null != currentBaseSite)
		{
			if (currentBaseSite.getLocale().equalsIgnoreCase(LOCALE_US))
			{
				countryCode = "usa";
			}
		}
		return countryCode;
	}

	protected AbstractPageModel getAccountRegisterPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId(REGISTRATION_CMS_PAGE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractRegisterPageController#getView()
	 */
	@Override
	protected String getView()
	{
		// XXX Auto-generated method stub
		return null;
	}
}
