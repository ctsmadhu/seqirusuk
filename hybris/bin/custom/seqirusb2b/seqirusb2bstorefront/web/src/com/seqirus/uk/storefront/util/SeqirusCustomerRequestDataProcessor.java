/**
 *
 */
package com.seqirus.uk.storefront.util;

import de.hybris.platform.commercefacades.user.data.CountryData;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.seqirus.uk.facades.cutomer.data.SeqirusCustomerRegistrationAddressData;
import com.seqirus.uk.facades.cutomer.data.SeqirusCustomerRegistrationData;
import com.seqirus.uk.storefront.forms.ContactsAndAddressesForm;
import com.seqirus.uk.storefront.forms.CustomerRegistrationForm;


/**
 * @author 700196
 *
 */
public class SeqirusCustomerRequestDataProcessor
{

	/**
	 * @param custRegistrationForm
	 * @return SeqirusCustomerRegistrationData
	 */
	public SeqirusCustomerRegistrationData populateFormData(final CustomerRegistrationForm custRegistrationForm)
	{
		final SeqirusCustomerRegistrationData customerRegistrationData = new SeqirusCustomerRegistrationData();
		if (StringUtils.isNotBlank(custRegistrationForm.getUserEmail()))
		{
			customerRegistrationData.setUserEmail(custRegistrationForm.getUserEmail());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getUserName()))
		{
			customerRegistrationData.setUserName(custRegistrationForm.getUserName());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getCompanyName()))
		{
			customerRegistrationData.setCompanyName(custRegistrationForm.getCompanyName());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getCompanyType()))
		{
			customerRegistrationData.setCompanyType(custRegistrationForm.getCompanyType());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getCompanyRegNumber()))
		{
			customerRegistrationData.setCompanyRegNumber(custRegistrationForm.getCompanyRegNumber());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getBusinessType())
				&& !custRegistrationForm.getBusinessType().equals("OTHER"))
		{
			customerRegistrationData.setBusinessType(custRegistrationForm.getBusinessType());
		}
		else
		{
			customerRegistrationData.setBusinessType(custRegistrationForm.getOtherBusinessType());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getVatNumber()))
		{
			customerRegistrationData.setVatNumber(custRegistrationForm.getVatNumber());
		}

		if (null != custRegistrationForm.getTradingSince())
		{
			customerRegistrationData.setTradingSince(custRegistrationForm.getTradingSince());
		}

		if (StringUtils.isNotBlank(custRegistrationForm.getFirstName()))
		{
			customerRegistrationData.setFirstName(custRegistrationForm.getFirstName());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getLastName()))
		{
			customerRegistrationData.setLastName(custRegistrationForm.getLastName());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getEmail()))
		{
			customerRegistrationData.setEmail(custRegistrationForm.getEmail());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getPhoneNumber()))
		{
			customerRegistrationData.setPhoneNumber(custRegistrationForm.getPhoneNumber());
		}
		if (null != custRegistrationForm.getPhoneExt())
		{
			customerRegistrationData.setPhoneExt(custRegistrationForm.getPhoneExt().toString());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getJobTitle()))
		{
			customerRegistrationData.setJobTitle(custRegistrationForm.getJobTitle());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getNhcNumber()))
		{
			customerRegistrationData.setNhcNumber(custRegistrationForm.getNhcNumber());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getBuildingStreet()))
		{
			customerRegistrationData.setBuildingStreet(custRegistrationForm.getBuildingStreet());
		}

		if (StringUtils.isNotBlank(custRegistrationForm.getAdditionalStreet()))
		{
			customerRegistrationData.setAdditionalStreet(custRegistrationForm.getAdditionalStreet());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getCity()))
		{
			customerRegistrationData.setCity(custRegistrationForm.getCity());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getUpdateCompanyInfoFlag()))
		{
			customerRegistrationData.setUpdateCustInfoFlag(custRegistrationForm.getUpdateCompanyInfoFlag());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getCountry()))
		{
			final CountryData country = new CountryData();
			country.setName(custRegistrationForm.getCountry());
			customerRegistrationData.setCountry(country);

		}

		if (StringUtils.isNotBlank(custRegistrationForm.getPostalCode()))
		{
			customerRegistrationData.setPostCode(custRegistrationForm.getPostalCode());
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getPayerContactSubmitFlag())
				&& custRegistrationForm.getPayerContactSubmitFlag().equals("true"))
		{
			setPayingInfo(custRegistrationForm, customerRegistrationData);
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getBillingContactSubmitFlag())
				&& custRegistrationForm.getBillingContactSubmitFlag().equals("true"))
		{
			setBillingInfo(custRegistrationForm, customerRegistrationData);
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getInvoiceContactSubmitFlag())
				&& custRegistrationForm.getInvoiceContactSubmitFlag().equals("true"))
		{
			setInvoicingContractData(custRegistrationForm, customerRegistrationData);
		}
		if (StringUtils.isNotBlank(custRegistrationForm.getShipLocationSubmitFlag())
				&& custRegistrationForm.getShipLocationSubmitFlag().equals("true"))
		{
			setShippingLocations(custRegistrationForm, customerRegistrationData);
		}

		return customerRegistrationData;
	}


	/**
	 * @param custRegistrationForm
	 */
	private void setPayingInfo(final CustomerRegistrationForm custRegistrationForm,
			final SeqirusCustomerRegistrationData customerRegistrationData)
	{
		if (null != custRegistrationForm.getPayingContactInfo())
		{
			final ContactsAndAddressesForm payerAddress = custRegistrationForm.getPayingContactInfo();

			final SeqirusCustomerRegistrationAddressData payerAddressData = new SeqirusCustomerRegistrationAddressData();


			if (StringUtils.isNotBlank(payerAddress.getEmail()))
			{
				payerAddressData.setEmail(payerAddress.getEmail());
			}
			if (StringUtils.isNotBlank(payerAddress.getAddressID()))
			{
				payerAddressData.setAddressID(payerAddress.getAddressID());
			}

			if (StringUtils.isNotBlank(payerAddress.getAddressLine1()))
			{
				payerAddressData.setLine1(payerAddress.getAddressLine1());
			}

			if (StringUtils.isNotBlank(payerAddress.getPhone()))
			{
				payerAddressData.setPhone(payerAddress.getPhone());
			}

			if (StringUtils.isNotBlank(payerAddress.getAddressLine2()))
			{
				payerAddressData.setLine2(payerAddress.getAddressLine2());
			}

			if (null != payerAddress.getPhoneExt())
			{
				payerAddressData.setPhoneExt(payerAddress.getPhoneExt());
			}

			if (StringUtils.isNotBlank(payerAddress.getCity()))
			{
				payerAddressData.setCity(payerAddress.getCity());
			}
			if (StringUtils.isNotBlank(payerAddress.getCountry()))
			{
				final CountryData country = new CountryData();
				country.setName(payerAddress.getCountry());
				payerAddressData.setCountry(country);

			}
			if (StringUtils.isNotBlank(payerAddress.getJobTitle()))
			{
				payerAddressData.setJobTitle(payerAddress.getJobTitle());
			}

			if (StringUtils.isNotBlank(payerAddress.getPostalCode()))
			{
				payerAddressData.setPostalCode(payerAddress.getPostalCode());
			}

			if (StringUtils.isNotBlank(payerAddress.getFirstName()))
			{
				payerAddressData.setFirstName(payerAddress.getFirstName());
			}
			if (StringUtils.isNotBlank(payerAddress.getLastName()))
			{
				payerAddressData.setLastName(payerAddress.getLastName());
			}
			if (StringUtils.isNotBlank(payerAddress.getJobTitle()))
			{
				payerAddressData.setJobTitle(payerAddress.getJobTitle());
			}
			if (StringUtils.isNotBlank(payerAddress.getOrganizationName()))
			{
				payerAddressData.setCompanyName(payerAddress.getOrganizationName());
			}
			customerRegistrationData.setPayingContactData(payerAddressData);
		}
	}

	/**
	 * @param custRegistrationForm
	 * @param customerRegistrationData
	 */
	private void setBillingInfo(final CustomerRegistrationForm custRegistrationForm,
			final SeqirusCustomerRegistrationData customerRegistrationData)
	{
		if (null != custRegistrationForm.getBillingContactInfo())
		{
			final ContactsAndAddressesForm billingAddress = custRegistrationForm.getBillingContactInfo();


			final SeqirusCustomerRegistrationAddressData billingAddressData = new SeqirusCustomerRegistrationAddressData();

			if (StringUtils.isNotBlank(billingAddress.getEmail()))
			{
				billingAddressData.setEmail(billingAddress.getEmail());
			}
			if (StringUtils.isNotBlank(billingAddress.getAddressID()))
			{
				billingAddressData.setAddressID(billingAddress.getAddressID());
			}

			if (StringUtils.isNotBlank(billingAddress.getAddressLine1()))
			{
				billingAddressData.setLine1(billingAddress.getAddressLine1());
			}

			if (StringUtils.isNotBlank(billingAddress.getPhone()))
			{
				billingAddressData.setPhone(billingAddress.getPhone());
			}

			if (StringUtils.isNotBlank(billingAddress.getAddressLine2()))
			{
				billingAddressData.setLine2(billingAddress.getAddressLine2());
			}

			if (null != billingAddress.getPhoneExt())
			{
				billingAddressData.setPhoneExt(billingAddress.getPhoneExt());
			}

			if (StringUtils.isNotBlank(billingAddress.getCity()))
			{
				billingAddressData.setCity(billingAddress.getCity());
			}

			if (StringUtils.isNotBlank(billingAddress.getCountry()))
			{
				final CountryData country = new CountryData();
				country.setName(billingAddress.getCountry());
				billingAddressData.setCountry(country);

			}
			if (StringUtils.isNotBlank(billingAddress.getJobTitle()))
			{
				billingAddressData.setJobTitle(billingAddress.getJobTitle());
			}

			if (StringUtils.isNotBlank(billingAddress.getPostalCode()))
			{
				billingAddressData.setPostalCode(billingAddress.getPostalCode());
			}
			if (StringUtils.isNotBlank(billingAddress.getFirstName()))
			{
				billingAddressData.setFirstName(billingAddress.getFirstName());
			}
			if (StringUtils.isNotBlank(billingAddress.getLastName()))
			{
				billingAddressData.setLastName(billingAddress.getLastName());
			}
			if (StringUtils.isNotBlank(billingAddress.getJobTitle()))
			{
				billingAddressData.setJobTitle(billingAddress.getJobTitle());
			}
			if (StringUtils.isNotBlank(billingAddress.getOrganizationName()))
			{
				billingAddressData.setCompanyName(billingAddress.getOrganizationName());
			}

			customerRegistrationData.setBillingContactData(billingAddressData);
		}
	}

	/**
	 * @param custRegistrationForm
	 * @param customerRegistrationData
	 */
	private void setInvoicingContractData(final CustomerRegistrationForm custRegistrationForm,
			final SeqirusCustomerRegistrationData customerRegistrationData)
	{

		if (null != custRegistrationForm.getInvoicingContractInfo())
		{
			final ContactsAndAddressesForm invoiceContract = custRegistrationForm.getInvoicingContractInfo();

			final SeqirusCustomerRegistrationAddressData invoiceContractsData = new SeqirusCustomerRegistrationAddressData();

			if (StringUtils.isNotBlank(invoiceContract.getEmail()))
			{
				invoiceContractsData.setEmail(invoiceContract.getEmail());
			}
			if (StringUtils.isNotBlank(invoiceContract.getAddressID()))
			{
				invoiceContractsData.setAddressID(invoiceContract.getAddressID());
			}

			if (StringUtils.isNotBlank(invoiceContract.getAddressLine1()))
			{
				invoiceContractsData.setLine1(invoiceContract.getAddressLine1());
			}

			if (StringUtils.isNotBlank(invoiceContract.getPhone()))
			{
				invoiceContractsData.setPhone(invoiceContract.getPhone());
			}

			if (StringUtils.isNotBlank(invoiceContract.getAddressLine2()))
			{
				invoiceContractsData.setLine2(invoiceContract.getAddressLine2());
			}

			if (null != invoiceContract.getPhoneExt())
			{
				invoiceContractsData.setPhoneExt(invoiceContract.getPhoneExt());
			}

			if (StringUtils.isNotBlank(invoiceContract.getCity()))
			{
				invoiceContractsData.setCity(invoiceContract.getCity());
			}

			if (StringUtils.isNotBlank(invoiceContract.getCountry()))
			{
				final CountryData country = new CountryData();
				country.setName(invoiceContract.getCountry());
				invoiceContractsData.setCountry(country);

			}

			if (StringUtils.isNotBlank(invoiceContract.getJobTitle()))
			{
				invoiceContractsData.setJobTitle(invoiceContract.getJobTitle());
			}

			if (StringUtils.isNotBlank(invoiceContract.getPostalCode()))
			{
				invoiceContractsData.setPostalCode(invoiceContract.getPostalCode());
			}

			if (StringUtils.isNotBlank(invoiceContract.getAdditionalEmail()))
			{
				invoiceContractsData.setAdditionalEmail(invoiceContract.getAdditionalEmail());
			}
			if (StringUtils.isNotBlank(invoiceContract.getFirstName()))
			{
				invoiceContractsData.setFirstName(invoiceContract.getFirstName());
			}
			if (StringUtils.isNotBlank(invoiceContract.getLastName()))
			{
				invoiceContractsData.setLastName(invoiceContract.getLastName());
			}
			if (StringUtils.isNotBlank(invoiceContract.getJobTitle()))
			{
				invoiceContractsData.setJobTitle(invoiceContract.getJobTitle());
			}
			if (StringUtils.isNotBlank(invoiceContract.getOrganizationName()))
			{
				invoiceContractsData.setCompanyName(invoiceContract.getOrganizationName());
			}

			customerRegistrationData.setInvoiceContractData(invoiceContractsData);

		}
	}


	/**
	 * @param custRegistrationForm
	 * @param customerRegistrationData
	 */
	private void setShippingLocations(final CustomerRegistrationForm custRegistrationForm,
			final SeqirusCustomerRegistrationData customerRegistrationData)
	{
		if (null != custRegistrationForm.getShippingLocations())
		{
		final List<ContactsAndAddressesForm> shippingAddressList = custRegistrationForm.getShippingLocations();

		final List<SeqirusCustomerRegistrationAddressData> shippingDataList = new ArrayList<>();

		for (final ContactsAndAddressesForm shippingAddress : shippingAddressList)
		{
		  if(StringUtils.isNotBlank(shippingAddress.getFirstName())){

			final SeqirusCustomerRegistrationAddressData shippingData = new SeqirusCustomerRegistrationAddressData();
			if (StringUtils.isNotBlank(shippingAddress.getAddressLine1()))
		{
				shippingData.setLine1(shippingAddress.getAddressLine1());
		}

			if (StringUtils.isNotBlank(shippingAddress.getAddressLine2()))
		{
				shippingData.setLine2(shippingAddress.getAddressLine2());
		}

			if (StringUtils.isNotBlank(shippingAddress.getOrganizationName()))
			{
				shippingData.setCompanyName(shippingAddress.getOrganizationName());
			}

			if (StringUtils.isNotBlank(shippingAddress.getCity()))
		{
				shippingData.setCity(shippingAddress.getCity());
		}

			if (StringUtils.isNotBlank(shippingAddress.getAddressID()))
			{
				shippingData.setAddressID(shippingAddress.getAddressID());
			}
			if (StringUtils.isNotBlank(shippingAddress.getPostalCode()))
		{
				shippingData.setPostalCode(shippingAddress.getPostalCode());
		}

		if (StringUtils.isNotBlank(shippingAddress.getCountry()))
		{
			final CountryData country = new CountryData();
			country.setName(shippingAddress.getCountry());
			shippingData.setCountry(country);

		}
		if (StringUtils.isNotBlank(shippingAddress.getFirstName()))
		{
			shippingData.setFirstName(shippingAddress.getFirstName());
		}
		if (StringUtils.isNotBlank(shippingAddress.getLastName()))
		{
			shippingData.setLastName(shippingAddress.getLastName());
		}
		if (StringUtils.isNotBlank(shippingAddress.getEmail()))
		{
			shippingData.setEmail(shippingAddress.getEmail());
		}
		if (StringUtils.isNotBlank(shippingAddress.getPhone()))
		{
			shippingData.setPhone(shippingAddress.getPhone());
		}
		if (StringUtils.isNotBlank(shippingAddress.getMobilePhone()))
		{
			shippingData.setCellphone(shippingAddress.getMobilePhone());
		}
		if (null != shippingAddress.getPhoneExt())
		{
			shippingData.setPhoneExt(shippingAddress.getPhoneExt());
		}
		if (StringUtils.isNotBlank(shippingAddress.getLicenseName()))
		{
			shippingData.setLicenseName(shippingAddress.getLicenseName());
		}
		if (StringUtils.isNotBlank(shippingAddress.getLicenseNumber()))
		{
			shippingData.setLicenseNumber(shippingAddress.getLicenseNumber());
		}
		if (StringUtils.isNotBlank(shippingAddress.getNhsCode()))
		{
			shippingData.setNhsNumber(shippingAddress.getNhsCode());
		}
		shippingDataList.add(shippingData);
		}
	}
		customerRegistrationData.setShippingLocationsData(shippingDataList);
		}
	}
}
