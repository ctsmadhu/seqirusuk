/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.uk.test.constants;

/**
 * 
 */
public class Seqirusb2bTestConstants extends GeneratedSeqirusb2bTestConstants
{

	public static final String EXTENSIONNAME = "seqirusb2btest";

}
