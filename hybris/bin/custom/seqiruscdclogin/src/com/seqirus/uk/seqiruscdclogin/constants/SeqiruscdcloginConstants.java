/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.seqirus.uk.seqiruscdclogin.constants;

/**
 * Global class for all Seqiruscdclogin constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class SeqiruscdcloginConstants extends GeneratedSeqiruscdcloginConstants
{
	public static final String EXTENSIONNAME = "seqiruscdclogin";

	private SeqiruscdcloginConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
